INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'DICOM_file_types', 'dcm, DCM, bin', 'java.lang.String','Files extensions that can be used for DICOM Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'DSUB_file_types', 'xml, XML, dsub, DSUB, txt, TXT, log', 'java.lang.String', 'Files extensions that can be used for DSUB Document Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'HL7v3_file_types', 'xml, XML, hl7, HL7, hl7v3, HL7V3, HL7v3, txt, TXT, log','java.lang.String', 'Files extensions that can be used for DSUB Document Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'ATNA_file_types', 'xml, XML, syslog, log, txt, TXT, log', 'java.lang.String', 'Files extensions that can be used for ATNA Audit message Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'SAML_file_types', 'xml, XML, saml, SAML, log', 'java.lang.String', 'Files extensions that can be used for SAML Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'SVS_file_types', 'xml, XML, svs, SVS, txt, TXT, log', 'java.lang.String','Files extensions that can be used for HPD Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'X509_file_types', 'pem, txt, PEM, TXT, CRT, crt', 'java.lang.String','Files extensions that can be used for X509 certificate Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'XDS_file_types', 'xml, XML', 'java.lang.String', 'allowed extensions for XDS validator')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'XDW_file_types', 'xml, XML, txt, TXT, log, xdw, XDW', 'java.lang.String', '')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'XML_file_types', 'xml, XML, txt, TXT, log', 'java.lang.String','Files extensions that can be used for XML Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'TLS_file_types', 'pem, txt, PEM, TXT, CRT, crt', 'java.lang.String', 'Files extensions that can be used for X509 certificate Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'PDF_file_types', 'pdf', 'java.lang.String','Files extensions that can be used for PDF Validation')
    ON CONFLICT (preference_name) DO NOTHING ;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'),
            'mca_show_zip_mimtype', 'false', 'java.lang.Boolean','Show/Hide the Mimtype analyze for zip files in Message Content Analyzer')
    ON CONFLICT (preference_name) DO NOTHING ;

