INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
VALUES (nextval('cmn_application_preference_id_seq'),
        'validators_discrimination_rules',
        '{}',
        'java.lang.String',
        'Regular expression discriminators to select validation profile.') ON CONFLICT (preference_name) DO NOTHING;
