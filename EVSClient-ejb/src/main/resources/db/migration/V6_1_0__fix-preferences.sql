DELETE FROM cmn_application_preference WHERE preference_name = 'DICOM_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'DSUB_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'HL7v3_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'ATNA_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'SAML_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'SVS_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'X509_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'XDS_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'XDW_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'XML_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'TLS_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'PDF_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'X509_file_types';
DELETE FROM cmn_application_preference WHERE preference_name = 'cas_url';

-- Stranger DB state fixe for ANS pprod environment
DELETE FROM cmn_application_preference WHERE preference_name = 'cda_validator_app_url';
DELETE FROM cmn_application_preference WHERE preference_name = 'cda_scorecard_xsl_path';

UPDATE xval_date_format_type SET precision = 10 WHERE label='WITH_TIMEZONE' AND precision <> 10;
UPDATE xval_date_format_type SET precision = 9  WHERE label='TO_THE_SECOND' AND precision <> 9;
UPDATE xval_date_format_type SET precision = 8  WHERE label='TO_THE_MINUTE' AND precision <> 8;
UPDATE xval_date_format_type SET precision = 7  WHERE label='TO_THE_DAY' AND precision <> 7;
