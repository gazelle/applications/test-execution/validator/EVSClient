INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
VALUES (nextval('cmn_application_preference_id_seq'),
        'DEFAULT_file_types',
        'xml, XML, hl7, HL7, hl7v3, HL7V3, HL7v3, txt, TXT, log, json, dsub, DSUB, dcm, DCM, bin, syslog, saml, SAML, svs, SVS, pem, txt, PEM, CRT, crt, xdw, XDW, pdf, PDF, csv, CSV',
        'java.lang.String',
        'Files extensions that can be used for default validation') ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
VALUES (nextval('cmn_application_preference_id_seq'),
        'default_repository',
        '/opt/evs/validatedObjects/DEFAULT',
        'java.lang.String',
        'Where to store DEFAULT files') ON CONFLICT (preference_name) DO NOTHING;