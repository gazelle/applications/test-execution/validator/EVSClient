--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.24
-- Dumped by pg_dump version 9.6.24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_application_preference (
    id integer NOT NULL,
    class_name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    preference_name character varying(64) NOT NULL,
    preference_value character varying(512) NOT NULL
);


ALTER TABLE public.cmn_application_preference OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_path_linking_a_document (
    id integer NOT NULL,
    comment character varying(255),
    path character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: evsc_api_key; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_api_key (
    value character varying(255) NOT NULL,
    creation_date timestamp without time zone,
    expiration_date timestamp without time zone,
    owner_organization character varying(255),
    owner_username character varying(255)
);


ALTER TABLE public.evsc_api_key OWNER TO gazelle;

--
-- Name: evsc_caller_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_caller_metadata (
    id integer NOT NULL,
    entrypoint character varying(255)
);


ALTER TABLE public.evsc_caller_metadata OWNER TO gazelle;

--
-- Name: evsc_caller_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_caller_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_caller_metadata_id_seq OWNER TO gazelle;

--
-- Name: evsc_calling_tool; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_calling_tool (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    tool_type integer,
    url character varying(255) NOT NULL
);


ALTER TABLE public.evsc_calling_tool OWNER TO gazelle;

--
-- Name: evsc_calling_tool_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_calling_tool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_calling_tool_id_seq OWNER TO gazelle;

--
-- Name: evsc_conf_composition; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_conf_composition (
    composed_id integer NOT NULL,
    component_id integer NOT NULL
);


ALTER TABLE public.evsc_conf_composition OWNER TO gazelle;

--
-- Name: evsc_digital_signature_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_digital_signature_service_conf (
    id integer NOT NULL
);


ALTER TABLE public.evsc_digital_signature_service_conf OWNER TO gazelle;

--
-- Name: evsc_embedded_validation_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_embedded_validation_service_conf (
    id integer NOT NULL
);


ALTER TABLE public.evsc_embedded_validation_service_conf OWNER TO gazelle;

--
-- Name: evsc_extension_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_extension_service_conf (
    id integer NOT NULL,
    description text,
    display_name character varying(255),
    validation_type character varying(255) NOT NULL,
    factory_canonical_classname character varying(255),
    available boolean
);


ALTER TABLE public.evsc_extension_service_conf OWNER TO gazelle;

--
-- Name: evsc_handled_object; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_handled_object (
    discriminator character varying(31) NOT NULL,
    id integer NOT NULL,
    original_file_name character varying(255),
    role character varying(255),
    file_path character varying(255)
);


ALTER TABLE public.evsc_handled_object OWNER TO gazelle;

--
-- Name: evsc_handled_object_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_handled_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_handled_object_id_seq OWNER TO gazelle;

--
-- Name: evsc_ip_loc; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_ip_loc (
    id integer NOT NULL,
    country character varying(255) NOT NULL,
    ip character varying(255) NOT NULL,
    last_check timestamp without time zone NOT NULL
);


ALTER TABLE public.evsc_ip_loc OWNER TO gazelle;

--
-- Name: evsc_ip_loc_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_ip_loc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_ip_loc_id_seq OWNER TO gazelle;

--
-- Name: evsc_menu_group; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_menu_group (
    id integer NOT NULL,
    is_available boolean,
    label character varying(255) NOT NULL,
    order_in_gui integer
);


ALTER TABLE public.evsc_menu_group OWNER TO gazelle;

--
-- Name: evsc_menu_group_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_menu_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_menu_group_id_seq OWNER TO gazelle;

--
-- Name: evsc_menu_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_menu_standard (
    menu_group_id integer NOT NULL,
    referenced_standard_id integer NOT NULL
);


ALTER TABLE public.evsc_menu_standard OWNER TO gazelle;

--
-- Name: evsc_named_stylesheet; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_named_stylesheet (
    name character varying(255),
    id integer NOT NULL,
    referenced_standard_id integer
);


ALTER TABLE public.evsc_named_stylesheet OWNER TO gazelle;

--
-- Name: evsc_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_oid (
    id integer NOT NULL
);


ALTER TABLE public.evsc_oid OWNER TO gazelle;

--
-- Name: evsc_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_oid_id_seq OWNER TO gazelle;

--
-- Name: evsc_other_caller_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_other_caller_metadata (
    tool_object_id character varying(255),
    tool_oid character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.evsc_other_caller_metadata OWNER TO gazelle;

--
-- Name: evsc_owner_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_owner_metadata (
    id integer NOT NULL,
    organization character varying(255),
    user_ip character varying(255),
    username character varying(255)
);


ALTER TABLE public.evsc_owner_metadata OWNER TO gazelle;

--
-- Name: evsc_owner_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_owner_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_owner_metadata_id_seq OWNER TO gazelle;

--
-- Name: evsc_processing; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_processing (
    id integer NOT NULL,
    validation_date timestamp without time zone,
    oid character varying(255),
    caller_id integer NOT NULL,
    owner_id integer,
    sharing_id integer NOT NULL
);


ALTER TABLE public.evsc_processing OWNER TO gazelle;

--
-- Name: evsc_processing_conf_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_processing_conf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_processing_conf_id_seq OWNER TO gazelle;

--
-- Name: evsc_processing_evsc_handled_object; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_processing_evsc_handled_object (
    evsc_processing_id integer NOT NULL,
    objects_id integer NOT NULL
);


ALTER TABLE public.evsc_processing_evsc_handled_object OWNER TO gazelle;

--
-- Name: evsc_processing_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_processing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_processing_id_seq OWNER TO gazelle;

--
-- Name: evsc_proxy_caller_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_proxy_caller_metadata (
    proxy_type character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.evsc_proxy_caller_metadata OWNER TO gazelle;

--
-- Name: evsc_referenced_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_referenced_standard (
    id integer NOT NULL,
    description text,
    display_name character varying(255),
    validation_type character varying(255) NOT NULL,
    icon_style_class character varying(255),
    validator_filter character varying(255)
);


ALTER TABLE public.evsc_referenced_standard OWNER TO gazelle;

--
-- Name: evsc_referenced_standard_ref; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_referenced_standard_ref (
    id integer NOT NULL,
    referenced_standard_id integer
);


ALTER TABLE public.evsc_referenced_standard_ref OWNER TO gazelle;

--
-- Name: evsc_report; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_report (
    discriminator character varying(31) NOT NULL,
    id integer NOT NULL,
    archive_path character varying(255)
);


ALTER TABLE public.evsc_report OWNER TO gazelle;

--
-- Name: evsc_report_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_report_id_seq OWNER TO gazelle;

--
-- Name: evsc_scorecard_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_scorecard_service_conf (
    id integer NOT NULL
);


ALTER TABLE public.evsc_scorecard_service_conf OWNER TO gazelle;

--
-- Name: evsc_sharing_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_sharing_metadata (
    id integer NOT NULL,
    is_private boolean,
    privacy_key character varying(255)
);


ALTER TABLE public.evsc_sharing_metadata OWNER TO gazelle;

--
-- Name: evsc_sharing_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_sharing_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_sharing_metadata_id_seq OWNER TO gazelle;

--
-- Name: evsc_stylesheet; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_stylesheet (
    id integer NOT NULL,
    location character varying(255)
);


ALTER TABLE public.evsc_stylesheet OWNER TO gazelle;

--
-- Name: evsc_stylesheet_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_stylesheet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_stylesheet_id_seq OWNER TO gazelle;

--
-- Name: evsc_system_validation_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_system_validation_service_conf (
    binary_path character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.evsc_system_validation_service_conf OWNER TO gazelle;

--
-- Name: evsc_validation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation (
    lucky_validation boolean,
    validation_service character varying(255),
    validator_keyword character varying(255),
    validator_version character varying(255),
    validation_service_version character varying(255),
    status character varying(255),
    validation_type character varying(255),
    id integer NOT NULL,
    original_report_id integer,
    validation_report_id integer
);


ALTER TABLE public.evsc_validation OWNER TO gazelle;

--
-- Name: evsc_validation_digital_signature; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_digital_signature (
    id integer NOT NULL
);


ALTER TABLE public.evsc_validation_digital_signature OWNER TO gazelle;

--
-- Name: evsc_validation_evsc_referenced_standard_ref; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_evsc_referenced_standard_ref (
    evsc_validation_id integer NOT NULL,
    referencedstandards_id integer NOT NULL
);


ALTER TABLE public.evsc_validation_evsc_referenced_standard_ref OWNER TO gazelle;

--
-- Name: evsc_validation_extension; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_extension (
    status character varying(255),
    validation_oid character varying(255),
    id integer NOT NULL,
    extension_report_id integer
);


ALTER TABLE public.evsc_validation_extension OWNER TO gazelle;

--
-- Name: evsc_validation_report_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_report_type (
    id integer NOT NULL,
    report_format character varying(255),
    stylesheet_id integer
);


ALTER TABLE public.evsc_validation_report_type OWNER TO gazelle;

--
-- Name: evsc_validation_report_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.evsc_validation_report_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evsc_validation_report_type_id_seq OWNER TO gazelle;

--
-- Name: evsc_validation_scorecard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_scorecard (
    id integer NOT NULL
);


ALTER TABLE public.evsc_validation_scorecard OWNER TO gazelle;

--
-- Name: evsc_validation_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_validation_service_conf (
    id integer NOT NULL,
    description text,
    display_name character varying(255),
    validation_type character varying(255) NOT NULL,
    factory_canonical_classname character varying(255),
    available boolean,
    validationreporttype_id integer
);


ALTER TABLE public.evsc_validation_service_conf OWNER TO gazelle;

--
-- Name: evsc_web_extension_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_web_extension_service_conf (
    url character varying(255),
    zip_transport boolean,
    id integer NOT NULL
);


ALTER TABLE public.evsc_web_extension_service_conf OWNER TO gazelle;

--
-- Name: evsc_web_validation_service_conf; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.evsc_web_validation_service_conf (
    url character varying(255),
    zip_transport boolean,
    id integer NOT NULL
);


ALTER TABLE public.evsc_web_validation_service_conf OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: inputs_with_file_keywords_ut; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.inputs_with_file_keywords_ut (
    tested_rule_id integer NOT NULL,
    inputs_with_file_keywords character varying(255)
);


ALTER TABLE public.inputs_with_file_keywords_ut OWNER TO gazelle;

--
-- Name: mca_analysis; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_analysis (
    analysis_status character varying(255),
    content_analyzer_version character varying(255),
    id integer NOT NULL,
    root_analysis_part_id integer
);


ALTER TABLE public.mca_analysis OWNER TO gazelle;

--
-- Name: mca_analysis_part; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_analysis_part (
    id integer NOT NULL,
    decoded_part_file_path character varying(255),
    doc_type character varying(255),
    encoded_type character varying(255),
    end_offset integer,
    no_defined_namespace boolean,
    oid character varying(255),
    part_log bytea,
    start_offset integer,
    validation_oid character varying(255),
    validation_type character varying(255),
    parent_part_id integer,
    xvalmatch_id integer
);


ALTER TABLE public.mca_analysis_part OWNER TO gazelle;

--
-- Name: mca_analysis_part_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_analysis_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_analysis_part_id_seq OWNER TO gazelle;

--
-- Name: mca_analysis_part_namespaces; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_analysis_part_namespaces (
    analysis_part_id integer NOT NULL,
    namespace_uri character varying(255),
    namespace_prefix character varying(255) NOT NULL
);


ALTER TABLE public.mca_analysis_part_namespaces OWNER TO gazelle;

--
-- Name: mca_content_analysis_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_content_analysis_config (
    id integer NOT NULL,
    doc_type character varying(255),
    validation_type character varying(255),
    byte_pattern bytea,
    sample text,
    string_pattern character varying(255),
    unwanted_content character varying(255)
);


ALTER TABLE public.mca_content_analysis_config OWNER TO gazelle;

--
-- Name: mca_content_analysis_config_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_content_analysis_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_content_analysis_config_seq OWNER TO gazelle;

--
-- Name: mca_doc_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_doc_type (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.mca_doc_type OWNER TO gazelle;

--
-- Name: mca_doc_type_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_doc_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_doc_type_seq OWNER TO gazelle;

--
-- Name: mca_file_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_file_config (
    id integer NOT NULL,
    file_name character varying(255),
    regex boolean,
    x_val_input_type character varying(255)
);


ALTER TABLE public.mca_file_config OWNER TO gazelle;

--
-- Name: mca_file_config_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_file_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_file_config_id_seq OWNER TO gazelle;

--
-- Name: mca_folder_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_folder_config (
    id integer NOT NULL,
    folder_name character varying(255)
);


ALTER TABLE public.mca_folder_config OWNER TO gazelle;

--
-- Name: mca_folder_config_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_folder_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_folder_config_id_seq OWNER TO gazelle;

--
-- Name: mca_folder_file_children; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_folder_file_children (
    folder_id integer NOT NULL,
    file_child_id integer NOT NULL
);


ALTER TABLE public.mca_folder_file_children OWNER TO gazelle;

--
-- Name: mca_folder_folder_children; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_folder_folder_children (
    folder_id integer NOT NULL,
    folder_child_id integer NOT NULL
);


ALTER TABLE public.mca_folder_folder_children OWNER TO gazelle;

--
-- Name: mca_mime_type_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_mime_type_config (
    id integer NOT NULL,
    doc_type character varying(255),
    validation_type character varying(255),
    mime_type character varying(255),
    sample text
);


ALTER TABLE public.mca_mime_type_config OWNER TO gazelle;

--
-- Name: mca_mime_type_config_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_mime_type_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_mime_type_config_seq OWNER TO gazelle;

--
-- Name: mca_starts_with; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_starts_with (
    content_analysis_config_id integer NOT NULL,
    startswith character varying(255)
);


ALTER TABLE public.mca_starts_with OWNER TO gazelle;

--
-- Name: mca_validation_types; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_validation_types (
    analysis_id integer NOT NULL,
    validation_type character varying(255)
);


ALTER TABLE public.mca_validation_types OWNER TO gazelle;

--
-- Name: mca_x_validation_matching; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_x_validation_matching (
    id integer NOT NULL,
    x_val_inpt_type character varying(255),
    x_validator_oid character varying(255)
);


ALTER TABLE public.mca_x_validation_matching OWNER TO gazelle;

--
-- Name: mca_x_validation_matching_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_x_validation_matching_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_x_validation_matching_id_seq OWNER TO gazelle;

--
-- Name: mca_xml_tag_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_xml_tag_config (
    id integer NOT NULL,
    doc_type character varying(255),
    validation_type character varying(255),
    namespace character varying(255),
    sample text,
    tag_name character varying(255)
);


ALTER TABLE public.mca_xml_tag_config OWNER TO gazelle;

--
-- Name: mca_xml_tag_config_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_xml_tag_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_xml_tag_config_seq OWNER TO gazelle;

--
-- Name: mca_zip_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_zip_config (
    id integer NOT NULL,
    doc_type character varying(255),
    validation_type character varying(255),
    archive_name character varying(255),
    x_validator_oid character varying(255)
);


ALTER TABLE public.mca_zip_config OWNER TO gazelle;

--
-- Name: mca_zip_config_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mca_zip_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mca_zip_config_id_seq OWNER TO gazelle;

--
-- Name: mca_zip_file_children; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_zip_file_children (
    zip_structure_id integer NOT NULL,
    folder_id integer NOT NULL
);


ALTER TABLE public.mca_zip_file_children OWNER TO gazelle;

--
-- Name: mca_zip_folder_children; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mca_zip_folder_children (
    zip_structure_id integer NOT NULL,
    folder_id integer NOT NULL
);


ALTER TABLE public.mca_zip_folder_children OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.package_name_for_profile_oid (
    id integer NOT NULL,
    package_name character varying(255),
    profile_oid character varying(255)
);


ALTER TABLE public.package_name_for_profile_oid OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.package_name_for_profile_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.package_name_for_profile_oid_id_seq OWNER TO gazelle;

--
-- Name: referenced_standard_ref_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.referenced_standard_ref_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referenced_standard_ref_id_seq OWNER TO gazelle;

--
-- Name: revinfo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


ALTER TABLE public.revinfo OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    combined boolean,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    combined boolean,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_actor_aud OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    maybe_supportive boolean,
    actor_integration_profile_id integer NOT NULL,
    document_section integer,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    maybe_supportive boolean,
    actor_integration_profile_id integer,
    document_section integer,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain OWNER TO gazelle;

--
-- Name: tf_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_audit_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_audit_message (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    audited_event integer,
    comment character varying(255),
    event_code_type character varying(255),
    oid character varying(255),
    audited_transaction integer,
    document_section integer,
    issuing_actor integer
);


ALTER TABLE public.tf_audit_message OWNER TO gazelle;

--
-- Name: tf_audit_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_audit_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_audit_message_id_seq OWNER TO gazelle;

--
-- Name: tf_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    document_dateofpublication timestamp without time zone,
    md5_hash_code character varying(255) NOT NULL,
    document_lifecyclestatus integer,
    document_linkstatus integer,
    document_linkstatusdescription character varying(255),
    document_name character varying(255) NOT NULL,
    document_revision character varying(255),
    document_title character varying(255),
    document_type integer NOT NULL,
    document_url character varying(255) NOT NULL,
    document_volume character varying(255),
    document_domain_id integer NOT NULL
);


ALTER TABLE public.tf_document OWNER TO gazelle;

--
-- Name: tf_document_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    document_dateofpublication timestamp without time zone,
    md5_hash_code character varying(255),
    document_lifecyclestatus integer,
    document_linkstatus integer,
    document_linkstatusdescription character varying(255),
    document_name character varying(255),
    document_revision character varying(255),
    document_title character varying(255),
    document_type integer,
    document_url character varying(255),
    document_volume character varying(255),
    document_domain_id integer
);


ALTER TABLE public.tf_document_aud OWNER TO gazelle;

--
-- Name: tf_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_id_seq OWNER TO gazelle;

--
-- Name: tf_document_sections; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    section character varying(255),
    type integer,
    document_id integer
);


ALTER TABLE public.tf_document_sections OWNER TO gazelle;

--
-- Name: tf_document_sections_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    section character varying(255),
    type integer,
    document_id integer
);


ALTER TABLE public.tf_document_sections_aud OWNER TO gazelle;

--
-- Name: tf_document_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_sections_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_domain_aud OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_profile OWNER TO gazelle;

--
-- Name: tf_domain_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile_aud (
    rev integer NOT NULL,
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_domain_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    hl7_version character varying(255) NOT NULL,
    message_order_control_code character varying(8),
    message_type character varying(15) NOT NULL,
    profile_content character varying(255),
    profile_oid character varying(64) NOT NULL,
    profile_type character varying(255),
    actor_id integer,
    domain_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain (
    hl7_message_profile_id integer NOT NULL,
    affinity_domain_id integer NOT NULL
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain_aud (
    rev integer NOT NULL,
    hl7_message_profile_id integer NOT NULL,
    affinity_domain_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    hl7_version character varying(255),
    message_order_control_code character varying(8),
    message_type character varying(15),
    profile_content character varying(255),
    profile_oid character varying(64),
    profile_type character varying(255),
    actor_id integer,
    domain_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_hl7_message_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_hl7_message_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(32) NOT NULL,
    name character varying(128),
    documentsection integer,
    integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(32),
    name character varying(128),
    documentsection integer,
    integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128),
    reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link (
    integration_profile_id integer NOT NULL,
    integration_profile_type_id integer NOT NULL
);


ALTER TABLE public.tf_integration_profile_type_link OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link_aud (
    rev integer NOT NULL,
    integration_profile_id integer NOT NULL,
    integration_profile_type_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_integration_profile_type_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    actor_integration_profile_id integer,
    transaction_id integer,
    transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link OWNER TO gazelle;

--
-- Name: tf_profile_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    actor_integration_profile_id integer,
    transaction_id integer,
    transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_profile_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_profile_link_id_seq OWNER TO gazelle;

--
-- Name: tf_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule (
    id integer NOT NULL,
    comment character varying(1024),
    cause_id integer,
    consequence_id integer
);


ALTER TABLE public.tf_rule OWNER TO gazelle;

--
-- Name: tf_rule_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_criterion (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    list_or boolean,
    single_actor_id integer,
    aiporule_id integer,
    single_integration_profile_id integer,
    single_option_id integer
);


ALTER TABLE public.tf_rule_criterion OWNER TO gazelle;

--
-- Name: tf_rule_criterion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_criterion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_criterion_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_list_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_list_criterion (
    tf_rule_criterion_id integer NOT NULL,
    aipocriterions_id integer NOT NULL,
    tf_rule_list_criterion_id integer NOT NULL
);


ALTER TABLE public.tf_rule_list_criterion OWNER TO gazelle;

--
-- Name: tf_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    keyword character varying(255),
    name text,
    network_communication_type integer,
    url text,
    version character varying(255)
);


ALTER TABLE public.tf_standard OWNER TO gazelle;

--
-- Name: tf_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    keyword character varying(255),
    name text,
    network_communication_type integer,
    url text,
    version character varying(255)
);


ALTER TABLE public.tf_standard_aud OWNER TO gazelle;

--
-- Name: tf_standard_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_standard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_standard_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    reference character varying(2048),
    documentsection integer,
    transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128),
    reference character varying(2048),
    documentsection integer,
    transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction_aud OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    from_actor_id integer,
    to_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_transaction_link OWNER TO gazelle;

--
-- Name: tf_transaction_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    from_actor_id integer,
    to_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_transaction_link_aud OWNER TO gazelle;

--
-- Name: tf_transaction_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_link_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_option_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_option_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_option_type_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard (
    transaction_id integer NOT NULL,
    standard_id integer NOT NULL
);


ALTER TABLE public.tf_transaction_standard OWNER TO gazelle;

--
-- Name: tf_transaction_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard_aud (
    rev integer NOT NULL,
    transaction_id integer NOT NULL,
    standard_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_transaction_standard_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    keyword character varying(32) NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE public.tf_transaction_status_type OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(255),
    keyword character varying(32),
    name character varying(64)
);


ALTER TABLE public.tf_transaction_status_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    usage character varying(255),
    transaction_id integer NOT NULL
);


ALTER TABLE public.tf_ws_transaction_usage OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    usage character varying(255),
    transaction_id integer
);


ALTER TABLE public.tf_ws_transaction_usage_aud OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_ws_transaction_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_ws_transaction_usage_id_seq OWNER TO gazelle;

--
-- Name: ut_test_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_test_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_test_file_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    active boolean NOT NULL,
    comment text,
    expected_result character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    last_modifier character varying(255),
    last_result boolean,
    last_run_date timestamp without time zone,
    last_success_date timestamp without time zone,
    last_tested_version character varying(255),
    reason_for_failure text,
    tested_rule_id integer
);


ALTER TABLE public.ut_unit_test OWNER TO gazelle;

--
-- Name: ut_unit_test_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_file (
    id integer NOT NULL,
    comment text,
    filepath character varying(255),
    input_keyword character varying(255),
    input_type integer,
    unit_test_id integer
);


ALTER TABLE public.ut_unit_test_file OWNER TO gazelle;

--
-- Name: ut_unit_test_file_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_file_log (
    log_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE public.ut_unit_test_file_log OWNER TO gazelle;

--
-- Name: ut_unit_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_unit_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_unit_test_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.ut_unit_test_log (
    id integer NOT NULL,
    additional_info text,
    effective_result integer,
    reason_for_failure text,
    run_date timestamp without time zone,
    success boolean NOT NULL,
    tested_version character varying(255),
    unit_test_id integer
);


ALTER TABLE public.ut_unit_test_log OWNER TO gazelle;

--
-- Name: ut_unit_test_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ut_unit_test_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ut_unit_test_log_id_seq OWNER TO gazelle;

--
-- Name: xval_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_assertion (
    id integer NOT NULL,
    assertion_id character varying(255) NOT NULL,
    id_scheme character varying(255) NOT NULL
);


ALTER TABLE public.xval_assertion OWNER TO gazelle;

--
-- Name: xval_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_assertion_id_seq OWNER TO gazelle;

--
-- Name: xval_assertion_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_assertion_rule (
    assertion_id integer NOT NULL,
    rule_id integer NOT NULL
);


ALTER TABLE public.xval_assertion_rule OWNER TO gazelle;

--
-- Name: xval_cross_validation_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validation_log (
    id integer NOT NULL,
    affinity_domain character varying(255),
    calling_tool_oid character varying(255),
    detailed_result text,
    external_id character varying(255),
    gazelle_cross_validator character varying(255),
    is_public boolean,
    message_type_proxy character varying(255),
    oid character varying(255),
    privacy_key character varying(255),
    result integer,
    "timestamp" timestamp without time zone,
    username character varying(255)
);


ALTER TABLE public.xval_cross_validation_log OWNER TO gazelle;

--
-- Name: xval_cross_validation_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validation_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validation_log_id_seq OWNER TO gazelle;

--
-- Name: xval_cross_validator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator (
    id integer NOT NULL,
    affinitydomain character varying(255),
    description text,
    last_modified_date timestamp without time zone,
    last_modifier character varying(255),
    name character varying(255),
    oid character varying(255),
    status integer,
    version character varying(255)
);


ALTER TABLE public.xval_cross_validator OWNER TO gazelle;

--
-- Name: xval_cross_validator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validator_id_seq OWNER TO gazelle;

--
-- Name: xval_cross_validator_parent; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator_parent (
    validator_id integer NOT NULL,
    validator_parent_id integer NOT NULL
);


ALTER TABLE public.xval_cross_validator_parent OWNER TO gazelle;

--
-- Name: xval_cross_validator_reference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_cross_validator_reference (
    id integer NOT NULL,
    affinity_domain character varying(255),
    name character varying(255),
    version character varying(255)
);


ALTER TABLE public.xval_cross_validator_reference OWNER TO gazelle;

--
-- Name: xval_cross_validator_reference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_cross_validator_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_cross_validator_reference_id_seq OWNER TO gazelle;

--
-- Name: xval_date1_format_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_date1_format_type (
    date_comparison_id integer NOT NULL,
    date_format_id integer NOT NULL
);


ALTER TABLE public.xval_date1_format_type OWNER TO gazelle;

--
-- Name: xval_date2_format_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_date2_format_type (
    date_comparison_id integer NOT NULL,
    date_format_id integer NOT NULL
);


ALTER TABLE public.xval_date2_format_type OWNER TO gazelle;

--
-- Name: xval_date_format_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_date_format_type (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    "precision" integer NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.xval_date_format_type OWNER TO gazelle;

--
-- Name: xval_date_format_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_date_format_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_date_format_type_id_seq OWNER TO gazelle;

--
-- Name: xval_deprecated_validator_version; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_deprecated_validator_version (
    id integer NOT NULL,
    affinity_domain character varying(255),
    comment text,
    file_path character varying(255),
    last_modified timestamp without time zone,
    last_modifier character varying(255),
    name character varying(255),
    version character varying(255)
);


ALTER TABLE public.xval_deprecated_validator_version OWNER TO gazelle;

--
-- Name: xval_deprecated_validator_version_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_deprecated_validator_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_deprecated_validator_version_id_seq OWNER TO gazelle;

--
-- Name: xval_expression; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_expression (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    lang character varying(255),
    value_set_id character varying(255),
    math_comparator integer,
    delta double precision,
    logical_operator integer,
    date_operator integer,
    basic_operator integer,
    var character varying(255),
    locator_id integer,
    expression_id integer,
    left_member_id integer,
    right_member_id integer,
    locator_1_id integer,
    locator_2_id integer,
    xpath_id integer,
    expression_set_id integer
);


ALTER TABLE public.xval_expression OWNER TO gazelle;

--
-- Name: xval_expression_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_expression_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_expression_id_seq OWNER TO gazelle;

--
-- Name: xval_locator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_locator (
    id integer NOT NULL,
    applies_on character varying(255) NOT NULL,
    path text NOT NULL
);


ALTER TABLE public.xval_locator OWNER TO gazelle;

--
-- Name: xval_locator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_locator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_locator_id_seq OWNER TO gazelle;

--
-- Name: xval_log_id; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_log_id (
    log_id integer,
    id integer NOT NULL
);


ALTER TABLE public.xval_log_id OWNER TO gazelle;

--
-- Name: xval_logical_operation_expression_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_logical_operation_expression_set (
    xval_expression_id integer NOT NULL,
    expressionset_id integer NOT NULL,
    xval_expression_set_id integer NOT NULL
);


ALTER TABLE public.xval_logical_operation_expression_set OWNER TO gazelle;

--
-- Name: xval_math_member; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_math_member (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    math_operator integer,
    value double precision,
    member1_id integer,
    member2_id integer,
    locator_id integer
);


ALTER TABLE public.xval_math_member OWNER TO gazelle;

--
-- Name: xval_math_member_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_math_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_math_member_id_seq OWNER TO gazelle;

--
-- Name: xval_namespace; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_namespace (
    id integer NOT NULL,
    prefix character varying(255),
    uri character varying(255)
);


ALTER TABLE public.xval_namespace OWNER TO gazelle;

--
-- Name: xval_namespace_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_namespace_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_namespace_id_seq OWNER TO gazelle;

--
-- Name: xval_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_object_type (
    id integer NOT NULL,
    extension character varying(255),
    kind integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.xval_object_type OWNER TO gazelle;

--
-- Name: xval_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_object_type_id_seq OWNER TO gazelle;

--
-- Name: xval_referenced_object; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_referenced_object (
    id integer NOT NULL,
    description text,
    keyword character varying(255),
    object_type integer,
    xsdlocation character varying(255)
);


ALTER TABLE public.xval_referenced_object OWNER TO gazelle;

--
-- Name: xval_referenced_object_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_referenced_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_referenced_object_id_seq OWNER TO gazelle;

--
-- Name: xval_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_rule (
    id integer NOT NULL,
    applies_to bytea,
    description text NOT NULL,
    keyword character varying(255) NOT NULL,
    last_modified_date timestamp without time zone,
    last_modifier character varying(255) NOT NULL,
    level integer NOT NULL,
    status integer NOT NULL,
    textual_expression text,
    version character varying(255) NOT NULL,
    expression_id integer NOT NULL,
    gazelle_x_validator_id integer
);


ALTER TABLE public.xval_rule OWNER TO gazelle;

--
-- Name: xval_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_rule_id_seq OWNER TO gazelle;

--
-- Name: xval_uploaded_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_uploaded_file (
    id integer NOT NULL,
    directory character varying(255),
    input_description character varying(255),
    keyword character varying(255),
    input_max integer,
    input_min integer,
    input_object_type integer
);


ALTER TABLE public.xval_uploaded_file OWNER TO gazelle;

--
-- Name: xval_uploaded_file_files; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_uploaded_file_files (
    xval_uploaded_file_id integer NOT NULL,
    element character varying(255)
);


ALTER TABLE public.xval_uploaded_file_files OWNER TO gazelle;

--
-- Name: xval_uploaded_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_uploaded_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_uploaded_file_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_configuration (
    id integer NOT NULL,
    assertion_manager_url character varying(255),
    dicom_library integer,
    value_set_provider_url character varying(255),
    validator_id integer
);


ALTER TABLE public.xval_validator_configuration OWNER TO gazelle;

--
-- Name: xval_validator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_configuration_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_configuration_namespace; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_configuration_namespace (
    configuration_id integer NOT NULL,
    namespace_id integer NOT NULL
);


ALTER TABLE public.xval_validator_configuration_namespace OWNER TO gazelle;

--
-- Name: xval_validator_input; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_input (
    id integer NOT NULL,
    max_quantity integer,
    message_type character varying(255),
    min_quantity integer,
    gazelle_cross_validator_id integer,
    referenced_object_id integer
);


ALTER TABLE public.xval_validator_input OWNER TO gazelle;

--
-- Name: xval_validator_input_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_input_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_input_id_seq OWNER TO gazelle;

--
-- Name: xval_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xval_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.xval_validator_usage OWNER TO gazelle;

--
-- Name: xval_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xval_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xval_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_path_linking_a_document cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: evsc_api_key evsc_api_key_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_api_key
    ADD CONSTRAINT evsc_api_key_pkey PRIMARY KEY (value);


--
-- Name: evsc_caller_metadata evsc_caller_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_caller_metadata
    ADD CONSTRAINT evsc_caller_metadata_pkey PRIMARY KEY (id);


--
-- Name: evsc_calling_tool evsc_calling_tool_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_calling_tool
    ADD CONSTRAINT evsc_calling_tool_pkey PRIMARY KEY (id);


--
-- Name: evsc_digital_signature_service_conf evsc_digital_signature_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_digital_signature_service_conf
    ADD CONSTRAINT evsc_digital_signature_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_embedded_validation_service_conf evsc_embedded_validation_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_embedded_validation_service_conf
    ADD CONSTRAINT evsc_embedded_validation_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_extension_service_conf evsc_extension_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_extension_service_conf
    ADD CONSTRAINT evsc_extension_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_handled_object evsc_handled_object_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_handled_object
    ADD CONSTRAINT evsc_handled_object_pkey PRIMARY KEY (id);


--
-- Name: evsc_ip_loc evsc_ip_loc_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_ip_loc
    ADD CONSTRAINT evsc_ip_loc_pkey PRIMARY KEY (id);


--
-- Name: evsc_menu_group evsc_menu_group_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_menu_group
    ADD CONSTRAINT evsc_menu_group_pkey PRIMARY KEY (id);


--
-- Name: evsc_named_stylesheet evsc_named_stylesheet_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_named_stylesheet
    ADD CONSTRAINT evsc_named_stylesheet_pkey PRIMARY KEY (id);


--
-- Name: evsc_oid evsc_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_oid
    ADD CONSTRAINT evsc_oid_pkey PRIMARY KEY (id);


--
-- Name: evsc_other_caller_metadata evsc_other_caller_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_other_caller_metadata
    ADD CONSTRAINT evsc_other_caller_metadata_pkey PRIMARY KEY (id);


--
-- Name: evsc_owner_metadata evsc_owner_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_owner_metadata
    ADD CONSTRAINT evsc_owner_metadata_pkey PRIMARY KEY (id);


--
-- Name: evsc_processing evsc_processing_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing
    ADD CONSTRAINT evsc_processing_pkey PRIMARY KEY (id);


--
-- Name: evsc_proxy_caller_metadata evsc_proxy_caller_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_proxy_caller_metadata
    ADD CONSTRAINT evsc_proxy_caller_metadata_pkey PRIMARY KEY (id);


--
-- Name: evsc_referenced_standard evsc_referenced_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_referenced_standard
    ADD CONSTRAINT evsc_referenced_standard_pkey PRIMARY KEY (id);


--
-- Name: evsc_referenced_standard_ref evsc_referenced_standard_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_referenced_standard_ref
    ADD CONSTRAINT evsc_referenced_standard_ref_pkey PRIMARY KEY (id);


--
-- Name: evsc_report evsc_report_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_report
    ADD CONSTRAINT evsc_report_pkey PRIMARY KEY (id);


--
-- Name: evsc_scorecard_service_conf evsc_scorecard_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_scorecard_service_conf
    ADD CONSTRAINT evsc_scorecard_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_sharing_metadata evsc_sharing_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_sharing_metadata
    ADD CONSTRAINT evsc_sharing_metadata_pkey PRIMARY KEY (id);


--
-- Name: evsc_stylesheet evsc_stylesheet_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_stylesheet
    ADD CONSTRAINT evsc_stylesheet_pkey PRIMARY KEY (id);


--
-- Name: evsc_system_validation_service_conf evsc_system_validation_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_system_validation_service_conf
    ADD CONSTRAINT evsc_system_validation_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation_digital_signature evsc_validation_digital_signature_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_digital_signature
    ADD CONSTRAINT evsc_validation_digital_signature_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation_extension evsc_validation_extension_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT evsc_validation_extension_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation evsc_validation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation
    ADD CONSTRAINT evsc_validation_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation_report_type evsc_validation_report_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_report_type
    ADD CONSTRAINT evsc_validation_report_type_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation_scorecard evsc_validation_scorecard_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_scorecard
    ADD CONSTRAINT evsc_validation_scorecard_pkey PRIMARY KEY (id);


--
-- Name: evsc_validation_service_conf evsc_validation_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_service_conf
    ADD CONSTRAINT evsc_validation_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_web_extension_service_conf evsc_web_extension_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_web_extension_service_conf
    ADD CONSTRAINT evsc_web_extension_service_conf_pkey PRIMARY KEY (id);


--
-- Name: evsc_web_validation_service_conf evsc_web_validation_service_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_web_validation_service_conf
    ADD CONSTRAINT evsc_web_validation_service_conf_pkey PRIMARY KEY (id);


--
-- Name: mca_analysis_part_namespaces mca_analysis_part_namespaces_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis_part_namespaces
    ADD CONSTRAINT mca_analysis_part_namespaces_pkey PRIMARY KEY (analysis_part_id, namespace_prefix);


--
-- Name: mca_analysis_part mca_analysis_part_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis_part
    ADD CONSTRAINT mca_analysis_part_pkey PRIMARY KEY (id);


--
-- Name: mca_analysis mca_analysis_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis
    ADD CONSTRAINT mca_analysis_pkey PRIMARY KEY (id);


--
-- Name: mca_content_analysis_config mca_content_analysis_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_content_analysis_config
    ADD CONSTRAINT mca_content_analysis_config_pkey PRIMARY KEY (id);


--
-- Name: mca_doc_type mca_doc_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_doc_type
    ADD CONSTRAINT mca_doc_type_pkey PRIMARY KEY (id);


--
-- Name: mca_file_config mca_file_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_file_config
    ADD CONSTRAINT mca_file_config_pkey PRIMARY KEY (id);


--
-- Name: mca_folder_config mca_folder_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_folder_config
    ADD CONSTRAINT mca_folder_config_pkey PRIMARY KEY (id);


--
-- Name: mca_mime_type_config mca_mime_type_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_mime_type_config
    ADD CONSTRAINT mca_mime_type_config_pkey PRIMARY KEY (id);


--
-- Name: mca_x_validation_matching mca_x_validation_matching_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_x_validation_matching
    ADD CONSTRAINT mca_x_validation_matching_pkey PRIMARY KEY (id);


--
-- Name: mca_xml_tag_config mca_xml_tag_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_xml_tag_config
    ADD CONSTRAINT mca_xml_tag_config_pkey PRIMARY KEY (id);


--
-- Name: mca_zip_config mca_zip_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_zip_config
    ADD CONSTRAINT mca_zip_config_pkey PRIMARY KEY (id);


--
-- Name: package_name_for_profile_oid package_name_for_profile_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.package_name_for_profile_oid
    ADD CONSTRAINT package_name_for_profile_oid_pkey PRIMARY KEY (id);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: tf_actor_aud tf_actor_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT tf_actor_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_aud tf_actor_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT tf_actor_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option_aud tf_actor_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT tf_actor_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_affinity_domain_aud tf_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT tf_affinity_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_affinity_domain tf_affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT tf_affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_audit_message tf_audit_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT tf_audit_message_pkey PRIMARY KEY (id);


--
-- Name: tf_document_aud tf_document_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT tf_document_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document tf_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT tf_document_pkey PRIMARY KEY (id);


--
-- Name: tf_document_sections_aud tf_document_sections_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT tf_document_sections_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document_sections tf_document_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT tf_document_sections_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_aud tf_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT tf_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_profile_aud tf_domain_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT tf_domain_profile_aud_pkey PRIMARY KEY (rev, domain_id, integration_profile_id);


--
-- Name: tf_domain_profile tf_domain_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT tf_domain_profile_pkey PRIMARY KEY (domain_id, integration_profile_id);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud tf_hl7_message_profile_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT tf_hl7_message_profile_affinity_domain_aud_pkey PRIMARY KEY (rev, hl7_message_profile_id, affinity_domain_id);


--
-- Name: tf_hl7_message_profile_aud tf_hl7_message_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT tf_hl7_message_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_hl7_message_profile tf_hl7_message_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT tf_hl7_message_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_aud tf_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT tf_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option_aud tf_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT tf_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_status_type_aud tf_integration_profile_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT tf_integration_profile_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_status_type tf_integration_profile_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT tf_integration_profile_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_type_aud tf_integration_profile_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT tf_integration_profile_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_type_link_aud tf_integration_profile_type_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT tf_integration_profile_type_link_aud_pkey PRIMARY KEY (rev, integration_profile_id, integration_profile_type_id);


--
-- Name: tf_integration_profile_type tf_integration_profile_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT tf_integration_profile_type_pkey PRIMARY KEY (id);


--
-- Name: tf_profile_link_aud tf_profile_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT tf_profile_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_profile_link tf_profile_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT tf_profile_link_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_criterion tf_rule_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT tf_rule_criterion_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_list_criterion tf_rule_list_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT tf_rule_list_criterion_pkey PRIMARY KEY (tf_rule_list_criterion_id);


--
-- Name: tf_rule tf_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT tf_rule_pkey PRIMARY KEY (id);


--
-- Name: tf_standard_aud tf_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT tf_standard_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_standard tf_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT tf_standard_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_aud tf_transaction_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT tf_transaction_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link_aud tf_transaction_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT tf_transaction_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link tf_transaction_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT tf_transaction_link_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_option_type_aud tf_transaction_option_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT tf_transaction_option_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_option_type tf_transaction_option_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT tf_transaction_option_type_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_standard_aud tf_transaction_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT tf_transaction_standard_aud_pkey PRIMARY KEY (rev, transaction_id, standard_id);


--
-- Name: tf_transaction_status_type_aud tf_transaction_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT tf_transaction_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_status_type tf_transaction_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT tf_transaction_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_ws_transaction_usage_aud tf_ws_transaction_usage_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT tf_ws_transaction_usage_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_ws_transaction_usage tf_ws_transaction_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT tf_ws_transaction_usage_pkey PRIMARY KEY (id);


--
-- Name: xval_rule uk_1cf5qn39ldgfcb6t2orjobc63; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT uk_1cf5qn39ldgfcb6t2orjobc63 UNIQUE (version, keyword);


--
-- Name: xval_date2_format_type uk_1ev9k1sxe2pi55hbqp0871q45; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date2_format_type
    ADD CONSTRAINT uk_1ev9k1sxe2pi55hbqp0871q45 UNIQUE (date_comparison_id, date_format_id);


--
-- Name: evsc_processing_evsc_handled_object uk_3v046f0iw1pay396ple7djea1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing_evsc_handled_object
    ADD CONSTRAINT uk_3v046f0iw1pay396ple7djea1 UNIQUE (objects_id);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tf_integration_profile uk_4ojy2hqgxhoytlcrjkp9h9lr3; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_4ojy2hqgxhoytlcrjkp9h9lr3 UNIQUE (name);


--
-- Name: cmn_application_preference uk_535ry4wkukk8xivd9vqvu06hp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT uk_535ry4wkukk8xivd9vqvu06hp UNIQUE (preference_name);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: xval_assertion_rule uk_6vqu0bbk75hhc9045s2los5ef; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT uk_6vqu0bbk75hhc9045s2los5ef UNIQUE (assertion_id, rule_id);


--
-- Name: tf_transaction_status_type uk_712597f0v980ai4l7v0cg1rrh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_712597f0v980ai4l7v0cg1rrh UNIQUE (keyword);


--
-- Name: tf_rule_list_criterion uk_81shykw7emr3q41anx9ksqdb6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT uk_81shykw7emr3q41anx9ksqdb6 UNIQUE (aipocriterions_id);


--
-- Name: tf_standard uk_862df7ewvip5ajs8fpnnx9hm6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT uk_862df7ewvip5ajs8fpnnx9hm6 UNIQUE (keyword);


--
-- Name: ut_unit_test uk_89jnp3ftpf9x1fabhp0ld3g6y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT uk_89jnp3ftpf9x1fabhp0ld3g6y UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_transaction_status_type uk_8ojkw0me2bv4tsm5vyrmwb6gu; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_8ojkw0me2bv4tsm5vyrmwb6gu UNIQUE (name);


--
-- Name: tf_transaction_link uk_97iyim1a054rgjkpa32j9ksxb; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT uk_97iyim1a054rgjkpa32j9ksxb UNIQUE (from_actor_id, to_actor_id, transaction_id);


--
-- Name: tf_affinity_domain uk_9lriehprhbeo91cxqn7rqwjsy; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT uk_9lriehprhbeo91cxqn7rqwjsy UNIQUE (keyword);


--
-- Name: tf_transaction_standard uk_9xw54omnofi1bn7px13kr6jyk; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT uk_9xw54omnofi1bn7px13kr6jyk UNIQUE (standard_id, transaction_id);


--
-- Name: xval_logical_operation_expression_set uk_ax6ugs1gj1apnnfv5fwny0p3r; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT uk_ax6ugs1gj1apnnfv5fwny0p3r UNIQUE (expressionset_id);


--
-- Name: xval_cross_validator_parent uk_b48phy6ijhrg1lbypmrv7jxqv; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT uk_b48phy6ijhrg1lbypmrv7jxqv UNIQUE (validator_parent_id, validator_id);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_integration_profile_type_link uk_d8f4vv4juh946m3kbs3o0w7p2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT uk_d8f4vv4juh946m3kbs3o0w7p2 UNIQUE (integration_profile_id, integration_profile_type_id);


--
-- Name: evsc_validation_evsc_referenced_standard_ref uk_e4yedt9c4nmpwhrfeq5ea6bat; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_evsc_referenced_standard_ref
    ADD CONSTRAINT uk_e4yedt9c4nmpwhrfeq5ea6bat UNIQUE (referencedstandards_id);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: tf_transaction_option_type uk_fv74hod7rl73bhyerub82q67c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_fv74hod7rl73bhyerub82q67c UNIQUE (name);


--
-- Name: tf_document_sections uk_gqqt5kk1xa5npiaptbwwdu9bt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT uk_gqqt5kk1xa5npiaptbwwdu9bt UNIQUE (section, document_id);


--
-- Name: tf_integration_profile_status_type uk_gs2vk7vmy1ggt3p34n1f5da6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_gs2vk7vmy1ggt3p34n1f5da6 UNIQUE (keyword);


--
-- Name: evsc_calling_tool uk_hu2lu3qafhgkbnd4r7q07b8v1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_calling_tool
    ADD CONSTRAINT uk_hu2lu3qafhgkbnd4r7q07b8v1 UNIQUE (label);


--
-- Name: evsc_menu_group uk_jct8rtnk47c8yfmgedm5vcgjk; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_menu_group
    ADD CONSTRAINT uk_jct8rtnk47c8yfmgedm5vcgjk UNIQUE (label);


--
-- Name: tf_integration_profile_type uk_jx75w0mlp0ptnjsbiv8576nl0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT uk_jx75w0mlp0ptnjsbiv8576nl0 UNIQUE (keyword);


--
-- Name: tf_hl7_message_profile uk_k6ujf0i2mjoaf4w15wk7fq89y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT uk_k6ujf0i2mjoaf4w15wk7fq89y UNIQUE (profile_oid);


--
-- Name: tf_domain_profile uk_luennoioveseb7edrlppqaum2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT uk_luennoioveseb7edrlppqaum2 UNIQUE (domain_id, integration_profile_id);


--
-- Name: tf_profile_link uk_mei0k3s7jd4oyc3000w96e3oi; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT uk_mei0k3s7jd4oyc3000w96e3oi UNIQUE (actor_integration_profile_id, transaction_id, transaction_option_type_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: tf_hl7_message_profile_affinity_domain uk_n5trtb5a0bt27vlsad418w4rj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT uk_n5trtb5a0bt27vlsad418w4rj UNIQUE (hl7_message_profile_id, affinity_domain_id);


--
-- Name: xval_date1_format_type uk_nd3edbh71skwbul566bbad0n1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date1_format_type
    ADD CONSTRAINT uk_nd3edbh71skwbul566bbad0n1 UNIQUE (date_comparison_id, date_format_id);


--
-- Name: tf_transaction_option_type uk_nix1duwegwr24dpbqa0u7mgm9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_nix1duwegwr24dpbqa0u7mgm9 UNIQUE (keyword);


--
-- Name: evsc_menu_standard uk_njh624mu7lyjio7qo0gblfdmg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_menu_standard
    ADD CONSTRAINT uk_njh624mu7lyjio7qo0gblfdmg UNIQUE (menu_group_id, referenced_standard_id);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: xval_referenced_object uk_ovjkbvb6m0bhbkhqbibe6cipg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_referenced_object
    ADD CONSTRAINT uk_ovjkbvb6m0bhbkhqbibe6cipg UNIQUE (keyword);


--
-- Name: xval_validator_configuration_namespace uk_p261236c5epprob8ilej79yy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT uk_p261236c5epprob8ilej79yy1 UNIQUE (configuration_id, namespace_id);


--
-- Name: ut_unit_test_file_log uk_p5f447j78nqr5q5njfp5kri80; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT uk_p5f447j78nqr5q5njfp5kri80 UNIQUE (file_id, log_id);


--
-- Name: tf_integration_profile_status_type uk_pejuyujydm4t4ylimltqlogfc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_pejuyujydm4t4ylimltqlogfc UNIQUE (name);


--
-- Name: xval_date_format_type uk_pkusyc6guyfkhuivtb34n8dll; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date_format_type
    ADD CONSTRAINT uk_pkusyc6guyfkhuivtb34n8dll UNIQUE (label);


--
-- Name: tf_document uk_q7ne4eu5ts5lgfl3aces2ikin; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT uk_q7ne4eu5ts5lgfl3aces2ikin UNIQUE (md5_hash_code);


--
-- Name: evsc_conf_composition uk_qdg82dvv903ufxi0y7bgrbngr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_conf_composition
    ADD CONSTRAINT uk_qdg82dvv903ufxi0y7bgrbngr UNIQUE (composed_id, component_id);


--
-- Name: tf_ws_transaction_usage uk_s65caasaiq9j4f8f99hikh9qm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT uk_s65caasaiq9j4f8f99hikh9qm UNIQUE (usage);


--
-- Name: mca_mime_type_config uk_t2amjku8apods3vn8axul19b1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_mime_type_config
    ADD CONSTRAINT uk_t2amjku8apods3vn8axul19b1 UNIQUE (mime_type);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: xval_object_type uk_tkobc8f91qnv6pqxhsugdqlf1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_object_type
    ADD CONSTRAINT uk_tkobc8f91qnv6pqxhsugdqlf1 UNIQUE (name);


--
-- Name: ut_unit_test_file ut_unit_test_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file
    ADD CONSTRAINT ut_unit_test_file_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test_log ut_unit_test_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_log
    ADD CONSTRAINT ut_unit_test_log_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test ut_unit_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT ut_unit_test_pkey PRIMARY KEY (id);


--
-- Name: xval_assertion xval_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion
    ADD CONSTRAINT xval_assertion_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validation_log xval_cross_validation_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validation_log
    ADD CONSTRAINT xval_cross_validation_log_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validator xval_cross_validator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator
    ADD CONSTRAINT xval_cross_validator_pkey PRIMARY KEY (id);


--
-- Name: xval_cross_validator_reference xval_cross_validator_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_reference
    ADD CONSTRAINT xval_cross_validator_reference_pkey PRIMARY KEY (id);


--
-- Name: xval_date_format_type xval_date_format_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date_format_type
    ADD CONSTRAINT xval_date_format_type_pkey PRIMARY KEY (id);


--
-- Name: xval_deprecated_validator_version xval_deprecated_validator_version_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_deprecated_validator_version
    ADD CONSTRAINT xval_deprecated_validator_version_pkey PRIMARY KEY (id);


--
-- Name: xval_expression xval_expression_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT xval_expression_pkey PRIMARY KEY (id);


--
-- Name: xval_locator xval_locator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_locator
    ADD CONSTRAINT xval_locator_pkey PRIMARY KEY (id);


--
-- Name: xval_log_id xval_log_id_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT xval_log_id_pkey PRIMARY KEY (id);


--
-- Name: xval_logical_operation_expression_set xval_logical_operation_expression_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT xval_logical_operation_expression_set_pkey PRIMARY KEY (xval_expression_set_id);


--
-- Name: xval_math_member xval_math_member_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT xval_math_member_pkey PRIMARY KEY (id);


--
-- Name: xval_namespace xval_namespace_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_namespace
    ADD CONSTRAINT xval_namespace_pkey PRIMARY KEY (id);


--
-- Name: xval_object_type xval_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_object_type
    ADD CONSTRAINT xval_object_type_pkey PRIMARY KEY (id);


--
-- Name: xval_referenced_object xval_referenced_object_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_referenced_object
    ADD CONSTRAINT xval_referenced_object_pkey PRIMARY KEY (id);


--
-- Name: xval_rule xval_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT xval_rule_pkey PRIMARY KEY (id);


--
-- Name: xval_uploaded_file xval_uploaded_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_uploaded_file
    ADD CONSTRAINT xval_uploaded_file_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_configuration xval_validator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration
    ADD CONSTRAINT xval_validator_configuration_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_input xval_validator_input_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT xval_validator_input_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_usage xval_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_usage
    ADD CONSTRAINT xval_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: xval_validator_configuration_namespace fk_19x0hbg6r4ay0ubeh3bwlsp3u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT fk_19x0hbg6r4ay0ubeh3bwlsp3u FOREIGN KEY (namespace_id) REFERENCES public.xval_namespace(id);


--
-- Name: tf_integration_profile fk_1cdstaxi6v493qidfghxv24ry; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_1cdstaxi6v493qidfghxv24ry FOREIGN KEY (integration_profile_status_type_id) REFERENCES public.tf_integration_profile_status_type(id);


--
-- Name: ut_unit_test fk_1hjjeam5i6l71jbftje43x6yn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test
    ADD CONSTRAINT fk_1hjjeam5i6l71jbftje43x6yn FOREIGN KEY (tested_rule_id) REFERENCES public.xval_rule(id);


--
-- Name: mca_validation_types fk_1jehgy8feynw6e1jf4rlqq2gg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_validation_types
    ADD CONSTRAINT fk_1jehgy8feynw6e1jf4rlqq2gg FOREIGN KEY (analysis_id) REFERENCES public.mca_analysis(id);


--
-- Name: tf_rule fk_1nipbe194qol7ts0abf705jv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_1nipbe194qol7ts0abf705jv4 FOREIGN KEY (consequence_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: xval_expression fk_22ie064gm9erjye7x0j5658ao; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_22ie064gm9erjye7x0j5658ao FOREIGN KEY (right_member_id) REFERENCES public.xval_math_member(id);


--
-- Name: evsc_embedded_validation_service_conf fk_2h3oalr8n9l1rlcxnmge5cv97; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_embedded_validation_service_conf
    ADD CONSTRAINT fk_2h3oalr8n9l1rlcxnmge5cv97 FOREIGN KEY (id) REFERENCES public.evsc_validation_service_conf(id);


--
-- Name: evsc_menu_standard fk_2k3rn73kuvkipipqlm9dff73t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_menu_standard
    ADD CONSTRAINT fk_2k3rn73kuvkipipqlm9dff73t FOREIGN KEY (referenced_standard_id) REFERENCES public.evsc_referenced_standard(id);


--
-- Name: evsc_validation fk_2lp3ppj43qycid4b6186a1q8q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation
    ADD CONSTRAINT fk_2lp3ppj43qycid4b6186a1q8q FOREIGN KEY (original_report_id) REFERENCES public.evsc_report(id);


--
-- Name: xval_log_id fk_2rahgovgpmx0utgokrjhmpe4t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT fk_2rahgovgpmx0utgokrjhmpe4t FOREIGN KEY (id) REFERENCES public.xval_uploaded_file(id);


--
-- Name: mca_zip_file_children fk_2s830dbn0053fjgg31nv7u3yj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_zip_file_children
    ADD CONSTRAINT fk_2s830dbn0053fjgg31nv7u3yj FOREIGN KEY (folder_id) REFERENCES public.mca_file_config(id);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_37u9uwxammh3m9bg9jfueia3i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_37u9uwxammh3m9bg9jfueia3i FOREIGN KEY (affinity_domain_id) REFERENCES public.tf_affinity_domain(id);


--
-- Name: tf_transaction_standard_aud fk_384id91wu9ujfmxhkbj25hfl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT fk_384id91wu9ujfmxhkbj25hfl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: mca_folder_file_children fk_38s2gegimcg90s6e2o25f9ne4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_folder_file_children
    ADD CONSTRAINT fk_38s2gegimcg90s6e2o25f9ne4 FOREIGN KEY (file_child_id) REFERENCES public.mca_file_config(id);


--
-- Name: evsc_other_caller_metadata fk_3fo2phxtmkfr5nlhlkgdktw1v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_other_caller_metadata
    ADD CONSTRAINT fk_3fo2phxtmkfr5nlhlkgdktw1v FOREIGN KEY (id) REFERENCES public.evsc_caller_metadata(id);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud fk_3kwlpb038de40g5jrhoa83fab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT fk_3kwlpb038de40g5jrhoa83fab FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: evsc_web_validation_service_conf fk_3qns9mki0m85sijfoa52gs1tt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_web_validation_service_conf
    ADD CONSTRAINT fk_3qns9mki0m85sijfoa52gs1tt FOREIGN KEY (id) REFERENCES public.evsc_validation_service_conf(id);


--
-- Name: evsc_processing_evsc_handled_object fk_3v046f0iw1pay396ple7djea1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing_evsc_handled_object
    ADD CONSTRAINT fk_3v046f0iw1pay396ple7djea1 FOREIGN KEY (objects_id) REFERENCES public.evsc_handled_object(id);


--
-- Name: ut_unit_test_log fk_43gcwunrbx9ita0uugls13tx8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_log
    ADD CONSTRAINT fk_43gcwunrbx9ita0uugls13tx8 FOREIGN KEY (unit_test_id) REFERENCES public.ut_unit_test(id);


--
-- Name: evsc_processing fk_4c22p9k4j8dlk3f4mphkve0vs; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing
    ADD CONSTRAINT fk_4c22p9k4j8dlk3f4mphkve0vs FOREIGN KEY (owner_id) REFERENCES public.evsc_owner_metadata(id);


--
-- Name: tf_affinity_domain_aud fk_4ji8wqgg51x3x2k4jgl52y07b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT fk_4ji8wqgg51x3x2k4jgl52y07b FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_expression fk_4ndhx9ggnxgm63ye062iur8eb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_4ndhx9ggnxgm63ye062iur8eb FOREIGN KEY (locator_id) REFERENCES public.xval_locator(id);


--
-- Name: xval_math_member fk_4xbk50fvrv7g64nnefsyepc7w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_4xbk50fvrv7g64nnefsyepc7w FOREIGN KEY (member1_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_date2_format_type fk_564odqlqwsx3wgu5d86pl9wdh; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date2_format_type
    ADD CONSTRAINT fk_564odqlqwsx3wgu5d86pl9wdh FOREIGN KEY (date_format_id) REFERENCES public.xval_date_format_type(id);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_link fk_5lra5ub3x8tyhweuxa4wfv1bi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_5lra5ub3x8tyhweuxa4wfv1bi FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_domain_profile fk_5xrcq9irc0lfqkd1hfwfggh7q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_5xrcq9irc0lfqkd1hfwfggh7q FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_rule_list_criterion fk_65ptid9agnv6s0kkvttoxmtqx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_65ptid9agnv6s0kkvttoxmtqx FOREIGN KEY (tf_rule_criterion_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: xval_expression fk_6n0x2rg3maqbyjgsdqg3bshc2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_6n0x2rg3maqbyjgsdqg3bshc2 FOREIGN KEY (expression_id) REFERENCES public.xval_expression(id);


--
-- Name: tf_hl7_message_profile fk_6ok2phqeqflrttiroorpowqhb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_6ok2phqeqflrttiroorpowqhb FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_integration_profile_type_link_aud fk_6qrcdc6ud9tdwmr0h3vc85un2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT fk_6qrcdc6ud9tdwmr0h3vc85un2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_cross_validator_parent fk_6ugk5r645ycax1vlaxck13cvi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT fk_6ugk5r645ycax1vlaxck13cvi FOREIGN KEY (validator_parent_id) REFERENCES public.xval_cross_validator_reference(id);


--
-- Name: tf_rule_criterion fk_6ybxwerfkie3n814ffvgjghvk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_6ybxwerfkie3n814ffvgjghvk FOREIGN KEY (single_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_profile_link fk_7gn0cpke6kjpbcps2iy70rvxx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_7gn0cpke6kjpbcps2iy70rvxx FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: tf_integration_profile_aud fk_7r7ffh8bhd4yj0gsn6o900j72; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT fk_7r7ffh8bhd4yj0gsn6o900j72 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_list_criterion fk_81shykw7emr3q41anx9ksqdb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_81shykw7emr3q41anx9ksqdb6 FOREIGN KEY (aipocriterions_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: evsc_menu_standard fk_8h8i6mc1re4myebhqaeuxc7b0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_menu_standard
    ADD CONSTRAINT fk_8h8i6mc1re4myebhqaeuxc7b0 FOREIGN KEY (menu_group_id) REFERENCES public.evsc_menu_group(id);


--
-- Name: tf_audit_message fk_8i38nhkvxfbynrf6n14x74sni; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_8i38nhkvxfbynrf6n14x74sni FOREIGN KEY (audited_transaction) REFERENCES public.tf_transaction(id);


--
-- Name: xval_logical_operation_expression_set fk_8u31doqfmu66majudya14xlsf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT fk_8u31doqfmu66majudya14xlsf FOREIGN KEY (xval_expression_id) REFERENCES public.xval_expression(id);


--
-- Name: tf_transaction fk_8u4sa8axnsn6v8jy23skc3qty; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_8u4sa8axnsn6v8jy23skc3qty FOREIGN KEY (transaction_status_type_id) REFERENCES public.tf_transaction_status_type(id);


--
-- Name: tf_profile_link fk_92x366ed20bldob29khteawg4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_92x366ed20bldob29khteawg4 FOREIGN KEY (transaction_option_type_id) REFERENCES public.tf_transaction_option_type(id);


--
-- Name: tf_hl7_message_profile fk_9by8jwr94eeb0bme1bsql5hdb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_9by8jwr94eeb0bme1bsql5hdb FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_rule fk_9cynyuydbfmqhresmj8n1uuqo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_9cynyuydbfmqhresmj8n1uuqo FOREIGN KEY (cause_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: mca_zip_file_children fk_9ecec0nvluujpiu0fxra0hb9y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_zip_file_children
    ADD CONSTRAINT fk_9ecec0nvluujpiu0fxra0hb9y FOREIGN KEY (zip_structure_id) REFERENCES public.mca_zip_config(id);


--
-- Name: xval_log_id fk_9f26kefa9c0svy5i1sdixnrsg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_log_id
    ADD CONSTRAINT fk_9f26kefa9c0svy5i1sdixnrsg FOREIGN KEY (log_id) REFERENCES public.xval_cross_validation_log(id);


--
-- Name: mca_folder_file_children fk_9fs08fodl6ygjud0uam6iv5bc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_folder_file_children
    ADD CONSTRAINT fk_9fs08fodl6ygjud0uam6iv5bc FOREIGN KEY (folder_id) REFERENCES public.mca_folder_config(id);


--
-- Name: tf_transaction_link_aud fk_9gvvnh0aqrll2mp1u89a5msy0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT fk_9gvvnh0aqrll2mp1u89a5msy0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: evsc_validation fk_9xeuvvgn3nhlrh9uwh0icyovp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation
    ADD CONSTRAINT fk_9xeuvvgn3nhlrh9uwh0icyovp FOREIGN KEY (validation_report_id) REFERENCES public.evsc_report(id);


--
-- Name: tf_rule_criterion fk_ac8qpll9fw2bdtemmbkeos5pd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_ac8qpll9fw2bdtemmbkeos5pd FOREIGN KEY (single_integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_aud fk_ae8odst29dbdua80lcyqyckjp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT fk_ae8odst29dbdua80lcyqyckjp FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_link fk_apnhy27aykxqfd0exsrxlnns7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_apnhy27aykxqfd0exsrxlnns7 FOREIGN KEY (from_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_transaction_standard fk_atvilp88b07p05km224t7n5l4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_atvilp88b07p05km224t7n5l4 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: xval_logical_operation_expression_set fk_ax6ugs1gj1apnnfv5fwny0p3r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_logical_operation_expression_set
    ADD CONSTRAINT fk_ax6ugs1gj1apnnfv5fwny0p3r FOREIGN KEY (expressionset_id) REFERENCES public.xval_expression(id);


--
-- Name: tf_integration_profile_type_link fk_b1w97il0injkp7gasy79nqj2q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_b1w97il0injkp7gasy79nqj2q FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: xval_rule fk_ba4yvkhb4yutvi5wvkgq6jd8o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT fk_ba4yvkhb4yutvi5wvkgq6jd8o FOREIGN KEY (expression_id) REFERENCES public.xval_expression(id);


--
-- Name: tf_document_aud fk_c8up7729eltevk3c03jfnxd8f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT fk_c8up7729eltevk3c03jfnxd8f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_criterion fk_cguvfq0yml0ppij05b9549s2e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_cguvfq0yml0ppij05b9549s2e FOREIGN KEY (single_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_integration_profile_option_aud fk_cju6k73ktqmuwinim0g8h12e0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT fk_cju6k73ktqmuwinim0g8h12e0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_expression fk_co24xyo1qpv11tqen99vfjglx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_co24xyo1qpv11tqen99vfjglx FOREIGN KEY (left_member_id) REFERENCES public.xval_math_member(id);


--
-- Name: evsc_validation_report_type fk_cr4a7bvwes4gjprh3glig8v0p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_report_type
    ADD CONSTRAINT fk_cr4a7bvwes4gjprh3glig8v0p FOREIGN KEY (stylesheet_id) REFERENCES public.evsc_stylesheet(id);


--
-- Name: mca_analysis fk_csh836a8yepewvvoh92m1mcrb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis
    ADD CONSTRAINT fk_csh836a8yepewvvoh92m1mcrb FOREIGN KEY (id) REFERENCES public.evsc_processing(id);


--
-- Name: xval_date2_format_type fk_cubkoqik626cfhn9xrdlutud0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date2_format_type
    ADD CONSTRAINT fk_cubkoqik626cfhn9xrdlutud0 FOREIGN KEY (date_comparison_id) REFERENCES public.xval_expression(id);


--
-- Name: tf_domain_aud fk_d3mp0skbxicng7w2tdumqnuel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT fk_d3mp0skbxicng7w2tdumqnuel FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: mca_analysis_part fk_d5rjhud0w2p6mxpacg0nhhjih; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis_part
    ADD CONSTRAINT fk_d5rjhud0w2p6mxpacg0nhhjih FOREIGN KEY (parent_part_id) REFERENCES public.mca_analysis_part(id);


--
-- Name: tf_actor_aud fk_d66h3bo446ibhr2oql90bmq9h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT fk_d66h3bo446ibhr2oql90bmq9h FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_expression fk_dbdek6sy168h9xucgdwmuphnp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_dbdek6sy168h9xucgdwmuphnp FOREIGN KEY (locator_2_id) REFERENCES public.xval_locator(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: evsc_validation fk_dprbj89w4uforrfamslj5s2kt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation
    ADD CONSTRAINT fk_dprbj89w4uforrfamslj5s2kt FOREIGN KEY (id) REFERENCES public.evsc_processing(id);


--
-- Name: tf_domain_profile fk_du7myrlv6npj0053efkqu9rpg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_du7myrlv6npj0053efkqu9rpg FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_link fk_dy6dg8etr367irrt8xqoh3q9g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_dy6dg8etr367irrt8xqoh3q9g FOREIGN KEY (to_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: xval_validator_configuration fk_e11p1i3ndovl9eknuxh9vwj2h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration
    ADD CONSTRAINT fk_e11p1i3ndovl9eknuxh9vwj2h FOREIGN KEY (validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: tf_integration_profile_type_link fk_e1ifbmgpje5wcfsg34tuvvq83; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_e1ifbmgpje5wcfsg34tuvvq83 FOREIGN KEY (integration_profile_type_id) REFERENCES public.tf_integration_profile_type(id);


--
-- Name: evsc_web_extension_service_conf fk_e4flec41r2kxu1p1w0p3qbd2n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_web_extension_service_conf
    ADD CONSTRAINT fk_e4flec41r2kxu1p1w0p3qbd2n FOREIGN KEY (id) REFERENCES public.evsc_extension_service_conf(id);


--
-- Name: evsc_validation_evsc_referenced_standard_ref fk_e4yedt9c4nmpwhrfeq5ea6bat; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_evsc_referenced_standard_ref
    ADD CONSTRAINT fk_e4yedt9c4nmpwhrfeq5ea6bat FOREIGN KEY (referencedstandards_id) REFERENCES public.evsc_referenced_standard_ref(id);


--
-- Name: xval_math_member fk_epaka7b4vh8da52bquik69x45; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_epaka7b4vh8da52bquik69x45 FOREIGN KEY (member2_id) REFERENCES public.xval_math_member(id);


--
-- Name: xval_expression fk_evtx49btrdn5cl3kixbhu8uc4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_evtx49btrdn5cl3kixbhu8uc4 FOREIGN KEY (locator_1_id) REFERENCES public.xval_locator(id);


--
-- Name: evsc_validation_evsc_referenced_standard_ref fk_f1wnoo004qivmlnkdqo9r02s2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_evsc_referenced_standard_ref
    ADD CONSTRAINT fk_f1wnoo004qivmlnkdqo9r02s2 FOREIGN KEY (evsc_validation_id) REFERENCES public.evsc_validation(id);


--
-- Name: mca_starts_with fk_fb8rqwmdaokh39qmlu62r8xv3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_starts_with
    ADD CONSTRAINT fk_fb8rqwmdaokh39qmlu62r8xv3 FOREIGN KEY (content_analysis_config_id) REFERENCES public.mca_content_analysis_config(id);


--
-- Name: tf_actor_integration_profile_aud fk_fffhdfnac6f1ry2ijq1jsehya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT fk_fffhdfnac6f1ry2ijq1jsehya FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: evsc_named_stylesheet fk_fvv052hurilhmb8m2jgdntrni; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_named_stylesheet
    ADD CONSTRAINT fk_fvv052hurilhmb8m2jgdntrni FOREIGN KEY (id) REFERENCES public.evsc_stylesheet(id);


--
-- Name: tf_standard_aud fk_g07iv8pl5kcogd8i5e0vp5dpc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT fk_g07iv8pl5kcogd8i5e0vp5dpc FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_profile_link fk_gfv3euo4c19tpch111m3859t2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_gfv3euo4c19tpch111m3859t2 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_hl7_message_profile fk_gmenyb86ceqff9ujrud4p4elf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_gmenyb86ceqff9ujrud4p4elf FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: ut_unit_test_file_log fk_h9i6r1hbkmv4f854l0kc0suya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT fk_h9i6r1hbkmv4f854l0kc0suya FOREIGN KEY (log_id) REFERENCES public.ut_unit_test_log(id);


--
-- Name: xval_expression fk_hqbt43wnfcr9rqd8eglo4b3lk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_hqbt43wnfcr9rqd8eglo4b3lk FOREIGN KEY (xpath_id) REFERENCES public.xval_locator(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: evsc_scorecard_service_conf fk_ii13oh1ck5lvwqt07g755b4o5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_scorecard_service_conf
    ADD CONSTRAINT fk_ii13oh1ck5lvwqt07g755b4o5 FOREIGN KEY (id) REFERENCES public.evsc_web_extension_service_conf(id);


--
-- Name: mca_zip_folder_children fk_ipr833xvcyl6bp5vhwl26gio3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_zip_folder_children
    ADD CONSTRAINT fk_ipr833xvcyl6bp5vhwl26gio3 FOREIGN KEY (folder_id) REFERENCES public.mca_folder_config(id);


--
-- Name: tf_document_sections fk_iqelub40m4bwli6jc4p1gahw0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT fk_iqelub40m4bwli6jc4p1gahw0 FOREIGN KEY (document_id) REFERENCES public.tf_document(id);


--
-- Name: mca_analysis_part_namespaces fk_ir6h8j1gt6e84uatdtp6dmc9g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis_part_namespaces
    ADD CONSTRAINT fk_ir6h8j1gt6e84uatdtp6dmc9g FOREIGN KEY (analysis_part_id) REFERENCES public.mca_analysis_part(id);


--
-- Name: tf_rule_criterion fk_j241r3eqgmslvierka38b8g0n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_j241r3eqgmslvierka38b8g0n FOREIGN KEY (aiporule_id) REFERENCES public.tf_rule(id);


--
-- Name: evsc_processing fk_j5n2vv0qietiyg3cfj264jq5q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing
    ADD CONSTRAINT fk_j5n2vv0qietiyg3cfj264jq5q FOREIGN KEY (caller_id) REFERENCES public.evsc_caller_metadata(id);


--
-- Name: tf_domain_profile_aud fk_j86yg7l48lurifx013ijn01hj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT fk_j86yg7l48lurifx013ijn01hj FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_option_type_aud fk_j92xo8oyqun96s7ct4dxoqj21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT fk_j92xo8oyqun96s7ct4dxoqj21 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: mca_zip_folder_children fk_joq2j3vrueso1ue7p0cj7d96t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_zip_folder_children
    ADD CONSTRAINT fk_joq2j3vrueso1ue7p0cj7d96t FOREIGN KEY (zip_structure_id) REFERENCES public.mca_zip_config(id);


--
-- Name: tf_document fk_kghw5fo7w5wbq5ayl4bo1hwok; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT fk_kghw5fo7w5wbq5ayl4bo1hwok FOREIGN KEY (document_domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_audit_message fk_kqshjsm4nxkadcwjw3d3csiny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_kqshjsm4nxkadcwjw3d3csiny FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: evsc_processing_evsc_handled_object fk_ktnfs1c0io45dooyrkwd610jj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing_evsc_handled_object
    ADD CONSTRAINT fk_ktnfs1c0io45dooyrkwd610jj FOREIGN KEY (evsc_processing_id) REFERENCES public.evsc_processing(id);


--
-- Name: tf_integration_profile_status_type_aud fk_kuduyri7glq93006crkrnufkw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT fk_kuduyri7glq93006crkrnufkw FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: evsc_conf_composition fk_l0flfv3goi5237dfuwccv2nkk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_conf_composition
    ADD CONSTRAINT fk_l0flfv3goi5237dfuwccv2nkk FOREIGN KEY (component_id) REFERENCES public.evsc_validation_service_conf(id);


--
-- Name: tf_actor_integration_profile_option fk_l1fdaa86cbvmpqmp1q3f0hwcm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_l1fdaa86cbvmpqmp1q3f0hwcm FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- Name: tf_actor_integration_profile_option_aud fk_l3r8wknk5xpfwpagfodbrevi2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT fk_l3r8wknk5xpfwpagfodbrevi2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_expression fk_me8smxc0qwetrec75evhd3ols; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_expression
    ADD CONSTRAINT fk_me8smxc0qwetrec75evhd3ols FOREIGN KEY (expression_set_id) REFERENCES public.xval_expression(id);


--
-- Name: ut_unit_test_file fk_mfwl6smcrvm48qietgxg2dpjf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file
    ADD CONSTRAINT fk_mfwl6smcrvm48qietgxg2dpjf FOREIGN KEY (unit_test_id) REFERENCES public.ut_unit_test(id);


--
-- Name: xval_date1_format_type fk_mka3npe94ucgqba3237sp6v78; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date1_format_type
    ADD CONSTRAINT fk_mka3npe94ucgqba3237sp6v78 FOREIGN KEY (date_comparison_id) REFERENCES public.xval_expression(id);


--
-- Name: xval_date1_format_type fk_n0csssvrei26pt53ekrqcrfq7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_date1_format_type
    ADD CONSTRAINT fk_n0csssvrei26pt53ekrqcrfq7 FOREIGN KEY (date_format_id) REFERENCES public.xval_date_format_type(id);


--
-- Name: ut_unit_test_file_log fk_n0piwt1fuuhu3fublnfe7s5r8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.ut_unit_test_file_log
    ADD CONSTRAINT fk_n0piwt1fuuhu3fublnfe7s5r8 FOREIGN KEY (file_id) REFERENCES public.ut_unit_test_file(id);


--
-- Name: mca_folder_folder_children fk_n2h5wnqf3cxvxojfoice15tv7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_folder_folder_children
    ADD CONSTRAINT fk_n2h5wnqf3cxvxojfoice15tv7 FOREIGN KEY (folder_child_id) REFERENCES public.mca_folder_config(id);


--
-- Name: tf_transaction fk_nmsbssdcd0ll5g9dvehic1cdg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_nmsbssdcd0ll5g9dvehic1cdg FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: evsc_conf_composition fk_nnkiyfjpw52tvv0nui4ry8k1g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_conf_composition
    ADD CONSTRAINT fk_nnkiyfjpw52tvv0nui4ry8k1g FOREIGN KEY (composed_id) REFERENCES public.evsc_referenced_standard(id);


--
-- Name: tf_integration_profile_type_aud fk_np3xu3ya9brlrhx2jxadk1gl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT fk_np3xu3ya9brlrhx2jxadk1gl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: inputs_with_file_keywords_ut fk_opsx8cuptuagh2hvbfagq3yla; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.inputs_with_file_keywords_ut
    ADD CONSTRAINT fk_opsx8cuptuagh2hvbfagq3yla FOREIGN KEY (tested_rule_id) REFERENCES public.ut_unit_test(id);


--
-- Name: xval_math_member fk_otlt6gkwq94wrf8lgvwwutvrp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_math_member
    ADD CONSTRAINT fk_otlt6gkwq94wrf8lgvwwutvrp FOREIGN KEY (locator_id) REFERENCES public.xval_locator(id);


--
-- Name: evsc_system_validation_service_conf fk_owiv7c0ge9vyejvugyh93xn64; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_system_validation_service_conf
    ADD CONSTRAINT fk_owiv7c0ge9vyejvugyh93xn64 FOREIGN KEY (id) REFERENCES public.evsc_validation_service_conf(id);


--
-- Name: evsc_named_stylesheet fk_p01m8y69q9j4rbv1mrfnic12u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_named_stylesheet
    ADD CONSTRAINT fk_p01m8y69q9j4rbv1mrfnic12u FOREIGN KEY (referenced_standard_id) REFERENCES public.evsc_referenced_standard(id);


--
-- Name: evsc_validation_scorecard fk_p8uvuhy8ne9rxtmfw7kf494s9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_scorecard
    ADD CONSTRAINT fk_p8uvuhy8ne9rxtmfw7kf494s9 FOREIGN KEY (id) REFERENCES public.evsc_validation_extension(id);


--
-- Name: xval_assertion_rule fk_pc9wo48sbq9iadwfwqv42e402; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT fk_pc9wo48sbq9iadwfwqv42e402 FOREIGN KEY (rule_id) REFERENCES public.xval_rule(id);


--
-- Name: xval_validator_input fk_pcryas065fb7ckp4ptaxgy62h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT fk_pcryas065fb7ckp4ptaxgy62h FOREIGN KEY (referenced_object_id) REFERENCES public.xval_referenced_object(id);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_pfco9j3tuok8p2pxb7jrqh47y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_pfco9j3tuok8p2pxb7jrqh47y FOREIGN KEY (hl7_message_profile_id) REFERENCES public.tf_hl7_message_profile(id);


--
-- Name: evsc_validation_digital_signature fk_pgntid6j1vvh51aq2n0ir6sy7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_digital_signature
    ADD CONSTRAINT fk_pgntid6j1vvh51aq2n0ir6sy7 FOREIGN KEY (id) REFERENCES public.evsc_validation_extension(id);


--
-- Name: xval_uploaded_file_files fk_pk86umabhm525rag7057alkqm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_uploaded_file_files
    ADD CONSTRAINT fk_pk86umabhm525rag7057alkqm FOREIGN KEY (xval_uploaded_file_id) REFERENCES public.xval_uploaded_file(id);


--
-- Name: xval_validator_configuration_namespace fk_pmuaetod2utrbeaujd2qi3de3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_configuration_namespace
    ADD CONSTRAINT fk_pmuaetod2utrbeaujd2qi3de3 FOREIGN KEY (configuration_id) REFERENCES public.xval_validator_configuration(id);


--
-- Name: evsc_proxy_caller_metadata fk_pqka4x74gc7bs8ilu514krj4s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_proxy_caller_metadata
    ADD CONSTRAINT fk_pqka4x74gc7bs8ilu514krj4s FOREIGN KEY (id) REFERENCES public.evsc_other_caller_metadata(id);


--
-- Name: evsc_validation_extension fk_qdfwx53ur9vdac4vqr4kmo4y8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT fk_qdfwx53ur9vdac4vqr4kmo4y8 FOREIGN KEY (extension_report_id) REFERENCES public.evsc_report(id);


--
-- Name: mca_folder_folder_children fk_qoes87cfh73iyclc2ruqvqiui; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_folder_folder_children
    ADD CONSTRAINT fk_qoes87cfh73iyclc2ruqvqiui FOREIGN KEY (folder_id) REFERENCES public.mca_folder_config(id);


--
-- Name: tf_hl7_message_profile_aud fk_qx4njevahh67dplugi2iqgn9f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT fk_qx4njevahh67dplugi2iqgn9f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage_aud fk_qxy2usxp9p277j5m1gyjgrlxv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT fk_qxy2usxp9p277j5m1gyjgrlxv FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: evsc_processing fk_qy2k1jynm5xpsq8txckp3tp1y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_processing
    ADD CONSTRAINT fk_qy2k1jynm5xpsq8txckp3tp1y FOREIGN KEY (sharing_id) REFERENCES public.evsc_sharing_metadata(id);


--
-- Name: tf_document_sections_aud fk_rfgwvh14e22kxi49wt8rtus68; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT fk_rfgwvh14e22kxi49wt8rtus68 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: xval_rule fk_rl2kqdy60m3vqrvg3xk6pxe4r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_rule
    ADD CONSTRAINT fk_rl2kqdy60m3vqrvg3xk6pxe4r FOREIGN KEY (gazelle_x_validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: tf_transaction_standard fk_ru5fqa00qtwrerdt1oqqsjlv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_ru5fqa00qtwrerdt1oqqsjlv4 FOREIGN KEY (standard_id) REFERENCES public.tf_standard(id);


--
-- Name: xval_cross_validator_parent fk_s25p6c28veqf8ht3x0tjjnl7t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_cross_validator_parent
    ADD CONSTRAINT fk_s25p6c28veqf8ht3x0tjjnl7t FOREIGN KEY (validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: tf_integration_profile fk_s62lad7mgha6mk7y8d1j4ww6a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_s62lad7mgha6mk7y8d1j4ww6a FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: evsc_validation_extension fk_s9jty93uscbkopxei2idfdmvu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT fk_s9jty93uscbkopxei2idfdmvu FOREIGN KEY (id) REFERENCES public.evsc_processing(id);


--
-- Name: mca_analysis_part fk_sloylh0sp1x3hukmoinwijrsj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis_part
    ADD CONSTRAINT fk_sloylh0sp1x3hukmoinwijrsj FOREIGN KEY (xvalmatch_id) REFERENCES public.mca_x_validation_matching(id);


--
-- Name: evsc_digital_signature_service_conf fk_t0f0erds6a1q7j55dyh0td70i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_digital_signature_service_conf
    ADD CONSTRAINT fk_t0f0erds6a1q7j55dyh0td70i FOREIGN KEY (id) REFERENCES public.evsc_web_extension_service_conf(id);


--
-- Name: xval_assertion_rule fk_t0pmq9dirdqx12fs3ywkgvxel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_assertion_rule
    ADD CONSTRAINT fk_t0pmq9dirdqx12fs3ywkgvxel FOREIGN KEY (assertion_id) REFERENCES public.xval_assertion(id);


--
-- Name: xval_validator_input fk_ta15jmuhsritupgp2u7xfwvl9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xval_validator_input
    ADD CONSTRAINT fk_ta15jmuhsritupgp2u7xfwvl9 FOREIGN KEY (gazelle_cross_validator_id) REFERENCES public.xval_cross_validator(id);


--
-- Name: mca_analysis fk_tb2i2ex0rww3nfpkdq6qqpm88; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mca_analysis
    ADD CONSTRAINT fk_tb2i2ex0rww3nfpkdq6qqpm88 FOREIGN KEY (root_analysis_part_id) REFERENCES public.mca_analysis_part(id);


--
-- Name: tf_profile_link_aud fk_tbg8e3v0117w55eah8dspnd4f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT fk_tbg8e3v0117w55eah8dspnd4f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_status_type_aud fk_to2vmdf96j8l3ek5jv1sf1qul; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT fk_to2vmdf96j8l3ek5jv1sf1qul FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage fk_tqey90ukaddb48sit5ikipdps; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT fk_tqey90ukaddb48sit5ikipdps FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_audit_message fk_ululbp46ig0t093pgkr38y6h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_ululbp46ig0t093pgkr38y6h FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- Name: evsc_validation_service_conf fk_wvsd9rqhct588vlmb7of8t32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.evsc_validation_service_conf
    ADD CONSTRAINT fk_wvsd9rqhct588vlmb7of8t32 FOREIGN KEY (validationreporttype_id) REFERENCES public.evsc_validation_report_type(id);


--
-- PostgreSQL database dump complete
--

