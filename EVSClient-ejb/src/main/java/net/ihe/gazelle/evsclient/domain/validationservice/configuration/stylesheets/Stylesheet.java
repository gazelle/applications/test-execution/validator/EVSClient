package net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "evsc_stylesheet", schema = "public")
@SequenceGenerator(name = "evsc_stylesheet_sequence", sequenceName = "evsc_stylesheet_id_seq", allocationSize = 1)
public class Stylesheet implements Serializable {

    private static final long serialVersionUID = 8920000396165771216L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evsc_stylesheet_sequence")
    private Integer id;

    private String location; // (file path or url)

    @Transient
    private byte[] content;

    public Stylesheet() {
    }

    public Stylesheet(String location) {
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stylesheet that = (Stylesheet) o;
        return Objects.equals(location, that.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }
}
