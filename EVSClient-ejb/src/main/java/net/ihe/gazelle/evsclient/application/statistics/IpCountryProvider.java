package net.ihe.gazelle.evsclient.application.statistics;

public interface IpCountryProvider {
    String findCountry(String ipAddress) throws LocalizationException;
}
