package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;


import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationDTO;
import net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public interface ValidationsApiService {
      Response createValidation(ValidationDTO validation, String authorization, String prefer,
                                SecurityContext securityContext, HttpServletRequest request)
      throws NotFoundException;
      Response getValidationByOid(String oid, String privacyKey, String authorization, String accept, SecurityContext securityContext, HttpServletRequest request)
      throws NotFoundException;
      Response getValidationReportByOid(String oid, String privacyKey, String severityThreshold, String authorization, String accept, SecurityContext securityContext, HttpServletRequest request)
      throws NotFoundException;

      Response getValidationProfiles();

      Response getValidationProfilesByServiceName(String serviceName);
}
