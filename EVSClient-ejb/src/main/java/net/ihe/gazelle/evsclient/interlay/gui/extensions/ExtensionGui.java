package net.ihe.gazelle.evsclient.interlay.gui.extensions;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationExtensionServiceFacade;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.extension.ExtensionStatus;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ExtensionServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.WebExtensionServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.MbvReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationReportDTO;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.document.InvalidStylesheetException;
import net.ihe.gazelle.evsclient.interlay.gui.document.ReportConverter;
import net.ihe.gazelle.evsclient.interlay.gui.processing.AbstractProcessingBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;
import net.ihe.gazelle.evsclient.interlay.gui.validation.Reporter;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ExtensionGui<E extends ValidationExtension, C extends ExtensionServiceConf<E,?,C,?>> extends AbstractProcessingBeanGui<ValidationExtension, ValidationExtensionServiceFacade> implements Serializable {
    private static final long serialVersionUID = 1;
    private static Logger log = LoggerFactory.getLogger(ExtensionGui.class);

    public static <T extends ValidationExtension> Class<T> getExtensionClass(Class<? extends ExtensionServiceConf<T,?,?,?>> cls) {
        Class<T> c = (Class<T>)extensions.get(cls);
        if (c==null) {
            c=(Class<T>)TypeUtils.getFirstTypeArgument(WebExtensionServiceConf.class, (Class<? extends WebExtensionServiceConf>) cls);
            extensions.put(cls,c);
        }
        return c;
    }
    private static Map<Class<?>,Class<?>> extensions = new THashMap<>();

    private Class<E> extensionClass;
    private Class<C> extensionConfClass;
    private E extension;
    private boolean pollExtension = false;
    private String errorMessage = null;
    private ValidationServiceFacade validationServiceFacade;
    private boolean async = true;
    private WeakHashMap<Validation,ExtensionServiceConf<?, ?, ?, ?>> econf = new WeakHashMap<Validation, ExtensionServiceConf<?, ?, ?, ?>>();
    private Validation validation;
    private Reporter reporter;

    public ExtensionGui(Class<C> extensionConfClass, ValidationServiceFacade validationServiceFacade, CallerMetadataFactory callerMetadataFactory, GazelleIdentity identity, ApplicationPreferenceManager applicationPreferenceManager) {
        super(getExtensionClass(extensionConfClass), validationServiceFacade.getValidationExtensionServiceFacade(), callerMetadataFactory, identity, applicationPreferenceManager, null);
        this.validationServiceFacade = validationServiceFacade;
        this.extensionConfClass = extensionConfClass;
        this.extensionClass = (Class<E>) cls;
        this.reporter = new Reporter(labelizer,extensionClass);

    }

    public <C extends ExtensionServiceConf<?,?,?,?>> boolean isExtensionAvailable(Validation validation) {
        try {
            this.validation = validation;
            return getExtensionServiceConf().isAvailable();
        } catch (Exception e) {
            return false;
        }
    }

    private ExtensionServiceConf<?, ?, ?, ?> getExtensionServiceConf() throws net.ihe.gazelle.evsclient.domain.validationservice.configuration.ServiceConfException, net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException {
        ExtensionServiceConf<?, ?, ?, ?> c = econf.get(validation);
        if (c==null) {
            c = validationServiceFacade.getValidationExtensionServiceFacade().getExtensionConfiguration(extensionConfClass, validation);
            econf.put(validation,c);
        }
        return c;
    }


    public void pollExtensionStatus(){
        if (extension != null) {
            pollExtension = true;
            checkExtensionStatus(extension.getExtensionStatus());
        }
    }

    public boolean isExtensionExists(){
        if (extension ==null) {
            try {
                this.extension = processingManager.computeExtension(validation, getEvsCallerMetadata(), getOwnerMetadata(), extensionClass);
            } catch (Exception e) {
                setErrorMessage(I18n.get("net.ihe.gazelle.evs.unable-to-get-"+Labelizer.kebabCase(extensionClass),e.getMessage()));
                GuiMessage.logMessage(StatusMessage.Severity.ERROR,errorMessage,e);
            }
            pollExtensionStatus();
        }
        return (extension != null && ExtensionStatus.AVAILABLE.equals(extension.getExtensionStatus()));
    }

    private void checkExtensionStatus(ExtensionStatus status) {
        if (pollExtension) {
            log.info(Labelizer.kebabCase(extensionClass)+" status: {}", extension.getExtensionStatus());
            switch (status == null ? ExtensionStatus.NOT_AVAILABLE : status) {
                case AVAILABLE:
                    this.pollExtension =false;
                    setErrorMessage(null);
                    if (this.extension!=null
                            && this.extension.getExtensionReport()!=null
                            && this.reporter.getValidationReport()==null

                    ) {
                        try {
                            ValidationReport report = new MbvReportMapper()
                                    .transform(extension.getExtensionReport().getContent());
                            this.reporter.setValidationReport(new ValidationReportDTO(report, SeverityLevel.INFO));
                        } catch (ReportTransformationException e) {
                            setErrorMessage(I18n.get("net.ihe.gazelle.evs.invalid-report", e.getMessage()));
                            GuiMessage.logMessage(StatusMessage.Severity.ERROR, errorMessage);
                        }
                    }
                    break;
                case IN_PROGRESS:
                    processPendingStatus();
                    break;
                case NOT_AVAILABLE:
                    setErrorMessage(I18n.get("net.ihe.gazelle.evs."+Labelizer.kebabCase(extensionClass)+"-not-available"));
                    pollExtension = false;
                    break;
                case FAILED_ON_SERVER_SIDE:
                    setErrorMessage(I18n.get("net.ihe.gazelle.evs."+Labelizer.kebabCase(extensionClass)+"-computing-error"));
                    pollExtension = false;
                    break;
                case FAILED_ON_CLIENT_SIDE:
                    setErrorMessage(I18n.get("net.ihe.gazelle.evs."+Labelizer.kebabCase(extensionClass)+"-storing-error"));
                    pollExtension = false;
                    break;
                case COMPUTE_AGAIN:
                case NEVER_COMPUTED:
                    buildExtension();
                    break;
                default:
                    // Is is possible ?
            }
        }
    }

    private void processPendingStatus() {
        try {
            extension = processingManager.refresh(extension);
            if (extension != null && extension.getExtensionStatus().equals(ExtensionStatus.IN_PROGRESS)) {
                setErrorMessage(I18n.get("net.ihe.gazelle.evs."+Labelizer.kebabCase(extensionClass)+"-computation"));
            } else {
                // status changed : relaunch polling result
                checkExtensionStatus(extension == null ? null : extension.getExtensionStatus());
            }
        } catch (Exception e) {
            extension.setExtensionStatus(ExtensionStatus.FAILED_ON_SERVER_SIDE);
            processingManager.merge(extension);
            // status changed to error : relaunch polling result
            checkExtensionStatus(null);
        }
    }




    private void buildExtension() {
        try {
            setErrorMessage(I18n.get("net.ihe.gazelle.evs."+Labelizer.kebabCase(extensionClass)+"-computation"));
            processingManager.executeExtensionJob(validation, extension,this.async);
            extension = processingManager.refresh(extension);
        } catch (UnexpectedProcessingException e) {
            setErrorMessage(I18n.get("net.ihe.gazelle.evs.unable-to-build-"+Labelizer.kebabCase(extensionClass)+"", e.getMessage()));
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,errorMessage,e);
        }

    }

    public boolean isPollExtension() {
        return pollExtension || extension ==null;
    }


    public void computeAgain() {
        if (extension ==null) {
            try {
                this.extension = processingManager.computeExtension(validation, getEvsCallerMetadata(), getOwnerMetadata(), extensionClass);
            } catch (UnexpectedProcessingException e) {
                log.warn("failed to get extension to compute again.");
            }
        }
        if (extension !=null) {
            extension.setExtensionStatus(ExtensionStatus.COMPUTE_AGAIN);
            extension = extensionClass.cast(processingManager.merge(extension));
        }
        pollExtension = true;
        buildExtension();
    }

    public void downloadExtensionReport() {
        ReportExporterManager.exportToFile("application/xml",
                extension.getExtensionReport().getContent(), extension.getId()+"-"+Labelizer.kebabCase(extensionClass)+".xml");
    }

    public boolean isExtensionInError() {
        return (extension == null || (ExtensionStatus.FAILED_ON_SERVER_SIDE.equals(extension.getExtensionStatus())));
    }

    public boolean showErrorMessage() {
        log.info("show-"+Labelizer.kebabCase(extensionClass)+"-message {}",StringUtils.isNotEmpty(errorMessage));
        return StringUtils.isNotEmpty(errorMessage);
    }
    public String getErrorMessage() {
        log.info("get-"+Labelizer.kebabCase(extensionClass)+"-message {}",errorMessage);
        return errorMessage;
    }
    private void setErrorMessage(String errorMessage) {
        log.info("set-"+Labelizer.kebabCase(extensionClass)+"-message {}",errorMessage);
        this.errorMessage = errorMessage;
    }

    public E getExtension() {
        return extension;
    }

    public Reporter getReporter() {
        return this.reporter;
    }


    public String getHtmlExtensionReport() {
        String xsl = getXslTransformation();
        String html = null;
        try {
            html = ReportConverter.transformXMLStringToHTML(new String(
                    extension.getExtensionReport()
                            .getContent()
                    , StandardCharsets.UTF_8), xsl);
            pollExtension = false;
        } catch (InvalidStylesheetException e) {
            setErrorMessage(I18n.get("net.ihe.gazelle.evs.invalid-stylesheet", e.getMessage()));
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, errorMessage);
        }
        return html;
    }

    public String getXslTransformation() {
        return PreferenceService.getString(Labelizer.snakeCase(extensionClass)+"_xsl_path");
    }

    public String getLabel() {
        return reporter.getLabel();
    }

    public String getId() {
        return reporter.getId();
    }

    public String getCode() {
        return reporter.getCode();
    }

    public String getLabel(String value, Object ... params) {
        return reporter.getLabel(value,params);
    }

    public String getLabel(String value) {
        return reporter.getLabel(value);
    }

    public String getCode(String value) {
        return reporter.getCode(value);
    }
}
