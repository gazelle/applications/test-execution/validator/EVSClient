/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.gui.Authorizations;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.domain.menu.EVSMenuGroup;
import net.ihe.gazelle.evsclient.domain.menu.EVSMenuGroupQuery;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.menu.XValidationAuthorizations;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EVSMenu {

    private static final Logger LOGGER = LoggerFactory.getLogger(EVSMenu.class);
    private static final String NO_EXTENSION = "None";

    private static MenuGroup MENU;

    public static synchronized Menu getMenu() {
        if (EVSMenu.MENU == null) {
            EVSMenu.MENU = EVSMenu.computeMenu();
        }
        return EVSMenu.MENU;
    }

    public List<String> getMenuNameByReferencedStandard(int referencedStandardId) throws Exception {
        List<String> menus = new ArrayList<>();
        List<Menu> evsMenuBar = getMenu().getItems(); // get menu bar with referenced standard items, addons, etc..
        // iterate over Menu to find referenced standards associated to menu.
        for (Menu menu : evsMenuBar) {
            for (Menu internalMenu : menu.getItems()) { // iterate over items inside menu entries
                if (!internalMenu.getItems().isEmpty()) { // to be sure that this is a validation menu
                    Page page = internalMenu.getItems().get(0).getPage();
                    if(page instanceof PageValidate) { // return only validation pages. ie : get(0) return validation page and is sufficient
                        ReferencedStandard rs = ((PageValidate) page).getStandard();
                        if (rs!=null && rs.getId() == referencedStandardId) {
                            menus.add(menu.getPage().getLabel());
                        }
                    }
                }
            }
        }
        return menus;
    }

    public static void forceUpdate() {
        synchronized (EVSMenu.class) {
            EVSMenu.MENU = EVSMenu.computeMenu();
        }
    }

    private static MenuGroup computeMenu() {
        // all elements
        final ArrayList<Menu> children = new ArrayList<>();

        // Customized menu begins here
        final EVSMenuGroupQuery query = new EVSMenuGroupQuery();
        query.available().eq(Boolean.TRUE);
        query.orderInGui().order(true);
        final List<EVSMenuGroup> menusFromDb = query.getListNullIfEmpty();

        if (menusFromDb != null) {
            for (final EVSMenuGroup group : menusFromDb) {
                final List<Menu> groupItems = new ArrayList<>();
                if (group.getStandards() != null && !group.getStandards().isEmpty()) {
                    Collections.sort(group.getStandards(), EVSMenu.COMPARATOR);
                    for (final ReferencedStandard standard : group.getStandards()) {
                        final List<Menu> standardItems = new ArrayList<>();
                        standardItems.add(new MenuEntry(
                                new PageValidate(standard)));
                        standardItems.add(new MenuEntry(new PageLogs(standard)));
                        EVSMenu.addStatistics(standardItems, standard);
                        if(standard.getValidationType().equals(ValidationType.XVAL)){
                            standardItems.add(new MenuEntry(XValidationPages.DOC_HOME));
                        }
                        EVSMenu.addMenu(groupItems, standardItems, standard.getName(),
                                standard.getIconStyleClass());
                    }
                }
                EVSMenu.addMenu(children, groupItems, group.getLabel(), null);
            }
        }

        // Add-ons menu
        if (PreferenceService.getBoolean("add_ons_enabled")) {
            final List<Menu> addOnsMenu = new ArrayList<>();

            // Validator detector Menu
            final List<Menu> detectorItems = new ArrayList<>();
            EVSMenu.addValidate(detectorItems, "/messageContentAnalyzer.xhtml", "net.ihe.gazelle.mca.MessageContentAnalyzer");
            EVSMenu.addLogs(detectorItems, "/common/messageContentAnalyzerResultLogs.seam",
                    "net.ihe.gazelle.mca.MessageContentAnalyzerResultLogs");

            detectorItems.add(new MenuEntry(Pages.MCA_CONFIG));

            EVSMenu.addMenu(addOnsMenu, detectorItems, "net.ihe.gazelle.mca.MessageContentAnalyzer", "fa fa-barcode");



            // Cross Validator
            final List<Menu> xvalidation = new ArrayList<>();
            if (PreferenceService.getBoolean("add_on_x_validation_enabled")) {
                xvalidation.add(new MenuEntry(XValidationPages.VAL_VALIDATION));
                xvalidation.add(new MenuEntry(XValidationPages.VAL_LOGS));
                EVSMenu.addMenu(addOnsMenu, xvalidation, "net.ihe.gazelle.xval.GazelleCrossValidator", "fa fa-sitemap", Authorizations.CROSS_VALIDATION);
            }


            addOnsMenu.add(new MenuEntry(Pages.SCHEMATRONS));
            addOnsMenu.add(new MenuEntry(Pages.STATISTICS));
            addOnsMenu.add(new MenuEntry(Pages.API_KEY_MANAGEMENT));

            EVSMenu.addMenu(children, addOnsMenu, "gazelle.evs.client.addons.menu", "fa fa-wrench");
        }

        // Administration Menu
        final List<Menu> adminItems = new ArrayList<>();
        adminItems.add(new MenuEntry(Pages.VALIDATION_SERVICES));
        adminItems.add(new MenuEntry(Pages.REFERENCED_STANDARDS));
        adminItems.add(new MenuEntry(Pages.MENU_CONFIG));
        adminItems.add(new MenuEntry(Pages.CALLING_TOOLS));
        adminItems.add(new MenuEntry(Pages.ADMIN_TEMPLATES));
        adminItems.add(new MenuEntry(Pages.CONFIGURATION));

        if (PreferenceService.getBoolean("add_ons_enabled") && Boolean.parseBoolean(PreferenceService.getString("x_validation_editor"))) {
            final List<Menu> xvalidationAdmin = new ArrayList<>();

            xvalidationAdmin.add(new MenuEntry(XValidationPages.ADMIN_IMPORT_VALIDATORS));
            xvalidationAdmin.add(new MenuEntry(XValidationPages.ADMIN_VALIDATORS));

            EVSMenu.addMenu(adminItems, xvalidationAdmin, "net.ihe.gazelle.xval.admin", "fa fa-sitemap", XValidationAuthorizations.TEST_DESIGNER);
        }

        EVSMenu.addMenu(children, adminItems, "gazelle.evs.client.admin.main.menu", "fa fa-television",
                Authorizations.ADMIN);

        return new MenuGroup(null, children);
    }

    /**
     * This comparator is used to sort the statistics by decreasing result
     */
    protected static final Comparator<ReferencedStandard> COMPARATOR = new Comparator<ReferencedStandard>() {

        @Override
        public int compare(final ReferencedStandard o1, final ReferencedStandard o2) {
            return o1.getName().compareTo(o2.getName());
        }

    };

    private static void addValidate(final List<Menu> items, final String url, final String label) {
        items.add(new MenuEntry(new PageValidate(url, label)));
    }

    private static void addLogs(final List<Menu> items, final String link, final String label) {
        items.add(new MenuEntry(new PageLogs(link, label)));
    }


    private static void addStatistics(final List<Menu> items, final ReferencedStandard standard) {
        items.add(new MenuEntry(new PageStatistics(standard)));
    }

    private static void addMenu(final List<Menu> parentItems, final List<Menu> items, final String title, final String img) {
        addMenu(parentItems, items, title, img, Authorizations.IS_LOGGED_IF_NEEDED);
    }

    private static void addMenu(final List<Menu> parentItems, final List<Menu> items, final String title, final String img,
                                final Authorization... authorizations) {
        EVSMenu.LOGGER.debug("adding menu :{} {}", title, img);
        parentItems.add(new MenuGroup(new PageMenu(title, img, authorizations), items));
    }

    public static void appendUrlParameters(StringBuilder builder, String id) {
        if (id != null && !id.isEmpty()) {
            builder.append('?');
            builder.append(QueryParamEvs.REF_STANDARD_ID);
            builder.append('=');
            builder.append(id);
        }
    }
}
