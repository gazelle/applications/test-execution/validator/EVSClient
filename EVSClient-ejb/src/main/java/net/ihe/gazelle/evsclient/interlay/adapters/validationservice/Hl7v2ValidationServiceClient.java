package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.Hl7ReportMapper;
import net.ihe.gazelle.hl7.validator.client.HL7v2Validator;
import net.ihe.gazelle.hl7.validator.client.ValidationContextBuilder;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Hl7MessageProfileQuery;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ValidationProperties(
        name="HL7v2",
        types =ValidationType.HL7V2)
@ReportParser(Hl7ReportMapper.class)
public class Hl7v2ValidationServiceClient extends AbstractValidationServiceClient implements WebValidationService {

    private HL7v2Validator gazelleValidator;
    private Map<String,List<String>> validators;

    public Hl7v2ValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager,configuration);
        this.gazelleValidator = new HL7v2Validator(configuration.getUrl(),this.retrieveDefaultFaultLevels());
        this.validators = new THashMap<>();
    }
    private Map<String, ValidationContextBuilder.FaultLevel> retrieveDefaultFaultLevels() {
        final Map<String, ValidationContextBuilder.FaultLevel> validationOptions = new THashMap<>();
        validationOptions.put(ValidationContextBuilder.LENGTH,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_length_fault_level")));
        validationOptions.put(ValidationContextBuilder.MESSAGE_STRUCTURE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_structure_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATATYPE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_datatype_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATAVALUE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_value_fault_level")));
        return validationOptions;
    }

    @Override
    // TODO
    public String about() {
        return null;
    }

    @Override
    public String getName() {
        return "Gazelle HL7v2 Validation Service";
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList(new String[]{"text/plain"});
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter)  {
        if (!validators.containsKey(validatorFilter)) {
            Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
            query.affinityDomains().labelToDisplay().eq(validatorFilter);
            ArrayList<String> v = new ArrayList<>();
            validators.put(validatorFilter,v);
            for (Hl7MessageProfile p : query.getList()) {
                v.add(p.getProfileOid());
            }
        }
        return validators.get(validatorFilter);
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validatorKeyword) {
        return this.gazelleValidator
                .validate(new String(objects[0].getContent(),StandardCharsets.UTF_8), validatorKeyword, "ER7")
                .getBytes(StandardCharsets.UTF_8);
    }


}
