package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.EmbeddedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_embedded_validation_service_conf", schema = "public")
public class EmbeddedValidationServiceConf extends ValidationServiceConf {
    private static final long serialVersionUID = 4226957458185624647L;

    public EmbeddedValidationServiceConf() {
        super();
    }

    @Override
    public Class<? extends ValidationService> getValidationServiceInterface() {
        return EmbeddedValidationService.class;
    }

}
