package net.ihe.gazelle.evsclient.domain.validation.request;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class ValidationItem {

    private String itemId;

    private byte[] content;

    private String role;

    private String location;

    public ValidationItem() {
    }

    public String getItemId() {
        return itemId;
    }

    public ValidationItem setItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    public byte[] getContent() {
        return content;
    }

    public ValidationItem setContent(byte[] content) {
        this.content = content;
        return this;
    }

    public String getRole() {
        return role;
    }

    public ValidationItem setRole(String role) {
        this.role = role;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public ValidationItem setLocation(String location) {
        this.location = location;
        return this;
    }

    public boolean isContentValid(){
        return content != null && content.length > 0;
    }

    public boolean isItemIdValid() {
        return itemId == null || !itemId.isEmpty();
    }

    public boolean isLocationValid(){
        return location == null || isURLValid(location);
    }

    public boolean isRoleValid(){
        return role == null || !role.isEmpty();
    }

    public boolean isRoleDefined(){
        return role != null && !role.isEmpty();
    }

    public boolean isURLValid(String url) {
        try {
            new URL(url).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
        return true;
    }

    public boolean isValid(){
        return isContentValid() && isItemIdValid() && isLocationValid() && isRoleValid();
    }

}
