package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.security.ApiKeyDao;
import net.ihe.gazelle.evsclient.application.statistics.IpLocDao;
import net.ihe.gazelle.evsclient.application.validation.ExtensionDao;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfDao;
import net.ihe.gazelle.evsclient.interlay.dao.security.ApiKeyDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.statistics.IpLocDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.validation.ExtensionDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.validation.ValidationDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.validationservice.configuration.ValidationServiceConfDaoImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.persistence.EntityManagerFactory;

@Name("evsDaoFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class DaoFactory {


    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @In( value = "entityManagerFactory", create = true )
    private EntityManagerFactory entityManagerFactory;

    @Factory( value = "validationDao")
    public ValidationDao getValidationDao() {
        return new ValidationDaoImpl(entityManagerFactory, applicationPreferenceManager);
    }

    @Factory( value = "serviceConfDao")
    public ValidationServiceConfDao getServiceConfDao() {
        return new ValidationServiceConfDaoImpl(entityManagerFactory);
    }


    @Factory( value = "apiKeyDao")
    public ApiKeyDao getApiKeyDao() {
        return new ApiKeyDaoImpl(entityManagerFactory);
    }

    @Factory( value = "ipLocDao")
    public IpLocDao getIpLocDao() {
        return new IpLocDaoImpl(entityManagerFactory);
    }

    @Factory( value = "extensionDao")
    public ExtensionDao getExtensionDao() {
        return new ExtensionDaoImpl(entityManagerFactory, applicationPreferenceManager);
    }

}
