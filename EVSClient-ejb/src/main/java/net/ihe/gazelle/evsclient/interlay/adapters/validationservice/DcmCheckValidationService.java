package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.MbvReportMapper;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ValidationProperties(
        name="DCM Check",
        types=ValidationType.DICOM)
@ReportParser(PlainReportMapper.class)
public class DcmCheckValidationService extends AbstractSystemValidationService {

    private String command;

    public DcmCheckValidationService(ApplicationPreferenceManager applicationPreferenceManager, SystemValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        this.command = this.executor.getBinaryLocation(configuration.getBinaryPath(),"/opt/offis/dicom/bin");

    }

    @Override
    public String getName() {
        return "dcmcheck";
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter) {
        return Arrays.asList("dcmcheck");
    }

    @Override
    public String about() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(executor.exec("man -f "+command,false));
        } catch (Exception e) {
            sb.append(command);
        }
        return sb.toString();
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/dicom");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            File dcmFile = File.createTempFile("dcmcheck_", ".dcm");
            FileOutputStream outputStream = new FileOutputStream(dcmFile);
            try {
                outputStream.write(objects[0].getContent());
            } finally {
                outputStream.close();
            }
            try {
                String stdout = executor.exec(
                        command +" "+dcmFile.getAbsolutePath(),false);
                ValidationReport report = new DcmcheckReportBuilder().build(
                        this,
                        validator,
                        this.getServiceVersion(),
                        stdout
                );
                return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                        .marshal(report).getBytes(StandardCharsets.UTF_8);
            } finally {
                dcmFile.delete();
            }
        } catch (Exception e) {
            LOGGER.error(String.format("an unexpected error occurred during the validation: %s", e.getMessage()));
            throw new ValidationServiceOperationException(e);
        }
    }

    @Override
    public String getServiceVersion() {
        try {
            String stderr = executor.exec(command + " --version", true);
            return stderr.substring(stderr.indexOf(58) + 2);
        } catch (Exception e) {
            return "";
        }
    }
}