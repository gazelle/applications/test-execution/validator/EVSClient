package net.ihe.gazelle.evsclient.domain.validationservice.extension;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validationservice.ProcessingService;

/**
 * Extension Validation Service API.
 * <p>
 * Interact with a extension validation service.
 * <p>
 *
 * @author pdaigremont
 */
public interface ExtensionService<T extends AbstractProcessing> extends ProcessingService {

   /**
    * Post process a validation. Returns a report of this post-processing.
    * Entries for the process may be any of the validation process attributes : handled objects, report, validation service properties, etc.
    *
    * @param processing   the extension to post-process.
    *
    * @return a report of the process, usually a plain text, XML or JSON structured document but in raw bytes.
    *
    * @throws ExtensionServiceOperationException if the requested process raised an unexpected issue or if the service is unavailable at the time
    *                                             of the call.
    */
   byte[] process(T processing) throws ExtensionServiceOperationException;

   T createExtension(Validation validation, EVSCallerMetadata evsCallerMetadata, OwnerMetadata ownerMetadata);

}
