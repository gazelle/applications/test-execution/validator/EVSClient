package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.SystemValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceType;

public enum SystemValidationServiceTypes implements ValidationServiceType<SystemValidationService> {

    DICOM3TOOLS_SERVICE("Dicom3Tools Check", Dicom3toolsValidationService.class),
    DCMCHECK_SERVICE("DCM Check", DcmCheckValidationService.class);

    private final String value;

    private final Class<? extends SystemValidationService> validationServiceClass;

    private boolean implemented = true;


    SystemValidationServiceTypes(String value, Class<? extends SystemValidationService> validationServiceClass) {
        this.value = value;
        this.validationServiceClass = validationServiceClass;
    }

    SystemValidationServiceTypes(String value,
                                 Class<? extends SystemValidationService> validationServiceClass,
                                 boolean implemented) {
        this(value, validationServiceClass);
        this.implemented = implemented;
    }


    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public Class<? extends SystemValidationService> getValidationServiceClass() {
        return this.validationServiceClass;
    }

    @Override
    public boolean isImplemented() {
        return this.implemented;
    }
}
