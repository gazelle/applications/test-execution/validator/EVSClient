package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import com.gitb.tr.BAR;
import com.gitb.tr.TAR;
import com.gitb.tr.TestAssertionGroupReportsType;
import com.gitb.tr.TestAssertionReportType;
import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class GitbReportMapper implements ValidationReportMapper, OriginalReportParser<TAR> {
    private static String NOT_PROVIDED = "Not provided";

    @Override
    public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
        TAR gitbRapport = parseOriginalReport(originalReport);
        ValidationOverview buildOverview = buildOverview(gitbRapport);
        ValidationReport outputValidationReport = new ValidationReport(UUID.randomUUID().toString(),
                buildOverview);
        if (isSubreportEmpty(gitbRapport)) {
            String subreportName = "Overall Result Report";
            ValidationSubReport subReport = buildValidationSubReport(subreportName);
            subReport.addConstraintValidation(addConstraintValidationForEmptyReport(subreportName));
            outputValidationReport.addSubReport(subReport);
        } else {
            String subreportName = "Report";
            ValidationSubReport subReport;
            subReport = buildValidationSubReport(subreportName);
            outputValidationReport.addSubReport(parseSubReport(subReport, gitbRapport.getReports()));
        }
        return outputValidationReport;
    }

    @Override
    public TAR parseOriginalReport(byte[] originalReport) throws ReportTransformationException {
        if (originalReport != null && originalReport.length > 0) {
            try (InputStream reportInputStream = new ByteArrayInputStream(originalReport)) {
                JAXBContext jaxbContext = JAXBContext.newInstance(TAR.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<TAR> jaxbElement = (JAXBElement<TAR>) unmarshaller.unmarshal(reportInputStream);
                TAR tarObject = jaxbElement.getValue();
                return tarObject;
            } catch (IOException | JAXBException e) {
                throw new ReportTransformationException("unable-to-parse-report", e);
            }
        } else {
            throw new ReportTransformationException("Empty original report");
        }
    }

    private boolean isSubreportEmpty(TAR report) {
        return (report.getCounters().getNrOfAssertions().equals(BigInteger.valueOf(0))
                && report.getCounters().getNrOfErrors().equals(BigInteger.valueOf(0))
                && report.getCounters().getNrOfWarnings().equals(BigInteger.valueOf(0)));
    }

    private ConstraintValidation addConstraintValidationForEmptyReport(String subreportName) {
        return new ConstraintValidation(subreportName
                , ConstraintPriority.MANDATORY, ValidationTestResult.PASSED);
    }

    private static ValidationSubReport buildValidationSubReport(String subreportName) {
        return new ValidationSubReport(subreportName, new ArrayList<String>());
    }

    private ValidationSubReport parseSubReport(ValidationSubReport subReport, TestAssertionGroupReportsType parsedOriginalReport) throws ReportTransformationException {
        if (parsedOriginalReport != null) {
            for (JAXBElement<TestAssertionReportType> assertion : parsedOriginalReport.getInfoOrWarningOrError()) {
                subReport.addConstraintValidation(parseAssertion(assertion));
            }
        }
        return subReport;
    }

    private ConstraintValidation parseAssertion(JAXBElement<TestAssertionReportType> assertion) throws ReportTransformationException {
        if (assertion != null && assertion.getValue() != null) {
            BAR assertionReport = (BAR) assertion.getValue();
            switch (assertion.getName().getLocalPart()) {
                case "error":
                    return buildConstraintValidation(assertionReport, ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
                case "warning":
                    return buildConstraintValidation(assertionReport, ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
                case "info":
                    return buildConstraintValidation(assertionReport, ConstraintPriority.MANDATORY, ValidationTestResult.PASSED);
                default:
                    throw new ReportTransformationException("Unexpected GITB assertion report type:  " + assertion.getName().getLocalPart());
            }
        } else {
            throw new ReportTransformationException("Can not parse null assertion");
        }
    }

    private static ConstraintValidation buildConstraintValidation(BAR assertionReport, ConstraintPriority mandatory, ValidationTestResult failed) {
        ConstraintValidation constraintValidation = new ConstraintValidation(assertionReport.getDescription(), mandatory, failed);
        constraintValidation.setLocationInValidatedObject(assertionReport.getLocation());
        constraintValidation.setFormalExpression(assertionReport.getTest());
        return constraintValidation;
    }

    private ValidationOverview buildOverview(TAR gitbReport) {
        return getOverviewValidationServiceName(gitbReport);
    }

    private ValidationOverview getOverviewValidationServiceName(TAR gitbReport) {
        XMLGregorianCalendar xmlGregorianCalendar = gitbReport.getDate();
        Date date = xmlGregorianCalendar.toGregorianCalendar().getTime();
        ValidationOverview outputValidationReport;
        if (gitbReport.getReports() != null && gitbReport.getOverview() != null) {
            outputValidationReport =
                    new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
                            date,
                            gitbReport.getOverview().getValidationServiceName() != null ? gitbReport.getOverview().getValidationServiceName() : NOT_PROVIDED,
                            gitbReport.getOverview().getValidationServiceVersion() != null ? gitbReport.getOverview().getValidationServiceVersion() : NOT_PROVIDED,
                            gitbReport.getOverview().getProfileID() != null ? gitbReport.getOverview().getProfileID() : NOT_PROVIDED,
                            gitbReport.getOverview().getCustomizationID() != null ? gitbReport.getOverview().getCustomizationID() : NOT_PROVIDED,
                            null);
            return outputValidationReport;
        } else {
            return new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
                    date,
                    NOT_PROVIDED,
                    NOT_PROVIDED,
                    NOT_PROVIDED,
                    NOT_PROVIDED,
                    null);
        }
    }
}