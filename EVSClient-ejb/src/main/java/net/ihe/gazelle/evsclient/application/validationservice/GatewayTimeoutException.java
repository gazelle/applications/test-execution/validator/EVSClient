package net.ihe.gazelle.evsclient.application.validationservice;

public class GatewayTimeoutException extends Exception {
   private static final long serialVersionUID = -4089375827529543712L;

   public GatewayTimeoutException(String s) {
      super(s);
   }

   public GatewayTimeoutException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
