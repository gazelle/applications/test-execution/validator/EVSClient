package net.ihe.gazelle.evsclient.domain.validation.report;

/**
 * Priority of the Constraint
 *  if constraint is failed :
 *      Mandatory shall raise an error,
 *      Recommended shall raise a warning,
 *      Permitted shall raise an info;
 *
 *  @author fde
 *  @version $Id: $Id
 */
public enum ConstraintPriority {
    MANDATORY,
    RECOMMENDED,
    PERMITTED
}
