package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MbvReportMapper extends AbstractReportMapper implements ValidationReportMapper, OriginalReportParser<DetailedResult> {

   private static final Logger LOG = LoggerFactory.getLogger(MbvReportMapper.class);

   @Override
   public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
      DetailedResult parsedOriginalReport = parseOriginalReport(originalReport);
      ValidationReport outputValidationReport = new ValidationReport(generateUuid(),
            buildOverview(parsedOriginalReport));
      for (ValidationSubReport subReport : parseSubReports(parsedOriginalReport)) {
         outputValidationReport.addSubReport(subReport);
      }
      return outputValidationReport;
   }

   @Override
   public DetailedResult parseOriginalReport(byte[] originalReport) throws ReportTransformationException {
      if (originalReport!=null && originalReport.length > 0) {
         try (ByteArrayInputStream reportInputStream = new ByteArrayInputStream(originalReport)) {
            final JAXBContext jax = JAXBContext.newInstance(DetailedResult.class);
            final Unmarshaller unm = jax.createUnmarshaller();
            return (DetailedResult) unm.unmarshal(reportInputStream);
         } catch (IOException | IllegalArgumentException | JAXBException e) {
            throw new ReportTransformationException("Unable to parse detailed result", e);
         }
      } else {
         throw new ReportTransformationException("Empty original report");
      }
   }

   private ValidationOverview buildOverview(DetailedResult parsedOriginalReport) {
      if (parsedOriginalReport.getValidationResultsOverview() != null) {
         return parseSpecificOverview(parsedOriginalReport.getValidationResultsOverview());
      } else {
         return new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
               ValidationReportMapper.UNKNOWN, ValidationReportMapper.UNKNOWN,
               ValidationReportMapper.UNKNOWN);
      }
   }

   protected ValidationOverview parseSpecificOverview(ValidationResultsOverview originalOverview) {
      String validationServiceName = originalOverview.getValidationServiceName();
      String validationServiceVersion = originalOverview.getValidationServiceVersion();
      String validatorID = originalOverview.getValidationEngine();
      String validatorVersion = originalOverview.getValidationEngineVersion();

      ValidationOverview overview = new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
            parseValidationDate(originalOverview.getValidationDate(), originalOverview.getValidationTime()),
            validationServiceName != null ? validationServiceName : UNKNOWN,
            validationServiceVersion != null ? validationServiceVersion : UNKNOWN,
            validatorID != null ? validatorID : UNKNOWN
      );
      overview.setValidatorVersion(validatorVersion != null ? validatorVersion : UNKNOWN);
      return overview;
   }

   private Iterable<? extends ValidationSubReport> parseSubReports(DetailedResult result) {
      List<ValidationSubReport> subReports = new ArrayList<>();

      if (result.getDocumentWellFormed() != null) {
         subReports.add(parseSubReport(result.getDocumentWellFormed()));
      }
      if (result.getDocumentValidXSD() != null) {
         subReports.add(parseSubReport(result.getDocumentValidXSD()));
      }
      if (result.getMDAValidation() != null && result.getMDAValidation().getWarningOrErrorOrNote() != null) {
         subReports.add(parseSubReport(result.getMDAValidation()));
      }
      return subReports;
   }

   private ValidationSubReport parseSubReport(DocumentWellFormed documentWellFormed) {
      List<XSDMessage> xsdMessages = documentWellFormed.getXSDMessage();
      String documentResult = documentWellFormed.getResult();
      ValidationSubReport vsr = new ValidationSubReport("Syntactic validation"
            , new ArrayList<String>()
            // TODO specify standards. (for all XML technologies, well formed is about XML standard). This may vary
            //  for HL7v2, FHIR, DICOM.
      );

      if (xsdMessages.isEmpty()) {
         addDefaultStatus(documentResult, vsr,
               "Failure: The document is not well-formed.",
               "Success: The document you have validated is supposed to be a well-formed document.",
               "Undetermined: Your document has not been determined as being well-formed or not."
         );
      } else {
         for (XSDMessage message : xsdMessages) {
            vsr.addConstraintValidation(parseXSDMessage(message, documentResult));
         }
      }
      return vsr;
   }

   private ValidationSubReport parseSubReport(DocumentValidXSD documentValidXSD) {
      List<XSDMessage> xsdMessages = documentValidXSD.getXSDMessage();
      String documentResult = documentValidXSD.getResult();

      ValidationSubReport vsr = new ValidationSubReport("Schema validation"
            , new ArrayList<String>()
            // Standards' schemas depend on the validator (wstrust, ebRim, CDA, etc...) this list is not completable
            // if it's not returned with the original report.
      );

      if (xsdMessages.isEmpty()) { //because there is no messages if everything is OK (<result>PASSED</result>)
         addDefaultStatus(documentResult, vsr,
               "Failure: Your document has not been validated with the appropriate schema",
               "Success: Your document has been validated with the appropriate schema",
               "Undetermined: Your document has not been fully validated with the appropriate schema"
         );
      } else {
         for (XSDMessage message : xsdMessages) {
            vsr.addConstraintValidation(parseXSDMessage(message, documentResult));
         }
      }
      return vsr;

   }

   private ValidationSubReport parseSubReport(MDAValidation mdaValidation) {
      List<Object> notifications = mdaValidation.getWarningOrErrorOrNote();
      String documentResult = mdaValidation.getResult();
      ValidationSubReport vsr = new ValidationSubReport("Object Checker validation"
            , new ArrayList<String>() // TODO
      );

      if (notifications.isEmpty()) {
         addDefaultStatus(documentResult, vsr,
               "Failure: Your document has not been validated without any notifications",
               "Success: Your document has been validated without any notifications",
               "Your document has not been fully validated without any notifications"
         );
      } else {
         for (Object notification : mdaValidation.getWarningOrErrorOrNote()) {
            if (notification instanceof Notification) {
               vsr.addConstraintValidation(parseNotification((Notification) notification));
            } else {
               LOG.warn("invalid-mda-validation-notification {}", notification); // should never happened
            }
         }
      }
      return vsr;
   }

   private ConstraintValidation parseXSDMessage(XSDMessage message, String result) {
      return new ConstraintValidation(
            null, // TODO
            null, // TODO
            message.getMessage(),
            null,
            parseLocation(message),
            null, // TODO
            null,
            null,// TODO
            parsePriority(message),
            parseResult(result) // TODO
      );
   }

   private ConstraintPriority parsePriority(XSDMessage message) {
      return ConstraintPriority.MANDATORY;
   }

   private String parseLocation(XSDMessage message) {
      StringBuilder sb = new StringBuilder();
      if (message.getLineNumber() != null) {
         sb.append(" line: ").append(message.getLineNumber());
         if (message.getColumnNumber() != null) {
            sb.append(" column: ").append(message.getColumnNumber());
         }
      }
      return sb.toString();
   }

   public ConstraintValidation parseNotification(Notification notification) {
      return new ConstraintValidation(
            notification.getIdentifiant(),
            notification.getType(),
            notification.getDescription(),
            null,
            parseLocation(notification),
            null,
            parseAssertions(notification.getAssertions()),
            null,
            parsePriority(notification),
            parseNotificationResult(notification)
      );
   }

   protected String parseLocation(Notification notification) {
      if (notification.getLocation() == null) {
         return null;
      }
      StringBuilder sb = new StringBuilder(notification.getLocation());
      if (notification instanceof Error) {
         Error n = ((Error) notification);
         if (n.getStartLine() != null) {
            sb.append(" line: ").append(n.getStartLine());
            if (n.getStartColumn() != null) {
               sb.append(" column: ").append(n.getStartColumn());
            }
            if (n.getEndLine() != null) {
               sb.append(" until line: ").append(n.getEndLine());
               if (n.getStartColumn() != null) {
                  sb.append(" column: ").append(n.getEndColumn());
               }
            }
         }
      } else if (notification instanceof Warning) {
         Warning n = ((Warning) notification);
         if (n.getStartLine() != null) {
            // Duplicated line of codes, because Errors methods and Warning methods are duplicated and does not
            // inherit from a common interface.
            sb.append(" line: ").append(n.getStartLine());
            if (n.getStartColumn() != null) {
               sb.append(" column: ").append(n.getStartColumn());
            }
            if (n.getEndLine() != null) {
               sb.append(" until line: ").append(n.getEndLine());
               if (n.getStartColumn() != null) {
                  sb.append(" column: ").append(n.getEndColumn());
               }
            }
         }
      }
      return sb.toString();
   }

   protected List<String> parseAssertions(List<Assertion> assertions) {
      List<String> assertionIDs = new ArrayList<>();
      if (assertions != null) {
         for (Assertion a : assertions) {
            assertionIDs.add(a.getAssertionId());
         }
      }
      return assertionIDs;
   }

   protected ValidationTestResult parseNotificationResult(Notification notification) {
      return notification instanceof Note ? ValidationTestResult.PASSED : ValidationTestResult.FAILED;
   }

   protected ConstraintPriority parsePriority(Notification notification) {
      if (notification instanceof Error || notification instanceof Note) {
         return ConstraintPriority.MANDATORY;
      } else if (notification instanceof Warning) {
         return ConstraintPriority.RECOMMENDED;
      } else {
         return ConstraintPriority.PERMITTED;
      }
   }
}
