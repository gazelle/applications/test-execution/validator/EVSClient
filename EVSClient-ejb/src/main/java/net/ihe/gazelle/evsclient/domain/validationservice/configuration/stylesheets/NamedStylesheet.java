package net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets;

import javax.persistence.*;

@Entity
@Table(name = "evsc_named_stylesheet", schema = "public")
public class NamedStylesheet extends Stylesheet {
    private static final long serialVersionUID = 7120502678878447995L;

    @Column(name = "name")
    private String name;

    public NamedStylesheet() {
    }

    public NamedStylesheet(String location, String name) {
        super(location);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
