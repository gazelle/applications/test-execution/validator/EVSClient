package net.ihe.gazelle.evsclient.interlay.dto.rest;

import java.io.IOException;

public class MarshallingException extends IOException {
   private static final long serialVersionUID = 560161937370579725L;

   public MarshallingException() {
   }

   public MarshallingException(String s) {
      super(s);
   }

   public MarshallingException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public MarshallingException(Throwable throwable) {
      super(throwable);
   }
}
