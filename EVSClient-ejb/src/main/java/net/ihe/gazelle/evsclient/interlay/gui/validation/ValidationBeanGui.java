package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.document.Renderer;
import net.ihe.gazelle.evsclient.interlay.gui.document.XmlDocumentRenderer;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

public class ValidationBeanGui extends AbstractSignableValidationBeanGui {

    public ValidationBeanGui(ValidationServiceFacade validationServiceFacade,
                             CallerMetadataFactory callerMetadataFactory,
                             GazelleIdentity identity,
                             ApplicationPreferenceManager applicationPreferenceManager,
                             EmailNotificationManager emailNotificationManager) {
        super(ValidationBeanGui.class, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager, null, emailNotificationManager);

    }


    // TODO
    public void validateSignatures(Validation validation) {
/*        final Thread dsValidationThread = new Thread(new DigitalSignatureValidator(validation));
        dsValidationThread.start();
        try {
            dsValidationThread.join();
        } catch (final InterruptedException e) {
            this.updateAbortedValidation(e);
            Thread.currentThread().interrupt();
        }*/
    }


    @Override
    protected String getValidatorUrl() {
        return null;
    }

    @Override
    public String performAnotherValidation() {
        return null;
    }

    @Override
    public String revalidate() {
        return null;
    }

    public boolean isPrettyViewEnabled(){
        for(Renderer<?,?> renderer : super.getPresenter().getRenderers()){
            if(renderer instanceof XmlDocumentRenderer){
                return ((XmlDocumentRenderer)renderer).getOptions().getPrettyView();
            }
        }
        return false;
    }

    public String getDefaultPageNameTitle(){
        return I18n.get("net.ihe.gazelle.evs.ValidateObjects", getReferencedStandardName());
    }

}
