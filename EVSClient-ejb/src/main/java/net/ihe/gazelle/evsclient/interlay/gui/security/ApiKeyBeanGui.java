/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.security;

import net.ihe.gazelle.evsclient.application.security.ApiKeyManager;
import net.ihe.gazelle.evsclient.domain.security.ApiKey;

import java.util.List;

public class ApiKeyBeanGui {

    private List<ApiKey> apiKeys;

    private final ApiKeyManager apiKeyManager;

    public ApiKeyBeanGui(ApiKeyManager apiKeyManager) {
        this.apiKeyManager = apiKeyManager;
        init();
    }

    public void init() {
        apiKeys = apiKeyManager.getAllKeysOfCurrentUser();
    }

    public boolean displayCreateNewKeyButton() {
        return apiKeyManager.isAllKeysOfCurrentUserExpired(apiKeys);
    }

    public void createNewKey() {
        apiKeys.add(0, apiKeyManager.createNewApiKeyForCurrentUser());
    }

    public List<ApiKey> getApiKeys() {
        return apiKeys;
    }
}
