package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.callingtools.common.model.CallingTool;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.processing.OtherCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.ProxyCallerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.processing.AbstractProcessingBeanGui;
import org.apache.james.mime4j.util.CharsetUtil;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.international.StatusMessage;

import javax.xml.bind.DatatypeConverter;
import java.util.Map;

public class RemotePermanentLinkSender {

    private AbstractProcessingBeanGui<Validation, ValidationServiceFacade> gui;

    protected RemotePermanentLinkSender(AbstractProcessingBeanGui<Validation, ValidationServiceFacade> gui) {
        super();
        this.gui = gui;
    }

    /**
     * Redirect to the caller with base and additional parameters
     * @param params Map of additional parameters, if a parameter present in the Map
     *               and was already processed manually, it will not be added to the URL
     */
    public void sendPermaLink(Map<String, String> params) {
        if (gui.getSelectedObject().getOid() != null && gui.getSelectedObject().getCaller() instanceof OtherCallerMetadata) {
            String toolOid = ((OtherCallerMetadata) gui.getSelectedObject().getCaller()).getToolOid();
            final CallingTool tool = CallingTool.getToolByOid(toolOid);
            if (tool != null && tool.getUrl() != null && !tool.getUrl().isEmpty()) {
                try {
                    redirect(tool, params);
                } catch (Exception e) {
                    GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Fail to redirect to " + tool.getUrl());
                }
            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR,"OID " + toolOid + " does not match any known tool");
            }
        }
    }

    public void sendPermaLink(){
        sendPermaLink(null);
    }

    private void redirect(CallingTool tool, Map<String, String> params) {
        FacesManager.instance().redirectToExternalURL(buildURL(tool,DatatypeConverter.printBase64Binary(gui.getSelectedObject().getOid().getBytes(CharsetUtil.UTF_8)), params));
    }


    private String buildURL(CallingTool tool, String oid, Map<String, String> params) {
        final StringBuilder builder = new StringBuilder(tool.getUrl());
        if (tool.getToolType() == CallingTool.ToolType.PROXY) {
            builder.append(redirectToProxy(((ProxyCallerMetadata) gui.getSelectedObject().getCaller()).getProxyType()))
                    .append("?" + QueryParam.PROCESSING_ID + "=");
        } else if (tool.getToolType() == CallingTool.ToolType.TM) {
            builder.append("?" + QueryParamEvs.FILE_ID + "=");
        }

        // OTHER CALLING TOOLS
        // ===================
        builder.append(((OtherCallerMetadata) gui.getSelectedObject().getCaller()).getToolObjectId());
        if (tool.getUrl().matches(".*?(\\?|\\&)"+QueryParam.PROCESSING_OID+"=$")) {
            // Callling tool URL ends with "oid=": it means that remote tool uses an oid
            // which clash with the legacy PROCESSING_OID mechanism.
            // In particular, it's a MCA behaviour (oid must not the validation oid, but the analysis oid).
            // Result will be return as an encoded validationRef structure.
            final String validationref = DatatypeConverter.printBase64Binary(ValidationRef.Serialization.toByteArray(new ValidationRef(gui.getSelectedObject().getOid(), gui.getSelectedObject().getValidationStatus().name())));
            builder.append("&" + QueryParam.VALIDATION_REF + "=").append(validationref);
        } else {
            builder.append("&" + QueryParam.PROCESSING_OID + "=").append(oid);
        }


        // CHECK FOR ADDITIONAL NOT ADDED PARAMS (AND SWITCH THE PRIVACY KEY)
        String result = builder.toString();
        if(params != null){
            for(Map.Entry<String, String> entry : params.entrySet()){
                if(!result.contains(entry.getKey())){
                    String key = entry.getKey();
                    if(key.equals(QueryParam.CALLER_PRIVACY_KEY)){
                        key = QueryParam.PRIVACY_KEY;
                    }
                    builder.append("&").append(key).append("=").append(entry.getValue());
                }
            }
        }
        return builder.toString();
    }

    public String redirectToProxy(final String type) {

        if (type.contains("HL7")) {
            return "/messages/hl7.seam";
        }
        if (type.contains("RAW")) {
            return "/messages/raw.seam";
        }
        if (type.contains("DICOM")) {
            return "/messages/dicom.seam";
        }
        if (type.contains("SYSLOG")) {
            return "/messages/syslog.seam";
        }
        if (type.contains("HTTP")) {
            return "/messages/http.seam";
        } else {
            return null;
        }
    }
}
