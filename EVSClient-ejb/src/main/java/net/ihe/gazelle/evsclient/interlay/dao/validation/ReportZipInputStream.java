package net.ihe.gazelle.evsclient.interlay.dao.validation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

/**
 * Custom inputStream dedicated to uncompress a report from a zip archive.
 * It will uncompress content on the fly while reading the report from the file system.
 *
 * @author ceoche
 */
class ReportZipInputStream extends ZipInputStream {

   /**
    * Create a {@link ReportZipOutputStream}.
    *
    * @param reportArchiveFileInputStream {@link FileInputStream} that point to the archive file to read.
    * @throws IOException in case of I/O reading error.
    */
   ReportZipInputStream(FileInputStream reportArchiveFileInputStream) throws IOException {


      super(reportArchiveFileInputStream);
      ZipEntry entry = getNextEntry();
      if(entry == null) {
         throw new ZipException("No report to extract from the ZIP archive");
      }
   }
}
