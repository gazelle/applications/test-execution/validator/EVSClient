package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;

public interface WebValidationService extends ConfigurableValidationService<WebValidationServiceConf> {
    String getUrl() ;
    boolean isZipTransport();
}
