package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationExecutionQueue;
import net.ihe.gazelle.evsclient.domain.validationservice.ConfigurableValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.jar.Manifest;

public abstract class AbstractConfigurableValidationService<T extends ValidationServiceConf> extends AbstractConfigurableService<T> implements ConfigurableValidationService<T> {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractConfigurableValidationService.class);
    private static Map<Integer,Map<String,List<Validator>>> cache = new THashMap<>();

    public static void clearValidatorsCache() {
        synchronized (cache) {
            cache.clear();
        }
    }

    protected AbstractConfigurableValidationService(ApplicationPreferenceManager applicationPreferenceManager, T configuration) {
        super(applicationPreferenceManager,configuration);
    }

    @Override
    public List<Validator> getValidators() {
        return getValidators(null);
    }

    @Override
    public List<Validator> getValidators(final String validatorFilter) {
        synchronized (cache) {
            if (!cache.containsKey(this.getConfiguration().getId())) {
                cache.put(this.getConfiguration().getId(),new THashMap<String, List<Validator>>());
            }
            Map<String, List<Validator>> c = cache.get(this.getConfiguration().getId());
            if (!c.containsKey(validatorFilter==null?"":validatorFilter)) {
                final List<Validator> validators = new ArrayList<>();
                Exception exception = ValidationExecutionQueue.run(this.getConfiguration(), new Callable<Exception>() {
                    @Override
                    public Exception call() throws Exception {
                        List<String> validatorNames = getValidatorNames(validatorFilter);
                        if (validatorNames != null) {
                            Collections.sort(validatorNames);
                            for (String keyword : validatorNames) {
                                validators.add(new Validator(keyword));
                            }
                        }
                        return null;
                    }
                }, "getValidatorNames");
                if (exception!=null) {
                    clearValidatorsCache(); // in case of error while asking validators, it may be a context problem : reset cache because it a may be a sign that service's configuration has changed.
                    return validators;
                } else {
                    c.put(validatorFilter == null ? "" : validatorFilter, validators);
                }
            }
            return c.get(validatorFilter==null?"":validatorFilter);
        }

    }

    protected Manifest getManifest(String name) throws IOException {
        Enumeration<URL> resources = getClass().getClassLoader()
                .getResources("META-INF/MANIFEST.MF");
        while (resources.hasMoreElements()) {
            try {
                Manifest manifest = new Manifest(resources.nextElement().openStream());
                if (manifest.getEntries().containsKey(name)) {
                    return manifest;
                }
            } catch (IOException E) {
                // handle
            }
        }
        throw new EOFException();
    }

    // FIXME only for embbedded service only DO NOT USE !
    protected String getVersion(String module, String version) {
        try {
            return getManifest(module)
                    .getEntries()
                    .get(module)
                    .getValue(version);
        } catch (Exception e) {
            return "";
        }
    }

    protected abstract List<String> getValidatorNames(String validatorFilter);

}
