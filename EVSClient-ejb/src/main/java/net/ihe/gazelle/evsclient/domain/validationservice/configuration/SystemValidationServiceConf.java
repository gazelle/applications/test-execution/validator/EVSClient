package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.SystemValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_system_validation_service_conf", schema = "public")
public class SystemValidationServiceConf extends ValidationServiceConf {
    private static final long serialVersionUID = -4320316999510172493L;

    @Column(name = "binary_path")
    private String binaryPath;

    public SystemValidationServiceConf() {
        super();
    }

    public String getBinaryPath() {
        return binaryPath;
    }

    public void setBinaryPath(String binaryPath) {
        this.binaryPath = binaryPath;
    }

    @Override
    public Class<? extends ValidationService> getValidationServiceInterface() {
        return SystemValidationService.class;
    }
}
