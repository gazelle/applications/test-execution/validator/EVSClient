package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.AbstractProcessingCacheManager;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;

public class ValidationCacheManagerImpl extends AbstractProcessingCacheManager<Validation> {

    public ValidationCacheManagerImpl(ApplicationPreferenceManager applicationPreferenceManager, ValidationDao validationDao) {
        super(applicationPreferenceManager, validationDao);
    }

    @Override
    public String getStatus(Validation validation) {
        return validation.getValidationStatus().name();
    }

    @Override
    public String getPermanentLink(Validation validation) {
        return new AbstractGuiPermanentLink(applicationPreferenceManager) {
            @Override
            public String getResultPageUrl() {
                return Pages.DETAILED_RESULT.getMenuLink();
            }
        }.getPermanentLink(validation);
    }
}
