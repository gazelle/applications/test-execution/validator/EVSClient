package net.ihe.gazelle.evsclient.application.validation;

public interface ValidationCacheManager {

    String getValidationData(final String oid, final String externalId, final String toolOid, final String queryCache, final String objectToQuery) ;

    String getValidationDateService(final String oid, final String externalId, final String toolOid, final String queryCache);

    String getValidationPermanentLinkService(final String oid, final String externalId, final String toolOid, final String queryCache);


    String getValidationStatusService(final String oid, final String externalId, final String toolOid, final String queryCache);

    String getValidationStatusAndPermanentLinkService(final String oid, final String externalId, final String toolOid, final String queryCache);

    boolean refreshGazelleCacheWebService(final String externalId, final String toolOid, final String oid, final String queryCache);
}
