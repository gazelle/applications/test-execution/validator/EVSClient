package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.interlay.dao.FileRepository;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import net.ihe.gazelle.evsclient.interlay.util.FileUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

class ReportFileDAO {

   private static final String REPORT_FILE_EXTENSION = ".xml";
   private static final String ARCHIVE_FILE_EXTENSION = ".zip";

   private final ApplicationPreferenceManager applicationPreferenceManager;

   ReportFileDAO(ApplicationPreferenceManager applicationPreferenceManager) {
      this.applicationPreferenceManager = applicationPreferenceManager;
   }

   protected <T extends Serializable> void saveReportInFile(ReportFile<T> report, ReportFileRepository reportRepository,
                                                          String validationId) {
      if(report.getArchivePath() == null) {
         try {
            report.setArchivePath(reportRepository.buildFilePath(applicationPreferenceManager, validationId));
            writeAndCompressReport(report, reportRepository.getReportFileName(report.getArchivePath()));
         } catch (IllegalStateException | IOException e ) {
            throw new ReportNotSavedException("Unable to write report content in archive " + report.getArchivePath(),
                  e);
         }
      }
   }

   private <T extends Serializable> void writeAndCompressReport(ReportFile<T> report, String reportFileName) throws IOException {
      File archiveFile = new File(report.getArchivePath());
      FileUtil.createParentDirectories(archiveFile);
      try(ReportZipOutputStream reportOutputStream =
                new ReportZipOutputStream(new FileOutputStream(archiveFile), reportFileName)){
         report.marshall(report.getContent(), reportOutputStream);
      }
   }

   static class ReportFileRepository extends FileRepository {
      public ReportFileRepository(Repository repository, String reportPrefix) {
         super(repository.getRootPathPreferenceKey(), reportPrefix, ARCHIVE_FILE_EXTENSION);
      }

      public String getReportFileName(String archiveFilePath) {
         final File archiveFile = new File(archiveFilePath);
         return archiveFile.getName().replaceFirst("\\"+ ARCHIVE_FILE_EXTENSION +"$", REPORT_FILE_EXTENSION);
      }
   }
}
