package net.ihe.gazelle.evsclient.interlay.factory;

import gnu.trove.set.hash.THashSet;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

class DefaultImplementationFinder {
    private String rootPackage;
    static class Serializer {
        String before = "";
        String after = "";
        String separator = ",";

        String serialize(Class<?>[] classes) {
            StringBuilder sb = new StringBuilder(before);
            Iterator<Class<?>> it = Arrays.asList(classes).iterator();
            while (it.hasNext()) {
                sb.append(it.next().getCanonicalName());
                if (it.hasNext()) {
                    sb.append(separator);
                }
            }
            sb.append(after);
            return sb.toString();
        }
    }
    static class NotAnImplementation extends RuntimeException {
        private static final long serialVersionUID = -3375584712349316134L;

        public NotAnImplementation(Class<?> implemented, String classname) {
            super("("+implemented.getCanonicalName()+")"+classname);
        }
    }
    static class NotAnInterface extends RuntimeException {
        private static final long serialVersionUID = 5782843827304092638L;

        public NotAnInterface(Class<?> implemented) {
            super(implemented.getCanonicalName());
        }
    }
    static class NotADefaultImplementation extends RuntimeException {
        private static final long serialVersionUID = 4726912284411133847L;

        public NotADefaultImplementation(Class<?> implementation) {
            super(implementation.getCanonicalName());
        }
    }

    static class NoDefaultImplementation extends RuntimeException {
        private static final long serialVersionUID = -6154089446695222927L;

        public NoDefaultImplementation(Class<?> implemented) {
            super(implemented.getCanonicalName());
        }
    }

    static class TooManyDefaultImplementations extends RuntimeException {
        private static final long serialVersionUID = -9019521935626251830L;

        public TooManyDefaultImplementations(Class<?> implemented, Class<?>[] implementations) {
            super(implemented.getCanonicalName() + ":" + new Serializer().serialize(implementations));
        }
    }

    public DefaultImplementationFinder(String rootPackage) {
        this.rootPackage = rootPackage;
    }

    <T> Class<? extends T> find(Class<T> implemented) {
            return find(implemented,rootPackage);
    }
    <T> Class<? extends T> find(Class<T> implemented,String rootPackage) {
        if (!implemented.isInterface()) {
            throw new NotAnInterface(implemented);
        }
        Reflections reflections = new Reflections(rootPackage);
        Set<Class<? extends T>> defaults = new THashSet<>();
        for (Class<? extends T> cls : reflections.getSubTypesOf(implemented)) {
            DefaultImplementation di = cls.getAnnotation(DefaultImplementation.class);
            if (di != null && di.value().isAssignableFrom(implemented)) {
                defaults.add(cls);
            }
        }
        if (defaults.isEmpty()) {
            throw new NoDefaultImplementation(implemented);
        } else if (defaults.size() > 1) {
            throw new TooManyDefaultImplementations(implemented, defaults.toArray(new Class<?>[]{}));
        }
        return defaults.iterator().next();
    }

    <T> T instantiate(Class<T> implementation) {
        try {
            return implementation.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new NotADefaultImplementation(implementation);
        }
    }

}
