package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DccheckReportBuilder extends DicomReportBuilder {

    private Pattern metadatum = Pattern.compile("^(.*?)(?:\\s*\\:)");
    private Pattern subreport = Pattern.compile("^(?:\\*\\*\\s*)(.*?)\\s+\\*\\*$");
    private Pattern errors = Pattern.compile("^\\s*(?i)error");
    private Pattern warnings = Pattern.compile("^\\s*(?i)warn");
    private Pattern notificationDetails = Pattern.compile("^(?:\\s+)(.*?)$");

    public DccheckReportBuilder() {
    }

    <S extends VersionedValidationService> ValidationReport build(S service, String validator, String version, String html) {

        Document doc = Jsoup.parse(
                        html
        );
        Element generalInformation = getSection(doc, "General Information");
        Element toolAndVersion = getSectionProperty(generalInformation, "Tool name and version");
        ValidationReport report = new ValidationReport(
                UUID.randomUUID().toString(),
                new ValidationOverview(
                        parseDisclaimer(doc),
                        service.getName(),
                        service.getServiceVersion(),
                        getValidatorName(toolAndVersion),
                        getValidatorVersion(toolAndVersion),
                        parseMetadata(generalInformation)));

        ValidationSubReport vsr = new ValidationSubReport(
                "DCCHECK Report",
                new ArrayList<String>()
        );

        Elements exceptions = getSection(doc, "Validation report")
                .select("div.rich-panel-body")
                .first()
                .select("p[style=font-color:red;]");
        if (exceptions!=null) {
            for (Element e:exceptions) {
                vsr.addUnexpectedError(new UnexpectedError("",e.ownText()));
            }
        }

        parseDocument(vsr, doc);

        report.addSubReport(vsr);
        return report;

    }

    private String getValidatorVersion(Element toolAndVersion) {
        return getSectionPropertyValue(toolAndVersion).replaceFirst("^\\s*\\w+\\s*","");
    }

    private String getValidatorName(Element toolAndVersion) {
        return getSectionPropertyValue(toolAndVersion).replaceFirst("^\\s*(\\w+)\\s+.*$","$1");
    }

    private String parseDisclaimer(Document doc) {
        try {
            return getSection(doc,"Disclaimer").select("div.rich-panel-body").text();
        } catch (Exception e) {
            return ValidationReportMapper.DEFAULT_DISCLAIMER;
        }
    }

    private Element getSection(Document doc,String section) {
        return doc.select("div.rich-panel:has(div.rich-panel-header:contains("+section+"))").first();
    }
    private Element getSectionProperty(Element section, String name) {
        return section.select("div.rich-panel-body").select("p:has(b:contains("+name+"))").first();
    }
    private String getSectionPropertyValue(Element property) {
        return property==null?null:property.ownText();
    }
    private String getSectionPropertyName(Element property) {
        if (property==null) {
            return null;
        }
        Element b = property.select("b").first();
        Matcher m = metadatum.matcher(b.text());
        if (m.matches()) {
            return m.group(1);
        } else {
            return b.ownText();
        }
    }
    private Elements getSectionProperties(Element section) {
        return section==null?null:section.select("div.rich-panel-body").select("p:has(b)");
    }



    private Element getNotificationProperty(Element notification, String name) {
        return notification.select("tr:has(td[width=15%]:contains("+name+"))").first();
    }
    private String getNotificationPropertyValue(Element property) {
        return property==null?null:property.select("td + td").first().text();
    }



    private Elements getErrors(Document doc) {
        return doc.select("table.Error");
    }
    private Elements getWarnings(Document doc) {
        return doc.select("table.warning");
    }

    private List<Metadata> parseMetadata(Element info) {
        List<Metadata> metadata = new ArrayList<>();
        if (info!=null) {
            for (Element p : getSectionProperties(info)) {
                String name = getSectionPropertyName(p);
                if (!name.contains("Tool name and version") && !name.contains("Disclaimer")) {
                    metadata.add(new Metadata(name, getSectionPropertyValue(p)));
                }
            }
        }
        return metadata;
    }

    private ConstraintValidation parseNotification(Element notification) {
        if (errors.matcher(notification.attr("class")).find()) {
            return new ConstraintValidation(parseConstraintId(notification),
                    parseConstraintType(notification),
                    parseConstraintDescription(notification),
                    parseFormalExpression(notification),
                    parseLocationInValidatedObject(notification),
                    parseValueInValidatedObject(notification),
                    parseAssertionIDs(notification),
                    parseUnexpectedErrors(notification),
                    ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
        } else if (warnings.matcher(notification.attr("class")).find()) {
            return new ConstraintValidation(parseConstraintId(notification),
                    parseConstraintType(notification),
                    parseConstraintDescription(notification),
                    parseFormalExpression(notification),
                    parseLocationInValidatedObject(notification),
                    parseValueInValidatedObject(notification),
                    parseAssertionIDs(notification),
                    parseUnexpectedErrors(notification),
                    ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
        } else {
            return new ConstraintValidation(parseConstraintId(notification),
                    parseConstraintType(notification),
                    parseConstraintDescription(notification),
                    parseFormalExpression(notification),
                    parseLocationInValidatedObject(notification),
                    parseValueInValidatedObject(notification),
                    parseAssertionIDs(notification),
                    parseUnexpectedErrors(notification),
                    ConstraintPriority.PERMITTED, ValidationTestResult.PASSED);
        }
    }

    private List<UnexpectedError> parseUnexpectedErrors(Element notification) {
        return null; // TODO
    }

    private List<String> parseAssertionIDs(Element notification) {
        List<String> assertions = new ArrayList<>();
        Element property = getNotificationProperty(notification, "References to standard");
        if (property!=null) {
            for (Element li :property.select("li")){
                assertions.add(li.text());
            }
        }
        return assertions;
    }

    private String parseValueInValidatedObject(Element notification) {
        return getNotificationPropertyValue(getNotificationProperty(notification,"Value"));
    }

    private String parseLocationInValidatedObject(Element notification) {
        return getNotificationPropertyValue(getNotificationProperty(notification,"Tag"));
    }

    private String parseFormalExpression(Element notification) {
        return null; // TODO
    }

    private String parseConstraintDescription(Element notification) {
        return getNotificationPropertyValue(getNotificationProperty(notification,"Finding"));
    }

    private String parseConstraintId(Element notification) {
        return getNotificationPropertyValue(getNotificationProperty(notification,"Name"));
    }

    private String parseConstraintType(Element notification) {
        return getNotificationPropertyValue(getNotificationProperty(notification," type"));
    }


    private void parseDocument(ValidationSubReport vsr, Document doc) {
        for (Element error:getErrors(doc)) {
            vsr.addConstraintValidation(parseNotification(error));
        }
        for (Element warning:getWarnings(doc)) {
            vsr.addConstraintValidation(parseNotification(warning));
        }
        if (vsr.getConstraints().isEmpty()) {
            vsr.addConstraintValidation(new ConstraintValidation("checked",ConstraintPriority.PERMITTED, ValidationTestResult.PASSED));
        }

    }
}
