package net.ihe.gazelle.evsclient.domain.validation.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ValidationOverview class
 *
 * @author fde
 * @version $Id: $Id
 */
public class ValidationOverview implements Serializable {

    private static final long serialVersionUID = -1507141731341525342L;

    private String disclaimer;
    private Date validationDateTime;
    private String validationServiceName;
    private String validationServiceVersion;
    private String validatorID;
    private String validatorVersion;
    private List<Metadata> additionalMetadata = new ArrayList<>(); // Sonar false-warning, both Metadata and ArrayList are serializable.
    private ValidationTestResult validationOverallResult;

    /**
     * Constructor with all Mandatory parameters
     * <p>
     * validationOverallResult is initialized to UNDEFINED with all constructors and update by adding subReport
     *
     * @param disclaimer               {@link String}
     * @param validationDateTime       {@link String}
     * @param validationServiceName    {@link String}
     * @param validationServiceVersion {@link String}
     * @param validatorID              {@link String}
     * @throws IllegalArgumentException if a mandatory parameter is null
     */
    public ValidationOverview(String disclaimer, Date validationDateTime, String validationServiceName, String validationServiceVersion,
                              String validatorID) {
        setDisclaimer(disclaimer);
        setValidationDateTime(validationDateTime);
        setValidationServiceName(validationServiceName);
        setValidationServiceVersion(validationServiceVersion);
        setValidatorID(validatorID);
        setValidationOverallResult(ValidationTestResult.UNDEFINED);
    }

    /**
     * Constructor with all Mandatory parameters
     * * validationDateTime will be set automatically
     * <p>
     * validationOverallResult is initialized to UNDEFINED with all constructors and update by adding subReport
     *
     * @param disclaimer               {@link String}
     * @param validationServiceName    {@link String}
     * @param validationServiceVersion {@link String}
     * @param validatorID              {@link String}
     * @throws IllegalArgumentException if a mandatory parameter is null
     */
    public ValidationOverview(String disclaimer, String validationServiceName, String validationServiceVersion, String validatorID) {
        setDisclaimer(disclaimer);
        setValidationDateTime(new Date());
        setValidationServiceName(validationServiceName);
        setValidationServiceVersion(validationServiceVersion);
        setValidatorID(validatorID);
        setValidationOverallResult(ValidationTestResult.UNDEFINED);
    }

    /**
     * Constructor with all parameters
     * <p>
     * validationOverallResult is initialized to UNDEFINED with all constructors and update by adding subReport
     *
     * @param disclaimer               can not be null
     * @param validationDateTime       can not be null
     * @param validationServiceName    can not be null
     * @param validationServiceVersion can not be null
     * @param validatorID              can not be null
     * @param validatorVersion         can be null
     * @param additionalMetadata       can be null
     * @throws IllegalArgumentException if a mandatory parameter is null
     */
    public ValidationOverview(String disclaimer, Date validationDateTime, String validationServiceName, String validationServiceVersion,
                              String validatorID, String validatorVersion, List<Metadata> additionalMetadata) {
        setDisclaimer(disclaimer);
        setValidationDateTime(validationDateTime);
        setValidationServiceName(validationServiceName);
        setValidationServiceVersion(validationServiceVersion);
        setValidatorID(validatorID);
        setValidatorVersion(validatorVersion);
        setAdditionalMetadata(additionalMetadata);
        setValidationOverallResult(ValidationTestResult.UNDEFINED);
    }

    /**
     * Constructor with all parameters
     * validationDateTime will be set automatically
     * <p>
     * validationOverallResult is initialized to UNDEFINED with all constructors and update by adding subReport
     *
     * @param disclaimer               {@link String} can not be null
     * @param validationServiceName    {@link String} can not be null
     * @param validationServiceVersion {@link String}
     * @param validatorID              {@link String}  can not be null
     * @param validatorVersion         {@link String}
     * @param additionalMetadata       {@link Metadata}
     * @throws IllegalArgumentException if a mandatory parameter is null
     */
    public ValidationOverview(String disclaimer, String validationServiceName, String validationServiceVersion, String validatorID,
                              String validatorVersion, List<Metadata> additionalMetadata) {
        setDisclaimer(disclaimer);
        setValidationDateTime(new Date());
        setValidationServiceName(validationServiceName);
        setValidationServiceVersion(validationServiceVersion);
        setValidatorID(validatorID);
        setValidatorVersion(validatorVersion);
        setAdditionalMetadata(additionalMetadata);
        setValidationOverallResult(ValidationTestResult.UNDEFINED);
    }

    /**
     * Copy constructor
     *
     * @param validationOverview {@link ValidationOverview} object
     */
    public ValidationOverview(ValidationOverview validationOverview) {
        setDisclaimer(validationOverview.getDisclaimer());
        setValidationDateTime(validationOverview.getValidationDateTime());
        setValidationServiceName(validationOverview.getValidationServiceName());
        setValidationServiceVersion(validationOverview.getValidationServiceVersion());
        setValidatorID(validationOverview.getValidatorID());
        setValidatorVersion(validationOverview.getValidatorVersion());
        setAdditionalMetadata(validationOverview.getAdditionalMetadata());
        setValidationOverallResult(validationOverview.getValidationOverallResult());
    }


    ValidationOverview() {
        // For JPA Only
    }

    /**
     * <p>Getter of the field <code>disclaimer</code></p>
     *
     * @return a {@link String} object
     */
    public String getDisclaimer() {
        return disclaimer;
    }

    /**
     * <p>Setter for the field <code>disclaimer</code></p>
     *
     * @param disclaimer can not be null
     * @throws IllegalArgumentException if disclaimer is null
     */
    public void setDisclaimer(String disclaimer) {
        if (disclaimer == null) {
            throw new IllegalArgumentException("Disclaimer can not be null");
        }
        this.disclaimer = disclaimer;
    }

    /**
     * <p>Getter of the field <code>validationDateTime</code></p>
     *
     * @return a {@link Date} object
     */
    public Date getValidationDateTime() {
        return new Date(validationDateTime.getTime());
    }

    /**
     * <p>Setter for the field <code>validationDateTime</code></p>
     *
     * @param validationDateTime can not be null
     * @throws IllegalArgumentException if validationDateTime is null
     */
    public void setValidationDateTime(Date validationDateTime) {
        if (validationDateTime == null) {
            throw new IllegalArgumentException("ValidationDateTime can not be null");
        }
        this.validationDateTime = new Date(validationDateTime.getTime());
    }

    /**
     * <p>Getter of the field <code>validationServiceName</code></p>
     *
     * @return a {@link String} object
     */
    public String getValidationServiceName() {
        return validationServiceName;
    }

    /**
     * <p>Setter for the field <code>validationServiceName</code></p>
     *
     * @param validationServiceName can not be null
     * @throws IllegalArgumentException if validationServiceName is null
     */
    public void setValidationServiceName(String validationServiceName) {
        if (validationServiceName == null) {
            throw new IllegalArgumentException("validationServiceName can not be null");
        }
        this.validationServiceName = validationServiceName;
    }

    /**
     * <p>Getter of the field <code>validationServiceVersion</code></p>
     *
     * @return a {@link String} object
     */
    public String getValidationServiceVersion() {
        return validationServiceVersion;
    }

    /**
     * <p>Setter for the field <code>validationServiceVersion</code></p>
     *
     * @param validationServiceVersion Version of the Validatin Service
     * @throws IllegalArgumentException if validationServiceVersion is null
     */
    public void setValidationServiceVersion(String validationServiceVersion) {
        this.validationServiceVersion = validationServiceVersion;
    }

    /**
     * <p>Getter of the field <code>validatorID</code></p>
     *
     * @return a {@link String} object
     */
    public String getValidatorID() {
        return validatorID;
    }

    /**
     * <p>Setter for the field <code>validatorID</code></p>
     *
     * @param validatorID can not be null
     * @throws IllegalArgumentException if validatorID is null
     */
    public void setValidatorID(String validatorID) {
        if (validatorID == null) {
            throw new IllegalArgumentException("validatorID can not be null");
        }
        this.validatorID = validatorID;

    }

    /**
     * <p>Getter of the field <code>validationServiceVersion</code></p>
     *
     * @return a {@link String} object
     */
    public String getValidatorVersion() {
        return validatorVersion;
    }

    /**
     * <p>Setter for the field <code>validatorVersion</code></p>
     *
     * @param validatorVersion can be null
     */
    public void setValidatorVersion(String validatorVersion) {
        this.validatorVersion = validatorVersion;
    }

    /**
     * <p>Getter of the field <code>additionalMetadata</code></p>
     *
     * @return a {@link List} object
     */
    public List<Metadata> getAdditionalMetadata() {
        return new ArrayList<>(additionalMetadata);
    }

    /**
     * <p>Setter for the field <code>additionalMetadata</code></p>
     *
     * @param additionalMetadata if parameter is null, the list is initialized
     */
    public void setAdditionalMetadata(List<Metadata> additionalMetadata) {
        this.additionalMetadata = (additionalMetadata == null) ? new ArrayList<Metadata>() : new ArrayList<Metadata>(additionalMetadata);
    }

    /**
     * <p>Add a Metadata element for the field <code>additionalMetadata</code></p>
     *
     * @param metadata can not be null\
     * @throws IllegalArgumentException if metadata is null
     */
    public void addAdditionalMetadata(Metadata metadata) {
        if (metadata == null) {
            throw new IllegalArgumentException("You can not add a null Metadata in AdditionalMetadata");
        }
        this.additionalMetadata.add(metadata);
    }

    /**
     * <p>Getter of the field <code>additionalMetadata</code></p>
     *
     * @return a {@link ValidationTestResult} object
     */
    public ValidationTestResult getValidationOverallResult() {
        return validationOverallResult;
    }

    /**
     * <p>Setter for the field <code>validationOverallResult</code></p>
     *
     * @param validationOverallResult can not be null
     * @throws IllegalArgumentException if the validationOverallResult is null
     */
    void setValidationOverallResult(ValidationTestResult validationOverallResult) {
        if (validationOverallResult == null) {
            throw new IllegalArgumentException("validationOverallResult can not be null");
        }
        this.validationOverallResult = validationOverallResult;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ValidationOverview)) {
            return false;
        }

        ValidationOverview overview = (ValidationOverview) o;

        if (!disclaimer.equals(overview.disclaimer)) {
            return false;
        }
        if (!validationDateTime.equals(overview.validationDateTime)) {
            return false;
        }
        if (!validationServiceName.equals(overview.validationServiceName)) {
            return false;
        }
        if (validationServiceVersion != null ? !validationServiceVersion.equals(overview.validationServiceVersion) : overview.validationServiceVersion != null) {
            return false;
        }
        if (!validatorID.equals(overview.validatorID)) {
            return false;
        }
        if (validatorVersion != null ? !validatorVersion.equals(overview.validatorVersion) : overview.validatorVersion != null) {
            return false;
        }
        if (!additionalMetadata.equals(overview.additionalMetadata)) {
            return false;
        }
        return validationOverallResult == overview.validationOverallResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = disclaimer.hashCode();
        result = 31 * result + validationDateTime.hashCode();
        result = 31 * result + validationServiceName.hashCode();
        result = 31 * result + (validationServiceVersion != null ? validationServiceVersion.hashCode() : 0);
        result = 31 * result + validatorID.hashCode();
        result = 31 * result + (validatorVersion != null ? validatorVersion.hashCode() : 0);
        result = 31 * result + additionalMetadata.hashCode();
        result = 31 * result + validationOverallResult.hashCode();
        return result;
    }
}