package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ExtensionDao;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtensionQuery;
import net.ihe.gazelle.evsclient.interlay.dao.AbstractProcessingDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.FileRepository;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class ExtensionDaoImpl extends AbstractProcessingDaoImpl<ValidationExtension> implements ExtensionDao {

   private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionDaoImpl.class);
   private static final FileRepository DEFAULT_REPOSITORY = RepositoryTypes.XML;

   private final ExtensionReportFileDAO extensionReportFileDao;

   public ExtensionDaoImpl(EntityManagerFactory entityManagerFactory,
                           ApplicationPreferenceManager applicationPreferenceManager) {
      super(entityManagerFactory, applicationPreferenceManager);
      extensionReportFileDao = new ExtensionReportFileDAO(applicationPreferenceManager);
   }

   @Override
   public ValidationExtension merge(ValidationExtension extension) {
      if (extension.getId() != null) {
         if (extension.getExtensionReport() != null && extension.getExtensionReport().getArchivePath()==null) {
            extensionReportFileDao.saveOriginalReportInFile(extension, getRepositoryType(extension));
         }
         if (extension.getExtensionReport() != null && extension.getExtensionReport().getId()==null) {
            getEntityManager().persist(extension.getExtensionReport());
         }
         EntityManager entityManager = getEntityManager();
         extension = entityManager.merge(extension);
         entityManager.flush();
         return extension;
      } else {
         throw new IllegalStateException(String.format("Validation must first be saved with a call to .create() " +
               "before being merged with .merge()"));
      }
   }

   @Override
   protected void deleteSpecificItems(ValidationExtension extension) throws UnexpectedProcessingException {
      //TODO (for real !) delete all validation report and extension !
   }

   @Override
   public ValidationExtension findByValidationOid(String oid) {
      EntityManager entityManager = entityManagerFactory.createEntityManager();
      ValidationExtensionQuery query = new ValidationExtensionQuery(entityManager);
      query.validationRef().oid().eq(oid);
      return query.getUniqueResult();
   }

   @Override
   public Repository getRepositoryType(ValidationExtension extension) {
      return DEFAULT_REPOSITORY;
   }
}
