package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.AbstractReportMapper;
import org.apache.commons.lang.StringEscapeUtils;

import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

class DicomReportBuilder extends AbstractReportMapper {
    private Pattern errors = Pattern.compile("^\\s*(?i)error");
    private Pattern warnings = Pattern.compile("^\\s*(?i)warn");

    public DicomReportBuilder() {
    }

    <S extends VersionedValidationService> ValidationReport build(S service, String validator, String version, String result) {

        ValidationReport report = new ValidationReport(
                UUID.randomUUID().toString(),
                new ValidationOverview(
                        ValidationReportMapper.DEFAULT_DISCLAIMER,
                        service.getName(),
                        service.getServiceVersion(),
                        validator,
                        version,
                        new ArrayList<Metadata>()));

        report.addSubReport(parseDocument(validator, result.split("\n")));

        return report;

    }

    private ValidationSubReport parseDocument(String validator, String[] notifications) {

        ValidationSubReport vsr = new ValidationSubReport(
                validator,
                new ArrayList<String>()
        );

        for (String notification : notifications) {
            notification = StringEscapeUtils.escapeHtml(notification);
            if (errors.matcher(notification).find()) {
                vsr.addConstraintValidation(new ConstraintValidation(notification,
                        ConstraintPriority.MANDATORY, ValidationTestResult.FAILED));
            } else if (warnings.matcher(notification).find()) {
                vsr.addConstraintValidation(new ConstraintValidation(notification,
                        ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED));
            } else {
                vsr.addConstraintValidation(new ConstraintValidation(notification,
                        ConstraintPriority.PERMITTED, ValidationTestResult.PASSED));
            }
        }
        return vsr;
    }
}
