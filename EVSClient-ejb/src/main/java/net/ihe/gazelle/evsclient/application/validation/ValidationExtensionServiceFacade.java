package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractProcessingManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.application.job.ExtensionJob;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.extension.ExtensionStatus;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ServiceConfException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.DigitalSignatureServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ExtensionServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ScorecardServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceFactory;
import org.jboss.seam.async.QuartzDispatcher;

import java.lang.reflect.InvocationTargetException;

public class ValidationExtensionServiceFacade extends AbstractProcessingManagerImpl<ValidationExtension> {

    private final ExtensionJob extensionJob;
    private final ValidationServiceConfManager validationServiceConfManager;

    public ValidationExtensionServiceFacade(ApplicationPreferenceManager applicationPreferenceManager, OidGeneratorManager oidGenerator, ExtensionJob extensionJob, ValidationServiceConfManager validationServiceConfManager) {
        super(
                extensionJob==null?null:extensionJob.getExtensionDao(), // protect from nullpointer for UT
                applicationPreferenceManager, oidGenerator);
        this.extensionJob = extensionJob;
        this.validationServiceConfManager = validationServiceConfManager;
    }

    public <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf> E executeExtension(Validation validation, EVSCallerMetadata caller, OwnerMetadata owner, Directive directive, boolean async) throws UnexpectedProcessingException {
        try {
            C conf = (C) getExtensionConfiguration(validation, directive);
            if (conf.isAvailable()) {
                S service = (S) getExtensionService(conf);
                E extension = (E) computeExtension(
                        validation,
                        service,
                        caller,
                        owner);
                return (E) executeExtensionJob(extension, service, async);
            }
            throw new UnexpectedProcessingException("Unable to execute validation extension");
        } catch (ServiceConfNotFoundException | ServiceConfException | NotLoadedException | ProcessingNotSavedException e) {
            throw new UnexpectedProcessingException("Unable to execute validation extension", e);
        }
    }

    public <E extends ValidationExtension> E computeExtension(
            Validation validation,
            EVSCallerMetadata evsCallerMetadata,
            OwnerMetadata ownerMetadata,
            Class<E> cls)
            throws UnexpectedProcessingException {
        return computeExtension(validation,evsCallerMetadata,ownerMetadata,new Directive(cls.getSimpleName(),"",null,null));
    }
    public <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf> E computeExtension(
            Validation validation,
            EVSCallerMetadata evsCallerMetadata,
            OwnerMetadata ownerMetadata,
            Directive directive)
            throws UnexpectedProcessingException {
        try {
            C conf = (C) getExtensionConfiguration(validation, directive);
            if (conf.isAvailable()) {
                S service = (S) getExtensionService(conf);
                return (E) computeExtension(validation, service, evsCallerMetadata, ownerMetadata);
            }
            throw new UnexpectedProcessingException("Unable to get validation extension");
        } catch (ServiceConfNotFoundException | ServiceConfException | NotLoadedException | ProcessingNotSavedException e) {
            throw new UnexpectedProcessingException("Unable to get validation extension", e);
        }
    }

    private <E extends ValidationExtension> E computeExtension(Validation validation,
                                                               ExtensionService<E> service,
                                                               EVSCallerMetadata evsCallerMetadata, OwnerMetadata ownerMetadata) throws NotLoadedException, UnexpectedProcessingException, ProcessingNotSavedException {
        E extension = (E) extensionJob.getExtensionDao().findByValidationOid(validation.getOid());
        if (extension == null) {
            extension = service.createExtension(validation, evsCallerMetadata, ownerMetadata);
            extension = (E) extensionJob.getExtensionDao().create(extension);
        }
        return extension;
    }

    public <E extends ValidationExtension> E refresh(E extension) {
        return (E) extensionJob.getExtensionDao().getObjectById(extension.getId());
    }

    private <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf, T extends ExtensionServiceFactory<S, C>> S getExtensionService(C conf) throws ServiceConfException {
        T factory = (T) getExtensionFactory(conf);
        return factory.getService();
    }

    private <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf<E, S, C, ?>> C getExtensionConfiguration(Validation validation, Directive directive) throws ServiceConfNotFoundException, ServiceConfException {
        if ("scorecard".equalsIgnoreCase(directive.serviceName)) {
            return (C) getExtensionConfiguration(ScorecardServiceConf.class, validation);
        } else if ("DigitalSignature".equalsIgnoreCase(directive.serviceName)) {
            return (C) getExtensionConfiguration(DigitalSignatureServiceConf.class, validation);
        }
        throw new ServiceConfNotFoundException(directive.serviceName);
    }

    public <C extends ExtensionServiceConf> C getExtensionConfiguration(Class<C> cls, Validation validation)
            throws ServiceConfException, ServiceConfNotFoundException {
        ValidationServiceConf conf = this.validationServiceConfManager.getValidationServiceConfByKeyword(validation.getValidationService().getName());
        try {
            return cls.getDeclaredConstructor(ValidationServiceConf.class, Validation.class)
                    .newInstance(conf, validation);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new ServiceConfException(String.format("Configuration error: Unable to instantiate factory %s for " +
                    "service %s", conf.getFactoryClassname(), conf.getName()), e);
        }
    }

    private <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf, T extends ExtensionServiceFactory<S, C>> T getExtensionFactory(C conf)
            throws ServiceConfException {
        try {
            Class<T> fc = (Class<T>) conf.getFactoryClass();
            return fc.getDeclaredConstructor(ApplicationPreferenceManager.class, conf.getClass())
                    .newInstance(applicationPreferenceManager, conf);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                IllegalAccessException e) {
            throw new ServiceConfException(String.format("Configuration error: Unable to instantiate factory %s for " +
                    "service %s", conf.getFactoryClassname(), conf.getName()), e);
        }
    }

    public <E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf> E executeExtensionJob(Validation validation, E extension, boolean async) throws UnexpectedProcessingException {
        try {
            C conf = (C) getExtensionConfiguration(validation, new Directive(extension.getClass().getSimpleName(),"",null,null));
            if (conf.isAvailable()) {
                S service = (S) getExtensionService(conf);
                return (E) executeExtensionJob(extension, service,async);
            }
            throw new UnexpectedProcessingException("Unable to compute validation extension");
        } catch (ServiceConfNotFoundException | ServiceConfException e) {
            throw new UnexpectedProcessingException("Unable to compute validation extension", e);
        }
    }
    private <E extends ValidationExtension, S extends ExtensionService<E>> E executeExtensionJob(E extension, S service, boolean async) {
        extension.setExtensionStatus(
                        ExtensionStatus.IN_PROGRESS
        );
        extension = (E) extensionJob.getExtensionDao().merge(extension);
        if (async) {
            submitExtensionJob(extension, service);
        } else {
            extensionJob.executeExtensionJob(extension, service);
        }
        return extension;
    }

    public <E extends ValidationExtension, S extends ExtensionService<E>> void submitExtensionJob(final E extension, S service) {
        QuartzDispatcher.instance().scheduleAsynchronousEvent("executeExtensionJob", extension, service);
    }

}
