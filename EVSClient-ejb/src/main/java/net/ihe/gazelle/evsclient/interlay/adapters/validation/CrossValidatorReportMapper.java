package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.xvalidation.core.model.Assertion;
import net.ihe.gazelle.xvalidation.core.report.Error;
import net.ihe.gazelle.xvalidation.core.report.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CrossValidatorReportMapper
      implements ValidationReportMapper, OriginalReportParser<GazelleCrossValidationReportType> {

   private static final String CROSS_VALIDATION_SERVICE_NAME = "Gazelle Cross Validation";

   @Override
   public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
      GazelleCrossValidationReportType xvalReport = parseOriginalReport(originalReport);
      ValidationReport outputValidationReport = new ValidationReport(generateUuid(),
            buildOverview(xvalReport));
      for (ValidationSubReport subReport : buildSubReports(xvalReport)) {
         outputValidationReport.addSubReport(subReport);
      }
      return outputValidationReport;
   }

   @Override
   public GazelleCrossValidationReportType parseOriginalReport(byte[] originalReport)
         throws ReportTransformationException {
      if (originalReport!=null && originalReport.length > 0) {
         try (ByteArrayInputStream reportInputStream = new ByteArrayInputStream(originalReport)) {
            final JAXBContext jax = JAXBContext.newInstance(GazelleCrossValidationReportType.class);
            final Unmarshaller unm = jax.createUnmarshaller();
            return (GazelleCrossValidationReportType) unm.unmarshal(reportInputStream);
         } catch (IOException | JAXBException e) {
            throw new ReportTransformationException("unable-to-parse-x-val-report", e);
         }
      } else {
         throw new ReportTransformationException("Empty original report");
      }
   }

   private String generateUuid() {
      return UUID.randomUUID().toString();
   }

   private ValidationOverview buildOverview(GazelleCrossValidationReportType xvalReport) {
      if (xvalReport.getReportSummary() != null) {
         return mapOverview(xvalReport.getReportSummary());
      } else {
         return new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
               CROSS_VALIDATION_SERVICE_NAME, ValidationReportMapper.UNKNOWN,
               ValidationReportMapper.UNKNOWN);
      }
   }

   private ValidationOverview mapOverview(Summary xvalSummary) {
      String validationServiceVersion = xvalSummary.getGazelleXValidator().getEngineVersion();
      String validatorID = xvalSummary.getGazelleXValidator().getCalledValidator().getName();
      String validatorVersion = xvalSummary.getGazelleXValidator().getCalledValidator().getVersion();

      return new ValidationOverview(
            DEFAULT_DISCLAIMER,
            mapValidationDate(xvalSummary.getValidationDate()),
            CROSS_VALIDATION_SERVICE_NAME,
            validationServiceVersion != null ? validationServiceVersion : UNKNOWN,
            validatorID != null ? validatorID : UNKNOWN,
            validatorVersion != null ? validatorVersion : UNKNOWN,
            new ArrayList<Metadata>()
      );
   }

   private Date mapValidationDate(XMLGregorianCalendar xvalCalendar) {
      return xvalCalendar != null ? xvalCalendar.toGregorianCalendar().getTime() : null;
   }

   private Iterable<? extends ValidationSubReport> buildSubReports(GazelleCrossValidationReportType xvalReport) {
      List<ValidationSubReport> subReports = new ArrayList<>();
      if (xvalReport.getNotifications() != null) {
         subReports.add(buildSubReport(xvalReport.getNotifications()));
      }
      return subReports;
   }

   private ValidationSubReport buildSubReport(NotificationsType notifications) {
      ValidationSubReport vsr = new ValidationSubReport(
            "Cross Validation",
            new ArrayList<String>() // TODO
      );
      if (notifications.getNotifications() != null) {
         for (Notification notification : notifications.getNotifications()) {
            vsr.addConstraintValidation(mapNotification(notification));
         }
      }
      return vsr;
   }

   private ConstraintValidation mapNotification(Notification notification) {
      if (notification instanceof Report) {
         return mapNotification(notification, ConstraintPriority.MANDATORY, ValidationTestResult.PASSED);
      } else if (notification instanceof Info) {
         return mapNotification(notification, ConstraintPriority.PERMITTED, ValidationTestResult.FAILED);
      } else if (notification instanceof Warning) {
         return mapNotification(notification, ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
      } else if (notification instanceof Error) {
         return mapNotification(notification, ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
      } else if (notification instanceof Aborted) {
         return mapAbortedNotification((Aborted) notification);
      } else {
         return mapNotification(notification, ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
      }
   }

   private ConstraintValidation mapNotification(Notification notification, ConstraintPriority priority,
                                                ValidationTestResult testResult) {
      return new ConstraintValidation(
            notification.getTest(),
            null,
            notification.getDescription(),
            notification.getExpression(),
            mapLocation(notification),
            mapValue(notification),
            mapAssertions(notification.getAssertion()),
            null,
            priority,
            testResult
      );
   }

   private ConstraintValidation mapAbortedNotification(Aborted abortedNotification) {
      ConstraintValidation constraintValidation = new ConstraintValidation(
            abortedNotification.getDescription(),
            ConstraintPriority.MANDATORY,
            ValidationTestResult.UNDEFINED);
      constraintValidation.setConstraintID(abortedNotification.getTest());
      constraintValidation.setFormalExpression(abortedNotification.getExpression());
      constraintValidation.setAssertionIDs(mapAssertions(abortedNotification.getAssertion()));
      constraintValidation.addUnexpectedError(new UnexpectedError("Aborted", abortedNotification.getReason()));
      return constraintValidation;
   }

   private String mapLocation(Notification notification) {
      return null;
   }

   private String mapValue(Notification notification) {
      return null;
   }

   private List<String> mapAssertions(List<Assertion> assertions) {
      List<String> assertionIDs = new ArrayList<>();
      if (assertions != null) {
         for (Assertion a : assertions) {
            assertionIDs.add(a.getAssertionId());
         }
      }
      return assertionIDs;
   }
}
