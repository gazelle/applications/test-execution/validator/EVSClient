package net.ihe.gazelle.evsclient.domain.validationservice;

public class ValidationServiceOperationException extends ServiceOperationException {
   private static final long serialVersionUID = -4627021740611340886L;

   public ValidationServiceOperationException() {
   }

   public ValidationServiceOperationException(String message) {
      super(message);
   }

   public ValidationServiceOperationException(String message, Throwable cause) {
      super(message, cause);
   }

   public ValidationServiceOperationException(Throwable cause) {
      super(cause);
   }
}
