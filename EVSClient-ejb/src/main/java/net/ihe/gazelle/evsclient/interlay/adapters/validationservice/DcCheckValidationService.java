package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.dicom.evs.api.utils.XMLRPCClient;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import org.apache.commons.io.IOUtils;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ValidationProperties(
        name="DC Check",
        types =ValidationType.DICOM)
@ReportParser(PlainReportMapper.class)
public class DcCheckValidationService extends AbstractValidationServiceClient implements VersionedValidationService {

    private final XMLRPCClient rpcClient;

    public DcCheckValidationService(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        Long validationTimeout = getValidationTimeout();
        if (validationTimeout != null) {
            rpcClient = new XMLRPCClient(getUrl(), validationTimeout.intValue());
        } else {
            rpcClient = new XMLRPCClient(getUrl(),Integer.MAX_VALUE);
        }
    }

    @Override
    public String about() {
        return "DC CHECK"; //TODO : DCCheck API ?
    }

    @Override
    public String getName() {
        return "DC Check";
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter) {
        return Arrays.asList("dccheck");
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/dicom");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            ArrayList<Object> params = new ArrayList();
            params.add(objects[0].getContent());
            ValidationReport report = new DccheckReportBuilder().build(
                    this,
                    validator,
                    this.getServiceVersion(),
                    getHTMLFormattedValidationReport(
                            (String)rpcClient.executeQuery("validate", params)
                    )
            );
            return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                    .marshal(report).getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOGGER.error(String.format("an unexpected error occurred during the validation: %s", e.getMessage()));
            throw new ValidationServiceOperationException(e);
        }
    }
    private String getHTMLFormattedValidationReport(String xml) throws TransformerException, IOException {
        String xslFileLocation = "dccheckTransform.xsl";
        TransformerFactory tFactory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", (ClassLoader)null);
        Transformer transformer = null;
        transformer = tFactory.newTransformer(new StreamSource(DcCheckValidationService.class.getResourceAsStream(xslFileLocation)));
        transformer.setOutputProperty("indent", "yes");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        StreamResult out = new StreamResult(baos);
        InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name()));
        transformer.transform(new StreamSource(is), out);
        String tmp = new String(baos.toByteArray(), StandardCharsets.UTF_8.name());
        baos.close();
        return tmp;
    }
    @Override
    public String getServiceVersion() {
        return ""; // TODO : DCCheck API ?
    }
}
