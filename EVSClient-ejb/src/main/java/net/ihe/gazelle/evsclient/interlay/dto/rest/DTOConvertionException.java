package net.ihe.gazelle.evsclient.interlay.dto.rest;

public class DTOConvertionException extends RuntimeException {

   private static final long serialVersionUID = -6524902612588354557L;

   public DTOConvertionException() {
   }

   public DTOConvertionException(String s) {
      super(s);
   }

   public DTOConvertionException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public DTOConvertionException(Throwable throwable) {
      super(throwable);
   }
}
