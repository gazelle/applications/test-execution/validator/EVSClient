package net.ihe.gazelle.evsclient.interlay.gui;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@MetaInfServices(PageLister.class)
public class EvsPageLister implements PageLister {
   @Override
   public Collection<Page> getPages() {
      List<Page> pages = new ArrayList<>();
      pages.addAll(Arrays.asList(Pages.values()));
      pages.addAll(Arrays.asList(XValidationPages.values()));
      return pages;
   }
}
