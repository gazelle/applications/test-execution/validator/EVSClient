package net.ihe.gazelle.evsclient.interlay.dto.rest;

import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * A marshaller is a class that can transform a Java object tree into a structured document (XML, JSON...) and
 * vis-versa.
 *
 * @param <D> Domain object the marshaller is able to serialize or deserialize
 */
public interface EvsMarshaller<D> {

   /**
    * Defines whether the output is a complete document (as opposed to a fragment of a document). In XML, complete
    * documents will include XML pre-processor header <pre>&lt;?xml version="... ?/&gt;</pre>
    *
    * @param completeDocument boolean, true to enable complete document (true by default)
    *
    * @return this instance of {@link XMLValidationReportMarshaller} (Java 8 syntax convinience)
    */
   EvsMarshaller<D> setCompleteDocument(boolean completeDocument);

   /**
    * Indicates whether the output will be a complete document (or a fragment document if false) for the marshaller.
    * Only affects {@link #marshal(Object)} and {@link #marshal(Object, OutputStream)} calls.
    *
    * @return true if enabled, false otherwise.
    */
   boolean isCompleteDocument();

   /**
    * Defines whether the output is indented on {@link #marshal(Object)} and {@link #marshal(Object, OutputStream)}
    * calls.
    *
    * @param indentation boolean, true to enable indentation (true by default).
    *
    * @return this instance of {@link XMLValidationReportMarshaller} (Java 8 syntax convinience)
    */
   EvsMarshaller<D> setIndentation(boolean identation);

   /**
    * Indicates whether the indentation is enabled or not for the marshaller. Only affects {@link #marshal(Object)} and
    * {@link #marshal(Object, OutputStream)} calls.
    *
    * @return true if enabled, false otherwise.
    */
   boolean isIndentation();

   /**
    * Transforms domain object into a structured UTF-8 String.
    *
    * @param domain domain object to marshal
    *
    * @return the object as string.
    *
    * @throws MarshallingException in case the marshalling encouters an unexpected issue.
    */
   public String marshal(D domain) throws MarshallingException;

   /**
    * Parse a structured UTF-8 string into a domain object.
    *
    * @param exportValue the String to marshal
    *
    * @return the domain object.
    *
    * @throws MarshallingException in case the string cannot not be parsed as domain object.
    */
   public D unmarshal(String exportValue) throws MarshallingException;

   /**
    * Transforms domain object into an output byte stream
    *
    * @param domain       domain object to marshal
    * @param outputStream outputstream to write with the transformed object.
    *
    * @throws MarshallingException in case the marshalling encouters an unexpected issue.
    */
   public void marshal(D domain, OutputStream outputStream) throws MarshallingException;

   /**
    * Parse a byte inputStream into a domain object.
    *
    * @param inputStream stream source to read from.
    *
    * @return the domain object
    *
    * @throws MarshallingException in case the byte stream cannot not be parsed as domain object.
    */
   public D unmarshal(InputStream inputStream) throws MarshallingException;

}
