package net.ihe.gazelle.evsclient.interlay.gui.validation;

import com.google.common.base.Joiner;
import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.EvsMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ConstraintValidationDTO;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationReportDTO;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationSubReportDTO;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;
import net.ihe.gazelle.evsclient.interlay.gui.document.DocumentPresenter;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import net.ihe.gazelle.evsclient.interlay.gui.document.JsonTreeRenderer;
import net.ihe.gazelle.evsclient.interlay.gui.document.XmlDocumentRenderer;
import net.ihe.gazelle.evsclient.interlay.gui.processing.ProcessingGui;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.international.StatusMessage;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reporter extends ProcessingGui {

    private static final String SPLIT_CAMEL_CASE = "(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])";

    private static final String LINE_COL_REGEX = "line(?:\\s*:\\s*|\\s+)(\\d+)";

    private static final Pattern LINE_COL_PATT = Pattern.compile(LINE_COL_REGEX);
    private static final int DEFAULT_REPORT_CONSTRAINTS_LIMIT = 200;


    private Pattern mandatory = Pattern.compile("constraintDescription");
    private JsonTreeRenderer jsonTreeRenderer;
    private ValidationReportDTO validationReport;

    public Reporter(Labelizer labelizer, Class<?> cls) {
        super(cls);
        this.labelizer = labelizer;
    }

    public ValidationReportDTO getValidationReport() {
        return validationReport;
    }

    public void setValidationReport(ValidationReportDTO validationReport) {
        this.validationReport = validationReport;
    }

    public String locate(ConstraintValidationDTO constraint) {
        // TODO
        return "";
    }

    @Override
    public String getLabel(String value, Object... params) {
        String valueLabel = value.toLowerCase().replaceAll("\\s", "-");
        String prefix = "net.ihe.gazelle.evs.";
        String label = I18n.get(prefix + valueLabel, params);
        if(label.startsWith("net.ihe")){
            StringBuilder sb = new StringBuilder(value).append(" ");
            for(Object param : params){
                sb.append(param.toString()).append(" ");
            }
            return sb.toString();
        }
        if(params != null && params.length > 0){
            if(label.contains(params[0].toString())){
                return label;
            }
            else {
                return label + " " + Joiner.on(" ").join(params);

            }
        }
        return label;
    }

    public String getColor(ValidationTestResult result) {
        switch (result) {
            case FAILED:
                return "red";
            case PASSED:
                return "green";
            case UNDEFINED:
                return "orange";
            default:
                return "gray";
        }
    }

    public String getColor(ConstraintValidationDTO constraint) {
        switch (constraint.getSeverity()) {
            case ERROR:
                return "red";
            case WARNING:
                return "orange";
            default:
                return "green";
        }
    }

    public boolean isDisplayed(String attribute, ConstraintValidationDTO constraint) {
        if (constraint == null) {
            return false;
        }
        switch (attribute) {
            case "constraintID":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getConstraintID());
            case "constraintType":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getConstraintType());
            case "constraintDescription":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getConstraintDescription());
            case "formalExpression":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getFormalExpression());
            case "locationInValidatedObject":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getLocationInValidatedObject());
            case "valueInValidatedObject":
                return mandatory.matcher(attribute).matches() || StringUtils.isNotEmpty(constraint.getValueInValidatedObject());
            case "assertionIDs":
                return mandatory.matcher(attribute).matches() || CollectionUtils.isNotEmpty(constraint.getAssertionIDs());
            case "unexpectedErrors":
                return mandatory.matcher(attribute).matches() || CollectionUtils.isNotEmpty(constraint.getUnexpectedErrors());
            default:
                return false;
        }
    }

    public boolean isUndefined(ValidationTestResult result) {
        return ValidationTestResult.UNDEFINED.equals(result);
    }

    private Map<String, SeverityLevel> constraintsSeverityFilters = new THashMap<>();

    public boolean getConstraintSeverityFiltered(ValidationSubReportDTO sub) {
        return getConstraintSeverityFilter(sub) != null;
    }

    public void setConstraintSeverityFiltered(ValidationSubReportDTO sub, boolean filter) {
        setConstraintSeverityFilter(sub, filter ? "INFO" : null);
    }

    public SeverityLevel getConstraintSeverityFilter(ValidationSubReportDTO sub) {
        return this.constraintsSeverityFilters.get(sub.getName());
    }

    public void setConstraintSeverityFilter(ValidationSubReportDTO sub, String severity) {
        this.constraintsSeverityFilters.put(sub.getName(), severity == null ? null : SeverityLevel.valueOf(severity));
    }

    public List<ConstraintValidationDTO> filterConstraints(ValidationSubReportDTO sub) {
        SeverityLevel filter = getConstraintSeverityFilter(sub);
        if (filter == null) {
            return sub.getConstraints();
        } else {
            return new ValidationSubReportDTO(sub.toDomain(), filter).getConstraints();
        }
    }

    public JsonTreeRenderer.JsonTreeNode renderJsonTree(String json) {
        try {
            DocumentPresenter dp = new DocumentPresenter(new HandledDocument(json.getBytes(StandardCharsets.UTF_8)));
            this.jsonTreeRenderer = dp.getRenderer(JsonTreeRenderer.class);
            this.jsonTreeRenderer.getOptions().setCaption("");
            return this.jsonTreeRenderer.render(dp.getDocument());
        } catch (FileReadException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                    MessageFormat.format("cant-load-validation-report-content {0}", e.getMessage()));
        }
        return new JsonTreeRenderer.JsonTreeNode();
    }

    public String renderJsonTreeNode(JsonTreeRenderer.JsonTreeNode node) {
        return jsonTreeRenderer.renderNode(node);
    }

    public String renderJsonTreeNodeType(JsonTreeRenderer.JsonTreeNode node) {
        return jsonTreeRenderer.renderNodeType(node);
    }

    public int getConstraintsLimit(){
        return DEFAULT_REPORT_CONSTRAINTS_LIMIT;
    }

    public boolean isSubReportTrimmed(ValidationSubReportDTO subReportDTO){
        if(subReportDTO == null){
            return false;
        }
        return subReportDTO.getConstraints().size() > getConstraintsLimit();
    }

    public String renderXMLReport(ValidationReport report) {
        try {
            EvsMarshaller<ValidationReport> vrMarshaller = new XMLValidationReportMarshaller(SeverityLevel.INFO);
            byte[] reportBytes = vrMarshaller.marshal(report).getBytes(StandardCharsets.UTF_8);
            DocumentPresenter dp = new DocumentPresenter(new HandledDocument(reportBytes));
            XmlDocumentRenderer r = dp.getRenderer(XmlDocumentRenderer.class);
            r.getOptions().setCaption("");
            return r.render(dp.getDocument());
        } catch (MarshallingException | FileReadException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                    MessageFormat.format("cant-load-validation-report-content {0}", e.getMessage()));
        }
        return null;
    }

    public String getExceptionName(String exception){
        if(exception == null){
            return "";
        }
        String[] splited = exception.split("\\.");
        if(splited.length == 0){
            return exception;
        }
        String lastClassName = splited[splited.length-1];
        return Joiner.on(" ").skipNulls().join(lastClassName.split(SPLIT_CAMEL_CASE));
    }

    public List<ConstraintValidationDTO> getConstraintsLimited(ValidationSubReportDTO subReport, int limit){
        if(subReport == null){
            return new ArrayList<>();
        }
        if(subReport.getConstraints().size() < limit){
            return subReport.getConstraints();
        }
        return subReport.getConstraints().subList(0,limit);
    }

    public List<ConstraintValidationDTO> getConstraintsLimited(ValidationSubReportDTO subReport){
        return getConstraintsLimited(subReport, DEFAULT_REPORT_CONSTRAINTS_LIMIT);
    }

    public double getCounterWidth(int counter, int total){
        float ratio = (float) counter / (float)total;
        if(ratio <= 0.01){
            return 1;
        }
        if(ratio > 0.96){
            return 96;
        }
        return Math.floor(ratio*100);
    }

    public String generateId(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public boolean isLocationXpath(String location){
        return location != null && location.startsWith("/");
    }

    // Get line number from the following format: "line: 1 column: 1" or "line: 1, column: 1"
    public String getLineNumber(String location){
        if(location == null){
            return "";
        }
        Matcher matcher = LINE_COL_PATT.matcher(location);
        if(matcher.find()){
            return matcher.group(1);
        }
        return "";

    }

    public String getLinkToLine(String lineNumber){
        if(lineNumber == null || lineNumber.isEmpty()){
            return "";
        }
        return "#line_" + lineNumber;
    }

    public String getConstraintSeverity(ConstraintValidationDTO constraint){
        if(constraint == null){
            return "";
        }
        return constraint.getSeverity().name().toLowerCase();
    }

}
