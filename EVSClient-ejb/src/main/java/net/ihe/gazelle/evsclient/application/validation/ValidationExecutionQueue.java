package net.ihe.gazelle.evsclient.application.validation;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.validationservice.GatewayTimeoutException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ServiceConfException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.SchematronValidationServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

// TODO Implements as a singleton instead of static ; not possible for the moment without deeply refactoring validators retrieving for services
public class ValidationExecutionQueue {
    private static final long serialVersionUID = 4295091362533217182L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationExecutionQueue.class);
    private static final THashMap<Integer, BlockingQueue<Callable<Exception>>> queues = new THashMap<Integer, BlockingQueue<Callable<Exception>>>();

    public static Exception run(final ValidationServiceConf validationServiceConf, Callable<Exception> execution, String label) {
        try {
            if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("####################### run {1} {0}", validationServiceConf.getId(), label));
            BlockingQueue<Callable<Exception>> queue = getQueue(validationServiceConf);
            if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("####################### offer {3} {0} {1}[{2}]", validationServiceConf.getId(), "Q"+queue.hashCode(), queue.size(), label));
            boolean offered = queue.offer(execution,
                    getTimeout(validationServiceConf), // TODO : should be a service configuration parameter
                    TimeUnit.MILLISECONDS);
            if (!offered) {
                LOGGER.error(MessageFormat.format("####################### aborted run {3} {0} {1}[{2}]", validationServiceConf.getId(), "Q"+queue.hashCode(), queue.size(), label));
                return new GatewayTimeoutException(validationServiceConf.getName());
            }
            if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("####################### call {3} {0} {1}[{2}]", validationServiceConf.getId(), "Q"+queue.hashCode(), queue.size(), label));
            Exception exception = execution.call();
            if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("####################### poll {3} {0} {1}[{2}]", validationServiceConf.getId(), "Q"+queue.hashCode(), queue.size(), label));
            queue.poll();
            if (exception==null) {
                if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("####################### successful run {3} {0} {1}[{2}]", validationServiceConf.getId(), "Q" + queue.hashCode(), queue.size(), label));
            } else {
                LOGGER.error(MessageFormat.format("####################### unsuccessful run {3} {0} {1}[{2}] {4}", validationServiceConf.getId(), "Q" + queue.hashCode(), queue.size(), label, exception.getMessage()));
            }
            return exception;
        } catch (final Exception e) {
            LOGGER.error(MessageFormat.format("####################### failed run {1} {0}", validationServiceConf.getId(), label),e);
            return e;
        }
    }

    private static BlockingQueue<Callable<Exception>> getQueue(ValidationServiceConf validationServiceConf) throws ServiceConfException {
        BlockingQueue<Callable<Exception>> res = queues.get(validationServiceConf.getId());
        if (res == null) {
            res = new LinkedBlockingQueue<>(getLimit(validationServiceConf));
            queues.put(validationServiceConf.getId(), res);
        }
        return res;
    }
    private static int getTimeout(ValidationServiceConf validationServiceConf) throws ServiceConfException {
        //TODO : should be a service configuration parameter
        return getPreference("validation_timeout",60000);
    }
    private static int getLimit(ValidationServiceConf validationServiceConf) throws ServiceConfException {
        //TODO : should be a service configuration parameter
        int limit;
        if (SchematronValidationServiceFactory.class.isAssignableFrom(validationServiceConf.getFactoryClass())) {
            limit = getPreference("concurrent_validation_limit_by_schematron_service",1);
        } else {
            limit = getPreference("concurrent_validation_limit_by_service",100);
        }
        return limit;
    }

    private static int getPreference(String key, int defaultValue) {
        int value = defaultValue;
        try {
            if (ApplicationPreferenceManagerImpl.instance().getValues().containsKey(key)) {
                try {
                    value = Integer.parseInt(ApplicationPreferenceManagerImpl.instance().getValues().get(key).toString());
                } catch (NumberFormatException nfe) {
                    LOGGER.warn("invalid-value",key);
                }
            }
        } catch (java.lang.IllegalStateException e) {
            // in UT : No application context active
        }
        return value;
    }
}
