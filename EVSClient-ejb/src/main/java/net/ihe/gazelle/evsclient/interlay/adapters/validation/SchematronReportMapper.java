package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintValidation;
import net.ihe.gazelle.validation.Notification;

public class SchematronReportMapper extends MbvReportMapper {

    @Override
    public ConstraintValidation parseNotification(Notification notification) {
        return new ConstraintValidation(
                notification.getIdentifiant(),
                notification.getType(),
                notification.getDescription(),
                notification.getTest(),
                super.parseLocation(notification),
                null,
                super.parseAssertions(notification.getAssertions()),
                null,
                super.parsePriority(notification),
                super.parseNotificationResult(notification)
        );
    }
}