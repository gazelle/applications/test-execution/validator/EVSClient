package net.ihe.gazelle.evsclient.interlay.factory;

public class ImplementationFinder extends DefaultImplementationFinder {

    public ImplementationFinder(String rootPackage) {
        super(rootPackage);
    }

    public <T> T instantiate(Class<T> implemented, String classname) {
        try {
            Class<?> cls = Thread.currentThread().getContextClassLoader().loadClass(classname);
            return implemented.cast(instantiate(cls));
        } catch (Exception e) {
            throw new NotAnImplementation(implemented,classname);
        }
    }

}
