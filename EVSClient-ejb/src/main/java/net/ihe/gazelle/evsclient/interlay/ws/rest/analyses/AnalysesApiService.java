package net.ihe.gazelle.evsclient.interlay.ws.rest.analyses;


import net.ihe.gazelle.evsclient.interlay.dto.rest.analysis.AnalysisForCreation;
import net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-08-27T13:39:11.206Z")
public interface AnalysesApiService {
      Response createAnalysis(AnalysisForCreation analysis, String authorization, String prefer, SecurityContext securityContext)
      throws NotFoundException;
      Response getAnalysisByOid(String oid, String privacyKey, String authorization, String accept, SecurityContext securityContext)
      throws NotFoundException;
}
