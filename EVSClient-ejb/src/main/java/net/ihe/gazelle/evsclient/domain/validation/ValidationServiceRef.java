/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validation;


import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ValidationServiceRef implements ServiceRef, Serializable {

    private static final long serialVersionUID = -7482942984080329263L;

    @Column(name = "validation_service")
    private String name;

    @Column(name = "validation_service_version")
    private String version = UNKNOWN;

    @Column(name = "validator_keyword")
    private String validatorKeyword;

    @Column(name = "validator_version")
    private String validatorVersion = UNKNOWN;

    public ValidationServiceRef(ValidationServiceConf avsc, String validator) {
        this(avsc.getName(),validator);
    }

    public ValidationServiceRef(String name, String validatorKeyword) {
        this.name = name;
        this.validatorKeyword = validatorKeyword;
    }

    public ValidationServiceRef() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getValidatorKeyword() {
        return validatorKeyword;
    }

    public void setValidatorKeyword(String validatorKeyword) {
        this.validatorKeyword = validatorKeyword;
    }

    public String getValidatorVersion() {
        return validatorVersion;
    }

    public void setValidatorVersion(String validatorVersion) {
        this.validatorVersion = validatorVersion;
    }
}
