package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.Dicom3toolsValidationService;

public class Dicom3toolsValidationServiceFactory extends AbstractValidationServiceFactory<Dicom3toolsValidationService, SystemValidationServiceConf> {

    public Dicom3toolsValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, SystemValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, Dicom3toolsValidationService.class);
    }
}
