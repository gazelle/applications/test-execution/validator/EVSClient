package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.evsclient.application.Stability;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.EvsMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.JSONValidationMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.XMLValidationMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.JSONValidationReportMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import javax.activation.MimeType;

public class MarshallerFactory {

   public EvsMarshaller<ValidationReport> getValidationReportMarshaller(MimeType mimeType, SeverityLevel severity) throws InvalidMimeTypeException {
      switch (mimeType.toString()) {
         case "application/xml":
         case "application/gzl.validation.report+xml":
            return new XMLValidationReportMarshaller(severity);
         case "application/json":
         case "application/gzl.validation.report+json":
            if (Stability.isExperimentalGrade()) {
               return new JSONValidationReportMarshaller();
            }
            throw new UnsupportedOperationException("JSON marshalling is not yet implemented");
         case "application/junit+xml":
            throw new UnsupportedOperationException("JUNIT Report marshalling is not yet implemented");
         case "application/svrl+xml":
            throw new UnsupportedOperationException("Schematron Report marshalling is not yet implemented");
         default:
            throw new InvalidMimeTypeException(mimeType.toString());
      }
   }

   EvsMarshaller<Validation> getValidationMarshaller(MimeType mimeType) throws InvalidMimeTypeException {
      switch (mimeType.toString()) {
         case "application/xml":
            return new XMLValidationMarshaller();
         case "application/json":
            if (Stability.isExperimentalGrade()) {
               return new JSONValidationMarshaller();
            }
            throw new UnsupportedOperationException("JSON marshalling is not yet implemented");
         default:
            throw new InvalidMimeTypeException(mimeType.toString());
      }
   }

}
