package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.CrossValidationService;

public class CrossValidationServiceFactory extends AbstractValidationServiceFactory<CrossValidationService,EmbeddedValidationServiceConf> {

    public CrossValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, EmbeddedValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, CrossValidationService.class);
    }

}
