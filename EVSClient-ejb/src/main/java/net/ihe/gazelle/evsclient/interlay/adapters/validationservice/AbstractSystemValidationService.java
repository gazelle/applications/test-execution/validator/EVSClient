package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.SystemValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.util.SystemExecutor;

public abstract class AbstractSystemValidationService extends AbstractConfigurableValidationService<SystemValidationServiceConf>  implements SystemValidationService {

    protected SystemExecutor executor;
    protected AbstractSystemValidationService(ApplicationPreferenceManager applicationPreferenceManager, SystemValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        this.executor = new SystemExecutor();
    }

    @Override
    // TODO
    public String about() {
        return null;
    }

}
