/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


@MappedSuperclass
public abstract class ProcessingConf implements Serializable {
    private static final long serialVersionUID = 1346536037461085498L;

    @Id
    @GeneratedValue(generator = "processing_conf_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    protected Integer id;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    protected String description;

    @Column(name = "display_name")
    protected String name;

    @NotNull
    @Column(name = "validation_type")
    @Enumerated(EnumType.STRING)
    private ValidationType validationType;

    protected ProcessingConf() {
        super();
    }

    protected ProcessingConf(Integer id, String description, String name) {
        this.id = id;
        this.description = description;
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessingConf that = (ProcessingConf) o;
        return Objects.equals(id, that.id) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
