/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationQuery;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

public abstract class ValidationSearch extends AbstractGuiPermanentLink<Validation>
      implements QueryModifier<Validation>,
      UserAttributeCommon {
   private static final long serialVersionUID = -7148165286228241254L;
   public static final String PRIVATE_USER_FILTER = "privateUser";
   public static final String OWNER_USERNAME_PATH = "owner.username";
   public static final String SHARING_IS_PRIVATE_PATH = "sharing.isPrivate";
   public static final String OWNER_ORGANIZATION_PATH = "owner.organization";
   protected Pages page;
   protected Filter<Validation> filter;
   private Date startDate;
   private Date endDate;
   private String objectType;
   private Integer standardId;
   private final Map<String, String> urlParams;
   private String currentSelectDate;
   private final GazelleIdentity identity = getIdentity();

   private transient UserService userService = getUserService();

   public ValidationSearch(ApplicationPreferenceManager applicationPreferenceManager, Pages page) {
      super(applicationPreferenceManager);
      this.page = page;
      urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
      if (urlParams.get(QueryParamEvs.REF_STANDARD_ID) != null) {
         try {
            standardId = Integer.parseInt(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
         } catch (final NumberFormatException e) {
            FacesManager.instance().redirect("/error.seam");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Standard id is not an integer");
         }
      }
   }

   public Integer getStandardId() {
      return standardId;
   }

   public void setStandardId(Integer standardId) {
      this.standardId = standardId;
   }

   public void reset() {
      if (this.filter != null) {
         this.filter.clear();
      }
      this.resetDate();
   }

   public void resetDate() {
      this.startDate = null;
      this.endDate = null;
      if (this.filter != null) {
         this.filter.getPossibleValues();
      }
      this.setCurrentSelectDate("--");
   }

   public abstract FilterDataModel<Validation> getDataModel();

   public Filter<Validation> getFilter() {
      if (this.filter == null) {

         final HQLCriterionsForFilter<Validation> criterions = getCriterions();
         criterions.addQueryModifier(this);
         this.filter = new Filter<>(criterions, urlParams);
         filter.getFormatters().put(PRIVATE_USER_FILTER, new UserValueFormatter(filter, PRIVATE_USER_FILTER));
      }
      return this.filter;
   }

   protected HQLCriterionsForFilter<Validation> getCriterions() {
      final ValidationQuery validationQuery = new ValidationQuery(); // add EM to Query
      final HQLCriterionsForFilter<Validation> result = validationQuery.getHQLCriterionsForFilter();
      result.addPath("validationStatus", validationQuery.validationStatus());
      result.addPath(PRIVATE_USER_FILTER, validationQuery.owner().username());
      result.addPath("institutionKeyword", validationQuery.owner().organization());
      result.addPath("entryPoint", validationQuery.caller().entryPoint());
      result.addPath("date", validationQuery.date());
      return result;
   }

   public void setFilter(final Filter<Validation> filter) {
      this.filter = filter;
   }


   public Date getStartDate() {
      if (this.startDate == null) {
         return null;
      } else {
         return (Date) this.startDate.clone();
      }
   }

   public final void setStartDate(final Date startDate) {
      this.startDate = (Date) startDate.clone();
      this.getFilter().modified();
   }

   public Date getEndDate() {
      if (this.endDate == null) {
         return null;
      } else {
         return (Date) this.endDate.clone();
      }
   }

   public final void setEndDate(final Date endDate) {
      if (endDate != null) {
         this.endDate = (Date) endDate.clone();
         this.getFilter().modified();
      }
   }

   @Override
   public void modifyQuery(final HQLQueryBuilder<Validation> queryBuilder,
                           final Map<String, Object> filterValuesApplied) {
      final ValidationQuery validationQuery = new ValidationQuery(queryBuilder);
      if (this.startDate != null) {
         validationQuery.date().ge(this.startDate);
      }
      if (this.endDate != null) {
         validationQuery.date().lt(this.endDate);
      }
      if (this.standardId != null) {
         validationQuery.referencedStandards().referencedStandardId().eq(this.standardId);
      }


      if (identity.isLoggedIn()) {
         if (identity.hasRole("monitor_role") || identity.hasRole("admin_role")) {
            // User is logged in. He/She is a monitor or an admin. He/she can see all the logs. No need to add any
            // restrictions
         } else {
            // if the user is not monitor, then he/she can see public logs, his logs and the ones from his/her
            // colleagues
            if (Boolean.TRUE.equals(applicationPreferenceManager.getBooleanValue("show_logs_from_colleagues"))) {
               queryBuilder.addRestriction(HQLRestrictions
                     .or(HQLRestrictions.eq(OWNER_USERNAME_PATH, identity.getUsername()),
                           HQLRestrictions.eq(SHARING_IS_PRIVATE_PATH, Boolean.FALSE),
                           HQLRestrictions.eq(OWNER_ORGANIZATION_PATH, identity.getOrganisationKeyword())
                     ));
            } else {
               queryBuilder.addRestriction(HQLRestrictions
                     .or(HQLRestrictions.eq(OWNER_USERNAME_PATH, identity.getUsername()),
                           HQLRestrictions.eq(SHARING_IS_PRIVATE_PATH, Boolean.FALSE)
                     ));
            }
         }
      } else {
         // User is not logged in. Can only see the public logs
         queryBuilder.addRestriction(HQLRestrictions.eq(SHARING_IS_PRIVATE_PATH, Boolean.FALSE));
      }
   }

   public String getCurrentSelectDate() {
      return this.currentSelectDate;
   }

   public void setCurrentSelectDate(final String currentSelectDate) {
      this.currentSelectDate = currentSelectDate;
   }

   public String getObjectType() {
      return this.objectType;
   }

   public void setObjectType(final String objectType) {
      this.objectType = objectType;
   }

   @Override
   public String getResultPageUrl() {
      return getPage().getMenuLink();
   }

   protected abstract Pages getPage();

   @Override
   public String getUserName(String userId) {
      return userService.getUserDisplayNameWithoutException(userId);
   }

   private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
      in.defaultReadObject();
      // Reload transiant fields after deserialization
      userService = getUserService();
   }

   private static GazelleIdentity getIdentity() {
      return (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
   }

   private static UserService getUserService() {
      return (UserService) Component.getInstance("gumUserService");
   }

}
