package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.Hl7v3ReportMapper;

@ValidationProperties(
    name = "Hl7v3",
    types = {
            ValidationType.HL7V3
})
@ReportParser(Hl7v3ReportMapper.class)
public class Hl7v3ValidationServiceClient extends ModelBasedValidationServiceClient{

    public Hl7v3ValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
    }
}
