package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.ArrayList;
import java.util.List;

public abstract class  CrossValidatorInputsGui {
    private ValidatorInput selectedInput;

    protected CrossValidatorInputsGui() {
    }

    public abstract boolean isValidatorSelected();

    public abstract GazelleCrossValidatorType getValidator();

    protected abstract Validation getValidation();

    public List<HandledObject> getHandledObjects(ValidatorInput input) {
        if (getValidation() == null || input == null) {
            return new ArrayList<>();
        } else {
            return filterByRole(getValidation().getObjects(), input.getKeyword());
        }
    }

    private List<HandledObject> filterByRole(List<HandledObject> objects, String keyword) {
        List<HandledObject> filtered = new ArrayList<>();
        if (StringUtils.isNotEmpty(keyword)) {
            for (HandledObject o : objects) {
                if (o.getRole() != null && keyword.equals(o.getRole())) {
                    filtered.add(o);
                }
            }
        }
        return filtered;
    }

    public void export(HandledObject object) {
        try {
            byte[] content = object.getContent();
            final String fileName = object.getOriginalFileName();
            ReportExporterManager.exportToFile("text/xml", content, fileName);
        } catch (FileReadException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Can not download the file : " + e.getMessage());
        }
    }

    public void remove(HandledObject object) {
        if (getValidation() != null) {
            getValidation().getObjects().remove(object);
        }
    }

    public boolean isUploadNeeded(ValidatorInput input) {
        if (input == null) {
            return false;
        }
        return getHandledObjects(input).size() < input.getMaxQuantity();
    }

    public boolean areAllInputsWithFiles() {
        List<ValidatorInput> inputs = getValidatorInputs();
        if (CollectionUtils.isEmpty(inputs)) {
            return false;
        }
        for (ValidatorInput input : getValidatorInputs()) {
            if (input.getMinQuantity() > getHandledObjects(input).size()) {
                return false;
            }
        }
        return true;
    }

    public boolean isSelectedInputUploadNeeded() {
        return isUploadNeeded(selectedInput);
    }


    public ValidatorInput getSelectedInput() {
        return selectedInput;
    }

    public void setSelectedInput(ValidatorInput selectedInput) {
        this.selectedInput = selectedInput;
    }

    public List<HandledObject> getSelectedInputHandledObjects() {
        return getHandledObjects(selectedInput);
    }

    public List<ValidatorInput> getValidatorInputs() {
        GazelleCrossValidatorType validator = getValidator();
        if (validator==null) {
            return new ArrayList<>();
        }
        return ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsForGazelleCrossValidator(validator);
    }

    public abstract boolean isValidationDone();
}
