package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintPriority;
import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintValidation;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationSubReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public abstract class AbstractReportMapper {

   private static final Logger LOG = LoggerFactory.getLogger(AbstractReportMapper.class);

   protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy, dd MM");
   protected SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

   protected String generateUuid() {
      return UUID.randomUUID().toString();
   }

   protected Date parseValidationDate(String date, String time) {
      if (date != null && time != null) {
         try {
            Date dateDate = dateFormat.parse(date);
            Date timeDate = timeFormat.parse(time);
            return new Date(dateDate.getTime() + timeDate.getTime());

         } catch (ParseException e) {
            LOG.warn(
                  "Unable to parse validationDate {} and validationTime {} with format 'yyyy, dd MM' and 'HH:mm:ss'.",
                  date, time);
         }
      }
      return new Date();
   }

   protected ValidationTestResult parseResult(String result) {
      if ("PASSED".equalsIgnoreCase(result)) {
         return ValidationTestResult.PASSED;
      } else if ("FAILED".equalsIgnoreCase(result)) {
         return ValidationTestResult.FAILED;
      } else {
         return ValidationTestResult.UNDEFINED;
      }
   }

   protected void addDefaultStatus(String documentResult, ValidationSubReport vsr, String failure, String success,
                                 String undefined) {
      String message;
      ValidationTestResult validationTestResult = parseResult(documentResult);
      switch (validationTestResult) {
         case FAILED:
            message = failure;
            break;
         case PASSED:
            message = success;
            break;
         default:
            message = undefined;
      }
      vsr.addConstraintValidation(
            new ConstraintValidation(message, ConstraintPriority.MANDATORY, validationTestResult));
   }

}
