package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;

import java.util.ArrayList;
import java.util.List;

/**
 * A directive represent the identification of one or more call to evs services made by one request. It can be calls to
 * validation services or processing services (extension).  It does not hold the call's parameters (objects and other
 * inputs).
 */
public class Directive {

   final String serviceName;
   final String profileName;
   final List<ReferencedStandard> referencedStandards;
   final List<Directive> extensionDirectives;

   /**
    * Create a simple directive
    *
    * @param serviceName         Name of the service that will be called. Must not be null.
    * @param profileName         Name of the profile that will be invoked when the service is called <i>(As example, it
    *                            is the Validator's name in case of Validation Service)</i>.
    * @param referencedStandards List of {@link ReferencedStandard} covered by the directive. The output processing
    *                            (result of the directive) will be associated with those standards for GUI display and
    *                            statistics.
    */
   public Directive(String serviceName, String profileName, List<ReferencedStandard> referencedStandards) {
      this(serviceName, profileName, referencedStandards, null);
   }

   /**
    * Create a composite directive
    *
    * @param serviceName         Name of the service that will be called.
    * @param profileName         Name of the profile that will be invoked when the service is called <i></i>(As example,
    *                            it is the Validator's name in case of Validation Service)</i>.
    * @param referencedStandards List of {@link ReferencedStandard} covered by the directive. The output processing
    *                            (result of the directive) will be associated with those standards for GUI display and
    *                            statistics.
    * @param extensionDirectives list of extensions (sub-directives) that will be called once the main is done.
    */
   public Directive(String serviceName, String profileName, List<ReferencedStandard> referencedStandards,
                    List<Directive> extensionDirectives) {
      if(serviceName != null && !serviceName.isEmpty()) {
         this.serviceName = serviceName;
         this.profileName = profileName;
         this.referencedStandards = referencedStandards != null ? referencedStandards :
               new ArrayList<ReferencedStandard>();
         this.extensionDirectives = extensionDirectives != null ? extensionDirectives : new ArrayList<Directive>();
      } else {
         throw new IllegalArgumentException("At least the service name must be defined in a directive");
      }
   }
}
