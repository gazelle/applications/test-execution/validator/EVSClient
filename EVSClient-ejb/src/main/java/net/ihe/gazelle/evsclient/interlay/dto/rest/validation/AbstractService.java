package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.ValidationServiceRef;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;

public abstract class AbstractService<T extends ValidationServiceRef> extends AbstractDTO<T> {

  protected AbstractService() {
    super();
  }

  protected AbstractService(T domain) {
    super(domain);
  }

  /**
   * Nom du service de validation
   **/

  @Schema(name= "name", example = "SchematronValidator", required = true, description = "Nom du service de validation")
  @JsonProperty("name")
  @NotNull
  @XmlAttribute(name = "name")
  public String getName() {
    return domain.getName();
  }
  public void setName(String name) {
    domain.setName(name);
  }

  /**
   * Nom du validateur
   **/

  @Schema(name= "validator", example = "ANS - CR-BIO - v2021-02-15", required = true, description = "Nom du validateur")
  @JsonProperty("validator")
  @NotNull
  @XmlAttribute(name = "validator")
  public String getValidator() {
    return domain.getValidatorKeyword();
  }
  public void setValidator(String validator) {
    domain.setValidatorKeyword(validator);
  }

}

