package net.ihe.gazelle.evsclient.interlay.dto.view.serviceconf;

import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ReportFormat;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets.Stylesheet;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.EmbeddedValidationServiceTypes;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.SystemValidationServiceTypes;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.WebValidationServiceTypes;

import java.util.Arrays;

public class ValidationServiceGuiDto {

   private int id;

   private String description;
   private String name;
   private boolean available;
   private ValidationType validationType;
   private String validationServiceType;
   private ValidationReportType validationReportType;

   private String binaryPath;

   private String url;
   private boolean zipTransport;

   public ValidationServiceGuiDto() {
      this.validationReportType = new ValidationReportType(new Stylesheet(), ReportFormat.XML);
   }

   public ValidationServiceGuiDto(ValidationServiceConf validationServiceConf, ValidationServiceConfManager validationServiceConfManager) {
      this();
      setCommonFields(validationServiceConf, validationServiceConfManager);
      if (validationServiceConf instanceof WebValidationServiceConf) {
         setSpecificFields((WebValidationServiceConf) validationServiceConf);
      } else if (validationServiceConf instanceof SystemValidationServiceConf) {
         setSpecificFields((SystemValidationServiceConf) validationServiceConf);
      } else if (validationServiceConf instanceof EmbeddedValidationServiceConf) {
         // no specifics yet.
      }
   }

   public void setSpecificFields(SystemValidationServiceConf systemValidationServiceConf) {
      setBinaryPath(systemValidationServiceConf.getBinaryPath().trim());
   }

   public void setSpecificFields(WebValidationServiceConf webValidationServiceConf) {
      setUrl(webValidationServiceConf.getUrl());
      setZipTransport(webValidationServiceConf.isZipTransport());
   }

   private void setCommonFields(ValidationServiceConf validationServiceConf,
                                ValidationServiceConfManager validationServiceConfManager) {
      setAvailable(validationServiceConf.isAvailable());
      setDescription(validationServiceConf.getDescription());
      setName(validationServiceConf.getName());
      setId(validationServiceConf.getId());
      setValidationType(validationServiceConf.getValidationType());
      setValidationReportType(validationServiceConf.getValidationReportType());
      setValidationServiceType(validationServiceConfManager.getServiceType(validationServiceConf)==null?"":validationServiceConfManager.getServiceType(validationServiceConf).name());
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public boolean isAvailable() {
      return available;
   }

   public void setAvailable(boolean available) {
      this.available = available;
   }

   public String getBinaryPath() {
      return binaryPath;
   }

   public void setBinaryPath(String binaryPath) {
      this.binaryPath = binaryPath;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }

   public boolean isZipTransport() {
      return zipTransport;
   }

   public void setZipTransport(boolean zipTransport) {
      this.zipTransport = zipTransport;
   }

   public String getValidationServiceType() {
      return validationServiceType;
   }

   public void setValidationServiceType(String validationServiceType) {
      this.validationServiceType = validationServiceType;
   }

   public ValidationType getValidationType() {
      return validationType;
   }

   public void setValidationType(ValidationType validationType) {
      this.validationType = validationType;
   }

   public String getStylesheetReportLocation() {
      return validationReportType.getStylesheet().getLocation();
   }

   public ValidationReportType getValidationReportType() {
      return validationReportType;
   }

   public void setValidationReportType(ValidationReportType validationReportType) {
      this.validationReportType = validationReportType;
   }

   public void setStylesheetReportLocation(String stylesheetReportLocation) {
      validationReportType.getStylesheet().setLocation(stylesheetReportLocation);
   }

   public boolean isEmbeddedServiceType() {
      return Arrays.asList(EmbeddedValidationServiceTypes.values()).toString().contains(validationServiceType);
   }

   public boolean isSystemServiceType() {
      return Arrays.asList(SystemValidationServiceTypes.values()).toString().contains(validationServiceType);
   }

   public boolean isWebServiceType() {
      return Arrays.asList(WebValidationServiceTypes.values()).toString().contains(validationServiceType);
   }

   public ValidationServiceConf toValidationServiceConf(ValidationServiceConfManager validationServiceConfManager) {
      if (isEmbeddedServiceType()) {
         EmbeddedValidationServiceConf embeddedValidationServiceConf = new EmbeddedValidationServiceConf();
         validationServiceConfManager.setServiceType(embeddedValidationServiceConf,
               EmbeddedValidationServiceTypes.valueOf(validationServiceType));
         setSharedField(this, embeddedValidationServiceConf);
         return embeddedValidationServiceConf;
      } else if (isSystemServiceType()) {
         SystemValidationServiceConf systemValidationServiceConf = new SystemValidationServiceConf();
         validationServiceConfManager.setServiceType(systemValidationServiceConf,
               SystemValidationServiceTypes.valueOf(validationServiceType));
         systemValidationServiceConf.setBinaryPath(getBinaryPath().trim());
         setSharedField(this, systemValidationServiceConf);
         return systemValidationServiceConf;
      } else if (isWebServiceType()) {
         WebValidationServiceConf webValidationServiceConf = new WebValidationServiceConf();
         validationServiceConfManager.setServiceType(webValidationServiceConf,
               WebValidationServiceTypes.valueOf(validationServiceType));
         webValidationServiceConf.setUrl(getUrl().trim());
         webValidationServiceConf.setZipTransport(isZipTransport());
         setSharedField(this, webValidationServiceConf);
         return webValidationServiceConf;
      }
      return null;
   }

   private static void setSharedField(ValidationServiceGuiDto validationServiceGuiDto,
                                      ValidationServiceConf validationServiceConf) {
      validationServiceConf.setAvailable(validationServiceGuiDto.isAvailable());
      validationServiceConf.setDescription(validationServiceGuiDto.getDescription());
      validationServiceConf.setName(validationServiceGuiDto.getName());
      validationServiceConf.setId(validationServiceGuiDto.getId());
      validationServiceConf.setValidationType(validationServiceGuiDto.getValidationType());
      validationServiceConf.setValidationReportType(validationServiceGuiDto.getValidationReportType());
   }
}
