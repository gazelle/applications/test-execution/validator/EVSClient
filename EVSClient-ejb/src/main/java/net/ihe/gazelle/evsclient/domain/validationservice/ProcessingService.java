package net.ihe.gazelle.evsclient.domain.validationservice;

public interface ProcessingService {
    /**
     * Get the meta information about the Service such as the author, a disclaimer or licensing.
     *
     * @return the meta information about the Service
     */
    String about();

    /**
     * Get the name of the Service
     *
     * @return the name of the Service
     */
    String getName();

}
