package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;

public class CrossValidationBrowserBeanGui extends AbstractValidationBrowserBeanGui {

    public CrossValidationBrowserBeanGui(final ValidationServiceFacade validationServiceFacade, ApplicationPreferenceManager applicationPreferenceManager) {
        super(validationServiceFacade, applicationPreferenceManager, Pages.XVAL_RESULT);
    }

}
