package net.ihe.gazelle.evsclient.application.statistics;

import net.ihe.gazelle.evsclient.domain.statistics.IpLoc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;

public class IpLocService {
    private static final Logger LOGGER = LoggerFactory.getLogger(IpLocService.class);

    private IpLocDao ipLocDao;
    private Integer validityPeriod;
    private IpCountryProvider provider;

    public IpLocService(IpLocDao ipLocDao, Integer validityPeriod, IpCountryProvider provider) {
        this.ipLocDao = ipLocDao;
        this.validityPeriod = validityPeriod;
        this.provider = provider;
    }

    public IpLoc computeLoc(String ip) {
        IpLoc loc = ipLocDao.findLastCheckedLocByIp(ip);
        if (loc==null) {
            // If we have not found the IP in the database, then we need to query outside tools.
            try {
                String country = provider.findCountry(ip);
                loc = ipLocDao.save(new IpLoc(ip,country,new Date()));
            } catch (LocalizationException e) {
                LOGGER.error("IP location validity could not be found.", e);
            } catch (IOException e) {
                LOGGER.error("IP location could not be saved.", e);
            }
        } else {
            if (validityPeriod != null
                    && validityPeriod >= 0
                    && loc.getLastCheck().before(new Date(System.currentTimeMillis() - validityPeriod * 3600 * 24))) {
                // location is no more valid : re-check ip location
                try {
                    String country = provider.findCountry(ip);
                    if (country.equals(loc.getCountry())) {
                        loc.setLastCheck(new Date());
                    } else {
                        loc = ipLocDao.save(new IpLoc(ip,country,new Date()));
                    }
                } catch (LocalizationException e) {
                    LOGGER.error("IP location validity could not be checked ; last known location will be used.", e);
                } catch (IOException e) {
                    LOGGER.error("IP location could not be saved ; last known location will be used.", e);
                }
            }
        }
        return loc;
    }


}
