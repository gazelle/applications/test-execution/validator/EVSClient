package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request.xml;


import javax.xml.bind.annotation.*;

@XmlRootElement(namespace = "http://evsobjects.gazelle.ihe.net/", name="validation")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationDto {

    @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "validationService")
    private ValidationServiceDto validationService;

    @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "object")
    private ObjectDto object;

    public ValidationServiceDto getValidationService() {
        return validationService;
    }

    public void setValidationService(ValidationServiceDto validationService) {
        this.validationService = validationService;
    }

    public ObjectDto getObject() {
        return object;
    }

    public void setObject(ObjectDto object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "ValidationDto{" +
                "validationService=" + validationService +
                ", object=" + object +
                '}';
    }
}


