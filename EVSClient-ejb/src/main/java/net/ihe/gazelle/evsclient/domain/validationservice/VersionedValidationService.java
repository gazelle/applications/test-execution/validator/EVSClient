package net.ihe.gazelle.evsclient.domain.validationservice;

public interface VersionedValidationService extends ValidationService {
    String getServiceVersion();
}
