package net.ihe.gazelle.evsclient.domain.validationservice.extension;

import net.ihe.gazelle.evsclient.domain.validationservice.ProcessingServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ExtensionServiceConf;

public interface ExtensionServiceFactory<S extends ExtensionService, C extends ExtensionServiceConf> extends ProcessingServiceFactory<S,C> {

}
