/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "evsc_validation_service_conf", schema = "public")
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(name = "processing_conf_sequence", sequenceName = "evsc_processing_conf_id_seq", allocationSize = 1)
public abstract class ValidationServiceConf extends AtomicProcessingConf<ValidationServiceFactory<?,?>> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1346536037461085498L;


    @OneToOne(cascade = CascadeType.ALL)
    protected ValidationReportType validationReportType;

    @Column(name = "available")
    protected boolean available;

    protected ValidationServiceConf() {
        super();
    }

    public ValidationReportType getValidationReportType() {
        return validationReportType;
    }

    public void setValidationReportType(ValidationReportType validationReportType) {
        this.validationReportType = validationReportType;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public abstract Class<? extends ValidationService> getValidationServiceInterface();

    @Transient
    public ReferencedStandard getReferencedStandard() {
        return (ReferencedStandard) composedServiceConf;
    }

    @Transient
    public void setReferencedStandard(ReferencedStandard referencedStandard) {
        this.composedServiceConf = referencedStandard;
    }

//    @Transient
//    public ValidationProperties getValidationProperties(ApplicationPreferenceManager apm) {
//        try {
//            return getServiceClass(apm).getAnnotation(ValidationProperties.class);
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationServiceConf that = (ValidationServiceConf) o;
        return Objects.equals(id, that.id) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
