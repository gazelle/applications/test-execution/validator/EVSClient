package net.ihe.gazelle.evsclient.domain.validation.report;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * <p>ValidationCounters</p>
 *
 * @author fde
 * @version $Id: $Id
 */
public class ValidationCounters implements Serializable {
   private static final long serialVersionUID = 5664990816504871723L;

   private Integer numberOfConstraints = 0;
   private Integer failedWithInfoNumber = 0;
   private Integer numberOfWarnings = 0;
   private Integer numberOfErrors = 0;

   /**
    * Constructor Validation Counter Counters are already initialized to 0
    */
   ValidationCounters() {
      // Counters are set to 0
   }

   /**
    * Copy Constructor Validation Counter
    *
    * @param counters {@link ValidationCounters} objectV
    */
   public ValidationCounters(ValidationCounters counters) {
      this.numberOfErrors = counters.getNumberOfErrors();
      this.numberOfConstraints = counters.getNumberOfConstraints();
      this.failedWithInfoNumber = counters.getFailedWithInfoNumber();
      this.numberOfWarnings = counters.getNumberOfWarnings();
   }

   /**
    * <p>Getter for the field <code>numberOfConstraints</code>.</p>
    *
    * @return numberOfConstraints a {@link Integer} object.
    */
   public Integer getNumberOfConstraints() {
      return numberOfConstraints;
   }

   /**
    * <p>Setter for the field <code>numberOfConstraints</code>.</p>
    *
    * @param numberOfConstraints a {@link Integer} object.
    */
   private void setNumberOfConstraints(Integer numberOfConstraints) {
      this.numberOfConstraints = numberOfConstraints;
   }

   /**
    * <p>Getter for the field <code>failedWithInfoNumber</code>.</p>
    *
    * @return failedWithInfoNumber a {@link Integer} object.
    */
   public Integer getFailedWithInfoNumber() {
      return failedWithInfoNumber;
   }

   /**
    * <p>Setter for the field <code>failedWithInfoNumber</code>.</p>
    *
    * @param failedWithInfoNumber a {@link Integer} object.
    */
   private void setFailedWithInfoNumber(Integer failedWithInfoNumber) {
      this.failedWithInfoNumber = failedWithInfoNumber;
   }

   /**
    * <p>Getter for the field <code>numberOfWarnings</code>.</p>
    *
    * @return numberOfWarnings a {@link Integer} object.
    */
   public Integer getNumberOfWarnings() {
      return numberOfWarnings;
   }

   /**
    * <p>Setter for the field <code>numberOfWarnings</code>.</p>
    *
    * @param numberOfWarnings a {@link Integer} object.
    */
   private void setNumberOfWarnings(Integer numberOfWarnings) {
      this.numberOfWarnings = numberOfWarnings;
   }

   /**
    * <p>Getter for the field <code>numberOfErrors</code>.</p>
    *
    * @return numberOfErrors a {@link Integer} object.
    */
   public Integer getNumberOfErrors() {
      return numberOfErrors;
   }

   /**
    * <p>Setter for the field <code>numberOfErrors</code>.</p>
    *
    * @param numberOfErrors a {@link Integer} object.
    */
   private void setNumberOfErrors(Integer numberOfErrors) {
      this.numberOfErrors = numberOfErrors;
   }

   /**
    * <p>Increment numberOfConstraints</p>
    */
   void incrementNumberOfConstraints() {
      this.numberOfConstraints++;
   }

   /**
    * <p>Increment failedWithInfoNumber</p>
    */
   void incrementFailedWithInfoNumber() {
      incrementNumberOfConstraints();
      this.failedWithInfoNumber++;
   }

   /**
    * <p>Increment numberOfWarnings</p>
    */
   void incrementNumberOfWarnings() {
      incrementNumberOfConstraints();
      this.numberOfWarnings++;
   }

   /**
    * <p>Increment numberOfErrors</p>
    */
   void incrementNumberOfErrors() {
      incrementNumberOfConstraints();
      this.numberOfErrors++;
   }

   /**
    * Update Counter when adding a subReport
    *
    * @param counters {@link ValidationCounters}
    */
   void addNumbersFromSubCounters(ValidationCounters counters) {
      this.setNumberOfConstraints(this.numberOfConstraints + counters.getNumberOfConstraints());
      this.setFailedWithInfoNumber(this.failedWithInfoNumber + counters.getFailedWithInfoNumber());
      this.setNumberOfWarnings(this.numberOfWarnings + counters.getNumberOfWarnings());
      this.setNumberOfErrors(this.numberOfErrors + counters.getNumberOfErrors());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }

      ValidationCounters that = (ValidationCounters) o;

      if (!numberOfConstraints.equals(that.numberOfConstraints)) {
         return false;
      }
      if (!failedWithInfoNumber.equals(that.failedWithInfoNumber)) {
         return false;
      }
      if (!numberOfWarnings.equals(that.numberOfWarnings)) {
         return false;
      }
      return numberOfErrors.equals(that.numberOfErrors);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      int result = numberOfConstraints.hashCode();
      result = 31 * result + failedWithInfoNumber.hashCode();
      result = 31 * result + numberOfWarnings.hashCode();
      result = 31 * result + numberOfErrors.hashCode();
      return result;
   }
}

