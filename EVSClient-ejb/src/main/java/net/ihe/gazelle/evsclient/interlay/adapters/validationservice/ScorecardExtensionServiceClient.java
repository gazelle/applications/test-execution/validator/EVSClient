package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.extension.Scorecard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ScorecardServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceOperationException;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ScorecardExtensionServiceClient extends AbstractConfigurableService<ScorecardServiceConf> implements ExtensionService<Scorecard> {

    public static class Zipper {
        public static void zip(InputStream in, OutputStream os) throws IOException {
            if (os != null && in != null) {
                byte[] buffer = new byte[8192];
                int read = 0;
                try (ZipOutputStream zos = new ZipOutputStream(os);) {
                    ZipEntry entry = new ZipEntry("mbvalResult.xml");
                    zos.putNextEntry(entry);
                    while (-1 != (read = in.read(buffer))) {
                        zos.write(buffer, 0, read);
                    }
                    in.close();
                }
            }
        }
    }

    public static String getScorecardFromValidator(String oid, byte[] b64DetailedResult, String validatorBaseUrl) throws ExtensionServiceOperationException {
        if (validatorBaseUrl == null || validatorBaseUrl.isEmpty()) {
            LOGGER.error("Validator URL is null or empty, cannot contact validator");
            return null;
        }

        StringBuilder validatorUrl = new StringBuilder(validatorBaseUrl);
        if (!validatorBaseUrl.endsWith("/")) {
            validatorUrl.append('/');
        }
        validatorUrl.append("rest/scorecard/computeAndReturnImmediately");
        ClientRequest request = new ClientRequest(validatorUrl.toString(), CLIENT_EXECUTOR);
        request.accept(MediaType.APPLICATION_XML);
        request.body("text/plain", b64DetailedResult);
        request.queryParameter(QueryParam.PROCESSING_OID, oid);
        ClientResponse<String> response;
        try {
            response = request.post(String.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
        if (OK == response.getStatus()) {
            String scorecard = response.getEntity();
            response.releaseConnection();
            return scorecard;
        } else {
            MultivaluedMap<String, String> headers = response.getHeaders();
            Set<String> headernames = headers.keySet();
            for (String head: headernames){
                LOGGER.info(head);
            }
            List<String> warnings = response.getHeaders().get("Warning");
            if (warnings != null && !warnings.isEmpty()) {
                StringBuilder errorMsg = new StringBuilder();
                for (String warning : warnings) {
                    errorMsg.append(warning);
                    errorMsg.append('\n');
                }
                response.releaseConnection();
                throw new ExtensionServiceOperationException(errorMsg.toString());
            } else {
                LOGGER.error(response.getResponseStatus().name());
                response.releaseConnection();
                throw new ExtensionServiceOperationException("Unable to get the scorecard. Tool returned error code: " + response.getResponseStatus());
            }
        }
    }

    private static final ApacheHttpClient4Executor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();
    private static Logger LOGGER = LoggerFactory.getLogger(ScorecardExtensionServiceClient.class);
    public static final String UTF_8 = "UTF-8";
    private static final int OK = 200;

    public ScorecardExtensionServiceClient(ApplicationPreferenceManager applicationPreferenceManager, ScorecardServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
    }

    public byte[] process(Scorecard scorecard) throws ExtensionServiceOperationException {
        if ( getConfiguration() == null || StringUtils.isEmpty(getConfiguration().getUrl())) {
            LOGGER.error("Validator URL is null or empty, cannot contact validator");
            return null;
        }
        if (scorecard.getObject().getContent() == null || scorecard.getObject().getContent().length<1) {
            return null;
        }
        String result = getScorecardFromValidator(
                        scorecard.getValidationRef().getOid(),
                        scorecard.getObject().getContent(),
                        getConfiguration().getUrl()
                );
        return result==null?null:result.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public Scorecard createExtension(Validation validation, EVSCallerMetadata caller, OwnerMetadata owner) {
        return new Scorecard(validation, caller, owner);
    }

    @Override
    public String about() {
        return "scorecard";
    }

    @Override
    public String getName() {
        return "scorecard";
    }


}
