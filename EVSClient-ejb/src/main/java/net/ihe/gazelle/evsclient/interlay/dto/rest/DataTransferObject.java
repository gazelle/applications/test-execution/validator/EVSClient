package net.ihe.gazelle.evsclient.interlay.dto.rest;

public interface DataTransferObject<T> {
   T toDomain();
}
