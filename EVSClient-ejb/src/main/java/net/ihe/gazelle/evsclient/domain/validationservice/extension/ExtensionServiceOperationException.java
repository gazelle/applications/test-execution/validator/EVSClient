package net.ihe.gazelle.evsclient.domain.validationservice.extension;

import net.ihe.gazelle.evsclient.domain.validationservice.ServiceOperationException;

public class ExtensionServiceOperationException extends ServiceOperationException {
   private static final long serialVersionUID = -4627021740611340886L;

   public ExtensionServiceOperationException() {
   }

   public ExtensionServiceOperationException(String message) {
      super(message);
   }

   public ExtensionServiceOperationException(String message, Throwable cause) {
      super(message, cause);
   }

   public ExtensionServiceOperationException(Throwable cause) {
      super(cause);
   }
}
