package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.atna.action.pki.ws.*;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import net.ihe.gazelle.pki.validator.client.CertificateValidatorServiceStub;
import net.ihe.gazelle.pki.validator.client.SOAPExceptionException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;

import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@ValidationProperties(
        name="Certificate Service",
        types=ValidationType.TLS)
@ReportParser(PlainReportMapper.class)
public class CertificateValidationServiceClient extends AbstractValidationServiceClient implements WebValidationService, VersionedValidationService {
    private static final long DEFAULT_TIMEOUT = 60000L;

    class TLSValidator {
        private transient CertificateValidatorServiceStub stub;
        private String about;

        private CertificateValidatorServiceStub getStub() throws AxisFault {
            if (this.stub == null) {
                this.stub = new CertificateValidatorServiceStub(CertificateValidationServiceClient.this.getUrl());
                this.stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
                this.stub._getServiceClient().getOptions().setProperty("disableSoapAction", true);
                this.stub._getServiceClient().getOptions().setProperty("SO_TIMEOUT", CertificateValidationServiceClient.this.getValidationTimeout().intValue());
                this.stub._getServiceClient().getOptions().setProperty("CONNECTION_TIMEOUT", CertificateValidationServiceClient.this.getValidationTimeout().intValue());
            }
            return this.stub;
        }

        public byte[] validate(byte[] pem, String validator, boolean revocation) throws RemoteException, MarshallingException {
            final Validate param = new Validate();

            param.setCertificatesInPEMFormat(new String(pem,StandardCharsets.UTF_8));

            param.setType(validator);
            param.setCheckRevocation(revocation);

            final ValidateE validateE = new ValidateE();
            validateE.setValidate(param);
            final ValidateResponseE resultE = getStub().validate(validateE);
            final ValidateResponse validateResponse = resultE.getValidateResponse();

            final CertificateValidatorResult result = validateResponse.getDetailedResult();
            final Date validationDate = new Date();

            ValidationReport report = new CertificateReportBuilder().build(
                    CertificateValidationServiceClient.this,
                    validator,
                    CertificateValidationServiceClient.this.getServiceVersion(),
                    result,
                    revocation
            );
            return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                    .marshal(report)
                    .getBytes(StandardCharsets.UTF_8);
        }

        protected String[] getValidatorNames(String discriminator) throws RemoteException, SOAPExceptionException {
            final GetListOfValidators getList = new GetListOfValidators();
            final GetListOfValidatorsE getListE = new GetListOfValidatorsE();
            getList.setDescriminator(discriminator);
            getListE.setGetListOfValidators(getList);
            final GetListOfValidatorsResponseE responseE = tlsValidator.getStub().getListOfValidators(getListE);
            final String[] getResponse = responseE.getGetListOfValidatorsResponse().getValidators();
            return getResponse;
        }

        protected String about() {
            if (this.about==null) {
                try {
                    AboutE e = new AboutE();
                    e.setAbout(new About());
                    this.about = tlsValidator.getStub().about(e).getAboutResponse().getAbout();
                } catch (RemoteException e) {
                    this.about = "";
                }
            }
            return about;
        }


    }

    private TLSValidator tlsValidator;

    public CertificateValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        this.tlsValidator = new TLSValidator();
    }

    @Override
    protected Long getValidationTimeout() {
        Long timeout = super.getValidationTimeout();
        return timeout==null?DEFAULT_TIMEOUT:timeout;
    }

    @Override
    public String about() {
        return tlsValidator.about();
    }

    @Override
    public String getName() {
        return this.configuration.getName();
    }

    @Override
    protected List<String> getValidatorNames(String discriminator) {
        try {
            return Arrays.asList(tlsValidator.getValidatorNames(discriminator));
        } catch (final RemoteException | SOAPExceptionException e) {
            throw new ValidationErrorException(e);
        }
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/x-pem-file");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            return tlsValidator.validate(objects[0].getContent(), validator, false);
        } catch (Exception e) {
            throw new ValidationServiceOperationException(e);
        }
    }

    @Override
    public String getServiceVersion() {
        return ""; // TODO
    }

}
