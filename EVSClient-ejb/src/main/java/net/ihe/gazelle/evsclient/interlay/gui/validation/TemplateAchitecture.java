package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.validation.DetailedResult;

public class TemplateAchitecture {

    public TemplateAchitecture() {
        super();
    }

    public GazelleTreeNodeImpl<Template> getListTemplatesArchitecture(final DetailedResult detailedResult) {
        final GazelleTreeNodeImpl<Template> treeNodeImpl = new GazelleTreeNodeImpl<>();
        if (detailedResult == null || detailedResult.getTemplateDesc() == null) {
            return null;
        }
        final Template temp = detailedResult.getTemplateDesc();
        updateTreeTemplate(temp, treeNodeImpl);

        return treeNodeImpl;
    }

    private void updateTreeTemplate(final Template temp, final GazelleTreeNodeImpl<Template> treeNodeImpl) {
        treeNodeImpl.setData(temp);
        int i = 0;
        for (final Template tempchild : temp.getTemplate()) {
            final GazelleTreeNodeImpl<Template> treeNodeImplChild = new GazelleTreeNodeImpl<>();
            updateTreeTemplate(tempchild, treeNodeImplChild);
            treeNodeImpl.addChild(i, treeNodeImplChild);
            i++;
        }
    }

    public String getIconClassForTemplate(final Template template) {
        String res = "gzl-icon-times";
        if (template != null) {
            final String validationOutcome = template.getValidation();
            if (validationOutcome != null) {
                if ("Warning".equals(validationOutcome)) {
                    res = "gzl-icon-exclamation-triangle";
                } else if ("Error".equals(validationOutcome)) {
                    res = "gzl-icon-times-circle";
                } else if ("Success".equals(validationOutcome)) {
                    res = "gzl-icon-checked";
                } else if ("Report".equals(validationOutcome)) {
                    res = "gzl-icon-checked";
                }
            }
        }
        return res;
    }
}
