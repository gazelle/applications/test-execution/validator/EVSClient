/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.domain.menu;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Name("evsMenuGroup")
@Table(name = "evsc_menu_group", schema = "public")
@SequenceGenerator(name = "evsc_menu_group_sequence", sequenceName = "evsc_menu_group_id_seq", allocationSize = 1)
public class EVSMenuGroup implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1466611063793929671L;

    @Id
    @NotNull
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(generator = "evsc_menu_group_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @NotNull
    @Column(name = "label", unique = true, nullable = false)
    private String label;

    @Column(name = "is_available")
    private Boolean available;

    @Column(name = "order_in_gui")
    private Integer orderInGui;

    @ManyToMany
    @JoinTable(name = "evsc_menu_standard",
            joinColumns = @JoinColumn(name = "menu_group_id"),
            inverseJoinColumns = @JoinColumn(name = "referenced_standard_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = { "menu_group_id", "referenced_standard_id" }))
    private List<ReferencedStandard> standards;

    @Transient
    private Boolean creation = Boolean.FALSE;

    public EVSMenuGroup() {
        this.standards = new ArrayList<>();
        this.available = Boolean.FALSE;
        this.orderInGui = 0;
    }

    public Boolean getCreation() {
        return this.creation;
    }

    public void setCreation(final Boolean creation) {
        this.creation = creation;
    }

    public EVSMenuGroup save() throws PersistenceException {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final EVSMenuGroup menuGroup = entityManager.merge(this);
        entityManager.flush();
        return menuGroup;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public Boolean getAvailable() {
        return this.available != null && this.available;
    }
    public void setAvailable(final Boolean available) {
        this.available = available;
    }

    public List<ReferencedStandard> getStandards() {
        return this.standards;
    }

    public void setStandards(final List<ReferencedStandard> standards) {
        if (standards != null) {
            this.standards = new ArrayList<>(standards);
        }
        else {
            this.standards = null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.label == null ? 0 : this.label.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final EVSMenuGroup other = (EVSMenuGroup) obj;
        if (this.label == null) {
            return other.label == null;
        } else return this.label.equals(other.label);
    }

    public Integer getOrderInGui() {
        return this.orderInGui;
    }

    public void setOrderInGui(final Integer orderInGui) {
        this.orderInGui = orderInGui;
    }
}
