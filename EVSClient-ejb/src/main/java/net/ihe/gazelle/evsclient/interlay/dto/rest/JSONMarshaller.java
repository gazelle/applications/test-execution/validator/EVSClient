package net.ihe.gazelle.evsclient.interlay.dto.rest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public abstract class JSONMarshaller<D,T extends DataTransferObject<D>> extends AbstractEvsMarshaller<D> {

   protected Class<T> cls;
   protected JSONMarshaller(Class<T> cls) {
      this.cls = cls;
   }
   /**
    * {@inheritDoc}
    */
   @Override
   public void marshal(D domain, OutputStream outputStream) throws MarshallingException {
      try {
         OutputStreamWriter osw = new OutputStreamWriter(outputStream);
         osw.write(
            new ObjectMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(toDTO(domain))
         );
      } catch (IOException e) {
         throw new MarshallingException("Unable to marshall Validation Report.", e);
      }
   }

   protected abstract T toDTO(D domain);

   /**
    * {@inheritDoc}
    */
   @Override
   public D unmarshal(InputStream inputStream) throws MarshallingException {
      try {
         T dto = new ObjectMapper()
                 .reader(cls)
                 .readValue(inputStream);
         return dto.toDomain();
      } catch (IOException e) {
         throw new MarshallingException("Unable to unmarshall Validation Report.", e);
      }
   }
}