/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application.preferences;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.preferences.PreferenceProvider;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.web.MultipartFilter;
import org.kohsuke.MetaInfServices;

import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
public class GazellePreferenceProvider implements PreferenceProvider {


    ApplicationPreferenceManager applicationPreferenceManager;

    public GazellePreferenceProvider() {
        applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
    }

    @Override
    public String getString(final String key) {
        if ("user_time_zone".equals(key)) {
            return null;
        }
        return applicationPreferenceManager.getStringValue(key);
    }

    @Override
    public Integer getInteger(final String key) {

        if ("upload_max_size".equals(key)) {
            final MultipartFilter multipartFilter = (MultipartFilter) Component.getInstance(MultipartFilter.class);
            return Integer.valueOf(multipartFilter.getMaxRequestSize());
        }
        final String stringValue = applicationPreferenceManager.getStringValue(key);
        if (StringUtils.isNumeric(stringValue)) {
            return Integer.valueOf(stringValue);
        } else {
            return null;
        }
    }

    @Override
    public Boolean getBoolean(final String key) {
        return applicationPreferenceManager.getBooleanValue(key);
    }

    @Override
    public Date getDate(final String key) {
        return applicationPreferenceManager.getDateValue(key);
    }

    @Override
    public Object getObject(final Object key) {
        return null;
    }

    @Override
    public void setObject(final Object key, final Object value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setString(final String key, final String value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setInteger(final String key, final Integer value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setBoolean(final String key, final Boolean value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDate(final String key, final Date value) {
        // TODO Auto-generated method stub

    }

    @Override
    public int compareTo(final PreferenceProvider arg0) {
        return this.getWeight().compareTo(arg0.getWeight());
    }

    @Override
    public boolean equals(final Object obj){

        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PreferenceProvider pp = (PreferenceProvider) obj ;
        return this.getWeight().equals(pp.getWeight());

    }

    @Override
    public int hashCode() {
        return this.getWeight().hashCode();
    }







    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return Integer.valueOf(-100);
        } else {
            return Integer.valueOf(100);
        }
    }

}
