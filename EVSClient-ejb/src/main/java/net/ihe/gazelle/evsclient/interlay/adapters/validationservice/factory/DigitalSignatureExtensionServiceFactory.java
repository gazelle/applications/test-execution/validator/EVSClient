package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractExtensionServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.DigitalSignatureServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.DigitalSignatureExtensionServiceClient;

public class DigitalSignatureExtensionServiceFactory extends AbstractExtensionServiceFactory<DigitalSignatureExtensionServiceClient, DigitalSignatureServiceConf>  {

    public DigitalSignatureExtensionServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, DigitalSignatureServiceConf configuration) {
        super(applicationPreferenceManager, configuration, DigitalSignatureExtensionServiceClient.class);
    }
}
