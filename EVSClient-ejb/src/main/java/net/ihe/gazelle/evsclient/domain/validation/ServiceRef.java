package net.ihe.gazelle.evsclient.domain.validation;

public interface ServiceRef {
    String UNKNOWN = "Unknown";

    String getName();

    void setName(String name);

    String getVersion();

    void setVersion(String version);
}
