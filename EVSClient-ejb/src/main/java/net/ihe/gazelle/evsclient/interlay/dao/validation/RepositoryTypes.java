package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.evsclient.interlay.dao.FileRepository;

/**
 * @deprecated We do not want more dependency to the enum
 * {@link net.ihe.gazelle.evsclient.domain.validation.ValidationType}. This class is duplicating those erroneous
 * concepts.
 */
@Deprecated
public class RepositoryTypes {
    public static final FileRepository ATNA = new FileRepository("atna_repository", "atna_", ".xml");
    public static final FileRepository AUDIT_MESSAGE = new FileRepository("atna_repository", "auditMessage_", ".xml");
    public static final FileRepository CDA = new FileRepository("cda_repository", "validatedCDA_", ".xml");
    public static final FileRepository CDA_SCORECARD = new FileRepository("cda_repository", "scorecard_", ".xml");
    public static final FileRepository CDA_PDF = new FileRepository("cda_repository", "pdfFromCDA_", ".pdf");
    public static final FileRepository DEFAULT = new FileRepository("default_repository", "default_", ".xml");
    public static final FileRepository DICOM = new FileRepository("dicom_repository", "validatedDICOM_", ".dcm");
    public static final FileRepository DICOM_WEB = new FileRepository("dicom_web_repository", "dicom_web_", ".txt");
    public static final FileRepository DSUB = new FileRepository("dsub_repository", "validatedDSUB_", ".xml");
    public static final FileRepository FHIR = new FileRepository("fhir_repository", "validatedFHIR_", ".xml");
    public static final FileRepository FHIR_JSON = new FileRepository("fhir_repository", "validatedFHIR_", ".json");
    public static final FileRepository HL7V2 = new FileRepository("hl7v2_repository", "validatedHL7v2_", ".txt");
    public static final FileRepository HL7V3 = new FileRepository("hl7v3_repository", "validatedHL7v3_", ".xml");
    public static final FileRepository HPD = new FileRepository("hpd_repository", "HPD_", ".xml");
    public static final FileRepository PDF = new FileRepository("pdf_repository", "validatedPDF_", ".pdf");
    public static final FileRepository SAML = new FileRepository("saml_repository", "assertion_", ".xml");
    public static final FileRepository SVS = new FileRepository("svs_repository", "SVS_", ".xml");
    public static final FileRepository TLS = new FileRepository("tls_repository", "tlsCertificates_", ".pem");
    public static final FileRepository XDS = new FileRepository("xds_repository", "validatedXDS_", ".xml");
    public static final FileRepository XDW = new FileRepository("xdw_repository", "validatedXDW_", ".xml");
    public static final FileRepository XML = new FileRepository("xml_repository", "validatedXML_", ".xml");
    public static final FileRepository XVAL = new FileRepository("xval_repository", "validatedXVAL_", ".xml");

    private RepositoryTypes() {
    }
}
