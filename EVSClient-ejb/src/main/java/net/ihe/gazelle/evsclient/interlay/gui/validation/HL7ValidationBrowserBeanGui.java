package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;

public class HL7ValidationBrowserBeanGui extends AbstractValidationBrowserBeanGui {
    public HL7ValidationBrowserBeanGui(final ValidationServiceFacade validationServiceFacade, ApplicationPreferenceManager applicationPreferenceManager) {
        super(validationServiceFacade,applicationPreferenceManager,Pages.HL7V2_RESULT);
    }

}
