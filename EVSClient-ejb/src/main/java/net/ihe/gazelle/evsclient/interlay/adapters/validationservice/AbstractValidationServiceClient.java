package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;


import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractValidationServiceClient
      extends AbstractConfigurableValidationService<WebValidationServiceConf> implements WebValidationService {

   protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractValidationServiceClient.class);

   protected AbstractValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager,
                                             WebValidationServiceConf configuration) {
      super(applicationPreferenceManager, configuration);
   }

   @Override
   public String getUrl() {
      return configuration.getUrl();
   }

   @Override
   public boolean isZipTransport() {
      return configuration.isZipTransport();
   }

}
