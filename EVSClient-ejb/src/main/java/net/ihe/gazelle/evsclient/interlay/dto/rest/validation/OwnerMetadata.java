package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Schema(description="Métadonnées du propriétaire de cette ressource. ")
@XmlRootElement(name = "owner")
@XmlAccessorType(XmlAccessType.FIELD)
public class OwnerMetadata extends AbstractDTO<net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata> {

    public OwnerMetadata(net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata domain) {
        super(domain);
    }

    public OwnerMetadata() {
        super();
    }

    @Schema(name= "username",example = "", description = ".") // TODO
    @JsonProperty("username")
    @XmlAttribute(name = "username")
    public String getUsername() {
        return domain.getUsername();
    }

    @Schema(name= "organization",example = "", description = ".") // TODO
    @JsonProperty("organization")
    @XmlAttribute(name = "organization")
    public String getOrganization() {
        return domain.getOrganization();
    }
}
