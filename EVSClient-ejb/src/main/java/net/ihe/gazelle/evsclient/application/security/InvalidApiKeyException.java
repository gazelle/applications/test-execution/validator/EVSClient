package net.ihe.gazelle.evsclient.application.security;

public class InvalidApiKeyException extends Exception {
   private static final long serialVersionUID = 1605419276533234311L;

   public InvalidApiKeyException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public InvalidApiKeyException(String s) {
      super(s);
   }
}
