package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;

/**
 * Mapper used to transform original reports returned by Validation Services into {@link ValidationReport}.
 * (The official validation report pivot that must be used by EVSClient and is components).
 *
 * It is recommended to create one implementation of this interface per ValidationService application.
 *
 */
public interface ValidationReportMapper {

   String DEFAULT_DISCLAIMER = "This report comes without licensing. \nUnless required by applicable law or agreed to in " +
          "writing, this report is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n\n" +
          "This disclaimer is by default added to any report stored or transformed by EVSClient if no other is provided by the Validation Service.";
   String UNKNOWN = "Unknown";

    /**
     * Transform an original report as {@link ValiationReport}.
     * @param originalReport byte array of the original report. Usually in text (plain, XMl or JSON).
     * @return
     * @throws ReportTransformationException
     */
    ValidationReport transform(byte[] originalReport) throws ReportTransformationException;

}
