/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.pages.Authorization;
import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

    ALL,

    IS_LOGGED_IF_NEEDED,

    LOGGED,

    ADMIN,

    MONITOR,

    SCHEMATRON, CROSS_VALIDATION;

    @Override
    public boolean isGranted(final Object... context) {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        boolean userIsLoggedIfNeeded = !applicationPreferenceManager.getBooleanValue("user_need_to_be_logged_in")
                || Identity.instance().isLoggedIn();
        switch (this) {
            case ALL:
                return true;
            case IS_LOGGED_IF_NEEDED:
                return userIsLoggedIfNeeded;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().hasRole("admin_role");
            case MONITOR:
                return Identity.instance().hasRole("monitor_role");
            case SCHEMATRON:
                return applicationPreferenceManager.getBooleanValue("display_SCHEMATRON_menu")
                        && userIsLoggedIfNeeded;
            case CROSS_VALIDATION:
                return applicationPreferenceManager.getBooleanValue("x_validation_enabled")
                        && userIsLoggedIfNeeded;
            default:
                return false;
        }
    }
}
