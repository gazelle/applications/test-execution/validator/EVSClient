package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;

import java.util.Locale;

public class Stability {
    public enum Level {
        PRODUCTION,EXPERIMENTAL,INVESTIGATION
    }
    private static Level grade;
    static {
        try {
            grade = Level.valueOf(
                    new ApplicationPreferenceManagerImpl().getStringValue("application_grade")
                            .toUpperCase(Locale.ROOT)
            );
        } catch (Exception e) {
            grade = Level.PRODUCTION;
        }
    }
    public static boolean isProductionGrade() {
        return grade.equals(Level.PRODUCTION);
    }
    public static boolean isExperimentalGrade() {
        return grade.ordinal()<=Level.EXPERIMENTAL.ordinal();
    }
    public static boolean isInvestigationGrade() {
        return grade.ordinal()<=Level.INVESTIGATION.ordinal();
    }
}
