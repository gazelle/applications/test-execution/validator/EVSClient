package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import net.ihe.gazelle.evsclient.domain.validation.request.ValidationItem;


@JsonRootName(value = "validationItem")
public class ValidationItemDTO  extends ValidationItem {


    public ValidationItemDTO() {
    }

    public ValidationItemDTO(ValidationItem validationItem){
        this.setItemId(validationItem.getItemId());
        this.setContent(validationItem.getContent());
        this.setLocation(validationItem.getLocation());
        this.setRole(validationItem.getRole());
    }

    @JsonProperty(value = "itemId")
    @Override
    public String getItemId() {
        return super.getItemId();
    }

    @JsonProperty(value = "content")
    @Override
    public byte[] getContent() {
        return super.getContent();
    }

    @JsonProperty(value = "role")
    @Override
    public String getRole() {
        return super.getRole();
    }

    @JsonProperty(value = "location")
    @Override
    public String getLocation() {
        return super.getLocation();
    }

    @JsonIgnore
    @Override
    public boolean isContentValid() {
        return super.isContentValid();
    }


    @JsonIgnore
    @Override
    public boolean isItemIdValid() {
        return super.isItemIdValid();
    }


    @JsonIgnore
    @Override
    public boolean isLocationValid() {
        return super.isLocationValid();
    }

    @JsonIgnore
    @Override
    public boolean isRoleValid() {
        return super.isRoleValid();
    }

    @JsonIgnore
    @Override
    public boolean isRoleDefined() {
        return super.isRoleDefined();
    }

    @JsonIgnore
    @Override
    public boolean isURLValid(String url) {
        return super.isURLValid(url);
    }

    @JsonIgnore
    @Override
    public boolean isValid() {
        return super.isValid();
    }

}
