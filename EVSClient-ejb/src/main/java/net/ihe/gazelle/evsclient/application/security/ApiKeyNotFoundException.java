package net.ihe.gazelle.evsclient.application.security;

public class ApiKeyNotFoundException extends Exception {
   private static final long serialVersionUID = -2636949158359924059L;

   public ApiKeyNotFoundException(String s) {
      super(s);
   }
}
