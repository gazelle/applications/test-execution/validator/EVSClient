package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.*;
import java.util.Objects;

@Schema(description="Un objet est la cible d'une validation ou d'une analyse. C'est communément un document, un message (requête ou réponse) ou une ressource devant être validé. ")
@XmlRootElement(name = "object")
@XmlAccessorType(XmlAccessType.NONE)
public class HandledObject extends AbstractDTO<net.ihe.gazelle.evsclient.domain.processing.HandledObject> {

  public HandledObject(net.ihe.gazelle.evsclient.domain.processing.HandledObject domain) {
    super(domain);
  }

  public HandledObject() {
    super();
  }

  /**
   * Type de l&#39;objet. Peut être défini lors de la création d&#39;un objet ou calculé par MCA.  *Le type de l&#39;objet permet à EVSClient d&#39;identifier quel service de validation et quel validateur appliquer. La liste des types d&#39;objets disponible est visible depuis EVSClient.* 
   **/
  
  @Schema(name= "objectType",example = "ANS-CR-BIO", description = "Type de l'objet. Peut être défini lors de la création d'un objet ou calculé par MCA.  *Le type de l'objet permet à EVSClient d'identifier quel service de validation et quel validateur appliquer. La liste des types d'objets disponible est visible depuis EVSClient.* ")
  @JsonProperty("objectType")
  @XmlAttribute(name = "objectType")
  public String getObjectType() {
    return null; // TODO
  }
  public void setObjectType(String objectType) {
    // TODO
  }

  /**
   * Nom du fichier original, sera utilisé pour définir le nom de la pièce-jointe par EVSClient en cas de téléchargement de l&#39;objet
   **/
  
  @Schema(name= "originalFileName",example = "crbio.xml", description = "Nom du fichier original, sera utilisé pour définir le nom de la pièce-jointe par EVSClient en cas de téléchargement de l'objet")
  @JsonProperty("originalFileName")
  @XmlAttribute(name = "originalFileName")
  public String getOriginalFileName() {
    return domain.getOriginalFileName();
  }
  public void setOriginalFileName(String originalFileName) {
    domain.setOriginalFileName(originalFileName);
  }

  /**
   * Contenu de l&#39;objet. Octets encodé en base 64. En cas de chaîne de caractères, le contenu doit décodable en UTF-8 par EVSClient. 
   **/
  
  @Schema(name= "content",example = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiID8+Cjw/eG1sLXN0eWxlc2hlZXQg dHlwZT0idGV4dC94c2wiICBocmVmPSIjIj8+Cjx4c2w6c3R5bGVzaGVldCB2ZXJzaW9uPSIxLjAi IAoJeG1sbnM6eHNsPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L1hTTC9UcmFuc2Zvcm0iCgl4bWxu czpleHNsPSJodHRwOi8vZXhzbHQub3JnL2NvbW1vbiIgCgl4bWxuczp4c2k9Imh0dHA6Ly93d3cu dzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIgoJeG1sbnM6ZGF0YT0idXJuOmFzaXAtc2Fu dGU6Y2ktc2lzIiAKCXhtbG5zOmM9InVybjpobDctb3JnOnYzIgoJeG1sbnM6bGFiPSJ1cm46b2lk OjEuMy42LjEuNC4xLjE5Mzc2LjEuMy4yIgoJeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIw MDAvMDkveG1sZHNpZyMiPgoKCTxkYXRhOkNvbnRlbnU+CgkJPGM6Q2xpbmljYWxEb2N1bWVudCB4 bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4bWxu czpsYWI9InVybjpvaWQ6MS4zLjYuMS40LjEuMTkzNzYuMS4zLjIiIGNsYXNzQ29kZT0iRE9DQ0xJ TiIgbW9vZENvZGU9IkVWTiIgeG1sbnM6Yz0idXJuOmhsNy1vcmc6djMiPgoJCTwvYzpDbGluaWNh bERvY3VtZW50PgoJPC9kYXRhOkNvbnRlbnU+CjwveHNsOnN0eWxlc2hlZXQ+ ", required = true, description = "Contenu de l'objet. Octets encodé en base 64. En cas de chaîne de caractères, le contenu doit décodable en UTF-8 par EVSClient. ")
  @JsonProperty("content")
  @NotNull
  @Pattern(regexp="^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "content")
  public byte[] getContent() {
    return domain.getContent();
  }
  public void setContent(byte[] content) {
    domain.setContent(content);
  }

  @Schema(name= "ref", description = "Reference de l'objet validé pour réutilisation par une autre validation")
  @JsonProperty("ref")
  @XmlAttribute(name = "ref")
  public String getRef() {
    if (domain.getId()!=null && domain.getId()>0) {
      return domain.getId().toString();
    }
    return null;
  }

  public void setRef(String ref) {
    this.domain.setId(Integer.parseInt(ref));
  }

  @Schema(name= "role", description = "Rôle de l'objet lors de la validation")
  @JsonProperty("role")
  @XmlAttribute(name = "role")
  public String getRole() {
      return domain.getRole();
  }
  public void setRole(String role) {
    domain.setRole(role);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HandledObject object = (HandledObject) o;
    return Objects.equals(getObjectType(), object.getObjectType()) &&
        Objects.equals(getOriginalFileName(), object.getOriginalFileName()) &&
        Objects.equals(getContent(), object.getContent());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getObjectType(), getOriginalFileName(), getContent());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Object {\n");
    
    sb.append("    objectType: ").append(toIndentedString(getObjectType())).append("\n");
    sb.append("    originalFileName: ").append(toIndentedString(getOriginalFileName())).append("\n");
    sb.append("    role: ").append(toIndentedString(getRole())).append("\n");
    sb.append("    content: ").append(toIndentedString(getContent())).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

