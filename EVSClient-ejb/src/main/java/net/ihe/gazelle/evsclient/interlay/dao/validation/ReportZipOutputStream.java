package net.ihe.gazelle.evsclient.interlay.dao.validation;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Custom outputStream dedicated to compress a report into a zip archive.
 * It will compress content on the fly while writing the report on the file system.
 *
 * @author ceoche
 */
public class ReportZipOutputStream extends ZipOutputStream {

   /**
    * Create a {@link ReportZipOutputStream}
    *
    * @param reportArchiveOutputStream {@link FileOutputStream} that point to the archive file to write.
    * @param reportFileName Name of the report file inside the ZIP archive.
    * @throws IOException in case of I/O writing failure.
    */
   ReportZipOutputStream(FileOutputStream reportArchiveOutputStream, String reportFileName) throws IOException {
      super(reportArchiveOutputStream);
      putNextEntry(new ZipEntry(reportFileName));
   }

}
