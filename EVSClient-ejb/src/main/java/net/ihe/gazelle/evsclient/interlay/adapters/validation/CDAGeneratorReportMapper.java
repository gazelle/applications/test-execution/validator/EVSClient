package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationOverview;
import net.ihe.gazelle.validation.ValidationResultsOverview;


public class CDAGeneratorReportMapper extends MbvReportMapper {

   // CDAGenerator is inverting ValidationService and ValidationEngine compared to other MBV DetailedResult reports.

   @Override
   protected ValidationOverview parseSpecificOverview(ValidationResultsOverview originalOverview) {
      String validationServiceName = originalOverview.getValidationEngine();
      String validationServiceVersion = originalOverview.getValidationEngineVersion();
      String validatorID = originalOverview.getValidationServiceName();

      ValidationOverview overview = new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
            parseValidationDate(originalOverview.getValidationDate(), originalOverview.getValidationTime()),
            validationServiceName != null ? validationServiceName : ValidationReportMapper.UNKNOWN,
            validationServiceVersion != null ? validationServiceVersion : ValidationReportMapper.UNKNOWN,
            validatorID != null ? validatorID : ValidationReportMapper.UNKNOWN
      );
      overview.setValidatorVersion(originalOverview.getValidationServiceVersion());
      return overview;
   }

}
