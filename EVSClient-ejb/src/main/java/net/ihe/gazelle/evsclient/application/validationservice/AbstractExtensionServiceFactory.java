package net.ihe.gazelle.evsclient.application.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ExtensionServiceConf;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractExtensionServiceFactory<T extends ExtensionService, C extends ExtensionServiceConf>
   extends AbstractServiceFactory<T,C> implements ExtensionServiceFactory<T, C> {


   // FIXME configuration for ValidationService Instance should be a parameter of getValidationService and not of the
   //  factory constructor.
   protected AbstractExtensionServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, C configuration,
                                          Class<T> extensionServiceClass) {
      super(applicationPreferenceManager,configuration,extensionServiceClass);
   }

   @Override
   public T getService() {
      try {
         return serviceClass
                 .getDeclaredConstructor(ApplicationPreferenceManager.class, getConfigurationClass())
                 .newInstance(applicationPreferenceManager, configuration);
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
         throw new RuntimeException(
                 "Unable To instantiate validation service for class " + serviceClass, e);
      }
   }

   @Override
   public Class<T> getServiceClass() {
      return (Class<T>) ((ParameterizedTypeImpl) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
   }
}
