package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

public interface WebServiceConf {
    String getUrl();

    void setUrl(String url);

    boolean isZipTransport();

    void setZipTransport(boolean zipTransport);
}
