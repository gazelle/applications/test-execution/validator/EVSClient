package net.ihe.gazelle.evsclient.domain.validation.report;

import java.io.Serializable;

/**
 * Metadata class
 *
 * @author fde
 * @version $Id: $Id
 */
public class Metadata implements Serializable {

    private static final long serialVersionUID = 5625808820252534567L;

    private String name;
    private String value;

    /**
     * Constructor with both parameter : name and value
     *
     * @param name  can not be null
     * @param value can be null
     * @throws IllegalArgumentException if name is null
     */
    public Metadata(String name, String value) {
        setName(name);
        setValue(value);
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link String} object.
     * @throws IllegalArgumentException if name is null
     */
    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can not be null");
        }
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param value a {@link String} object.
     */
    private void setValue(String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Metadata metadata = (Metadata) o;

        if (!name.equals(metadata.name)) {
            return false;
        }
        return value != null ? value.equals(metadata.value) : metadata.value == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
