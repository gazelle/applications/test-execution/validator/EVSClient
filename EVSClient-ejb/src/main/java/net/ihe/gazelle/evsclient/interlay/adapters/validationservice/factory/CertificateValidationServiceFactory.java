package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.CertificateValidationServiceClient;

public class CertificateValidationServiceFactory extends AbstractValidationServiceFactory<CertificateValidationServiceClient,WebValidationServiceConf> {

    public CertificateValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, CertificateValidationServiceClient.class);
    }
}
