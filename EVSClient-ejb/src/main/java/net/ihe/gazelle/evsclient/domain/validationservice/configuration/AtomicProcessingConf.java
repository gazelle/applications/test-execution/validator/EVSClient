/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.ProcessingServiceFactory;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class AtomicProcessingConf<F extends ProcessingServiceFactory<?,?>> extends ProcessingConf {
    /**
     *
     */
    private static final long serialVersionUID = 1346536037461085498L;

    @Column(name = "factory_canonical_classname")
    protected String factoryClassname;

    @Transient
    protected transient CompositeProcessingConf<?> composedServiceConf;

    protected AtomicProcessingConf() {
        super();
    }

    protected AtomicProcessingConf(Integer id, String description, String name, String factoryClassname) {
        super(id, description, name);
        this.factoryClassname = factoryClassname;
    }

    public String getFactoryClassname() {
        return factoryClassname;
    }

    public void setFactoryClassname(String factoryClassname) {
        this.factoryClassname = factoryClassname;
    }

    @Transient
    public Class<F> getFactoryClass() throws ServiceConfException {
        try {
            return (Class<F>) Class.forName(getFactoryClassname());
        } catch (ClassNotFoundException e) {
            throw new ServiceConfException(String.format("Configuration error: Unable to find factory class %s for " +
                    "service %s", getFactoryClassname(), name), e);
        }
    }
}
