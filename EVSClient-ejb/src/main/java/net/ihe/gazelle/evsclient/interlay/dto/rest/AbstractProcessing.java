package net.ihe.gazelle.evsclient.interlay.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.CallerMetadata;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.HandledObject;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.OwnerMetadata;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.SharingMetadata;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Schema(description = "Model abstrait et commun aux ressources **Validation** et **Analysis**.")
public abstract class AbstractProcessing<T extends net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing>
      extends AbstractDTO<T> {

   protected AbstractProcessing(T domain) {
      super(domain);
   }

   protected AbstractProcessing() {
      super();
   }

   @Schema(name = "oid", description = "")
   @JsonProperty("oid")
   @XmlAttribute(name = "oid")
   public String getOid() {
      return domain.getOid();
   }

   @Schema(name = "date", description = "")
   @JsonProperty("date")
   @XmlAttribute(name = "date")
   public Date getDate() {
      return domain.getDate();
   }

  /**
   **/
  @Schema(name= "object",required = true, description = "")
  @JsonProperty("objects")
  @NotNull
  @XmlElement(name = "object")
  public List<HandledObject> getObjects() {
    List<HandledObject> objects = new ArrayList<>();
    for(net.ihe.gazelle.evsclient.domain.processing.HandledObject object : domain.getObjects()) {
      objects.add(new HandledObject(object));
    }
    return objects;
  }

  public void setObjects(List<HandledObject> objects) {
     List<net.ihe.gazelle.evsclient.domain.processing.HandledObject> domainObjects = new ArrayList<>();
     for(HandledObject object: objects) {
        domainObjects.add(object.toDomain());
     }
     domain.setObjects(domainObjects);
  }

   /**
    * Métadonnées du propriétaire de cette ressource
    **/
   @Schema(name = "owner", description = "Métadonnées du propriétaire de cette ressource")
   @JsonProperty("owner")
   @XmlElement(name = "owner")
   public OwnerMetadata getOwner() {
      return domain.getOwner() != null ? new OwnerMetadata(domain.getOwner()) : null;
   }

   /**
    * Métadonnées sur les informations de partage de la ressource
    **/
   @Schema(name = "sharing", description = "Métadonnées sur les informations de partage de la ressource")
   @JsonProperty("sharing")
   @XmlElement(name = "sharing")
   public SharingMetadata getSharing() {
      return new SharingMetadata(domain.getSharing());
   }

   /**
    * Métadonnées sur la source de la demande de validation ou d&#39;analyse
    **/
   @Schema(name = "caller", description = "Métadonnées sur la source de la demande de validation ou d'analyse")
   @JsonProperty("caller")
   @XmlElement(name = "caller")
   public CallerMetadata getCaller() {
      return new CallerMetadata(domain.getCaller());
   }

   @Override
   public boolean equals(java.lang.Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      AbstractProcessing<?> abstractProcessing = (AbstractProcessing<?>) o;
      return Objects.equals(getOid(), abstractProcessing.getOid()) &&
            Objects.equals(getDate(), abstractProcessing.getDate()) &&
            Objects.equals(getObjects(), abstractProcessing.getObjects()) &&
            Objects.equals(getOwner(), abstractProcessing.getOwner()) &&
            Objects.equals(getSharing(), abstractProcessing.getSharing()) &&
            Objects.equals(getCaller(), abstractProcessing.getCaller());
   }

   @Override
   public int hashCode() {
      return Objects.hash(getOid(), getDate(), getObjects(), getOwner(), getSharing(), getCaller());
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class AbstractProcessing {\n");

      sb.append("    oid: ").append(toIndentedString(getOid())).append("\n");
      sb.append("    date: ").append(toIndentedString(getDate())).append("\n");
      sb.append("    objects: ").append(toIndentedString(getObjects())).append("\n");
      sb.append("    owner: ").append(toIndentedString(getOwner())).append("\n");
      sb.append("    sharing: ").append(toIndentedString(getSharing())).append("\n");
      sb.append("    caller: ").append(toIndentedString(getCaller())).append("\n");
      sb.append("}");
      return sb.toString();
   }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  protected String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

