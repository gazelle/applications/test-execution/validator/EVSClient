/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension;

import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.AtomicProcessingConf;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "evsc_extension_service_conf", schema = "public")
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(name = "processing_conf_sequence", sequenceName = "evsc_processing_conf_id_seq", allocationSize = 1)
public abstract class ExtensionServiceConf<E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf, T extends ExtensionServiceFactory<S,C>> extends AtomicProcessingConf<T> implements Serializable {

    private static final long serialVersionUID = 1346536037461085498L;

    @Column(name = "available")
    protected boolean available;


    /**
     * Protected constructor.
     * Every validation extension configuration class must implements a public constructor
     * which have only one parameter of Validation type.
     *   public MyExtensionServiceConf(Validation validation) {
     *    ...
     *   }
     * @param id
     * @param description
     * @param name
     * @param factoryClassname
     * @param available
     */
    protected ExtensionServiceConf(Integer id, String description, String name, String factoryClassname, boolean available) {
        super(id, description, name, factoryClassname);
        this.available = available;
    }

    protected ExtensionServiceConf() {
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExtensionServiceConf<?,?,?,?> that = (ExtensionServiceConf<?,?,?,?>) o;
        return Objects.equals(id, that.id) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
