package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.SchematronReportMapper;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import net.ihe.gazelle.sch.validator.ws.client.SchematronValidatorServiceStub;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.transport.http.HTTPConstants;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@ValidationProperties(
      name = "Schematron",
      types = {
            ValidationType.CDA,
            ValidationType.XML,
            ValidationType.WS_TRUST
      })
@ReportParser(SchematronReportMapper.class)
public class SchematronValidationServiceClient extends AbstractValidationServiceClient implements WebValidationService {

   private SchematronValidatorServiceStub schematronValidatorServiceStub;

   public SchematronValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
      super(applicationPreferenceManager, configuration);
      try {
         int validationTimeout = getValidationTimeout().intValue();
         schematronValidatorServiceStub =
               new SchematronValidatorServiceStub(getUrl());
         schematronValidatorServiceStub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION,
               Boolean.TRUE);
         schematronValidatorServiceStub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, validationTimeout);
         schematronValidatorServiceStub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, validationTimeout);
      } catch (AxisFault axisFault) {
         LOGGER.error("Could not instantiate schematron validator at : " + getUrl());
      }
   }

   @Override
   public String about() {
      return getName();
   }

   protected List<String> getValidatorNames(String validatorFilter) {
      try {
         List<String> validatorNames = new ArrayList<>();
         final SchematronValidatorServiceStub.GetListOfValidators param =
               new SchematronValidatorServiceStub.GetListOfValidators();
               param.setArg0(validatorFilter != null ? validatorFilter : "");
         final SchematronValidatorServiceStub.GetListOfValidatorsE paramE =
               new SchematronValidatorServiceStub.GetListOfValidatorsE();
         paramE.setGetListOfValidators(param);

         final String[] schematrons =
               schematronValidatorServiceStub.getListOfValidators(paramE).getGetListOfValidatorsResponse().get_return();
         if (schematrons != null && schematrons.length > 0) {
            validatorNames.addAll(Arrays.asList(schematrons));
            Collections.sort(validatorNames);
         }
         return validatorNames;
      } catch (RemoteException | SOAPExceptionException e) {
         throw new ValidationErrorException(e);
      }
   }

   @Override
   public String getName() {
      return "Schematron Validation Service";
   }

   @Override
   public List<String> getSupportedMediaTypes() {
      return Arrays.asList("application/xml");
   }

   @Override
   public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
      final SchematronValidatorServiceStub.ValidateBase64Document requestParams = new SchematronValidatorServiceStub.ValidateBase64Document();
      requestParams.setBase64Document(DatatypeConverter.printBase64Binary(objects[0].getContent()));
      requestParams.setValidator(validator);
      final SchematronValidatorServiceStub.ValidateBase64DocumentE validateBase64DocumentE =
            new SchematronValidatorServiceStub.ValidateBase64DocumentE();
      validateBase64DocumentE.setValidateBase64Document(requestParams);
      final SchematronValidatorServiceStub.ValidateBase64DocumentResponseE responseE;
      try {
         responseE = schematronValidatorServiceStub.validateBase64Document(validateBase64DocumentE);
      } catch (RemoteException | SOAPExceptionException e) {
         throw new ValidationServiceOperationException(e);
      }

      byte[] validationReport = responseE.getValidateBase64DocumentResponse().get_return().getBytes(StandardCharsets.UTF_8);

      if (validationReport.length > 0 && configuration.getValidationReportType().getStylesheet() != null) {
         validationReport = (new String(validationReport)
               .replace("XSLT_LOCATION", configuration.getValidationReportType().getStylesheet().getLocation()))
               .getBytes(StandardCharsets.UTF_8);
      }

      return validationReport;
   }
}
