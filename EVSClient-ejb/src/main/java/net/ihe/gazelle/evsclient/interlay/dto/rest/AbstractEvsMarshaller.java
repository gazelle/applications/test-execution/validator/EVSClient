package net.ihe.gazelle.evsclient.interlay.dto.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public abstract class AbstractEvsMarshaller<D> implements EvsMarshaller<D> {

   private boolean indentation = true;
   private boolean completeDocument = true;

   /**
    * {@inheritDoc}
    */
   @Override
   public final boolean isIndentation() {
      return indentation;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public final EvsMarshaller<D> setIndentation(boolean indentation) {
      this.indentation = indentation;
      return this;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public final boolean isCompleteDocument() {
      return completeDocument;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public final EvsMarshaller<D> setCompleteDocument(boolean completeDocument) {
      this.completeDocument = completeDocument;
      return this;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String marshal(D domain) throws MarshallingException {
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
         marshal(domain, baos);
         return new String(baos.toByteArray(), StandardCharsets.UTF_8);
      } catch (IOException e) {
         throw new MarshallingException("Unable to marshal Validation Report.", e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public D unmarshal(String exportValue) throws MarshallingException {
      try (ByteArrayInputStream bais = new ByteArrayInputStream(exportValue.getBytes(StandardCharsets.UTF_8))) {
         return unmarshal(bais);
      } catch (IOException e) {
         throw new MarshallingException("Unable to unmarshall ValidationReport.", e);
      }
   }
}
