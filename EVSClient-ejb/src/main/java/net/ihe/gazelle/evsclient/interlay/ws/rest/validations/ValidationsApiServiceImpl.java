package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.security.ApiKeyIdentity;
import net.ihe.gazelle.evsclient.application.security.ApiKeyManager;
import net.ihe.gazelle.evsclient.application.security.InvalidApiKeyException;
import net.ihe.gazelle.evsclient.application.validation.Directive;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.application.validationservice.GatewayTimeoutException;
import net.ihe.gazelle.evsclient.application.validationservice.ValidationServiceNotAvailableException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ReferencedStandardNotFoundException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.interlay.dto.rest.EvsMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ExtensionService;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.HandledObject;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationDTO;
import net.ihe.gazelle.evsclient.interlay.factory.ApplicationFactory;
import net.ihe.gazelle.evsclient.interlay.factory.EvsCommonApplicationFactory;
import net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URI;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationsApiServiceImpl implements ValidationsApiService {

   private static final Logger LOG = LoggerFactory.getLogger(ValidationsApiServiceImpl.class);
   private static final String RESPOND_ASYNC = "respond-async";
   private static MimeType[] acceptedValidationMimeTypes;
   private static MimeType[] acceptedValidationReportMimeTypes;

   static {
      try {
         acceptedValidationMimeTypes = new MimeType[]{
               new MimeType("application/xml"),
               new MimeType("application/json")
         };
         acceptedValidationReportMimeTypes = new MimeType[]{
               new MimeType("application/xml"),
               new MimeType("application/gzl.validation.report+xml"),
               new MimeType("application/json"),
               new MimeType("application/gzl.validation.report+json"),
               new MimeType("application/junit+xml"),
               new MimeType("application/svrl+xml")
         };
      } catch (MimeTypeParseException e) {
         e.printStackTrace();
      }
   }

   private final MarshallerFactory marshallerFactory = new MarshallerFactory();
   private final Pattern authenticationParser = Pattern.compile("GazelleAPIKey\\s+([^\\s]+)");
   private final Pattern preferParser = Pattern.compile("\\s*(\\w+)\\s*(?:=\\s*(\\w+))?");
   private final Comparator<Validator> equalizer = new Comparator<Validator>() {
      @Override
      public int compare(Validator a, Validator b) {
         return (a == null && b == null)
               || (a != null && b != null &&
               ((a.getKeyword() == null && b.getKeyword() == null) || (a.getKeyword().equals(b.getKeyword())))
         ) ? 0 : 1;
      }
   };

   private ApplicationFactory evsApplicationFactory;
   private EvsCommonApplicationFactory evsCommonApplicationFactory;
   private ApiKeyManager apiKeyManager;
   private ApplicationPreferenceManager applicationPreferenceManager;

   public ValidationsApiServiceImpl(ApplicationFactory evsApplicationFactory,
                                    EvsCommonApplicationFactory evsCommonApplicationFactory,
                                    ApiKeyManager apiKeyManager,
                                    ApplicationPreferenceManager applicationPreferenceManager) {
      this.evsApplicationFactory = evsApplicationFactory;
      this.evsCommonApplicationFactory = evsCommonApplicationFactory;
      this.apiKeyManager = apiKeyManager;
      this.applicationPreferenceManager = applicationPreferenceManager;
   }

   @Override
   @Transactional
   public Response createValidation(ValidationDTO validationRequest, String authorization, String prefer,
                                    SecurityContext securityContext, HttpServletRequest httpRequest)
         throws NotFoundException {
      ValidationServiceFacade facade = evsApplicationFactory.getValidationServiceFacade();

      boolean async = parsePreferHeader(prefer).containsKey(RESPOND_ASYNC);
      EVSCallerMetadata caller = evsCommonApplicationFactory.getCallerMetadataFactory()
            .getCallerMetadata(null, null, null, EntryPoint.WS);

      try {
         GazelleIdentity identity = authenticate(authorization);

         //FIXME this authorization control should be done in application layer (VSfacade) or it may lead to code
         // duplication
         boolean needsAuthentication = applicationPreferenceManager.getBooleanValue("user_need_to_be_logged_in");
         if (!identity.isLoggedIn() && needsAuthentication) {
            throw new UnauthorizedException("user-need-to-be-logged-in");
         }

         OwnerMetadata owner = buildOwnerMetadata(identity, httpRequest);

         if (validationRequest == null) {
            return failure(Response.Status.BAD_REQUEST, new MessageFormat("create-validation.no-validation"));
         }
         if (!isValidServiceSyntax(validationRequest.getValidationService())) {
            return failure(Response.Status.BAD_REQUEST,
                  new MessageFormat("create-validation.no-validation-service {0}"),
                  toJson(validationRequest.getValidationService()));
         }
         if (areObjectsMissing(validationRequest)) {
            return failure(Response.Status.BAD_REQUEST, new MessageFormat("create-validation" +
                  ".missing-object-content"));
         }

         Directive validationDirective;
         try {

            List<ReferencedStandard> standards = facade.inferReferencedStandards(
                  validationRequest.getValidationService().getName(),
                  validationRequest.getValidationService().getValidator());
            validationDirective = toDirective(validationRequest.getValidationService(), standards);

            Validation validation = facade.validate(
                  validationRequest.toDomain().getObjects(),
                  caller,
                  owner,
                  validationDirective,
                  async);

            return buildSuccessResponse(async, toResourceUri(validation));

         } catch (ServiceConfNotFoundException | ReferencedStandardNotFoundException e) {
            return failure(Response.Status.BAD_REQUEST, e,
                  new MessageFormat("create-validation {0} {1}"), e.getMessage(),
                  toJson(validationRequest.getValidationService()));
         } catch (ValidationServiceNotAvailableException e) {
            return failure(Response.Status.SERVICE_UNAVAILABLE, e,
                  new MessageFormat("create-validation.validation-service-not-available {0} {1}"),
                  e.getMessage(),
                  toJson(validationRequest.getValidationService()));
         } catch (GatewayTimeoutException e) {
            return failure(504, e,
                    new MessageFormat("create-validation.gateway-timeout {0} {1}"),
                    e.getMessage(),
                    toJson(validationRequest.getValidationService()));
         }
      } catch (InvalidAuthorizationHeaderException e) {
         return failure(Response.Status.BAD_REQUEST, e,
               new MessageFormat("create-validation." + e.getMessage() + " {0}"), authorization);
      } catch (UnauthorizedException | InvalidApiKeyException e) {
         return failure(Response.Status.UNAUTHORIZED, e,
               new MessageFormat("create-validation." + e.getMessage() + " {0}"), authorization);
      } catch (Throwable e) {
         // FIXME catch all Throwable is blocking Transactional mechanism
         return failure(Response.Status.INTERNAL_SERVER_ERROR, e,
               new MessageFormat("create-validation.validate-error {0}"), authorization);
      }
   }

   @Override
   @Transactional
   public Response getValidationByOid(String oid, String privacyKey, String authorization, String accept,
                                      SecurityContext securityContext, HttpServletRequest request)
         throws NotFoundException {
      try {
         MimeType mimeType = parseMimeType(accept, acceptedValidationMimeTypes);
         GazelleIdentity identity = authenticate(authorization);
         Validation validation =
               evsApplicationFactory.getValidationServiceFacade().getObjectByOID(oid, privacyKey, identity);

         switch (validation.getValidationStatus()) {
            case PENDING:
            case IN_PROGRESS:
               return Response.status(Response.Status.ACCEPTED)
                     .header("Location", toResourceUri(validation).toString())
                     .header("X-Progress", validation.getValidationStatus().name())
                     .build();
            default:
               EvsMarshaller<Validation> validationMarshaller =
                     marshallerFactory.getValidationMarshaller(mimeType);
               return Response.ok(validationMarshaller.marshal(validation)).build();
         }
      } catch (ForbiddenAccessException fe) {
         return failure(Response.Status.FORBIDDEN, new MessageFormat("get-validation." + fe.getMessage()));
      } catch (InvalidAuthorizationHeaderException e) {
         return failure(Response.Status.BAD_REQUEST, new MessageFormat("get-validation." + e.getMessage() + " {0}"),
               authorization);
      } catch (UnauthorizedException | InvalidApiKeyException e) {
         return failure(Response.Status.UNAUTHORIZED, new MessageFormat("get-validation." + e.getMessage() + " {0}"),
               authorization);
      } catch (InvalidMimeTypeException e) {
         return failure(Response.Status.NOT_ACCEPTABLE,
               new MessageFormat("get-validation." + e.getMessage() + " {0}"), accept);
      } catch (net.ihe.gazelle.evsclient.application.interfaces.NotFoundException e) {
         return failure(Response.Status.NOT_FOUND, new MessageFormat("get-validation." + e.getMessage() + " {0}"),
               oid);
      } catch (Throwable e) {
         return failure(Response.Status.INTERNAL_SERVER_ERROR, e,
               new MessageFormat("get-validation.{0} {1}"), e.getMessage(), oid);
      }
   }

   @Override
   @Transactional
   public Response getValidationReportByOid(String oid, String privacyKey, String severityThreshold, String authorization, String accept,
                                            SecurityContext securityContext, HttpServletRequest request)
         throws NotFoundException {
      try {
         MimeType mimeType = parseMimeType(accept, acceptedValidationReportMimeTypes);
         GazelleIdentity identity = authenticate(authorization);
         net.ihe.gazelle.evsclient.domain.validation.Validation validation =
               evsApplicationFactory.getValidationServiceFacade().getObjectByOID(oid, privacyKey, identity);
         switch (validation.getValidationStatus()) {
            case PENDING:
            case IN_PROGRESS:
               return failure(Response.Status.NOT_FOUND,
                     new MessageFormat("get-validation-report.report-not-available-yet {0}"), oid);
            default:
               SeverityLevel severityLevel = SeverityLevel.INFO;
               try {
                  severityLevel = SeverityLevel.valueOf(
                          applicationPreferenceManager.getStringValue("ws_validation_report_severity_threshold")
                                  .toUpperCase(Locale.ROOT)
                  );
               } catch (Exception e) {
                  LOG.warn("Invalid default severity filter",e);
               }
               try {
                  if (StringUtils.isNotEmpty(severityThreshold)) {
                     severityLevel = SeverityLevel.valueOf(
                             severityThreshold.toUpperCase(Locale.ROOT));
                  }
               } catch (Exception e) {
                  throw new InvalidParameterException("invalid severity",e);
               }
               EvsMarshaller<ValidationReport> vrMarshaller =
                     marshallerFactory.getValidationReportMarshaller(mimeType,severityLevel);
               return Response.ok(vrMarshaller.marshal(validation.getValidationReport().getContent()))
                     .build();
         }
      } catch (ForbiddenAccessException fe) {
         return failure(Response.Status.FORBIDDEN, fe, new MessageFormat("get-validation-report." + fe.getMessage()));
      } catch (InvalidAuthorizationHeaderException|InvalidParameterException e) {
         return failure(Response.Status.BAD_REQUEST, e,
               new MessageFormat("get-validation-report." + e.getMessage() + " {0}"), authorization);
      } catch (InvalidApiKeyException|UnauthorizedException e) {
         return failure(Response.Status.UNAUTHORIZED, e,
               new MessageFormat("get-validation-report." + e.getMessage() + " {0}"), authorization);
      } catch (InvalidMimeTypeException e) {
         return failure(Response.Status.NOT_ACCEPTABLE, e,
               new MessageFormat("get-validation-report." + e.getMessage() + " {0}"), accept);
      } catch (MarshallingException e) {
         return failure(Response.Status.INTERNAL_SERVER_ERROR, e,
               new MessageFormat("get-validation-report." + e.getMessage() + " {0}"), accept);
      } catch (Exception e) {
         return failure(Response.Status.NOT_FOUND, e,
               new MessageFormat("get-validation-report." + e.getMessage() + " {0}"), oid);
      }
   }

   @Override
   public Response getValidationProfiles() {
      ValidationServiceFacade facade = evsApplicationFactory.getValidationServiceFacade();
      return Response.ok(facade.getValidationProfiles()).build();
   }

   @Override
   public Response getValidationProfilesByServiceName(String serviceName) {
      ValidationServiceFacade facade = evsApplicationFactory.getValidationServiceFacade();
      return Response.ok(facade.getValidationProfilesByServiceName(serviceName)).build();
   }

   private Directive toDirective(
         net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationService validationService,
         List<ReferencedStandard> standards) {
      if (validationService.getExtensions() != null && !validationService.getExtensions().isEmpty()) {
         List<Directive> directives = new ArrayList<>();
         for (ExtensionService extension : validationService.getExtensions()) {
            directives.add(new Directive(extension.getName(), extension.getValidator(), null));
         }
         return new Directive(validationService.getName(), validationService.getValidator(), standards, directives);
      } else {
         return new Directive(validationService.getName(), validationService.getValidator(), standards);
      }
   }

   private Response buildSuccessResponse(boolean async, URI uri) {
      if (async) {
         return Response.status(Response.Status.ACCEPTED)
               .header("Location", uri.toString())
               .build();
      } else {
         return Response.created(uri)
               .header("Content-Location", uri.toString())
               .header("X-Validation-Report-Redirect", uri + "/report")
               .build();
      }
   }

   private boolean areObjectsMissing(ValidationDTO validationRequest) {
      return validationRequest.getObjects() == null || validationRequest.getObjects().isEmpty()
            || areObjectsMissing(validationRequest.getObjects());
   }

   private boolean areObjectsMissing(List<HandledObject> objects) {
      for (HandledObject object : objects) {
         if (isObjectMissing(object)) {
            return true;
         }
      }
      return false;
   }

   private boolean isObjectMissing(HandledObject object) {
      // Sonar is complaining about object.getContent() == null always false, but this is wrong because JAX-RS is
      // unfortunatly ignoring IllegalArgumentExeption raised from HandledObject::setContent(). So we end up with a
      // badly initialized object.
      return object.getContent() == null || (StringUtils.isEmpty(new String(object.getContent()).trim()))
            && StringUtils.isEmpty(object.getRef());
   }

   private MimeType parseMimeType(String accept, MimeType[] availableMimeTypes) throws InvalidMimeTypeException {
      if (accept != null && !accept.isEmpty()) {
         for (String accepted : accept.split("[\\s,]+")) {
            for (MimeType mt : availableMimeTypes) {
               try {
                  if (mt.match(accepted)) {
                     return mt;
                  }
               } catch (MimeTypeParseException e) {
                  LOG.debug("mime-type-not-matching {0} {1}", mt, accept);
               }
            }
         }
      }
      throw new InvalidMimeTypeException(accept);
   }

   private URI toResourceUri(net.ihe.gazelle.evsclient.domain.validation.Validation v) {
      return URI.create(MessageFormat.format("{0}/rest/validations/{1}",
            ApplicationPreferenceManagerImpl.instance().getApplicationUrl().replaceFirst("/$", ""), v.getOid()));
   }

   private String toJson(Object obj) {
      try {
         return "\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(obj) + "\n";
      } catch (JsonProcessingException e) {
         LOG.warn("to-json.cannot-serialize-object", e);
         return "\n" + obj.toString() + "\n";
      }
   }

   static class Failure {
      Exception exception;
      Object value;

      public Failure(Exception exception, Object value) {
         this.exception = exception;
         this.value = value;
      }
   }

   private Response failure(Response.Status status, MessageFormat format, String... messageParameters) {
      LOG.info(format.format(messageParameters));
      return Response.fromResponse(Response.status(status).build())
            .entity(format.format(messageParameters))
            .build();
   }

   private Response failure(Response.Status status, Throwable e, MessageFormat format, String... messageParameters) {
      LOG.error(format.format(messageParameters), e);
      return Response.fromResponse(Response.status(status).build())
            .entity(format.format(messageParameters))
            .build();
   }

   private Response failure(Integer code, Throwable e, MessageFormat format, String... messageParameters) {
      LOG.error(format.format(messageParameters), e);
      return Response.fromResponse(Response.status(code).build())
              .entity(format.format(messageParameters))
              .build();
   }

   private GazelleIdentity authenticate(String authzHeader)
         throws InvalidAuthorizationHeaderException, InvalidApiKeyException {

      if (authzHeader != null) {
         if (!StringUtils.isEmpty(authzHeader.trim())) {
            String apiKeyValue = parseApiKeyValue(authzHeader);
            return apiKeyManager.authenticate(apiKeyValue);
         } else {
            throw new InvalidAuthorizationHeaderException("authorization-is-empty");
         }
      } else {
         return new ApiKeyIdentity();
      }
   }

   private OwnerMetadata buildOwnerMetadata(GazelleIdentity identity, HttpServletRequest httpRequest) {
      if (identity.isLoggedIn()) {
         return new OwnerMetadata(getClientIpAddr(httpRequest), identity.getUsername(),
               identity.getOrganisationKeyword());
      } else {
         return new OwnerMetadata(getClientIpAddr(httpRequest), null, null);
      }
   }

   private String getClientIpAddr(HttpServletRequest request) {
      String ip = request.getHeader("X-Forwarded-For");
      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         ip = request.getHeader("Proxy-Client-IP");
      }
      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         ip = request.getHeader("WL-Proxy-Client-IP");
      }
      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         ip = request.getHeader("HTTP_CLIENT_IP");
      }
      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         ip = request.getHeader("HTTP_X_FORWARDED_FOR");
      }
      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         ip = request.getRemoteAddr();
      }
      return ip;
   }

   private String parseApiKeyValue(String authorization) throws InvalidAuthorizationHeaderException {
      Matcher m = authenticationParser.matcher(authorization);
      if (m.find()) {
         return m.group(1);
      } else {
         throw new InvalidAuthorizationHeaderException("bad-authorization-format");
      }
   }

   private boolean isValidServiceSyntax(
         net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationService validationService) {
      if (validationService == null
            || StringUtils.isEmpty(validationService.getName())
            || (StringUtils.isEmpty(validationService.getValidator()))) {
         return false;
      }
      return true;
   }

   private Map<String, String> parsePreferHeader(String prefer) {
      Map<String, String> p = new THashMap<>();
      if (prefer != null) {
         for (String token : prefer.split("[,; ]")) {
            Matcher m = preferParser.matcher(token);
            if (m.find()) {
               p.put(m.group(1), m.groupCount() > 1 ? m.group(2) : "");
            }
         }
      }
      return p;
   }

   static class InvalidAuthorizationHeaderException extends Exception {
      private static final long serialVersionUID = -5076359578869160449L;

      public InvalidAuthorizationHeaderException(String s) {
         super(s);
      }
   }
   static class InvalidParameterException extends Exception {
      private static final long serialVersionUID = 7147322371117722552L;

      public InvalidParameterException(String s, Exception e) {
         super(s,e);
      }
   }
}
