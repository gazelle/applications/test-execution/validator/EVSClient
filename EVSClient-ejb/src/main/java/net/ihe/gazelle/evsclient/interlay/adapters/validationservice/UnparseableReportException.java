package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

public class UnparseableReportException extends RuntimeException {
    public UnparseableReportException() {
    }

    public UnparseableReportException(String s) {
        super(s);
    }

    public UnparseableReportException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public UnparseableReportException(Throwable throwable) {
        super(throwable);
    }

    public UnparseableReportException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
