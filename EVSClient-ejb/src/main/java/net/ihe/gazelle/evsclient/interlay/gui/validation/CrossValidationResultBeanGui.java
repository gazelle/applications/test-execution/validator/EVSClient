package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.callingtools.common.model.CallingTool;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.domain.processing.OtherCallerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.view.validation.ValidationResultDto;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.document.InvalidStylesheetException;
import net.ihe.gazelle.evsclient.interlay.gui.document.ReportConverter;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.xvalidation.action.ReportIOManager;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.text.MessageFormat;
import java.util.Map;

public class CrossValidationResultBeanGui extends AbstractValidationProcessingBeanGui<Validation, ValidationServiceFacade> {


   private static final String UNKNOWN = "Unknown";
   private final RemotePermanentLinkSender remotePermanentLinkSender;
   /* USED */
   private Integer referencedStandardId;

   private ValidationReport report;
   private CrossValidatorInputsGui inputsGui;

   public CrossValidationResultBeanGui(ValidationServiceFacade validationServiceFacade,
                                       CallerMetadataFactory callerMetadataFactory,
                                       GazelleIdentity identity,
                                       ApplicationPreferenceManager applicationPreferenceManager) {
      super(CrossValidationResultBeanGui.class, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager,
       new AbstractGuiPermanentLink<Validation>(applicationPreferenceManager) {
         @Override
         public String getResultPageUrl() {
            return Pages.XVAL_RESULT.getMenuLink();
         }
      });
      this.remotePermanentLinkSender = new RemotePermanentLinkSender(this);
      try {
         initFromUrl(); // get by Id or Oid
      } catch (UnauthorizedException | ForbiddenAccessException | NotFoundException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to init result page", e);
      }
   }

   public void init(final ValidationResultDto validationResultDto, Integer referencedStandardId) {

      this.referencedStandardId = referencedStandardId;

      setSelectedObject(validationResultDto.getValidation());

      if (validationResultDto.isValidationDone()) {
         if (selectedObject != null && StringUtils.isNotEmpty(validationResultDto.getValidationType())) {
            try {
               initializeReport();
               inputsGui = new CrossValidatorInputsResultGui(
                       getValidation(),
                       GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorFromName(getValidatorName())
               );
            } catch (Exception e) {
               setSelectedObject(null);
               FacesMessages.instance()
                       .addFromResourceBundle("net.ihe.gazelle.evs.xml.ObjectTypeInvalid",
                               validationResultDto.getValidationType());
            }
         } else {
            FacesMessages.instance()
                    .addFromResourceBundle(StatusMessage.Severity.ERROR, "net.ihe.gazelle.evs.xml.noObjectOIDProvided");
            setSelectedObject(null);
         }
      }
   }

   @Override
   public void initFromUrl() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
      super.initFromUrl();
      final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext()
            .getRequestParameterMap();
      if (urlParams != null && !urlParams.isEmpty() && urlParams.containsKey(QueryParamEvs.REF_STANDARD_ID)) {
         this.referencedStandardId = Integer.valueOf(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
      }
      if (selectedObject != null) {
         initializeReport();
         inputsGui = new CrossValidatorInputsResultGui(
               getValidation(),
               GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorFromName(getValidatorName())
         );
      } else {
         if (urlParams!=null && !urlParams.containsKey("javax.faces.partial.ajax")) {
            FacesMessages.instance()
                    .addFromResourceBundle(StatusMessage.Severity.ERROR, "net.ihe.gazelle.evs.xml.noObjectOIDProvided");
         }
      }
   }

   private void initializeReport() {
      try {
         report = getValidation().getValidationReport().getContent();
      } catch (FileReadException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR,
               MessageFormat.format("cant-load-validation-report-content {0}", e.getMessage()));
      }
   }

   public ValidationReport getReport() {
      return report;
   }

   public String getValidationServiceName() {
      return getValidation().getValidationService().getName();
   }

   public String getValidationServiceVersion() {
      return getValidation().getValidationService().getVersion();
   }

   private Validation getValidation() {
      return selectedObject;
   }

   public boolean sendBackResultToTool() {
      if (selectedObject == null || !(selectedObject.getCaller() instanceof OtherCallerMetadata)) {
         return false;
      } else {
         return (!CallingTool.toolIsTM(((OtherCallerMetadata) selectedObject.getCaller()).getToolOid())
               || ((OtherCallerMetadata) selectedObject.getCaller()).getToolObjectId().startsWith("sample_"));
      }
   }

   public boolean displayInfoPopup() {
      return selectedObject != null && selectedObject.getCaller() instanceof OtherCallerMetadata
            && ((OtherCallerMetadata) selectedObject.getCaller()).getToolObjectId().startsWith("stepdata_");
   }

   public String getValidatorName() {
      return getValidation() != null ? getValidation().getValidationService().getValidatorKeyword() : UNKNOWN;
   }

   public String getValidatorVersion() {
      return getValidation() != null ? getValidation().getValidationService().getValidatorVersion() : UNKNOWN;
   }

   public String getValidationType() {
      return getValidation() != null ? getValidation().getValidationType().name() : UNKNOWN;
   }


   public CrossValidatorInputsGui getInputsGui() {
      return inputsGui;
   }


   @Override
   protected String getValidatorUrl() {
      return null;
   }

   public String performAnotherValidation() {
      GuiComponentType componentType = GuiComponentType.valueOf(getValidationType());
      return componentType.getValidatorView() +
            "?" + QueryParamEvs.REF_STANDARD_ID + "=" + referencedStandardId;
   }

   @Override
   public String revalidate() {
      GuiComponentType componentType = GuiComponentType.valueOf(getValidationType());
      return componentType.getValidatorView() +
            "?" + QueryParamEvs.REF_STANDARD_ID + "=" + referencedStandardId +
            "&" + QueryParam.PROCESSING_OID + "=" + selectedObject.getOid();
   }

   public String getValidationStatus() {
      return selectedObject != null ? selectedObject.getValidationStatus().name() : "N/A";
   }

   public void downloadResultAsXml() {
      if (selectedObject != null && selectedObject.getOriginalReport() != null) {
         byte[] originalXValReport = selectedObject.getOriginalReport().getContent();
         if (originalXValReport != null) {
            new ReportIOManager().exportReportToFile(
                  originalXValReport,
                  selectedObject.getOid() + ".xml");
         }
      } else {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to download, missing original XML report.");
      }
   }

   public void shareResult() {
      processingManager.shareResult(selectedObject);
   }

   /**
    * @deprecated Should be replaced by getTransformedDetailedResult, using
    */
   @Deprecated
   public String getHtmlResult() {
      if (selectedObject != null && selectedObject.getOriginalReport() != null) {
         return new ReportIOManager().exportReportToHtml(selectedObject.getOriginalReport().getContent());
      } else {
         return "Missing original XML report";
      }
   }

   public String getTransformedDetailedResult() {
      if (selectedObject == null || report == null) {
         return "";
      }
      try {
         String xml = new XMLValidationReportMarshaller().setIndentation(true).setCompleteDocument(false)
               .marshal(report);
         String xsl = null;

         if (selectedObject.getValidationService() != null) {
            ValidationServiceConf avsc = processingManager.getValidationServiceByKeyword(
                  getValidation().getValidationService().getName());
            xsl = avsc.getValidationReportType().getStylesheet().getLocation();
         }

         if (StringUtils.isNotEmpty(xml)) {
            if (xml.contains("?>")) {
               xml = xml.substring(xml.indexOf("?>") + 2);
            }
            return ReportConverter.transformXMLStringToHTML(xml, xsl, LocaleSelector.instance().getLanguage());
         }
      } catch (InvalidStylesheetException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "invalid-stylesheet", e);
      } catch (MarshallingException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "cant-serialize-validation-report", e);
      } catch (ServiceConfNotFoundException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR,
               MessageFormat.format("cant-find-validation-service {0}",
                     getValidation().getValidationService().getName()), e);
      }

      return null;
   }

   public void sendPermaLink() {
      remotePermanentLinkSender.sendPermaLink();
   }

   private static class CrossValidatorInputsResultGui extends CrossValidatorInputsGui {

      private Validation validation;
      private GazelleCrossValidatorType validatorType ;

      CrossValidatorInputsResultGui(Validation validation, GazelleCrossValidatorType validatorType) {
         this.validation = validation;
         this.validatorType = validatorType;
      }

      @Override
      protected Validation getValidation() {
         return validation;
      }

      @Override
      public boolean isValidationDone() {
         return true;
      }

      @Override
      public GazelleCrossValidatorType getValidator() {
         return validatorType;
      }

      @Override
      public boolean isValidatorSelected() {
         return validatorType != null;
      }

   }

}
