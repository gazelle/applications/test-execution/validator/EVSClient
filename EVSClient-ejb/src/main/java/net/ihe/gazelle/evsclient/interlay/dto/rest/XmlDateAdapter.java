package net.ihe.gazelle.evsclient.interlay.dto.rest;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class XmlDateAdapter extends XmlAdapter<String, Date> {

   public static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX";

   @Override
   public Date unmarshal(String stringDate) throws Exception {
      return new SimpleDateFormat(DATE_FORMAT).parse(stringDate);
   }

   @Override
   public String marshal(Date date) throws Exception {
      return new SimpleDateFormat(DATE_FORMAT).format(date);
   }
}
