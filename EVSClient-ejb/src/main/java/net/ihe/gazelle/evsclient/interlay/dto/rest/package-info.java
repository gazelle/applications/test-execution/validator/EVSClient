@XmlSchema(
        namespace = "http://evsobjects.gazelle.ihe.net/",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns={
                @XmlNs(namespaceURI="http://evsobjects.gazelle.ihe.net/", prefix="evs"),
        })
package net.ihe.gazelle.evsclient.interlay.dto.rest;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
