/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.evsclient.interlay.gui.Authorizations;
import net.ihe.gazelle.evsclient.interlay.gui.validation.GuiComponentType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;

public class PageValidate implements Page {

    private static final String DEFAULT_LABEL = "gazelle.evs.client.validate";
    private String label;
    private String url;
    private ReferencedStandard standard;

    public PageValidate(final ReferencedStandard standard) {
        this.standard = standard;
    }

    public PageValidate(final String url, final String label) {
        this.label = label;
        this.url = url;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getLabel() {
        if (this.label == null) {
            this.label = PageValidate.DEFAULT_LABEL;
        }
        return this.label;
    }

    @Override
    public String getLink() {
        if (this.url == null && standard != null) {
            final StringBuilder builder = new StringBuilder(GuiComponentType.getValidatorViewByValidationType(standard.getValidationType()));
            EVSMenu.appendUrlParameters(builder, String.valueOf(standard.getId()));
            this.url = builder.toString();
        }
        return this.url;
    }


    @Override
    public String getIcon() {
        return "fa fa-check-square-o";
    }

    @Override
    public Authorization[] getAuthorizations() {
        return new Authorization[]{ Authorizations.IS_LOGGED_IF_NEEDED };
    }

    @Override
    public String getMenuLink() {
        return this.getLink().replace(".xhtml", ".seam");
    }

    public ReferencedStandard getStandard() {
        return standard;
    }
}
