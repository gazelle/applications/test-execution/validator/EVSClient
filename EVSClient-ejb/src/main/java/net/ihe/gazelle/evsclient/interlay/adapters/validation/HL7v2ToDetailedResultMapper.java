package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.hl7.assertion.GazelleHL7Assertion;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.NistValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationCounters;
import net.ihe.gazelle.validation.ValidationResultsOverview;

import java.math.BigInteger;

/**
 * @deprecated HL7v2 Report must not be mapped to the deprecated report DetailedResult.
 * @See Hl7v2ReportMapper
 */
@Deprecated
class HL7v2ToDetailedResultMapper {

   DetailedResult buildDetailedResult(HL7v2ValidationReport report) {
      DetailedResult detailedResult = new DetailedResult();
      detailedResult.setValidationResultsOverview(buildOverview(report));
      detailedResult.setDocumentWellFormed(buildDocumentWellFormed(report));
      detailedResult.setMDAValidation(buildMDAValidation(report));
      detailedResult.setDocumentValidXSD(buildDocumentValidXSD(report));
      detailedResult.setNistValidation(buildNistValidation(report));
      detailedResult.setTemplateDesc(buildTemplateDesc(report));
      return detailedResult;
   }

   private Template buildTemplateDesc(HL7v2ValidationReport report) {
      return null;
   }

   private NistValidation buildNistValidation(HL7v2ValidationReport report) {
      return null;
   }

   private DocumentValidXSD buildDocumentValidXSD(HL7v2ValidationReport report) {
      return null;
   }

   private MDAValidation buildMDAValidation(HL7v2ValidationReport report) {
      MDAValidation v = new MDAValidation();
      v.setValidationCounters(buildCounters(report.getCounters()));
      v.setResult(report.getOverview().getValidationTestResult());
      buildNotifications(v,report);
      return v;
   }

   private void buildNotifications(MDAValidation v, HL7v2ValidationReport report) {
      for (Object n:report.getResults().getNotifications()) {
         v.getWarningOrErrorOrNote().add(buildNotification(n));
      }
   }
   private Notification buildNotification(Object n) {
      if (net.ihe.gazelle.hl7.validator.report.Error.class.isInstance(n)) {
         return buildError((net.ihe.gazelle.hl7.validator.report.Error)n);
      } else if (net.ihe.gazelle.hl7.validator.report.Warning.class.isInstance(n)) {
         return buildWarning((net.ihe.gazelle.hl7.validator.report.Warning)n);
      } else if (net.ihe.gazelle.hl7.validator.report.GazelleProfileException.class.isInstance(n)) {
         return buildError((net.ihe.gazelle.hl7.validator.report.GazelleProfileException)n);
      } else if (GazelleHL7Assertion.class.isInstance(n)) {
         return buildNote((GazelleHL7Assertion)n);
      }
      return null;
   }
   private net.ihe.gazelle.validation.Error buildError(net.ihe.gazelle.hl7.validator.report.GazelleProfileException n) {
      net.ihe.gazelle.validation.Error error = new net.ihe.gazelle.validation.Error();
      error.setIdentifiant("");
      error.setDescription(n.getDescription());
      error.setType(n.getFailureType());
      error.setTest("");
      error.setLocation(n.getHl7Path());
      error.setMessage(n.getMessage()==null?"":n.getMessage());
      return error;
   }
   private net.ihe.gazelle.validation.Error buildError(net.ihe.gazelle.hl7.validator.report.Error n) {
      net.ihe.gazelle.validation.Error error = new net.ihe.gazelle.validation.Error();
      error.setIdentifiant("");
      error.setDescription(n.getDescription());
      error.setType(n.getFailureType());
      error.setTest("");
      error.setLocation(n.getHl7Path());
      error.setMessage(n.getMessage()==null?"":n.getMessage());
      return error;
   }
   private net.ihe.gazelle.validation.Warning buildWarning(net.ihe.gazelle.hl7.validator.report.Warning n) {
      net.ihe.gazelle.validation.Warning warning = new net.ihe.gazelle.validation.Warning();
      warning.setIdentifiant("");
      warning.setDescription(n.getDescription());
      warning.setType(n.getFailureType());
      warning.setTest("");
      warning.setLocation(n.getHl7Path());
      warning.setMessage(n.getMessage()==null?"":n.getMessage());
      return warning;
   }
   private net.ihe.gazelle.validation.Note buildNote(GazelleHL7Assertion n) {
      net.ihe.gazelle.validation.Note note = new net.ihe.gazelle.validation.Note();
      note.setIdentifiant("");
      note.setDescription(n.getAssertion());
      note.setType(n.getType()==null?"":n.getType().getLabel());
      note.setTest("");
      note.setLocation(n.getPath());
      return note;
   }
   private ValidationCounters buildCounters(net.ihe.gazelle.hl7.validator.report.ValidationCounters counters) {
      ValidationCounters c = new ValidationCounters();
      c.setNrOfChecks(BigInteger.valueOf(counters.getNbOfAssertions()));
      c.setNrOfValidationErrors(BigInteger.valueOf(counters.getNbOfErrors()));
      c.setNrOfValidationWarnings(BigInteger.valueOf(counters.getNbOfWarnings()));
      c.setNrOfValidationInfos(BigInteger.valueOf(0));
      c.setNrOfValidationReports(BigInteger.valueOf(0));
      c.setNrOfValidationNotes(BigInteger.valueOf(0)); // TODO assertions - (errors + warnings) ?
      c.setNrOfValidationUnknown(BigInteger.valueOf(0));
      return c;
   }

   private DocumentWellFormed buildDocumentWellFormed(HL7v2ValidationReport report) {
      return null;
   }

   private ValidationResultsOverview buildOverview(HL7v2ValidationReport report) {
      net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview o = report.getOverview();
      ValidationResultsOverview overview = new ValidationResultsOverview();
      overview.setValidationDate(o.getValidationDate());
      overview.setValidationTime(o.getValidationTime());
      overview.setValidationServiceName(o.getValidationServiceName());
      overview.setValidationServiceVersion(o.getValidationServiceVersion());
      overview.setValidationEngine(o.getValidationServiceName());
      overview.setValidationEngineVersion(o.getValidationServiceVersion());
      overview.setValidationTestResult(o.getValidationTestResult());
      return overview;
   }

}
