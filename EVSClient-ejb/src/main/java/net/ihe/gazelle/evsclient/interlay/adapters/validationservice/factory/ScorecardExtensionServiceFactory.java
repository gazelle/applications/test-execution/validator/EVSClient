package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractExtensionServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ScorecardServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.ScorecardExtensionServiceClient;

public class ScorecardExtensionServiceFactory extends AbstractExtensionServiceFactory<ScorecardExtensionServiceClient, ScorecardServiceConf>  {

    public ScorecardExtensionServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, ScorecardServiceConf configuration) {
        super(applicationPreferenceManager, configuration, ScorecardExtensionServiceClient.class);
    }
}
