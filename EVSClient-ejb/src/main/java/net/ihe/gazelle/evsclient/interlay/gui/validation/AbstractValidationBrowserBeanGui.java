package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

public abstract class AbstractValidationBrowserBeanGui implements GuiPermanentLink<Validation> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractValidationBrowserBeanGui.class);

    protected Validation selectedValidationForDeletion;

    protected ValidationServiceFacade validationServiceFacade;

    protected ValidationSearch search;

    protected AbstractValidationBrowserBeanGui(final ValidationServiceFacade validationServiceFacade,
                                               ApplicationPreferenceManager applicationPreferenceManager
    ) {
        this(validationServiceFacade,applicationPreferenceManager,null);
    }
    protected AbstractValidationBrowserBeanGui(final ValidationServiceFacade validationServiceFacade,
                                            ApplicationPreferenceManager applicationPreferenceManager,
                                            final Pages page
                                            ) {
        this.validationServiceFacade = validationServiceFacade;
        this.search = new ValidationSearch(applicationPreferenceManager,page) {
            private static final long serialVersionUID = 7008930126716436160L;

            @Override
            public FilterDataModel<Validation> getDataModel() {
                return validationServiceFacade.getValidationsFilteredDataModel(search.getFilter());
            }
            protected Pages getPage() {
                try {
                    if (page==null) {
                        return Pages.valueOf(AbstractValidationBrowserBeanGui.this.getReferencedStandard().getValidationType().name().toUpperCase(Locale.ROOT) + "_RESULT");
                    } else {
                        return page;
                    }
                } catch (Exception e) {
                    return Pages.DETAILED_RESULT;
                }
            }
            @Override
            public String getPermanentLink(Validation selectedObject) {
                return super.getPermanentLink(selectedObject)+"&"+ QueryParamEvs.REF_STANDARD_ID +"="+ search.getStandardId();
            }
        };
    }

    public Validation getSelectedValidationForDeletion() {
        return selectedValidationForDeletion;
    }

    public void setSelectedValidationForDeletion(Validation selectedValidationForDeletion) {
        this.selectedValidationForDeletion = selectedValidationForDeletion;
    }

    public void deleteSelectedObject() {
        Validation object = this.getSelectedValidationForDeletion();
        try {
            validationServiceFacade.delete(object.getOid());
        } catch (ProcessingNotFoundException | UnexpectedProcessingException e) {
            LOGGER.error("Unable to delete validation", e);
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to delete validation : " + e.getMessage());
        }

    }

    public ValidationSearch getSearch() {
        return search;
    }

    public void setSearch(ValidationSearch search) {
        this.search = search;
    }

    @Override
    public String getPermanentLink(Validation selectedObject) {
        return search.getPermanentLink(selectedObject);
    }

    public ReferencedStandard getReferencedStandard() {
        return validationServiceFacade.getReferencedStandardById(search.getStandardId());
    }

    public String getReferencedStandardName() {
        ReferencedStandard referencedStandard = getReferencedStandard();
        return referencedStandard != null ? referencedStandard.getName() : "";
    }
}
