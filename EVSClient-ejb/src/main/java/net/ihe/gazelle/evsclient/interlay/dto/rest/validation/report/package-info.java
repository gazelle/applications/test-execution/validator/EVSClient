@XmlSchema(
        namespace = "http://validationreport.gazelle.ihe.net/",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns={
                @XmlNs(namespaceURI="http://validationreport.gazelle.ihe.net/", prefix="gvr"),
        })
package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;