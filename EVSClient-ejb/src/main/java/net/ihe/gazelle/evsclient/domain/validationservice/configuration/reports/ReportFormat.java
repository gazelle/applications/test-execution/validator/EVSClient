package net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports;

public enum ReportFormat {

    PLAIN_TEXT(".txt"),
    XML(".xml");

    private String extension;

    ReportFormat(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}
