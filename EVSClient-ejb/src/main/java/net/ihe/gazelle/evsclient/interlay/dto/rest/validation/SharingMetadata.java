package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Schema(description="Métadonnées sur les informations de partage de la ressource. ")
@XmlRootElement(name = "sharing")
@XmlAccessorType(XmlAccessType.FIELD)
public class SharingMetadata extends AbstractDTO<net.ihe.gazelle.evsclient.domain.processing.SharingMetadata> {


    public SharingMetadata(net.ihe.gazelle.evsclient.domain.processing.SharingMetadata domain) {
        super(domain);
    }

    public SharingMetadata() {
        super();
    }

    @Schema(name= "name",example = "", description = ".") // TODO
    @JsonProperty("private")
    @XmlAttribute(name = "private")
    public Boolean getPrivate() {
        return domain.getIsPrivate();
    }

}
