package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;

class ValidationReportFileDAO extends ReportFileDAO {

   private static final String ORIGINAL_REPORT_PREFIX = "original-report-";
   private static final String VALIDATION_REPORT_PREFIX = "validation-report-";

   ValidationReportFileDAO(ApplicationPreferenceManager applicationPreferenceManager) {
      super(applicationPreferenceManager);
   }

   /**
    * Save content of the original report on File System.
    *
    * @param validation Validation containing the originalReport to save.
    * @param repository Repository to save the file.
    *
    * @return the Validation (The originalReport may have been proxied)
    *
    * @throws ProcessingNotSavedException in case of file I/O exception or repository definition error.
    */
   Validation saveOriginalReportInFile(Validation validation, Repository repository) {
      if (validation.getOriginalReport() != null) {
         OriginalReportFile report = new OriginalReportFile(validation.getOriginalReport());
         validation.setOriginalReport(report);
         saveReportInFile(report, new ReportFileRepository(repository, ORIGINAL_REPORT_PREFIX), validation.getId().toString());
      }
      return validation;
   }

   /**
    * Save content of the validation report on File System.
    *
    * @param validation Validation containing the validationReport to save.
    * @param repository Repository to save the file.
    *
    * @return the Validation (The validationReport may have been proxied)
    *
    * @throws ProcessingNotSavedException in case of file I/O exception or repository definition error.
    */
   Validation saveValidationReportInFile(Validation validation, Repository repository) {
      if (validation.getValidationReport() != null) {
         ValidationReportFile report = new ValidationReportFile(validation.getValidationReport());
         validation.setValidationReport(report);
         saveReportInFile(report, new ReportFileRepository(repository, VALIDATION_REPORT_PREFIX), validation.getId().toString());
      }
      return validation;
   }

}
