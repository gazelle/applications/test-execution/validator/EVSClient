package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.EmbeddedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.CrossValidatorReportMapper;
import net.ihe.gazelle.xvalidation.ws.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ValidationProperties(
      name = "Cross Validation",
      types = ValidationType.XVAL)
@ReportParser(CrossValidatorReportMapper.class)
public class CrossValidationService extends AbstractConfigurableValidationService<EmbeddedValidationServiceConf>
      implements EmbeddedValidationService {

   // Webservice entry point is directly called through Java, this is why this class is still an EmbeddedValidationService.
   private GazelleCrossValidationWSInterface xValidationService;

   public CrossValidationService(ApplicationPreferenceManager applicationPreferenceManager,
                                 EmbeddedValidationServiceConf configuration) {
      super(applicationPreferenceManager, configuration);
      this.xValidationService = new GazelleCrossValidationWS();
   }

   @Override
   public String about() {
      return getName();
   }

   @Override
   public String getName() {
      return "Cross Validation";
   }

   @Override
   public List<String> getSupportedMediaTypes() {
      return Arrays.asList("application/xml");
   }

   protected List<String> getValidatorNames(String validatorFilter) {
      ValidatorListWrapper vlw = xValidationService.getAvailableValidators(validatorFilter);
      if (vlw == null) {
         return new ArrayList<>();
      } else {
         return vlw.getValidators();
      }
   }

   @Override
   public byte[] validate(HandledObject[] objects, String validatorKeyword) {
      FileListWrapper flw = new FileListWrapper();
      for (HandledObject object : objects) {
         flw.addFile(new FileWrapper(object.getRole(), object.getContent()));
      }
      try {
         return xValidationService.validate(flw, validatorKeyword).getBytes(StandardCharsets.UTF_8);
      } catch (RuntimeXValidationException e) {
         throw new ValidationErrorException(e);
      }
   }

   @Override
   public String getServiceVersion() {
       return getVersion("gazelle-x-validation-core","Implementation-Version");
   }
}
