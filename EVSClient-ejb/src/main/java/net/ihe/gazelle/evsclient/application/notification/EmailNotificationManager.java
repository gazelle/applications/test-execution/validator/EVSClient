/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application.notification;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.interlay.ws.rest.validations.GetValidationInfo;
import net.ihe.gazelle.preferences.PreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @author aberge
 * @class EmailNotification
 * <p/>
 * This class is used to send an email to the administrator and/or monitor of the application when the validation aborts
 */

public final class EmailNotificationManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2013153390754620929L;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailNotificationManager.class);

    private final ApplicationPreferenceManager applicationPreferenceManager;

    public EmailNotificationManager(ApplicationPreferenceManager applicationPreferenceManager) {
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    public void send(final Exception e, final String messageSubject, final String additionalMessage) {
        EmailNotificationManager.LOGGER.info("sending email...");

        // retrieving email addresses
        final String monitorEmail = applicationPreferenceManager.getStringValue("monitor_email");
        final String administratorEmail = applicationPreferenceManager.getApplicationAdminEmail();

        String emailContent =
                "This is a message from " + applicationPreferenceManager.getApplicationName() + " at "
                        + applicationPreferenceManager.getApplicationUrl() + "\n\n\n";

        if (additionalMessage != null) {
            emailContent = additionalMessage + '\n';
        }

        if (e != null) {
            final String message = e.getMessage();
            final StackTraceElement[] trace = e.getStackTrace();
            if (trace.length > 0) {

                final StringBuilder buf = new StringBuilder(emailContent);

                for (final StackTraceElement el : trace) {
                    buf.append("at ");
                    buf.append(el.getClassName());
                    buf.append('.');
                    buf.append(el.getMethodName());
                    buf.append("(line ");
                    buf.append( el.getLineNumber());
                    buf.append(")\n");
                }
                emailContent = message + "\n\n" + buf;
            }
        }

        try {
            final Boolean use_specific_smtp = PreferenceService.getBoolean("use_specific_smtp");
            String smtphost = "localhost";
            String username = "";
            String password = "";
            Integer port = Integer.valueOf(25);
            final Properties properties = new Properties();
            if (use_specific_smtp) {
                smtphost = PreferenceService.getString("smtp_host");
                username = PreferenceService.getString("smtp_username");
                password = PreferenceService.getString("smtp_password");
                port = PreferenceService.getInteger("smtp_port");
            }
            properties.put("mail.smtp.host", smtphost);

            final Session session = Session.getDefaultInstance(properties);
            session.setDebug(false);

            final Message mail = new MimeMessage(session);

            final InternetAddress from = new InternetAddress(administratorEmail,
                    applicationPreferenceManager.getApplicationName() );
            mail.setFrom(from);

            final InternetAddress[] to = new InternetAddress[2];
            to[0] = new InternetAddress(administratorEmail);
            if (monitorEmail != null) {
                to[1] = new InternetAddress(monitorEmail);
            }

            mail.setRecipients(RecipientType.TO, to);

            mail.setSubject(messageSubject);
            mail.setContent(emailContent, GetValidationInfo.TEXT_PLAIN);
            mail.saveChanges();

            if (use_specific_smtp) {
                final Transport tr = session.getTransport("smtp");
                tr.connect(smtphost, port, username, password);
                tr.sendMessage(mail, mail.getAllRecipients());
                tr.close();
            }
            else {
                Transport.send(mail);
            }
        } catch (UnsupportedEncodingException|MessagingException ex) {
            EmailNotificationManager.LOGGER.error("Cannot send email !",ex);
        }

    }

}
