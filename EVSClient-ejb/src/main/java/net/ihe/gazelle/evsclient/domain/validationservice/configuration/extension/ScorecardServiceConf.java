package net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension;

import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.extension.Scorecard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ServiceConfException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.ScorecardExtensionServiceClient;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.CDAGeneratorValidationServiceFactory;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.ScorecardExtensionServiceFactory;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_scorecard_service_conf", schema = "public")
public class ScorecardServiceConf extends WebExtensionServiceConf<Scorecard, ScorecardExtensionServiceClient, ScorecardServiceConf, ScorecardExtensionServiceFactory> {
    private static final long serialVersionUID = 6067224015483089438L;

    public ScorecardServiceConf(ValidationServiceConf validationServiceConf, Validation validation) {
        super(null,
                I18n.get("net.ihe.gazelle.evs.scorecard-description"),
                "scorecard",
                ScorecardExtensionServiceFactory.class.getCanonicalName(),
                isServiceAvailable(validationServiceConf,validation),
                buildUrl(validationServiceConf),
                isZipped(validationServiceConf));
    }

    public ScorecardServiceConf() {
        super();
    }

    public static boolean isServiceAvailable(ValidationServiceConf validationServiceConf, Validation validation) {
        try {
            return (ValidationStatus.DONE_PASSED.equals(validation.getValidationStatus())
                    ||ValidationStatus.DONE_FAILED.equals(validation.getValidationStatus()))
                && CDAGeneratorValidationServiceFactory.class.isAssignableFrom(validationServiceConf.getFactoryClass());
        } catch (ServiceConfException e) {
            return false;
        }
    }

    private static boolean isZipped(ValidationServiceConf validationServiceConf) {
        if (validationServiceConf instanceof WebValidationServiceConf) {
            return ((WebValidationServiceConf)validationServiceConf).isZipTransport();
        }
        return false;
    }

    private static String buildUrl(ValidationServiceConf validationServiceConf) {
        if (validationServiceConf instanceof WebValidationServiceConf) {
            String wsdl = ((WebValidationServiceConf)validationServiceConf).getUrl();
            int index = wsdl.indexOf("-ejb");
            if (index > 0) {
                return wsdl.substring(0, index);
            }
        }
        return null;
    }

}
