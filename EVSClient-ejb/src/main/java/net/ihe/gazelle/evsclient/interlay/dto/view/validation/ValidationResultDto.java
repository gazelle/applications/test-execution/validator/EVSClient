package net.ihe.gazelle.evsclient.interlay.dto.view.validation;

import net.ihe.gazelle.evsclient.domain.validation.Validation;

public class ValidationResultDto {

    private Validation validation;

    private boolean validationDone;

    private String validationType;

    private String validatorName;
    
    public ValidationResultDto(Validation validation, boolean validationDone, String validationType,
                               String validatorName) {
        this.validation = validation;
        this.validationDone = validationDone;
        this.validationType = validationType;
        this.validatorName = validatorName;
    }

    public Validation getValidation() {
        return validation;
    }

    public void setValidation(Validation validation) {
        this.validation = validation;
    }

    public boolean isValidationDone() {
        return validationDone;
    }

    public void setValidationDone(boolean validationDone) {
        this.validationDone = validationDone;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public String getValidatorName() {
        return validatorName;
    }

    public void setValidatorName(String validatorName) {
        this.validatorName = validatorName;
    }
}
