package net.ihe.gazelle.evsclient.application.validationservice.configuration;

public class ReferencedStandardNotFoundException extends Exception {
   private static final long serialVersionUID = 516115233505365198L;

   public ReferencedStandardNotFoundException(String s) {
      super(s);
   }

   public ReferencedStandardNotFoundException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
