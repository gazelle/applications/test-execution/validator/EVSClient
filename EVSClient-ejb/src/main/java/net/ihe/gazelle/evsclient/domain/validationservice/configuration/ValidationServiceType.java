package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;

public interface ValidationServiceType<T extends ValidationService> {

    String getValue();

    String name();

    Class<? extends T> getValidationServiceClass();

    boolean isImplemented();

}
