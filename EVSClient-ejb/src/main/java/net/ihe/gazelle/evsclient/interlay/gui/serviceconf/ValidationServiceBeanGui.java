package net.ihe.gazelle.evsclient.interlay.gui.serviceconf;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.*;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.EmbeddedValidationServiceTypes;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.SystemValidationServiceTypes;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.WebValidationServiceTypes;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationSubReportDTO;
import net.ihe.gazelle.evsclient.interlay.dto.view.serviceconf.ValidationServiceGuiDto;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class ValidationServiceBeanGui {
    private static final String DEFAULT_STYLESHEETS_ROOT = "https://gazelle.ihe.net/xsl/EVSClient/";

    private final ApplicationPreferenceManager applicationPreferenceManager;
    private final ValidationServiceConfManager validationServiceConfManager;

    private String stylesheetsroot = DEFAULT_STYLESHEETS_ROOT;
    private List<String> stylesheets;

    private ValidationServiceGuiDto validationServiceGuiDto;

    private Boolean creation;

    public ValidationServiceBeanGui(ValidationServiceConfManager validationServiceConfManager, ApplicationPreferenceManager applicationPreferenceManager) {
        this.validationServiceConfManager = validationServiceConfManager;
        this.applicationPreferenceManager = applicationPreferenceManager;
        init();
    }

    public Boolean isCreation() {
        return creation;
    }

    public void setCreation(Boolean creation) {
        this.creation = creation;
    }

    public ValidationServiceGuiDto getValidationServiceDto() {
        return validationServiceGuiDto;
    }

    public void setValidationServiceConfDto(ValidationServiceGuiDto validationServiceGuiDto) {
        this.validationServiceGuiDto = validationServiceGuiDto;
    }

    private void init() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.get(QueryParamEvs.VAL_SERVICE_ID) != null) {
            validationServiceGuiDto = new ValidationServiceGuiDto(
                  validationServiceConfManager.getValidationServiceById(Integer.valueOf(urlParams.get(QueryParamEvs.VAL_SERVICE_ID))),
                    validationServiceConfManager);
            this.creation = Boolean.FALSE;
        } else {
            validationServiceGuiDto = new ValidationServiceGuiDto();
            this.creation = Boolean.TRUE;
        }
    }

    private List<String> findURLsByExtention(String path, String extention) {
        List<String> names = new ArrayList<>();
        collectURLsByExtention(path,extention,names);
        return names;
    }
    private void collectURLsByExtention(String path, String extention,List<String> names) {
        try {
            Pattern parentPattern = Pattern.compile("Parent Directory");
            Document doc = Jsoup.connect(path).get();
            for (Element node : doc.select("td a")) {
                String resource = node.attr("href");
                if (resource.endsWith("/")&&!parentPattern.matcher(node.toString()).find()) {
                    collectURLsByExtention(path + resource, extention, names);
                } else if (resource.endsWith(extention)) {
                    names.add(path+resource);
                }
            }
        } catch (IOException ioe) {}
    }
    public List<String> getStylesheets() {
        if (stylesheets==null) {
            stylesheets = findURLsByExtention(stylesheetsroot,"xsl");
        }
        return stylesheets;
    }
    public void setStylesheets(List<String> stylesheets) {
        this.stylesheets = stylesheets;
    }
    public List<String> autocomplete(String prefix) {
        ArrayList<String> result = new ArrayList<String>();
        if ((prefix == null) || (prefix.length() == 0)) {
            stylesheetsroot = DEFAULT_STYLESHEETS_ROOT;
            stylesheets=null;
            for (String xsl:getStylesheets()) {
                result.add(xsl.trim());
            }
        } else {
            if (!prefix.startsWith(stylesheetsroot)
                && prefix.contains("/xsl/EVSClient/")) {
                    stylesheetsroot = prefix.replaceFirst("(https?://.*?/xsl/EVSClient/).*$","$1");
                    stylesheets=null;
            }
            for (String xsl:getStylesheets()) {
                String url = xsl.trim();
                if (url.startsWith(prefix)) {
                    result.add(url);
                }
            }
        }
        return result;
    }
    public List<ValidationServiceGuiDto> getAllValidationService() {
        List<ValidationServiceGuiDto> validationServiceGuiDtos = new ArrayList<>();
        for (ValidationServiceConf validationServiceProcessingConf : validationServiceConfManager.getAllValidationService()) {
            validationServiceGuiDtos.add(
                  new ValidationServiceGuiDto(
                        validationServiceProcessingConf,
                          validationServiceConfManager)
            );
        }
        return validationServiceGuiDtos;
    }

    public DataModel<ValidationServiceGuiDto> getAllValidationServiceDataModel(){
        return new GazelleListDataModel<>(getAllValidationService());
    }


    public String save() {

        validationServiceConfManager.saveValidationServiceConf(
              validationServiceGuiDto.toValidationServiceConf(validationServiceConfManager));

        FacesMessages.instance()
                .addFromResourceBundle("net.ihe.gazelle.evs.ValidationServiceHasBeenSuccessfullySaved");
        return backToServiceList();
    }

    public String backToServiceList() {
        return "/administration/validationServices.seam";
    }

    public List<SelectItem> getValidationServiceTypesAsSelectItems() {
        List<SelectItem> options = new ArrayList<>();

        SelectItem rootItem = new SelectItem();
        rootItem.setValue(null);
        rootItem.setNoSelectionOption(true);
        rootItem.setDisabled(true);
        rootItem.setLabel(ResourceBundle.instance().getString("gazelle.common.PleaseSelect"));
        options.add(rootItem);

        SelectItemGroup webServiceGroup = new SelectItemGroup("Web Service");
        List<SelectItem> selectItems = new ArrayList<>();
        for (WebValidationServiceTypes webServiceType : WebValidationServiceTypes.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(webServiceType);
            selectItem.setLabel(webServiceType.getValue());
            selectItem.setDisabled(!webServiceType.isImplemented());
            selectItems.add(selectItem);
        }
        webServiceGroup.setSelectItems(selectItems.toArray(new SelectItem[0]));
        options.add(webServiceGroup);

        SelectItemGroup embeddedGroup = new SelectItemGroup("Embedded service");
        selectItems = new ArrayList<>();
        for (EmbeddedValidationServiceTypes embeddedServiceType : EmbeddedValidationServiceTypes.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(embeddedServiceType);
            selectItem.setLabel(embeddedServiceType.getValue());
            selectItem.setDisabled(!embeddedServiceType.isImplemented());
            selectItems.add(selectItem);
        }
        embeddedGroup.setSelectItems(selectItems.toArray(new SelectItem[0]));
        options.add(embeddedGroup);

        SelectItemGroup systemServiceGroup = new SelectItemGroup("System Service");
        selectItems = new ArrayList<>();
        for (SystemValidationServiceTypes systemServiceType : SystemValidationServiceTypes.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(systemServiceType);
            selectItem.setLabel(systemServiceType.getValue());
            selectItem.setDisabled(!systemServiceType.isImplemented());
            selectItems.add(selectItem);
        }
        systemServiceGroup.setSelectItems(selectItems.toArray(new SelectItem[0]));
        options.add(systemServiceGroup);

        return options;
    }
    private ValidationServiceType<?> findValidationServiceType(String name, ValidationServiceType<?>[]... values) {
        for (ValidationServiceType<?>[] vsts : values) {
            for (ValidationServiceType<?> vst : vsts) {
                if (vst.name().equals(name)) {
                    return vst;
                }
            }
        }
        return null;
    }
    private ValidationServiceType<?> findValidationServiceType(String name) {
        return findValidationServiceType(name, WebValidationServiceTypes.values(), EmbeddedValidationServiceTypes.values(), SystemValidationServiceTypes.values());
    }

    public SelectItem[] getValidatorFriendlyNamesAsSelectItems() {

        List<SelectItem> validatorSelectedItems = new ArrayList<>();

        SelectItem rootItem = new SelectItem();
        rootItem.setValue(null);
        rootItem.setNoSelectionOption(true);
        rootItem.setDisabled(true);
        rootItem.setLabel(ResourceBundle.instance().getString("gazelle.common.PleaseSelect"));

        rootItem.setDisabled(true);
        rootItem.setValue(null);
        validatorSelectedItems.add(rootItem);

        final List<ValidationType> list = Arrays.asList(ValidationType.values());
        Collections.sort(list);

        Class<? extends ValidationService> cls = null;
        if (validationServiceGuiDto !=null && validationServiceGuiDto.getValidationServiceType()!=null) {
            ValidationServiceType vst = findValidationServiceType(validationServiceGuiDto.getValidationServiceType());
            if (vst!=null) {
                cls = vst.getValidationServiceClass();
            }
        }
        if (cls==null) {
            for (final ValidationType validator : list) {
                validatorSelectedItems.add(new SelectItem(validator, validator.name()));
            }
        } else {
            ValidationProperties properties = cls.getAnnotation(ValidationProperties.class);
            for (ValidationType property:properties.types()) {
                validatorSelectedItems.add(new SelectItem(property, property.name()));
            }
        }

        return validatorSelectedItems.toArray(new SelectItem[validatorSelectedItems.size()]);
    }


    public boolean isEmbeddedServiceTypeSelected() {
        if(validationServiceGuiDto.getValidationServiceType() != null) {
            return validationServiceGuiDto.isEmbeddedServiceType();
        }
        return false;
    }

    public boolean isSystemServiceTypeSelected() {
        if(validationServiceGuiDto.getValidationServiceType() != null){
            return validationServiceGuiDto.isSystemServiceType();
        }
        return false;
    }

    public boolean isWebServiceTypeSelected() {
        if(validationServiceGuiDto.getValidationServiceType() != null) {
            return validationServiceGuiDto.isWebServiceType();
        }
        return false;
    }
}
