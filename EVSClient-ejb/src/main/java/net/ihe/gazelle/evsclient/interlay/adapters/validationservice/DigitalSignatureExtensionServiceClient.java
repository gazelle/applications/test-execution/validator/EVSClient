package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.extension.DigitalSignature;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.DigitalSignatureServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceOperationException;
import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.mb.validator.client.ServiceConnectionError;
import net.ihe.gazelle.mb.validator.client.ServiceOperationError;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

public final class DigitalSignatureExtensionServiceClient extends AbstractConfigurableService<DigitalSignatureServiceConf> implements ExtensionService<DigitalSignature> {

    private static Logger LOGGER = LoggerFactory.getLogger(DigitalSignatureExtensionServiceClient.class);

    private final MBValidator mbValidator;

    public DigitalSignatureExtensionServiceClient(ApplicationPreferenceManager applicationPreferenceManager, DigitalSignatureServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        Long validationTimeout = getValidationTimeout();
        if (validationTimeout != null) {
            mbValidator = new MBValidator(configuration.getUrl(), validationTimeout);
        } else {
            mbValidator = new MBValidator(configuration.getUrl());
        }
    }

    public byte[] process(DigitalSignature digitalSignature) throws ExtensionServiceOperationException {
        if ( getConfiguration() == null || StringUtils.isEmpty(getConfiguration().getUrl())) {
            LOGGER.error("Validator URL is null or empty, cannot contact validator");
            return null;
        }
        if (digitalSignature.getObject().getContent() == null || digitalSignature.getObject().getContent().length<1) {
            return null;
        }
        try {
            return this.mbValidator.validate(
                            DatatypeConverter.printBase64Binary(digitalSignature.getObject().getContent()),
                            PreferenceService.getString("default_dsig_validator_name"),
                            true)
                    .getBytes(StandardCharsets.UTF_8);
        } catch (ServiceConnectionError | ServiceOperationError e) {
            throw new ExtensionServiceOperationException(e);
        }
    }

    @Override
    public DigitalSignature createExtension(Validation validation, EVSCallerMetadata caller, OwnerMetadata owner) {
        return new DigitalSignature(validation, caller, owner);
    }

    @Override
    public String about() {
        return this.configuration.getDescription();
    }

    @Override
    public String getName() {
        return this.configuration.getName();
    }


}
