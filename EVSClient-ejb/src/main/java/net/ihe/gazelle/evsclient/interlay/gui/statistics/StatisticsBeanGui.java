package net.ihe.gazelle.evsclient.interlay.gui.statistics;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.statistics.StatisticsManager;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.international.StatusMessage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StatisticsBeanGui {

    private final StatisticsManager statisticsManager;

    private final GuiPermanentLink<Validation> guiPermanentLink;

    public Date exportStartDate;

    public Date exportEndDate;

    public StatisticsBeanGui(StatisticsManager statisticsManager, AbstractGuiPermanentLink<Validation> guiPermanentLink) {
        this.statisticsManager = statisticsManager;
        this.guiPermanentLink = guiPermanentLink;
        initExportDateInterval();
    }

    private void initExportDateInterval() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.DATE, 1);
        currentDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) - 1);
        exportStartDate = currentDate.getTime();
        currentDate.set(Calendar.DATE, currentDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        exportEndDate = currentDate.getTime();
    }

    public Date getExportStartDate() {
        return exportStartDate;
    }

    public void setExportStartDate(Date exportStartDate) {
        this.exportStartDate = exportStartDate;
    }

    public Date getExportEndDate() {
        return exportEndDate;
    }

    public void setExportEndDate(Date exportEndDate) {
        exportEndDate.setHours(23);
        exportEndDate.setMinutes(59);
        exportEndDate.setSeconds(59);
        this.exportEndDate = exportEndDate;
    }

    public StatisticsDataRow getTotalDataRow() {
        StatisticsDataRow totalDataRow = statisticsManager.countTotalDataRow();
        totalDataRow.setName(ResourceBundle.instance().getString("net.ihe.gazelle.evs.Total"));
        return totalDataRow;
    }

    public List<StatisticsDataRow> getTotalDataRowGroupByReferencedStandard() throws Exception {
        return statisticsManager.countTotalDataRowGroupByReferencedStandard();
    }

    public void exportValidations() {
        try {
            String csvData = statisticsManager.createCsvData(exportStartDate, exportEndDate, getHeadersToExport(), guiPermanentLink);
            ReportExporterManager.exportToFile("text/csv", csvData, ResourceBundle.instance().getString("gazelle.evs.client.statistics").concat(".csv"));
        } catch (Exception e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
        }

    }

    private String[] getHeadersToExport() {
        return new String[] {
                ResourceBundle.instance().getString("net.ihe.gazelle.OID"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.ValidationDate"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.Menu")
                        .concat(" - ")
                        .concat(ResourceBundle.instance().getString("gazelle.evs.client.admin.referenced.standards")),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.ValidationService"),
                ResourceBundle.instance().getString("net.ihe.gazelle.ValidationServiceVersion"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.Validator"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.ValidatorVersion"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.ValidationStatus"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.interface"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.user.name"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evs.user.organization"),
                ResourceBundle.instance().getString("net.ihe.gazelle.evsclient.Private"),
                ResourceBundle.instance().getString("gazelle.evs.client.Permanent.Link")
        };
    }

    public String getParameterNameForStandardId(){
        return QueryParamEvs.REF_STANDARD_ID;
    }

    public List<Object[]> getStatisticsPerCountry(){
        List<Object[]> stats = statisticsManager.countPerCountry();
        // ensure result is not null for google map data table not complains
        if (stats==null||stats.isEmpty()) {
            stats = new ArrayList<Object[]>();
            stats.add(new Object[]{"France",0,0});
        }
        return stats;
    }


}
