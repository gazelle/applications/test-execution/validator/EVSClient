package net.ihe.gazelle.evsclient.interlay.dto.rest.analysis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.MessageFormat;

@Schema(description="URL de récupération du rapport détaillé de validation. ")
@XmlRootElement(name = "validationReportRef")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationRef extends AbstractDTO<net.ihe.gazelle.evsclient.domain.validation.ValidationRef> {


    public ValidationRef(net.ihe.gazelle.evsclient.domain.validation.ValidationRef domain) {
        super(domain);
    }

    public ValidationRef() {
        this(new net.ihe.gazelle.evsclient.domain.validation.ValidationRef());
    }

    @Schema(name= "location",example = "", description = ".") // TODO
    @JsonProperty("location")
    @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "location")
    public String getLocation() {
        return MessageFormat.format(
            "{0}/validations/{1}",ApplicationPreferenceManagerImpl.instance().getApplicationUrl(),domain.getOid()
        );
    }

    @Schema(name= "status",example = "", description = ".") // TODO
    @JsonProperty("status")
    @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "status")
    public String getStatus() {
        return domain.getValidationStatus();
    }
}
