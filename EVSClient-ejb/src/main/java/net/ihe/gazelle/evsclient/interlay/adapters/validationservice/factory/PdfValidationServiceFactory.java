package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.PdfValidationService;

public class PdfValidationServiceFactory extends AbstractValidationServiceFactory<PdfValidationService,EmbeddedValidationServiceConf> {

    public PdfValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, EmbeddedValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, PdfValidationService.class);
    }
}
