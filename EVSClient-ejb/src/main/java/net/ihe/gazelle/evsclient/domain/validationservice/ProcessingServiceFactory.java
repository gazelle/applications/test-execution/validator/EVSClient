package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;

public interface ProcessingServiceFactory<S extends ProcessingService, C extends ProcessingConf> {

    S getService();

    Class<S> getServiceClass();

}
