package net.ihe.gazelle.evsclient.domain.statistics;

import java.math.BigInteger;

public class StatisticsDataRow {

    private String menu;
    private int standardId;
    private String name;
    private BigInteger numberOfValidations;
    private BigInteger numberOfGuiValidations;
    private BigInteger numberOfWebServiceValidations;
    private BigInteger numberOfPassedValidations;
    private BigInteger numberOfFailedValidations;
    private BigInteger numberOfUndefinedValidations;

    public StatisticsDataRow() {
    }

    public StatisticsDataRow(BigInteger numberOfValidations, BigInteger numberOfGuiValidations, BigInteger numberOfWebServiceValidations, BigInteger numberOfPassedValidations, BigInteger numberOfFailedValidations, BigInteger numberOfUndefinedValidations) {
        this.numberOfValidations = numberOfValidations;
        this.numberOfGuiValidations = numberOfGuiValidations;
        this.numberOfWebServiceValidations = numberOfWebServiceValidations;
        this.numberOfPassedValidations = numberOfPassedValidations;
        this.numberOfFailedValidations = numberOfFailedValidations;
        this.numberOfUndefinedValidations = numberOfUndefinedValidations;
    }

    public StatisticsDataRow(String menu, int standardId, String name, BigInteger numberOfValidations, BigInteger numberOfGuiValidations, BigInteger numberOfWebServiceValidations, BigInteger numberOfPassedValidations, BigInteger numberOfFailedValidations, BigInteger numberOfUndefinedValidations) {
        this(numberOfValidations, numberOfGuiValidations, numberOfWebServiceValidations, numberOfPassedValidations, numberOfFailedValidations, numberOfUndefinedValidations);
        this.standardId = standardId;
        this.name = name;
        this.menu = menu;
    }

    public int getStandardId() {
        return standardId;
    }

    public void setStandardId(int standardId) {
        this.standardId = standardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getNumberOfValidations() {
        return numberOfValidations;
    }

    public void setNumberOfValidations(BigInteger numberOfValidations) {
        this.numberOfValidations = numberOfValidations;
    }

    public BigInteger getNumberOfGuiValidations() {
        return numberOfGuiValidations;
    }

    public void setNumberOfGuiValidations(BigInteger numberOfGuiValidations) {
        this.numberOfGuiValidations = numberOfGuiValidations;
    }

    public BigInteger getNumberOfWebServiceValidations() {
        return numberOfWebServiceValidations;
    }

    public void setNumberOfWebServiceValidations(BigInteger numberOfWebServiceValidations) {
        this.numberOfWebServiceValidations = numberOfWebServiceValidations;
    }

    public BigInteger getNumberOfPassedValidations() {
        return numberOfPassedValidations;
    }

    public void setNumberOfPassedValidations(BigInteger numberOfPassedValidations) {
        this.numberOfPassedValidations = numberOfPassedValidations;
    }

    public BigInteger getNumberOfFailedValidations() {
        return numberOfFailedValidations;
    }

    public void setNumberOfFailedValidations(BigInteger numberOfFailedValidations) {
        this.numberOfFailedValidations = numberOfFailedValidations;
    }

    public BigInteger getNumberOfUndefinedValidations() {
        return numberOfUndefinedValidations;
    }

    public void setNumberOfUndefinedValidations(BigInteger numberOfUndefinedValidations) {
        this.numberOfUndefinedValidations = numberOfUndefinedValidations;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}
