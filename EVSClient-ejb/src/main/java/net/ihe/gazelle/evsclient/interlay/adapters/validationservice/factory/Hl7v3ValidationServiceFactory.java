package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.Hl7v3ValidationServiceClient;

public class Hl7v3ValidationServiceFactory extends AbstractValidationServiceFactory<Hl7v3ValidationServiceClient, WebValidationServiceConf> {

    public Hl7v3ValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, Hl7v3ValidationServiceClient.class);
    }
}
