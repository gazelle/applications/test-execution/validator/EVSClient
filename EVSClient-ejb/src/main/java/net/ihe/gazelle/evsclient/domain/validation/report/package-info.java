/**
 * The ValidationReport model has been copied from the Java 11 modelapi.validation-model project and adapted into Java7
 * with some modification for JPA persistence and DTO transformation. The reference is the original Java11 project.
 */
package net.ihe.gazelle.evsclient.domain.validation.report;
