package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.DigitalSignatureServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension.ScorecardServiceConf;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.extensions.ExtensionGui;
import net.ihe.gazelle.evsclient.interlay.gui.menu.HomeBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.menu.MenuGroupBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.preferences.PreferenceKeyValidator;
import net.ihe.gazelle.evsclient.interlay.gui.remote.RemoteValidationBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.security.ApiKeyBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.serviceconf.ReferencedStandardBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.serviceconf.ValidationServiceBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.statistics.StatisticsBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.statistics.StatisticsByStandardBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.validation.*;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("evsGuiFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class GuiFactory {

    @In(value = "evsApplicationFactory")
    private ApplicationFactory applicationFactory;

    @In(value ="evsCommonApplicationFactory")
    private EvsCommonApplicationFactory evsCommonApplicationFactory;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @In(value = "org.jboss.seam.security.identity", create = true)
    private GazelleIdentity identity;

    @Factory(value= "validationBeanGui", scope = ScopeType.PAGE)
    public ValidationBeanGui getValidationBeanGui() {
        return new ValidationBeanGui(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager,
                getEmailNotificationManager());
    }

    @Factory(value= "validationResultBeanGui", scope = ScopeType.PAGE)
    public ValidationResultBeanGui getValidationResultBeanGui() {
        return new ValidationResultBeanGui(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager);
    }

    @Factory(value= "emailNotificationManager", scope = ScopeType.PAGE)
    public EmailNotificationManager getEmailNotificationManager() {
        return new EmailNotificationManager(applicationPreferenceManager);
    }

    @Factory(value= "referencedStandardBeanGui", scope = ScopeType.PAGE)
    public ReferencedStandardBeanGui getReferencedStandardBeanGui() {
        return new ReferencedStandardBeanGui(applicationFactory.getServiceConfManager());
    }

    @Factory(value= "validationServiceBeanGui", scope = ScopeType.PAGE)
    public ValidationServiceBeanGui getValidationServiceBeanGui() {
        return new ValidationServiceBeanGui(applicationFactory.getServiceConfManager(), applicationPreferenceManager);
    }

    @Factory(value= "menuGroupBeanGui", scope = ScopeType.PAGE)
    public MenuGroupBeanGui getMenuGroupBeanGui() {
        return new MenuGroupBeanGui(applicationFactory.getServiceConfManager());
    }

    @Factory(value= "homeBeanGui", scope = ScopeType.PAGE)
    public HomeBeanGui getHomeBeanGui() {
        return new HomeBeanGui();
    }


    @Factory(value= "validationBrowserBeanGui", scope = ScopeType.PAGE)
    public ValidationBrowserBeanGui getBrowserBeanGui() {
        return new ValidationBrowserBeanGui(applicationFactory.getValidationServiceFacade(), applicationPreferenceManager);
    }

    @Factory(value = "apiKeyBeanGui", scope = ScopeType.PAGE)
    public ApiKeyBeanGui getApiKeyBeanGui() {
        return new ApiKeyBeanGui(applicationFactory.getApiKeyManager());
    }

    @Factory(value = "preferenceKeyValidator", scope = ScopeType.PAGE)
    public PreferenceKeyValidator getPreferenceKeyValidator() {
        return new PreferenceKeyValidator(applicationFactory.getApplicationConfiguration());
    }

    @Factory(value = "scorecardExtensionGui", scope = ScopeType.PAGE)
    public ExtensionGui getScorecardExtensionGui() {
        return new ExtensionGui(ScorecardServiceConf.class,applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager);
    }

    @Factory(value = "dsigExtensionGui", scope = ScopeType.PAGE)
    public ExtensionGui getDigitalSignatureExtensionGui() {
        return new ExtensionGui(DigitalSignatureServiceConf.class,applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager);
    }

    @Factory(value= "hl7Validator", scope = ScopeType.PAGE)
    public HL7ValidationBeanGui getHL7ValidationManager() {
        return new HL7ValidationBeanGui(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager,
                null,
                getEmailNotificationManager());
    }
    @Factory(value= "hl7ValidationBrowserBeanGui", scope = ScopeType.PAGE)
    public HL7ValidationBrowserBeanGui getHl7BrowserBeanGui() {
        return new HL7ValidationBrowserBeanGui(applicationFactory.getValidationServiceFacade(), applicationPreferenceManager);
    }

    @Factory(value= "hl7ResultDisplay", scope = ScopeType.PAGE)
    public HL7MessageResultDisplay getHL7MessageResultDisplay() {
        return new HL7MessageResultDisplay(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager);
    }

    @Factory(value = "remoteValidationBeanGui", scope = ScopeType.PAGE)
    public RemoteValidationBeanGui getRemoteValidationBeanGui() {
        return new RemoteValidationBeanGui();
    }

    @Factory(value = "statisticsBeanGui", scope = ScopeType.PAGE)
    public StatisticsBeanGui getStatisticsBeanGui() {
        return new StatisticsBeanGui(applicationFactory.getStatisticsManager(), new AbstractGuiPermanentLink<Validation>(applicationPreferenceManager) {
            @Override
            public String getResultPageUrl() {
                return Pages.DETAILED_RESULT.getMenuLink();
            }
        });
    }

    @Factory(value = "statisticsByStandardBeanGui", scope = ScopeType.PAGE)
    public StatisticsByStandardBeanGui getStatisticsByStandardBeanGui() {
        return new StatisticsByStandardBeanGui(applicationFactory.getStatisticsManager(), applicationFactory.getServiceConfManager());
    }

    @Factory(value= "xvalGui", scope = ScopeType.PAGE)
    public CrossValidationGui getCrossValidationGui() {
        return new CrossValidationGui(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager,
                getEmailNotificationManager());
    }

    @Factory(value= "xvalBrowserGui", scope = ScopeType.PAGE)
    public CrossValidationBrowserBeanGui getCrossValidationBrowserGui() {
        return new CrossValidationBrowserBeanGui(applicationFactory.getValidationServiceFacade(), applicationPreferenceManager);
    }

    @Factory(value= "xvalResultGui", scope = ScopeType.PAGE)
    public CrossValidationResultBeanGui getCrossValidationResultGui() {
        return new CrossValidationResultBeanGui(
                applicationFactory.getValidationServiceFacade(),
                evsCommonApplicationFactory.getCallerMetadataFactory(),
                identity,
                applicationPreferenceManager);
    }

    @Factory(value= "pdfBrowserGui", scope = ScopeType.PAGE)
    public PdfValidationBrowserBeanGui getPdfBrowserGui() {
        return new PdfValidationBrowserBeanGui(applicationFactory.getValidationServiceFacade(), applicationPreferenceManager);
    }

}
