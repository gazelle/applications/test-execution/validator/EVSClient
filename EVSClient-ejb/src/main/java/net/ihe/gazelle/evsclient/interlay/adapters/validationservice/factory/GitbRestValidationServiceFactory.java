package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.GitbRestValidationServiceClient;

public class GitbRestValidationServiceFactory extends AbstractValidationServiceFactory<GitbRestValidationServiceClient, WebValidationServiceConf> {

    public GitbRestValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, GitbRestValidationServiceClient.class);
    }
}