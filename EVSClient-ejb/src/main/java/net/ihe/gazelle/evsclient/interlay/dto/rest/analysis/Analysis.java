package net.ihe.gazelle.evsclient.interlay.dto.rest.analysis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractProcessing;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Schema(description="Analyse de contenu d'un objet")
@XmlRootElement(namespace = "http://evsobjects.gazelle.ihe.net/", name="validation")
@XmlAccessorType(XmlAccessType.FIELD)
public class Analysis extends AbstractProcessing<net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis> {

  protected Boolean autoValidation;

  public Analysis(net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis domain) {
    super(domain);
  }

  public Analysis() {
    super();
  }



  /**
   **/
  
  @Schema(name= "contentAnalyzerVersion",description = "") // TODO
  @JsonProperty("contentAnalyzerVersion")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "contentAnalyzerVersion")
  public String getContentAnalyzerVersion() {
    return domain.getContentAnalyzerVersion();
  }

  /**
   * Déclenche automatiquement la validation sur tous les types de contenu détecté dans l&#39;objet
   **/
  
  @Schema(name= "autoValidation",example = "true", description = "Déclenche automatiquement la validation sur tous les types de contenu détecté dans l'objet")
  @JsonProperty("autoValidation")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "autoValidation")
  public Boolean isAutoValidation() {
    return autoValidation;
  }

  /**
   *
   * @return*/
  
  @Schema(name= "analysisStatus",description = "") // TODO
  @JsonProperty("analysisStatus")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "analysisStatus")
  public AnalysisStatus getAnalysisStatus() {
    return domain.getAnalysisStatus();
  }

  /**
   **/
  
  @Schema(name= "rootAnalyzedObjectPart",description = "")
  @JsonProperty("rootAnalyzedObjectPart")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "rootAnalyzedObjectPart")
  public AnalysisPart getRootAnalyzedObjectPart() {
    return new AnalysisPart(domain.getRootAnalysisPart());
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Analysis analysis = (Analysis) o;
    return Objects.equals(getContentAnalyzerVersion(), analysis.getContentAnalyzerVersion()) &&
        Objects.equals(isAutoValidation(), analysis.isAutoValidation()) &&
        Objects.equals(getAnalysisStatus(), analysis.getAnalysisStatus()) &&
        Objects.equals(getRootAnalyzedObjectPart(), analysis.getRootAnalyzedObjectPart());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getContentAnalyzerVersion(), isAutoValidation(), getAnalysisStatus(), getRootAnalyzedObjectPart());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Analysis {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    contentAnalyzerVersion: ").append(toIndentedString(getContentAnalyzerVersion())).append("\n");
    sb.append("    autoValidation: ").append(toIndentedString(isAutoValidation())).append("\n");
    sb.append("    analysisStatus: ").append(toIndentedString(getAnalysisStatus())).append("\n");
    sb.append("    rootAnalyzedObjectPart: ").append(toIndentedString(getRootAnalyzedObjectPart())).append("\n");
    sb.append("}");
    return sb.toString();
  }

}

