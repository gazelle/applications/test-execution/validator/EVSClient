package net.ihe.gazelle.evsclient.application.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;

import javax.faces.model.DataModel;
import java.util.List;

public interface ValidationServiceConfDao {

    ValidationServiceConf getValidationServiceByKeyword(final String inKeyword);

    ValidationServiceConf merge(ValidationServiceConf selectedValidationService);

    List<ValidationServiceConf> getListOfAvailableValidationServicesForStandard(final ReferencedStandard inStandard);

    List<ReferencedStandard> getAllReferencedStandard();

    DataModel<ReferencedStandard> getAllReferencedStandardDataModel();

    List<ReferencedStandard> getReferencedStandardFiltered(final String displayName, final ValidationType validationType, final String validatorFilter);

    ReferencedStandard merge(ReferencedStandard selectedReferencedStandard);

    ReferencedStandard getReferencedStandardById(Integer id);

    ValidationServiceConf getValidationServiceById(Integer id);

    ValidationReportType getValidationReportTypeById(Integer id);

    List<ValidationServiceConf> getAllValidationService();

    List<ValidationServiceConf> getValidationServiceFiltered(final String name, final ValidationType validationType, final Boolean available);

    List<ReferencedStandard> getReferencedStandardsByValidationServiceId(int validationServiceId);

}