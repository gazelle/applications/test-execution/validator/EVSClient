package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.interlay.dto.rest.DataTransferObject;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Schema(description = "Détails du rapport de validation.")
@XmlRootElement(name = "validationSubReport")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"subCounters", "unexpectedErrors", "standards", "constraints", "subReports"})
public class ValidationSubReportDTO implements DataTransferObject<ValidationSubReport> {

   private String name;
   private ValidationTestResult subReportResult;
   private List<ConstraintValidationDTO> constraints;
   private ValidationCountersDTO subCounters;
   private List<ValidationSubReportDTO> subReports;
   private List<String> standards;
   private List<UnexpectedErrorDTO> unexpectedErrorDTOS;

   public ValidationSubReportDTO(ValidationSubReport domain,SeverityLevel severity) {
      name = domain.getName();
      subReportResult = domain.getSubReportResult();
      constraints = mapConstraintValidations(domain.getConstraints(),severity);
      subCounters = new ValidationCountersDTO(domain.getSubCounters());
      subReports = domain.getSubReports() != null ? mapValidationSubReports(domain.getSubReports(),severity) : null;
      standards = domain.getStandards();
      unexpectedErrorDTOS = domain.getUnexpectedErrors() != null ? mapUnexpectedErrors(domain.getUnexpectedErrors()) : null;
   }

   public ValidationSubReportDTO() {
   }

   @Schema(name = "name", description = "") // TODO
   @JsonProperty("name")
   @XmlAttribute(name = "name")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Schema(name = "subReportResult", description = "") // TODO
   @JsonProperty("subReportResult")
   @XmlAttribute(name = "subReportResult")
   public ValidationTestResult getSubReportResult() {
      return subReportResult;
   }

   public void setSubReportResult(ValidationTestResult subReportResult) {
      this.subReportResult = subReportResult;
   }

   @Schema(name = "constraints", description = "") // TODO
   @JsonProperty("constraints")
   @XmlElement(name = "constraint")
   public List<ConstraintValidationDTO> getConstraints() {
      return constraints;
   }

   public void setConstraints(List<ConstraintValidationDTO> constraintDTOs) {
      this.constraints = constraintDTOs;
   }

   @Schema(name = "subCounters", description = "") // TODO
   @JsonProperty("subCounters")
   @XmlElement(name = "subCounters")
   public ValidationCountersDTO getSubCounters() {
      return subCounters;
   }

   public void setSubCounters(ValidationCountersDTO subCountersDTO) {
      this.subCounters = subCountersDTO;
   }

   @Schema(name = "subReports", description = "") // TODO
   @JsonProperty("subReports")
   @XmlElement(name = "subReport")
   public List<ValidationSubReportDTO> getSubReports() {
      return subReports;
   }

   public void setSubReports(List<ValidationSubReportDTO> subReportDTOs) {
      this.subReports = subReportDTOs;
   }

   @Schema(name = "standards", description = "") // TODO
   @JsonProperty("standards")
   @XmlElement(name = "standards")
   public List<String> getStandards() {
      return standards;
   }

   public void setStandards(List<String> standards) {
      this.standards = standards;
   }

   @Schema(name = "unexpectedErrors", description = "") // TODO
   @JsonProperty("unexpectedErrors")
   @XmlElement(name = "unexpectedError")
   public List<UnexpectedErrorDTO> getUnexpectedErrors() {
      return unexpectedErrorDTOS;
   }

   public void setUnexpectedErrors(List<UnexpectedErrorDTO> unexpectedErrorDTOS) {
      this.unexpectedErrorDTOS = unexpectedErrorDTOS;
   }

   @Override
   public ValidationSubReport toDomain() {
      ValidationSubReport validationSubReport = new ValidationSubReport(name, standards);
      if(constraints != null) {
         for (ConstraintValidationDTO constraintValidationDTO : constraints) {
            validationSubReport.addConstraintValidation(constraintValidationDTO.toDomain());
         }
      }
      if(unexpectedErrorDTOS != null) {
         for (UnexpectedErrorDTO exceptionDTO : unexpectedErrorDTOS) {
            validationSubReport.addUnexpectedError(exceptionDTO.toDomain());
         }
      }
      if(subReports != null) {
         for (ValidationSubReportDTO validationSubReportDTO : subReports) {
            validationSubReport.addSubReport(validationSubReportDTO.toDomain());
         }
      }
      return validationSubReport;
   }

   private List<ConstraintValidationDTO> mapConstraintValidations(List<ConstraintValidation> constraintValidations, SeverityLevel severity) {
      List<ConstraintValidationDTO> constraintDTOs = new ArrayList<>();
      for (ConstraintValidation constraintValidation : constraintValidations) {
         if (constraintValidation.getSeverity().compareTo(severity)>=0) {
            constraintDTOs.add(new ConstraintValidationDTO(constraintValidation));
         }
      }
      Collections.sort(constraintDTOs, new Comparator<ConstraintValidationDTO>() {
         @Override
         public int compare(ConstraintValidationDTO a, ConstraintValidationDTO b) {
            return b.getSeverity().compareTo(a.getSeverity());
         }
      });
      return constraintDTOs;
   }

   private List<ValidationSubReportDTO> mapValidationSubReports(List<ValidationSubReport> validationSubReports, SeverityLevel severity) {
      List<ValidationSubReportDTO> validationSubReportDTOs = new ArrayList<>();
      for (ValidationSubReport validationSubReport : validationSubReports) {
         validationSubReportDTOs.add(new ValidationSubReportDTO(validationSubReport,severity));
      }
      return validationSubReportDTOs;
   }

   private List<UnexpectedErrorDTO> mapUnexpectedErrors(List<UnexpectedError> unexpectedErrors) {
      List<UnexpectedErrorDTO> errorDTOS = new ArrayList<>();
      for (UnexpectedError unexpectedError : unexpectedErrors) {
         errorDTOS.add(new UnexpectedErrorDTO(unexpectedError));
      }
      return errorDTOS;
   }

}