package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import junit.framework.TestCase;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.DcmCheckValidationServiceFactory;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

@RunWith(PowerMockRunner.class)
public class DcmCheckValidationServiceIT extends TestCase {

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    DcmCheckValidationService service;

    @Before
    public void setUp() {
        SystemValidationServiceConf conf = new SystemValidationServiceConf();
        conf.setValidationType(ValidationType.DICOM);
        conf.setAvailable(true);
        conf.setName("DcmCheck Validation Service");
        conf.setId(1);
        conf.setBinaryPath("dcmcheck");
        conf.setFactoryClassname(DcmCheckValidationServiceFactory.class.getCanonicalName());
        service = new DcmCheckValidationService(applicationPreferenceManager, conf);
    }

    @Ignore
    @Test
    public void name() {
        Assert.assertEquals(service.getName(),"DICOM PS3");
    }

    @Ignore
    @Test
    public void getValidatorNames() {
        List<Validator> validators = service.getValidators();
        Assert.assertEquals(7,validators.size());
    }

    @Test
    public void about() {
        String about = service.about();
        Assert.assertNotNull(about);
        Assert.assertNotEquals("",about);
    }

    @Ignore
    @Test
    public void validate() throws ValidationServiceOperationException {
        final File f = loadFile("/dcm/176F66C6.dcm");
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "176F66C6.dcm");

            byte[] report = service.validate(new HandledObject[]{handledObject}, Dicom3toolsValidationService.NO_PROFILE);

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xml = builder.parse(new ByteArrayInputStream(report));
            XPath xPath = XPathFactory.newInstance().newXPath();
            Assert.assertTrue((boolean)xPath.compile("boolean(//validationReport[@result='FAILED'])").evaluate(xml, XPathConstants.BOOLEAN));
            Assert.assertTrue((boolean)xPath.compile("boolean(//counters[@numberOfErrors='10'])").evaluate(xml, XPathConstants.BOOLEAN));
        } catch (ValidationServiceOperationException | XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
    }

    @Ignore
    @Test
    public void getServiceVersion() {
        Assert.assertNotEquals("",service.getServiceVersion());
    }
}
