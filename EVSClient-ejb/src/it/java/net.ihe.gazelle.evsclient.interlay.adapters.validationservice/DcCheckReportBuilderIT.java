package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import junit.framework.TestCase;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.DcCheckValidationServiceFactory;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

@RunWith(PowerMockRunner.class)
public class DcCheckReportBuilderIT extends TestCase {

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    DcCheckValidationService service;

    @Before
    public void setUp() {
        WebValidationServiceConf conf = new WebValidationServiceConf();
        conf.setValidationType(ValidationType.DICOM);
        conf.setAvailable(true);
        conf.setName("DcmCheck Validation Service");
        conf.setId(1);
        conf.setFactoryClassname(DcCheckValidationServiceFactory.class.getCanonicalName());
        conf.setUrl("http://78.117.3.59:34789/DCCHECK");
        service = new DcCheckValidationService(applicationPreferenceManager, conf);
    }

    @Test
    public void buildFailedReportTest() {
        final File f = loadFile("/dcm/dccheck-failed.html");
        String fileContent;
        try {
            fileContent = FileUtils.readFileToString(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        ValidationReport report = new DccheckReportBuilder().build(service, "dccheck", "", fileContent);
        Assert.assertEquals(ValidationTestResult.FAILED,report.getValidationOverallResult());
        Assert.assertEquals(4,report.getCounters().getNumberOfErrors().longValue());
        Assert.assertEquals(1,report.getCounters().getNumberOfWarnings().longValue());
    }


    @Test
    public void buildWarnedReportTest() {
        final File f = loadFile("/dcm/dccheck-warn.html");
        String fileContent;
        try {
            fileContent = FileUtils.readFileToString(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        ValidationReport report = new DccheckReportBuilder().build(service, "dccheck", "", fileContent);
        Assert.assertEquals(ValidationTestResult.PASSED,report.getValidationOverallResult());
        Assert.assertEquals(0,report.getCounters().getNumberOfErrors().longValue());
        Assert.assertEquals(18,report.getCounters().getNumberOfWarnings().longValue());
    }

}
