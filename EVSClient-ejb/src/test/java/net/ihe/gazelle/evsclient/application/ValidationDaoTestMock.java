package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.statistics.IntervalForStatistics;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class ValidationDaoTestMock implements ValidationDao {

   List<Validation> validations = new ArrayList<>();

   @Override
   public FilterDataModel<Validation> getFilteredDataModel(Filter<Validation> filter) {
      return null;
   }

   @Override
   public Validation merge(Validation processing) {
      return null;
   }

   @Override
   public Validation create(Validation processing)
         throws UnexpectedProcessingException, ProcessingNotSavedException, NotLoadedException {
      validations.add(processing);
      return processing;
   }

   @Override
   public Validation getObjectById(Integer id, String privacyKey, boolean addSecurityRestriction,
                                   GazelleIdentity identity) {
      return null;
   }

   @Override
   public Validation getObjectById(Integer id) {
      return null;
   }

   @Override
   public Validation getObjectByOid(String oid, String privacyKey, boolean addSecurityRestriction,
                                    GazelleIdentity identity) {
      return null;
   }

   @Override
   public Validation getObjectByOid(String oid) {
      return null;
   }

   @Override
   public Validation getObjectByToolObjectIdAndToolOid(String toolObjectId, String toolOid) {
      return null;
   }

   @Override
   public void delete(String oid) throws ProcessingNotFoundException, UnexpectedProcessingException {

   }

   @Override
   public Repository getRepositoryType(Validation validation) {
      return null;
   }

   @Override
   public int countByEntryPoint(EntryPoint entryPoint) {
      return 0;
   }

   @Override
   public int countAll() {
      return 0;
   }

   @Override
   public List<Validation> findALlByDateBetween(Date startDate, Date endDate) {
      return null;
   }

   @Override
   public int countByValidationStatus(ValidationStatus validationStatus) {
      return 0;
   }

   @Override
   public int countByValidationStatusAndReferencedStandardId(ValidationStatus validationStatus, int standardId) {
      return 0;
   }

   @Override
   public StatisticsDataRow countTotalDataRow() {
      return null;
   }

   @Override
   public List<StatisticsDataRow> countTotalDataRowGroupByReferencedStandard() {
      return null;
   }

   @Override
   public List<Validation> findAllValidationByStandardId(int id) {
      return null;
   }

   @Override
   public List<Object[]> countPerValidationServiceByReferencedStandardId(int referencedStandardId) {
      return null;
   }

   @Override
   public List<Object[]> countPerValidatorByReferencedStandardId(int standardId) {
      return null;
   }

   @Override
   public List<Object[]> countPerDateIntervalByReferencedStandardId(int referencedStandardId,
                                                                    IntervalForStatistics interval) {
      return null;
   }

   @Override
   public List<Object[]> countPerCountry() {
      return null;
   }
}
