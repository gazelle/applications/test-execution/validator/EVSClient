package net.ihe.gazelle.evsclient.statistics;

import net.ihe.gazelle.evsclient.application.statistics.IpLocDao;
import net.ihe.gazelle.evsclient.application.statistics.IpLocService;
import net.ihe.gazelle.evsclient.domain.statistics.IpLoc;
import net.ihe.gazelle.evsclient.interlay.adapters.statistics.IpGeolocation;
import net.ihe.gazelle.evsclient.interlay.dao.statistics.IpLocDaoImpl;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class IpLocServiceTest {

    private static EntityManagerFactory entityManagerFactory;

    private final IpLocDao dao;

    @BeforeClass
    public static void init(){
        entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnitTestForStatistics");
        // force ApplicationPreferenceManager to use TU EntityManager (in memory H2 database)
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(entityManagerFactory.createEntityManager());
    }

    public IpLocServiceTest() {
        dao = new IpLocDao() {
            private IpLocDaoImpl delegate  = new IpLocDaoImpl(entityManagerFactory);
            @Override
            public IpLoc findLastCheckedLocByIp(String ip) {
                return delegate.findLastCheckedLocByIp(ip);
            }

            @Override
            public IpLoc save(IpLoc ipLoc) throws IOException {
                return ipLoc; // MOCK
            }
        };
    }

    /*
    @Test
    public void checkIpStackProvider() {
        IpLocService service = new IpLocService(dao, -1, new IpStack());
        IpLoc kereval = service.computeLoc("kereval.com");
        assertEquals("France", kereval.getCountry());
    }
    */

    @Test
    public void checkIpGeolocationProvider() {
        IpLocService service = new IpLocService(dao, -1, new IpGeolocation());
        IpLoc kereval = service.computeLoc("130.93.26.102");
        assertEquals("France", kereval.getCountry());
    }
}
