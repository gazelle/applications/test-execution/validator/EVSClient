package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.MbvReportMapper;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.ValidationResultsOverview;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ReportParser(MbvReportMapper.class)
public class ValidationServiceTestMock implements ValidationService {

   @Override
   public String about() {
      return null;
   }

   @Override
   public String getName() {
      return "Validation Service Mock For Unit Tests";
   }

   @Override
   public List<Validator> getValidators() {
      return null;
   }

   @Override
   public List<Validator> getValidators(String filter) {
      return null;
   }

   @Override
   public List<String> getSupportedMediaTypes() {
      return null;
   }

   @Override
   public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
      try {
         return buildSimpleDetailedResult();
      } catch (JAXBException e) {
         throw new ValidationServiceOperationException(e);
      }
   }

   private byte[] buildSimpleDetailedResult() throws JAXBException {
      DetailedResult detailedResult = new DetailedResult();
      detailedResult.setDocumentWellFormed(buildDocumentWellFormed());
      detailedResult.setValidationResultsOverview(buildValidationResultsOverview());
      return marshall(detailedResult);
   }

   private DocumentWellFormed buildDocumentWellFormed() {
      DocumentWellFormed documentWellFormed = new DocumentWellFormed();
      documentWellFormed.setResult("PASSED");
      return documentWellFormed;
   }

   private ValidationResultsOverview buildValidationResultsOverview() {
      ValidationResultsOverview validationResultsOverview = new ValidationResultsOverview();
      validationResultsOverview.setValidationTestResult("PASSED");
      Date now = new Date();
      validationResultsOverview.setValidationDate(new SimpleDateFormat("yyyy, dd MM").format(now));
      validationResultsOverview.setValidationTime(new SimpleDateFormat("hh:mm:ss").format(now));
      return validationResultsOverview;
   }

   private byte[] marshall(DetailedResult detailedResult) throws JAXBException {

      JAXBContext jaxbContext = JAXBContext.newInstance(DetailedResult.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      try {
         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
         jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
      } catch (PropertyException e) {
         throw new JAXBException(e);
      }
      QName qName = new QName("detailedResult");
      JAXBElement<DetailedResult> root = new JAXBElement<>(qName, DetailedResult.class, detailedResult);
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      jaxbMarshaller.marshal(root, outputStream);
      return outputStream.toByteArray();
   }
}
