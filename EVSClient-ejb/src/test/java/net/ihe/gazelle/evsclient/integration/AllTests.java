package net.ihe.gazelle.evsclient.integration;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MBValidatorTest.class, SCHValidatorTest.class })
public class AllTests {

}
