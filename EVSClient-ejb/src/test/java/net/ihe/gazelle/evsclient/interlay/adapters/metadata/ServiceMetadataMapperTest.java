package net.ihe.gazelle.evsclient.interlay.adapters.metadata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.GazelleValidation10ReportMapperTest;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ServiceMetadataMapperTest {

    private static final ObjectMapper mapper = new ObjectMapper();


    @Test
    public void getValidatorsTest() throws IOException {
        String serviceMetadataJson = loadMetadata("validationProfiles.json");
        JsonNode metadataNode = mapper.readTree(serviceMetadataJson);
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        List<Validator> validators = serviceMetadataMapper.getValidators(metadataNode);

        assertEquals(2, validators.size());

        assertEquals("profileName1", validators.get(0).getName());
        assertEquals("domain1", validators.get(0).getDomain());
        assertEquals("profileId1", validators.get(0).getKeyword());

        assertEquals("profileName2", validators.get(1).getName());
        assertEquals("domain2", validators.get(1).getDomain());
        assertEquals("profileId2", validators.get(1).getKeyword());

    }

    @Test
    public void getValidatorsTestEmptyArray() throws IOException {
        String serviceMetadataJson = "[]";
        JsonNode metadataNode = mapper.readTree(serviceMetadataJson);
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        List<Validator> validators = serviceMetadataMapper.getValidators(metadataNode);

        assertEquals(0, validators.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getValidatorsTestNoName() throws IOException {
        String serviceMetadataJson = "[[]]";
        JsonNode metadataNode = mapper.readTree(serviceMetadataJson);
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        serviceMetadataMapper.getValidators(metadataNode);
    }

    @Test
    public void getServiceMetadataTest() throws IOException {
        String serviceMetadataJson = loadMetadata("serviceMetadata.json");
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        ServiceMetadata serviceMetadata = serviceMetadataMapper.getServiceMetadata(serviceMetadataJson);

        assertEquals("Validation Web Service", serviceMetadata.getServiceName());

        assertEquals(2, serviceMetadata.getValidators().size());

        assertEquals("profileName1", serviceMetadata.getValidators().get(0).getName());
        assertEquals("domain1", serviceMetadata.getValidators().get(0).getDomain());
        assertEquals("profileId1", serviceMetadata.getValidators().get(0).getKeyword());

        assertEquals("profileName2", serviceMetadata.getValidators().get(1).getName());
        assertEquals("domain2", serviceMetadata.getValidators().get(1).getDomain());
        assertEquals("profileId2", serviceMetadata.getValidators().get(1).getKeyword());

    }


    @Test(expected = IllegalArgumentException.class)
    public void getServiceMetadataTestNoProvidedInterfaces() throws IOException {

        String serviceMetadataJson = "{\"serviceName\":\"Validation Web Service\"}";
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        ServiceMetadata serviceMetadata = serviceMetadataMapper.getServiceMetadata(serviceMetadataJson);

        assertEquals("Validation Web Service", serviceMetadata.getServiceName());

        assertEquals(0, serviceMetadata.getValidators().size());


    }

    @Test
    public void getValidatorsFromJsonTest() throws IOException {
        String validationProfilesJson = "[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]";
        ServiceMetadataMapper serviceMetadataMapper = new ServiceMetadataMapper();
        List<Validator> validators = serviceMetadataMapper.getValidators(validationProfilesJson);
        assertEquals(1, validators.size());
        assertEquals("profileID", validators.get(0).getKeyword());
        assertEquals("profileName", validators.get(0).getName());
        assertEquals("domain", validators.get(0).getDomain());
    }

    private String loadMetadata(String filename) throws IOException {
        InputStream is = GazelleValidation10ReportMapperTest.class.getResourceAsStream("/metadata/" + filename);
        return IOUtils.toString(is);
    }

}
