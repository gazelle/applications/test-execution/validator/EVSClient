package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationReportDAOTest {

   private static final Logger LOG = LoggerFactory.getLogger(ValidationReportDAOTest.class);

   @Test
   public void testLoadValidationReport() {
      ValidationReportFile validationReportFile = new ValidationReportFile();
      validationReportFile.setArchivePath(getResourceFilePath("/validation-reports/validation-report-2.zip"));
      ValidationReport validationReport = validationReportFile.getContent();
      Assert.assertNotNull(validationReport);
      Assert.assertEquals("fd6fbce8-6cca-4fd2-8724-892ca66c6a8a", validationReport.getUuid());
      Assert.assertEquals(3, validationReport.getCounters().getNumberOfConstraints().intValue());
      Assert.assertEquals(2, validationReport.getCounters().getNumberOfErrors().intValue());
   }

   private String getResourceFilePath(String projectResourcePath) {
      return ValidationReportDAOTest.class.getResource(projectResourcePath).getPath();
   }

}
