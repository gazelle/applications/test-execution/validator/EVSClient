package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;

public class ValidationServiceFactoryTestMock implements ValidationServiceFactory<ValidationServiceTestMock, ValidationServiceConf> {

   public ValidationServiceFactoryTestMock() {}

   public ValidationServiceFactoryTestMock(ApplicationPreferenceManager applicationPreferenceManager,
                                           EmbeddedValidationServiceConf validationServiceConf) {}

   @Override
   public ValidationServiceTestMock getService() {
      return new ValidationServiceTestMock();
   }

   @Override
   public Class<ValidationServiceTestMock> getServiceClass() {
      return ValidationServiceTestMock.class;
   }
}
