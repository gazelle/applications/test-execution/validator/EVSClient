package net.ihe.gazelle.evsclient.integration;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import net.ihe.gazelle.evsclient.domain.validation.extension.Scorecard;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.ScorecardExtensionServiceClient;
import org.junit.Ignore;
import org.junit.Test;

// FIXME ceoche: response = request.post(String.class); should be stubbed in ScorecardServiceClient instead of calling a production CDAGen
@Ignore
public class ScorecardServiceClientTest {

	@Test
	public void testGetScorecardFromValidator() throws IOException, Exception {
		String scorecard = ScorecardExtensionServiceClient.getScorecardFromValidator("1.2.3",
				Scorecard.getBase64ScorecardInput(readDoc("src/test/resources/scorecard/mbvResult.xml"),false),
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		assertTrue(scorecard.contains("<ValidationStatistics>"));
		assertTrue(scorecard.contains("<numberExecutedCheck>4237</numberExecutedCheck>"));
		assertTrue(scorecard.contains("<numberErrorsFound>68</numberErrorsFound>"));
	}
	
	@Test
	public void testContextualInformation() throws IOException, Exception {
		String scorecard = ScorecardExtensionServiceClient.getScorecardFromValidator("1.2.3",
				Scorecard.getBase64ScorecardInput(readDoc("src/test/resources/scorecard/mbvResult.xml"),true),
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		System.out.println(scorecard);
		assertTrue(scorecard.contains("<validationIdentifier>1.2.3</validationIdentifier>"));
		assertTrue(scorecard.contains("<statisticsIdentifier>"));
		assertTrue(scorecard.contains("<statisticToolIdentifier>Gazelle CDA Validation</statisticToolIdentifier>"));
		assertTrue(scorecard.contains("<statisticVersion>"));
		assertTrue(scorecard.contains("<effectiveDate>"));
	}
	
	@Test
	public void testGetScorecardFromValidator2() throws IOException, Exception {
		String scorecard = ScorecardExtensionServiceClient.getScorecardFromValidator("1.2.3",
				Scorecard.getBase64ScorecardInput(readDoc("src/test/resources/scorecard/mbvResult-sequoia65.xml"),true),
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		System.out.println(scorecard);
		assertTrue(scorecard.contains("<ValidationStatistics>"));
	}
	
	@Test
	public void testGetScorecardFromValidator3() throws IOException, Exception {
		for (int i=0;i<10; i++) {
			String scorecard = ScorecardExtensionServiceClient.getScorecardFromValidator("1.2.3",
					Scorecard.getBase64ScorecardInput(readDoc("src/test/resources/scorecard/mbvResult-sequoia66-strict.xml"),true),
					"https://gazellecontent.sequoiaproject.org/CDAGenerator");
			assertTrue(scorecard.contains("<ValidationStatistics>"));
			assertTrue(scorecard.contains("<numberExecutedCheck>"));
			assertTrue(scorecard.contains("<numberErrorsFound>"));
		}
	}
	
	@Test
	public void testGetScorecardFromValidator4() throws IOException, Exception {
		for (int i=0;i<10; i++) {
		String scorecard = ScorecardExtensionServiceClient.getScorecardFromValidator("1.2.3",
				Scorecard.getBase64ScorecardInput(readDoc("src/test/resources/scorecard/mbvResult-sequoia66-strict.xml"),false),
				"https://gazellecontent.sequoiaproject.org/CDAGenerator");
		assertTrue(scorecard.contains("<ValidationStatistics>"));
		assertTrue(scorecard.contains("<numberExecutedCheck>"));
		assertTrue(scorecard.contains("<numberErrorsFound>"));
		}
	}

	public static byte[] readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString().getBytes(StandardCharsets.UTF_8);
	}

}
