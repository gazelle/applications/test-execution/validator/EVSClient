package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationOverview;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationSubReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;

public class ReportMapperTest {

   private static final Logger LOGGER =  LoggerFactory.getLogger(ReportMapperTest.class);
   private static final int NB_OF_CONSTRAINTS_FOR_WELLFORMED_AND_XSD = 2;
   private static final boolean DISPLAY_REPORT_FOR_DEBUG = false;

   @Test
   public void testSchematronReport()
         throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("passed-cda-schematron-mbv-report.xml",
            new MbvReportMapper());

      Assert.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      assertOverview(report,
            "Gazelle Schematron Validator",
            "2.5.0",
            "IHE - PCC - Immunization Content (IC)",
            "1.0");
      assertSubReportResult("Syntactic validation", ValidationTestResult.PASSED, report.getSubReports());
      assertSubReportResult("Schema validation", ValidationTestResult.PASSED, report.getSubReports());
      assertSubReportResult("Object Checker validation", ValidationTestResult.PASSED, report.getSubReports());
      assertCounters(report,
            18 + NB_OF_CONSTRAINTS_FOR_WELLFORMED_AND_XSD,
            0,
            17,
            0);
   }

   @Test
   public void testGOCReport()
         throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("failed-cda-mbv-report.xml", new CDAGeneratorReportMapper());

      assertOverview(report,
            "Gazelle CDA Validation",
            "2.2.2",
            "IHE - PCC - Common templates",
            "20180312");
      Assert.assertEquals(ValidationTestResult.FAILED, report.getValidationOverallResult());
      assertCounters(report,
            209 + NB_OF_CONSTRAINTS_FOR_WELLFORMED_AND_XSD,
            13,
            9,
            0);
   }

   @Test
   public void testHL7v2Report()
         throws ReportTransformationException, DisplayException, ParseException {
      ValidationReport report = transformValidationReport("aborted-hl7v2-report.xml", new Hl7ReportMapper());

      assertOverview(report,
            "Gazelle HL7 Validator",
            "3.7.3",
            "1.3.6.1.4.12559.11.1.1.129",
            "Unknown");
      Assert.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult());
      // FIXME net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview must be reworked to avoid resetting the
      // date each time the original report is unmarshalled.
      // Date expectedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse("2021-10-27T08:17:00");
      // Assert.assertEquals(expectedDate, report.getValidationOverview().getValidationDateTime());
      assertCounters(report,
            444,
            9,
            27,
            0);
   }

   @Test
   public void testXDStarReport()
         throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("passed-xdstar-mbv-report.xml", new MbvReportMapper());

      assertOverview(report,
            "Gazelle XDSMetadata Validation",
            "2.5.8",
            "Unknown",
            "Unknown");
      Assert.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      assertCounters(report,
            5,
            0,
            0,
            0);
   }

   @Test
   public void testAbortedSchematronReport()
         throws ReportTransformationException, Exception {
      ValidationReport report = transformValidationReport("aborted-cda-schematron-mbv-report.xml",
            new MbvReportMapper());

      Assert.assertEquals(ValidationTestResult.FAILED, report.getValidationOverallResult());
      assertCounters(report,
            1,
            1,
            0,
            0);
   }

   @Test
   public void testXValReport() throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("failed-x-val-report.xml", new CrossValidatorReportMapper());
      Assert.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult());
      assertOverview(report,
            "Gazelle Cross Validation",
            "1.4.4",
            "CROSS_VALIDATEUR_GENERIQUE",
            "2022-4");
      assertCounters(report,
            44,
            5,
            0,
            0);
   }

   @Test
   public void testGitbReport() throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("passed-gitb-dsd-report.xml", new GitbReportMapper());
      Assert.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult());
      assertCounters(report,
              1,
              0,
              0,
              0);
   }

   @Test
   public void testFailGitbReport() throws ReportTransformationException, DisplayException {
      ValidationReport report = transformValidationReport("failed-gitb-dsd-report.xml", new GitbReportMapper());
      Assert.assertEquals(ValidationTestResult.FAILED, report.getValidationOverallResult());
      assertCounters(report,
              3,
              3,
              0,
              0);
   }

   private ValidationReport transformValidationReport(String resourceFileName,
                                                      ValidationReportMapper reportMapper)
         throws ReportTransformationException, DisplayException {
      ValidationReport report = reportMapper.transform(getOriginalReport(resourceFileName));
      displayReport4Debug(report);
      return report;
   }

   private void assertOverview(ValidationReport report, String validationServiceName, String validationServiceVersion,
                               String validator,
                               String validatorVersion) {
      ValidationOverview overview = report.getValidationOverview();
      Assert.assertEquals(validationServiceName, overview.getValidationServiceName());
      Assert.assertEquals(validationServiceVersion, overview.getValidationServiceVersion());
      Assert.assertEquals(validator, overview.getValidatorID());
      Assert.assertEquals(validatorVersion, overview.getValidatorVersion());
   }

   private void assertCounters(ValidationReport report, int numberOfConstraints, int numberOfErrors,
                               int numberOfWarnings, int numberOfFailedInfo) {
      Assert.assertEquals("Number of constraints should be " + numberOfConstraints,
            numberOfConstraints, report.getCounters().getNumberOfConstraints().intValue());
      Assert.assertEquals("Number of errors should be " + numberOfErrors,
            numberOfErrors, report.getCounters().getNumberOfErrors().intValue());
      Assert.assertEquals("Number of warnings should be " + numberOfWarnings,
            numberOfWarnings, report.getCounters().getNumberOfWarnings().intValue());
      Assert.assertEquals("Number of info should be " + numberOfFailedInfo,
            numberOfFailedInfo, report.getCounters().getFailedWithInfoNumber().intValue());
   }

   private void assertSubReportResult(String subReportName, ValidationTestResult expectedResult,
                                      List<ValidationSubReport> subReports) {
      for (ValidationSubReport vsr : subReports) {
         if (vsr.getName().equals(subReportName)) {
            Assert.assertEquals(expectedResult, vsr.getSubReportResult());
            return;
         }
      }
      Assert.fail(String.format("Sub report with name %s has not been found", subReportName));
   }

   private void displayReport4Debug(ValidationReport report) throws DisplayException {
      if (DISPLAY_REPORT_FOR_DEBUG) {
         try {
            LOGGER.info(new XMLValidationReportMarshaller().marshal(report));
         } catch (MarshallingException e) {
            throw new DisplayException(e);
         }
      }
   }

   private byte[] getOriginalReport(String reportName) {
      try {
         return Files.readAllBytes(
               Paths.get(ReportMapperTest.class.getResource("/original-reports/" + reportName).getPath()));
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   private static class DisplayException extends Exception {
      private static final long serialVersionUID = -8408625504761054210L;

      public DisplayException(Throwable throwable) {
         super(throwable);
      }
   }
}
