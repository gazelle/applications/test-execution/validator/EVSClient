package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ValidationReportMarhsallerTest {

   @Test
   public void testDefaultMarshalReport() {
      testMarshalReport(
            TestDataValidationReport.getDefault().getValidationReportResource(),
            TestDataValidationReport.getDefault().getValidationReport()
      );
   }

   @Test
   public void testMinimalMarshalReport() {
      testMarshalReport(
            TestDataValidationReport.getMinimal().getValidationReportResource(),
            TestDataValidationReport.getMinimal().getValidationReport()
      );
   }

   @Test
   public void testFullMarshalReport() {
      testMarshalReport(
            TestDataValidationReport.getFull().getValidationReportResource(),
            TestDataValidationReport.getFull().getValidationReport()
      );
   }

   private void testMarshalReport(String resourceReport, ValidationReport testReport) {
      XMLValidationReportMarshaller marshaller = new XMLValidationReportMarshaller();

      byte[] reportBytes = xmlMarshall(marshaller, testReport);

      assertExpectedMarshalledReport(resourceReport, reportBytes);

      ValidationReport unmarshalledValidationReport = xmlUnmarshall(marshaller, reportBytes);

      Assert.assertNotNull(unmarshalledValidationReport);

//      Uncomment to debug the last assert.
//      reportBytes = xmlMarshall(marshaller, unmarshalledValidationReport);
//      assertExpectedMarshalledReport(resourceReport, reportBytes);

      Assert.assertEquals(testReport, unmarshalledValidationReport);
   }

   private byte[] xmlMarshall(XMLValidationReportMarshaller marshaller, ValidationReport report) {
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
         marshaller.marshal(report, baos);
         return baos.toByteArray();
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   private ValidationReport xmlUnmarshall(XMLValidationReportMarshaller marshaller, byte[] reportBytes) {
      try (InputStream inputStream = new ByteArrayInputStream(reportBytes)) {
         return marshaller.unmarshal(inputStream);
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   private void assertExpectedMarshalledReport(String expectedResource, byte[] reportBytes) {
      Assert.assertNotNull(reportBytes);
      String reportString = new String(reportBytes, StandardCharsets.UTF_8);
      Assert.assertEquals(getExpectedMarhsalledReport(expectedResource), reportString);
   }

   private String getExpectedMarhsalledReport(String resource) {
      try {
         return new String(IOUtils.toByteArray(ValidationReportMarhsallerTest.class.getResourceAsStream(
               resource)), StandardCharsets.UTF_8);
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

}
