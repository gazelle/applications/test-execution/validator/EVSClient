/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application.security;

import net.ihe.gazelle.evsclient.domain.security.ApiKey;
import net.ihe.gazelle.evsclient.interlay.dao.security.ApiKeyDaoImpl;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.security.Credentials;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PreferenceService.class, GazelleIdentityImpl.class})
public class ApiKeyManagerTest {

    private static final String OWNER = "ADMIN";
    private static final String ORGANIZATION = "ADMIN_ORG";
    ApiKeyManager apiKeyManager;

    @Mock
    ApiKeyDao mockedApiKeyDao;
    @Mock
    GazelleIdentityImpl mockedSsoIdentity;

    @Before
    public void setUp() {
        apiKeyManager = new ApiKeyManager(mockedApiKeyDao);
    }

    @Test
    public void testAuthenticate() throws ParseException, ApiKeyNotFoundException, InvalidApiKeyException {
        String apiKey = "validKey";
        doReturn(getValidApiKey(apiKey)).when(mockedApiKeyDao).findByValue(apiKey);
        GazelleIdentity identity = apiKeyManager.authenticate(apiKey);
        assertEquals("When authentified with an apiKey, the identity must " +
              "be loaded with the owner of the apiKey", OWNER, identity.getUsername());
        assertEquals("When authentified with an apiKey, the identity must " +
              "be loaded with the organization's owner of the apiKey", ORGANIZATION, identity.getOrganisationKeyword());
    }

    @Test(expected = InvalidApiKeyException.class)
    public void testAuthenticateExpiredKeyError() throws InvalidApiKeyException, ParseException,
          ApiKeyNotFoundException {
        String apiKey = "expiredKey";
        doReturn(getExpiredApiKey(apiKey)).when(mockedApiKeyDao).findByValue(apiKey);
        apiKeyManager.authenticate(apiKey);
    }

    @Test(expected = InvalidApiKeyException.class)
    public void testAuthenticateNotFoundKeyError() throws ApiKeyNotFoundException, InvalidApiKeyException {
        String apiKey = "unknownKey";
        doThrow(ApiKeyNotFoundException.class).when(mockedApiKeyDao).findByValue(apiKey);
        apiKeyManager.authenticate(apiKey);
    }

    @Test
    public void generatedKeyShouldRespectLengthRestriction() {
        mockPreferenceService(1);
        String currentUser = "admin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        Credentials credentials = new Credentials();
        credentials.setUsername(currentUser);
        mockApiKeyDaoCreate();


        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        assertEquals(ApiKeyManager.API_KEY_LENGTH, returnedKey.getValue().length());
    }

    @Test
    public void generatedKeyShouldHaveACreationDate() {
        int validityDays = 10;
        mockPreferenceService(validityDays);

        String currentUser = "admin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        Credentials credentials = new Credentials();
        credentials.setUsername(currentUser);
        mockSsoIdentityCredentials(credentials);
        mockApiKeyDaoCreate();


        Calendar testInstant = Calendar.getInstance();
        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        long timeDiff = returnedKey.getCreationDate().getTime() - testInstant.getTime().getTime();
        assertTrue("Api key must have a creation date set to now (within 1 sec margin error).",
              timeDiff < 1000);
    }

    @Test
    public void generatedKeyShouldExpireGivenThePreference() {
        int validityDays = 10;
        mockPreferenceService(validityDays);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, validityDays);

        String currentUser = "admin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        Credentials credentials = new Credentials();
        credentials.setUsername(currentUser);
        mockSsoIdentityCredentials(credentials);
        mockApiKeyDaoCreate();


        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        assertEquals(sdf.format(cal.getTime()), sdf.format(returnedKey.getExpirationDate()));
    }

    @Test
    public void generatedKeyShouldExpireGivenThePreference2() {
        int validityDays = 60;
        mockPreferenceService(validityDays);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, validityDays);

        String currentUser = "admin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        Credentials credentials = new Credentials();
        credentials.setUsername(currentUser);
        mockSsoIdentityCredentials(credentials);
        mockApiKeyDaoCreate();


        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        assertEquals(sdf.format(cal.getTime()), sdf.format(returnedKey.getExpirationDate()));
    }

    @Test
    public void generatedKeyOwnerShouldBeCurrentUsername() {
        int validityDays = 1;
        mockPreferenceService(validityDays);
        String currentUser = "superadmin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        mockSsoIdentityCredentialsUsername(currentUser);
        mockApiKeyDaoCreate();

        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        assertEquals(currentUser, returnedKey.getOwner());
    }

    @Test
    public void generatedKeyOwnerShouldBeCurrentUsername2() {
        int validityDays = 1;
        mockPreferenceService(validityDays);
        String currentUser = "jdupont";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        mockSsoIdentityCredentialsUsername(currentUser);
        mockApiKeyDaoCreate();

        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        assertEquals(currentUser, returnedKey.getOwner());
    }

    @Test
    public void generatedKeyOwnerWillBeNullIfNotLoggedIn() {
        mockPreferenceService(1);
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(false);
        mockApiKeyDaoCreate();

        ApiKey returnedKey = apiKeyManager.createNewApiKeyForCurrentUser();

        assertNull(returnedKey.getOwner());
    }

    @Test
    public void isAllKeysExpiredShouldReturnTrueIfNoKey() {
        boolean res = apiKeyManager.isAllKeysOfCurrentUserExpired(new ArrayList<ApiKey>());

        assertTrue(res);
    }

    @Test
    public void isAllKeysExpiredShouldReturnTrueIfAllKeysAreExpired() {
        boolean res = apiKeyManager.isAllKeysOfCurrentUserExpired(forgeListOfExpiredKeys("user"));

        assertTrue(res);
    }

    @Test
    public void isAllKeysExpiredShouldReturnFalseIfAnyKeyIsExpired() {
        boolean res = apiKeyManager.isAllKeysOfCurrentUserExpired(forgeListWithAnExpiredKey("user"));

        assertFalse(res);
    }

    @Test
    public void isAllKeysExpiredShouldReturnFalseIfAllKeysAreValid() {
        boolean res = apiKeyManager.isAllKeysOfCurrentUserExpired(forgeListOfValidKeys("user"));

        assertFalse(res);
    }

    @Test
    public void getAllKeysShouldReturnAllKeysOfCurrentUser() {
        String currentUser = "oui-oui";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        mockSsoIdentityCredentialsUsername(currentUser);
        List<ApiKey> apiKeys = forgeListWithAnExpiredKey(currentUser);
        mockApiKeyDaoGetKeys(apiKeys, currentUser);

        List<ApiKey> returnedList = apiKeyManager.getAllKeysOfCurrentUser();

        assertNotNull(returnedList);
        assertEquals(apiKeys.size(), returnedList.size());
        assertThat(returnedList.size(), is(3));
        assertEquals(apiKeys.get(0), returnedList.get(0));
        assertEquals(apiKeys.get(1), returnedList.get(1));
        assertEquals(apiKeys.get(2), returnedList.get(2));
    }

    @Test
    public void getAllKeysShouldReturnAnEmptyListIfNoKey() {
        String currentUser = "admin";
        mockSsoIdentityInstance();
        mockSsoIdentityIsLoggedIn(true);
        Credentials credentials = new Credentials();
        credentials.setUsername(currentUser);
        mockSsoIdentityCredentials(credentials);
        List<ApiKey> listForCurrentUser = new ArrayList<>();
        mockApiKeyDaoGetKeys(listForCurrentUser, currentUser);

        List<ApiKey> returnedList = apiKeyManager.getAllKeysOfCurrentUser();

        assertNotNull(returnedList);
        assertTrue(returnedList.isEmpty());
    }

    private List<ApiKey> forgeListOfExpiredKeys(String owner) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, -1);
        ApiKey expired = new ApiKey("mockedValue1", owner, owner, new Date(), cal.getTime());
        cal.add(Calendar.DATE, -1);
        ApiKey expired2 = new ApiKey("mockedValue2", owner, owner, new Date(), cal.getTime());
        cal.set(Calendar.YEAR, 2012);
        ApiKey expired3 = new ApiKey("mockedValue3", owner, owner, new Date(), cal.getTime());
        return Arrays.asList(expired, expired2, expired3);
    }

    private List<ApiKey> forgeListWithAnExpiredKey(String owner) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, -1);
        ApiKey expired = new ApiKey("mockedValue4", owner, owner, new Date(), cal.getTime());
        cal.add(Calendar.MONTH, 1);
        ApiKey valid = new ApiKey("mockedValue5", owner, owner, new Date(), cal.getTime());
        cal.add(Calendar.YEAR, 1);
        ApiKey valid2 = new ApiKey("mockedValue6", owner, owner, new Date(), cal.getTime());
        return Arrays.asList(valid, expired, valid2);
    }

    private List<ApiKey> forgeListOfValidKeys(String owner) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        ApiKey valid = new ApiKey("mock", owner, owner, new Date(), cal.getTime());
        cal.add(Calendar.MONTH, 1);
        ApiKey valid2 = new ApiKey("mock", owner, owner, new Date(), cal.getTime());
        cal.add(Calendar.YEAR, 1);
        ApiKey valid3 = new ApiKey("mock", owner, owner, new Date(), cal.getTime());
        return Arrays.asList(valid, valid2, valid3);
    }

    private void mockApiKeyDaoCreate() {
        mockedApiKeyDao = Mockito.mock(ApiKeyDaoImpl.class);
        apiKeyManager.setApiKeyDao(mockedApiKeyDao);
        Mockito.when(mockedApiKeyDao.createKey(Mockito.<ApiKey>any())).thenAnswer(new Answer<ApiKey>() {
            @Override
            public ApiKey answer(InvocationOnMock iom) {
                Object[] args = iom.getArguments();
                return (ApiKey) args[0];
            }
        });
    }

    private void mockApiKeyDaoGetKeys(List<ApiKey> forgedApiKeys, String username) {
        mockedApiKeyDao = Mockito.mock(ApiKeyDaoImpl.class);
        apiKeyManager.setApiKeyDao(mockedApiKeyDao);
        Mockito.when(mockedApiKeyDao.getAllKeysOfUser(username)).thenReturn(forgedApiKeys);
    }

    private void mockSsoIdentityInstance() {
        PowerMockito.mockStatic(GazelleIdentityImpl.class);
        mockedSsoIdentity = Mockito.mock(GazelleIdentityImpl.class);
        PowerMockito.when(GazelleIdentityImpl.instance())
                .thenReturn(mockedSsoIdentity);
    }

    private void mockSsoIdentityIsLoggedIn(boolean isLoggedIn) {
        Mockito.when(mockedSsoIdentity.isLoggedIn()).thenReturn(isLoggedIn);
    }

    // FIXME verify usage of Credentials: seems no longer used
    private void mockSsoIdentityCredentials(Credentials forgedCredentials) {
        Mockito.when(mockedSsoIdentity.getCredentials()).thenReturn(forgedCredentials);
    }

    private void mockSsoIdentityCredentialsUsername(String forgedUsername) {
        Mockito.when(mockedSsoIdentity.getUsername()).thenReturn(forgedUsername);
    }

    private void mockPreferenceService(Integer validityDays) {
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getInteger("api_key_validity_days")).thenReturn(validityDays);
    }

    private ApiKey getValidApiKey(String apiKey) throws ParseException {
        return new ApiKey(apiKey, OWNER, ORGANIZATION, new Date(), new SimpleDateFormat( "yyyyMMdd" ).parse( "20990820" ));
    }

    private ApiKey getExpiredApiKey(String apiKey) throws ParseException {
        return new ApiKey(apiKey, OWNER, ORGANIZATION, new SimpleDateFormat( "yyyyMMdd" ).parse( "20200920" ), new SimpleDateFormat( "yyyyMMdd" ).parse( "20200820" ));
    }

}
