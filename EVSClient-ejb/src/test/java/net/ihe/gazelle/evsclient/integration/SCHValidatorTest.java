package net.ihe.gazelle.evsclient.integration;

import junit.framework.TestCase;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import net.ihe.gazelle.sch.validator.ws.client.SchematronServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.SchematronValidatorServiceStub;
import org.apache.axis2.Constants;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;

/**
 * Class Description : Integration testing related to webservices used on the validation of CDA Documents using the tool SchematronValidator.
 * The Schematron validator offer a webservice that allow to get the list of schematron, and allow to validate a document against a type of validation
 * <p/>
 * The aim of this class is to test the reachability of this webservice
 *
 * @author abderrazek boufahja / Kereval / IHE-Europe
 * @version 1.0 - 22 jul 2013
 * @see > abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 */
@Ignore
// [ceoche] FIXME those are not unit test. please make them independant from any deployed schematronvalidator
public class SCHValidatorTest extends TestCase {

    private static final String SCH_ENDPOINT =
             "https://gazelle.ihe.net/SchematronValidator-ejb/GazelleObjectValidatorService/GazelleObjectValidator?wsdl";

    /**
     * Get the list of vailable schematrons related to a kind of document
     *
     * @throws RemoteException
     * @throws SOAPExceptionException
     */
    @Ignore
    @Test
    public void testListSchematrons() throws RemoteException, SOAPExceptionException {
        SchematronServiceStub stub = new SchematronServiceStub(SCH_ENDPOINT);
        stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
        SchematronServiceStub.GetSchematronsForAGivenType params = new SchematronServiceStub.GetSchematronsForAGivenType();
        params.setFileType("CDA-IHE");
        SchematronServiceStub.GetSchematronsForAGivenTypeE paramsE = new SchematronServiceStub.GetSchematronsForAGivenTypeE();
        paramsE.setGetSchematronsForAGivenType(params);
        SchematronServiceStub.GetSchematronsForAGivenTypeResponseE responseE = stub.getSchematronsForAGivenType(paramsE);
        SchematronServiceStub.Schematron[] schematrons = responseE
                .getGetSchematronsForAGivenTypeResponse().get_return();
        assertTrue(schematrons.length > 0);
    }

    /**
     * validate a document with schematronValidator according to a kind of schemtron
     *
     * @throws RemoteException
     * @throws SOAPExceptionException
     */
    @Ignore
    @Test
    public void testValidationDocumentAgainstSchematron() throws IOException, SOAPExceptionException {
        SchematronValidatorServiceStub stub = new SchematronValidatorServiceStub(SCH_ENDPOINT);
        stub._getServiceClient().getOptions().setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, true);
        SchematronValidatorServiceStub.ValidateBase64Document params = new SchematronValidatorServiceStub.ValidateBase64Document();
        params.setValidator("IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)");
        params.setBase64Document(DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get("src/test/resources/cda/sample1.xml"))));
        SchematronValidatorServiceStub.ValidateBase64DocumentE paramsE = new SchematronValidatorServiceStub.ValidateBase64DocumentE();
        paramsE.setValidateBase64Document(params);
        SchematronValidatorServiceStub.ValidateBase64DocumentResponseE responseE = stub.validateBase64Document(paramsE);
        String detailedResult = responseE.getValidateBase64DocumentResponse().get_return();
        assertTrue(detailedResult.contains("<ValidationTestResult>"));
    }

}
