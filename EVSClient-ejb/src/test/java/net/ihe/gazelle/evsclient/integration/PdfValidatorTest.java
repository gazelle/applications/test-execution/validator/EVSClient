package net.ihe.gazelle.evsclient.integration;

import junit.framework.TestCase;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.PdfValidationService;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.PdfValidationServiceFactory;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.sch.validator.ws.client.SOAPExceptionException;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

/**
 * Class Description : Integration testing related to webservices used on the validation of CDA Documents using the tool SchematronValidator.
 * The Schematron validator offer a webservice that allow to get the list of schematron, and allow to validate a document against a type of validation
 * <p/>
 * The aim of this class is to test the reachability of this webservice
 *
 * @author abderrazek boufahja / Kereval / IHE-Europe
 * @version 1.0 - 22 jul 2013
 * @see > abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 */
@RunWith(PowerMockRunner.class)
public class PdfValidatorTest extends TestCase {

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    PdfValidationService pdfValidationService;

    @Before
    public void setUp() {
        EmbeddedValidationServiceConf conf = new EmbeddedValidationServiceConf();
        conf.setValidationType(ValidationType.PDF);
        conf.setAvailable(true);
        conf.setName("PDF Validation Service");
        conf.setId(1);
        conf.setFactoryClassname(PdfValidationServiceFactory.class.getCanonicalName());
        pdfValidationService = new PdfValidationService(applicationPreferenceManager,conf);
    }
    /**
     * Get the list of available validator
     */
    @Test
    public void testListValidator() throws RemoteException, SOAPExceptionException {
        assertTrue(pdfValidationService.getValidators().size()==1);
        assertEquals(pdfValidationService.getValidators().get(0).getKeyword(),"JHOVE");
    }

    /**
     * validate a document with pdf validator
     */
    @Test
    public void testPdfAValidDocumentAgainstPdfValidator() {
        final File f = loadFile("/pdf/2055.pdf");
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "document.pdf");

            byte[] report = pdfValidationService.validate(new HandledObject[]{handledObject}, "JHOVE");

            DocumentBuilder builder = new XmlDocumentBuilderFactory().getBuilder();
            Document xml = builder.parse(new ByteArrayInputStream(report));
            XPath xPath = XPathFactory.newInstance().newXPath();
            Assert.assertTrue((boolean)xPath.compile("boolean(//validationReport[@result='PASSED'])").evaluate(xml, XPathConstants.BOOLEAN));

        } catch (ValidationServiceOperationException | XPathExpressionException | DocumentBuilderFactoryException | SAXException | IOException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
    }

    /**
     * validate a document with pdf validator
     */
    @Test
    public void testInvalidDocumentAgainstPdfValidator() {
        final File f = loadFile("/pdf/document.pdf");
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "document.pdf");

            byte[] report = pdfValidationService.validate(new HandledObject[]{handledObject}, "JHOVE");

            DocumentBuilder builder = new XmlDocumentBuilderFactory().getBuilder();
            Document xml = builder.parse(new ByteArrayInputStream(report));
            XPath xPath = XPathFactory.newInstance().newXPath();
            Assert.assertTrue((boolean)xPath.compile("boolean(//validationReport[@result='FAILED'])").evaluate(xml, XPathConstants.BOOLEAN));

        } catch (ValidationServiceOperationException | XPathExpressionException | DocumentBuilderFactoryException | SAXException | IOException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
    }

}
