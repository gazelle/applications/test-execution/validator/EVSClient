package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import org.jboss.seam.mock.EnhancedMockHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(JUnitParamsRunner.class)
public class GetValidationInfoLegacyFilterTest {

   private static final String GET_VALIDATION_STATUS = "/GetValidationStatus";
   private static final String GET_VALIDATION_DATE = "/GetValidationDate";
   private static final String GET_VALIDATION_PERMANENT_LINK = "/GetValidationPermanentLink";
   private static final String GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID = "/GetLastResultStatusByExternalId";
   private static final String GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID = "/GetValidationPermanentLinkByExternalId";

   private static final String EVS_REST = "/evs/rest";
   private static final String EVS_REST_GET_VALIDATION_STATUS = EVS_REST + GET_VALIDATION_STATUS;
   private static final String EVS_REST_GET_VALIDATION_DATE = EVS_REST + GET_VALIDATION_DATE;
   private static final String EVS_REST_GET_VALIDATION_PERMANENT_LINK = EVS_REST + GET_VALIDATION_PERMANENT_LINK;
   private static final String EVS_REST_GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID = EVS_REST +
         GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID;
   private static final String EVS_REST_GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID =
         EVS_REST + GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID;

   private static final String PARAM_OID = "oid";
   private static final String PARAM_EXTERNAL_ID = "externalId";
   private static final String PARAM_TOOL_OID = "toolOid";


   private static final String KNOWN_OID_NEW_EVS = "1.2.3.74";
   private static final String KNOWN_OID_LEGACY_EVS = "1.3.6.1.4.1.12559.11.36.3.1.16355";
   private static final String KNOWN_EXT_ID_NEW_EVS = "stepdata_129813";
   private static final String KNOWN_EXT_ID_LEGACY_EVS = "stepdata_129815";
   private static final String CALLING_TOOL_OID = "1.3.6.1.4.1.12559.11.1.5";


   private HttpServletRequest servletRequest;
   private EnhancedMockHttpServletResponse servletResponse;
   private ArgumentCaptor<Integer> statusCaptor;
   private FilterChain filterChain;
   private ApplicationPreferenceManager applicationPreferenceManager;

   @Before
   public void setUp() throws IOException {
      servletResponse = new EnhancedMockHttpServletResponse();
      servletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
      statusCaptor = ArgumentCaptor.forClass(Integer.class);
      filterChain = new NewEvsFilterChainStub();
      mockApplicationPreferenceManager();
   }

   @Test
   @Parameters(method = "getEvsDataOidQuery, getLegacyEvsDataOidQuery")
   public void testGetValidationInfoWithOid(String query, String oidParam, int expectedStatus, String expectedBody)
         throws ServletException, IOException {
      GetValidationInfoLegacyFilter filter = setUpFilter();

      filter.doFilter(
            mockServletRequestOid(query, oidParam),
            servletResponse,
            filterChain);

      assertEquals(expectedStatus, servletResponse.getStatus());
      assertEquals(expectedBody, servletResponse.getContentAsString());
   }

   @Test
   @Parameters(method = "getLegacyEvsDataOidQuery")
   public void testGetValidationInfoFilterOff(String query, String oidParam, int expectedStatus, String expectedBody)
         throws ServletException, IOException {
      GetValidationInfoLegacyFilter filter = setUpDisabledFilter();

      filter.doFilter(
            mockServletRequestOid(query, oidParam),
            servletResponse,
            filterChain);

      assertEquals(HttpServletResponse.SC_NO_CONTENT, servletResponse.getStatus());
      assertEquals("", servletResponse.getContentAsString());
   }

   //FIXME new GetValidationInfo should always return PASSED, FAILED... instead of DONE_PASSED, DONE_FAILED...
   public Object getEvsDataOidQuery() {
      return new Object[]{
            new Object[]{EVS_REST_GET_VALIDATION_STATUS, KNOWN_OID_NEW_EVS, HttpServletResponse.SC_OK, "PASSED"},
            new Object[]{EVS_REST_GET_VALIDATION_DATE, KNOWN_OID_NEW_EVS,
                  HttpServletResponse.SC_OK, "17/01/22 11:40:20 (CET GMT+0100)"},
            new Object[]{EVS_REST_GET_VALIDATION_PERMANENT_LINK, KNOWN_OID_NEW_EVS,
                  HttpServletResponse.SC_OK, "http://localhost:8780/EVSClient/detailedResult.seam?oid=1.2.3.74"}
      };
   }

   public Object getLegacyEvsDataOidQuery() {
      return new Object[]{
            new Object[]{EVS_REST_GET_VALIDATION_STATUS, KNOWN_OID_LEGACY_EVS, HttpServletResponse.SC_OK, "PASSED"},
            new Object[]{EVS_REST_GET_VALIDATION_DATE, KNOWN_OID_LEGACY_EVS, HttpServletResponse.SC_OK, "2/8/22 5:30:13 " +
                  "PM (CET GMT+0100)"},
            new Object[]{EVS_REST_GET_VALIDATION_PERMANENT_LINK, KNOWN_OID_LEGACY_EVS,
                  HttpServletResponse.SC_OK, "https://qualification2.ihe-europe.net/EVSClient/detailedResult.seam" +
                  "?type=SAML&oid=1.3.6.1.4.1.12559.11.36.3.1.16355"}
      };
   }

   @Test
   @Parameters(method = "getEvsDataExternalIdQuery, getLegacyEvsDataExternalIdQuery")
   public void testGetValidationInfoWithExternalId(String query, String externalId, int expectedStatus,
                                                   String expectedBody)
         throws ServletException, IOException {
      GetValidationInfoLegacyFilter filter = setUpFilter();

      filter.doFilter(
            mockServletRequestExternalId(query, externalId),
            servletResponse,
            filterChain);

      assertEquals(expectedStatus, servletResponse.getStatus());
      assertEquals(expectedBody, servletResponse.getContentAsString());
   }

   //FIXME new GetValidationInfo should always return PASSED, FAILED... instead of DONE_PASSED, DONE_FAILED...
   public Object getEvsDataExternalIdQuery() {
      return new Object[]{
            new Object[]{EVS_REST_GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID, KNOWN_EXT_ID_NEW_EVS,
                  HttpServletResponse.SC_OK,
                  "FAILED"},
            new Object[]{EVS_REST_GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID, KNOWN_EXT_ID_NEW_EVS,
                  HttpServletResponse.SC_OK, "http://localhost:8780/EVSClient/detailedResult.seam?oid=1.2.3.136"}
      };
   }

   public Object getLegacyEvsDataExternalIdQuery() {
      return new Object[]{
            new Object[]{EVS_REST_GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID, KNOWN_EXT_ID_LEGACY_EVS,
                  HttpServletResponse.SC_OK,
                  "FAILED"},
            new Object[]{EVS_REST_GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID, KNOWN_EXT_ID_LEGACY_EVS,
                  HttpServletResponse.SC_OK, "https://qualification2.ihe-europe.net/EVSClient/detailedResult" +
                  ".seam?type=XDS&oid=1.3.6.1.4.1.12559.11.36.3.1.16356"}
      };
   }

   private void mockApplicationPreferenceManager() {
      applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
      Mockito.when(applicationPreferenceManager.getBooleanValue("legacy_results_redirection_enabled"))
            .thenReturn(true);
      Mockito.when(applicationPreferenceManager.getStringValue("legacy_results_evs_url"))
            .thenReturn("https://qualification2.ihe-europe.net/EVSClient/");
   }

   private GetValidationInfoLegacyFilter setUpFilter() {
      GetValidationInfoLegacyFilter filter = new FilterWithStubbedArchivedEVSClient(applicationPreferenceManager);
      return filter;
   }

   private GetValidationInfoLegacyFilter setUpDisabledFilter() {
      Mockito.when(applicationPreferenceManager.getBooleanValue("legacy_results_redirection_enabled"))
            .thenReturn(false);
      return setUpFilter();
   }

   private HttpServletRequest mockServletRequestOid(String requestURI, String oid) {
      Map<String, String> parameters = new HashMap<>();
      parameters.put(PARAM_OID, oid);
      servletRequest = Mockito.mock(HttpServletRequest.class);
      Mockito.when(servletRequest.getRequestURI()).thenReturn(requestURI);
      Mockito.when(servletRequest.getParameter(PARAM_OID)).thenReturn(parameters.get(PARAM_OID));
      Mockito.when(servletRequest.getParameterMap()).thenReturn(parameters);
      return servletRequest;
   }

   private HttpServletRequest mockServletRequestExternalId(String requestURI, String externalId) {
      Map<String, String> parameters = new HashMap<>();
      parameters.put(PARAM_EXTERNAL_ID, externalId);
      parameters.put(PARAM_TOOL_OID, CALLING_TOOL_OID);

      servletRequest = Mockito.mock(HttpServletRequest.class);
      Mockito.when(servletRequest.getRequestURI()).thenReturn(requestURI);
      Mockito.when(servletRequest.getParameter(PARAM_EXTERNAL_ID)).thenReturn(parameters.get(PARAM_EXTERNAL_ID));
      Mockito.when(servletRequest.getParameter(PARAM_TOOL_OID)).thenReturn(parameters.get(PARAM_TOOL_OID));
      Mockito.when(servletRequest.getParameterMap()).thenReturn(parameters);
      return servletRequest;
   }

   private static class FilterWithStubbedArchivedEVSClient extends GetValidationInfoLegacyFilter {

      public FilterWithStubbedArchivedEVSClient(ApplicationPreferenceManager applicationPreferenceManager) {
         super(applicationPreferenceManager);
      }

      @Override
      // STUB for LEGACY EVS, instead of really calling another EVS with an HTTP Client.
      protected GetValidationInfoLegacyFilter.HttpResponse performHttpGet(URL archivedEvsQueryURL)
            throws IOException {
         String query = archivedEvsQueryURL.toString();
         if (query.contains("oid=" + KNOWN_OID_LEGACY_EVS)) {
            if (query.contains(GET_VALIDATION_STATUS)) {
               return new HttpResponse(HttpServletResponse.SC_OK, "PASSED");
            } else if (query.contains(GET_VALIDATION_DATE)) {
               return new HttpResponse(HttpServletResponse.SC_OK, "2/8/22 5:30:13 PM (CET GMT+0100)");
            } else if (query.contains(GET_VALIDATION_PERMANENT_LINK)) {
               return new HttpResponse(HttpServletResponse.SC_OK, "https://qualification2.ihe-europe.net" +
                     "/EVSClient/detailedResult.seam?type=SAML&oid=1.3.6.1.4.1.12559.11.36.3.1.16355");
            }
         } else if (query.contains("externalId=" + KNOWN_EXT_ID_LEGACY_EVS)) {
            if(query.contains(GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID)) {
               return new HttpResponse(HttpServletResponse.SC_OK, "FAILED");
            } else if (query.contains(GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID)) {
               return new HttpResponse(HttpServletResponse.SC_OK, "https://qualification2.ihe-europe.net/EVSClient/detailedResult" +
               ".seam?type=XDS&oid=1.3.6.1.4.1.12559.11.36.3.1.16356");
            }
         }
         return new HttpResponse(HttpServletResponse.SC_NO_CONTENT, null);
      }
   }

   // STUB for GetValidationInfo Service
   private static class NewEvsFilterChainStub implements FilterChain {

      @Override
      public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse)
            throws IOException, ServletException {
         HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
         switch (httpServletRequest.getRequestURI()) {
            case EVS_REST_GET_VALIDATION_STATUS:
               serveBasedOnOidParam(servletResponse, httpServletRequest, "PASSED");
               break;
            case EVS_REST_GET_VALIDATION_DATE:
               serveBasedOnOidParam(servletResponse, httpServletRequest, "17/01/22 11:40:20 (CET GMT+0100)");
               break;
            case EVS_REST_GET_VALIDATION_PERMANENT_LINK:
               serveBasedOnOidParam(servletResponse, httpServletRequest,
                     "http://localhost:8780/EVSClient/detailedResult.seam?oid=1.2.3.74");
               break;
            case EVS_REST_GET_LAST_RESULT_STATUS_BY_EXTERNAL_ID:
               serveBasedOnExternalIdParam(servletResponse, httpServletRequest, "FAILED");
               break;
            case EVS_REST_GET_VALIDATION_PERMANENT_LINK_BY_EXTERNAL_ID:
               serveBasedOnExternalIdParam(servletResponse, httpServletRequest,
                     "http://localhost:8780/EVSClient/detailedResult.seam?oid=1.2.3.136");
               break;
            default:
               writeResponse(servletResponse, HttpServletResponse.SC_OK, "OTHER CONTENT");
         }

      }

      private void serveBasedOnOidParam(ServletResponse servletResponse, HttpServletRequest httpServletRequest,
                                        String body) throws IOException {
         if (KNOWN_OID_NEW_EVS.equals(httpServletRequest.getParameter(PARAM_OID))) {
            writeResponse(servletResponse, HttpServletResponse.SC_OK, body);
         } else {
            writeResponse(servletResponse, HttpServletResponse.SC_NO_CONTENT, null);
         }
      }

      private void serveBasedOnExternalIdParam(ServletResponse servletResponse, HttpServletRequest httpServletRequest,
                                               String body) throws IOException {
         if (KNOWN_EXT_ID_NEW_EVS.equals(httpServletRequest.getParameter(PARAM_EXTERNAL_ID)) &&
               CALLING_TOOL_OID.equals(httpServletRequest.getParameter(PARAM_TOOL_OID))) {
            writeResponse(servletResponse, HttpServletResponse.SC_OK, body);
         } else {
            writeResponse(servletResponse, HttpServletResponse.SC_NO_CONTENT, null);
         }
      }

      private void writeResponse(ServletResponse servletResponse, int status, String body) throws IOException {
         ((HttpServletResponse) servletResponse).setStatus(status);
         if (body != null) {
            PrintWriter printWriter = servletResponse.getWriter();
            printWriter.print(body);
            printWriter.flush();
         }
      }
   }


}
