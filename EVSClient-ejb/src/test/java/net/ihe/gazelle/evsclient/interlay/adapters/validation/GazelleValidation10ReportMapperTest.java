package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintPriority;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.*;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

public class GazelleValidation10ReportMapperTest {

    private static GazelleValidation10ReportMapper gazelleValidation10ReportMapper;

    private static ObjectMapper mapper;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    @BeforeClass
    public static void init() {
        gazelleValidation10ReportMapper = new GazelleValidation10ReportMapper();
        mapper = new ObjectMapper();
    }

    @Test
    public void testConvertValidRetroReport() throws IOException {
        String newReport = loadReport("new-validation-report.json");
        ValidationReportDTO validationReportDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReport);
        ValidationReport validationReport = validationReportDTO.toDomain();
        assertEquals(loadValidRetroReport(), validationReport);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertInvalidRetroReport() throws IOException {
        String newReport = "{\"field1\":\"value1\",\"field2\":\"value2\"}";
        ValidationReportDTO validationReportDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReport);
        validationReportDTO.toDomain();
    }

    @Test(expected = JsonParseException.class)
    public void testConvertInvalidJsonRetroReport() throws IOException {
        String newReport = "{\"field1\":\"value1\",\"field2\":\"value2\"";
        gazelleValidation10ReportMapper.convertToRetroReport(newReport);
    }

    @Test
    public void testGetValidationOverviewJsonObject() throws IOException {
        String newReportForOverview = "{  " +
                "    \"validationMethod\": {\n" +
                "       \"validationServiceName\": \"ValidationService\"," +
                "       \"validationServiceVersion\": \"1.0\"," +
                "       \"validationProfileID\": \"ValidationProfile\"," +
                "       \"validationProfileVersion\": \"1.0\"" +
                "     }," +
                "     \"disclaimer\": \"disc\"," +
                "     \"dateTime\": \"2019-01-01T00:00:00.000Z\"" +
                "}";
        JsonNode node = mapper.readTree(newReportForOverview);
        ObjectNode overviewJsonObject = gazelleValidation10ReportMapper.getValidationOverviewJsonObject(node);
        Assert.assertNotNull(overviewJsonObject);
        Assert.assertEquals("ValidationService", overviewJsonObject.get("validationServiceName").asText());
        Assert.assertEquals("1.0", overviewJsonObject.get("validationServiceVersion").asText());
        Assert.assertEquals("ValidationProfile", overviewJsonObject.get("validatorID").asText());
        Assert.assertEquals("1.0", overviewJsonObject.get("validatorVersion").asText());
        Assert.assertEquals("disc", overviewJsonObject.get("disclaimer").asText());
        Assert.assertEquals("2019-01-01T00:00:00.000Z", overviewJsonObject.get("validationDateTime").asText());

        ValidationOverviewDTO validationOverviewDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReportForOverview).getValidationOverview();
        assertEquals("ValidationService", validationOverviewDTO.getValidationServiceName());
        assertEquals("1.0", validationOverviewDTO.getValidationServiceVersion());
        assertEquals("ValidationProfile", validationOverviewDTO.getValidatorID());
        assertEquals("1.0", validationOverviewDTO.getValidatorVersion());
        assertEquals("disc", validationOverviewDTO.getDisclaimer());
        // Convert XMLGregorianCalendar to Date
        assertEquals("2019-01-01T00:00:00.000Z", validationOverviewDTO.getValidationDateTime().toString());


    }

    @Test
    public void testGetValidationOverviewJsonObjectWithNullValidationMethod() throws IOException {
        String newReportForOverview = "{  " +
                "     \"disclaimer\": \"disc\"," +
                "     \"dateTime\": \"2019-01-01T00:00:00.000+0100\"" +
                "}";
        JsonNode node = mapper.readTree(newReportForOverview);
        ObjectNode overviewJsonObject = gazelleValidation10ReportMapper.getValidationOverviewJsonObject(node);
        Assert.assertNotNull(overviewJsonObject);
        Assert.assertNull(overviewJsonObject.get("validationServiceName"));
        Assert.assertNull(overviewJsonObject.get("validationServiceVersion"));
        Assert.assertNull(overviewJsonObject.get("validatorID"));
        Assert.assertNull(overviewJsonObject.get("validatorVersion"));
        Assert.assertEquals("disc", overviewJsonObject.get("disclaimer").asText());
        Assert.assertEquals("2019-01-01T00:00:00.000+0100", overviewJsonObject.get("validationDateTime").asText());
    }

    @Test
    public void testGetCountersJsonObject() throws IOException {
        String newReportForCounters = "{ \"counters\": { " +
                "     \"numberOfAssertions\": 1," +
                "     \"numberOfFailedWithInfos\": 2," +
                "     \"numberOfFailedWithWarnings\": 3," +
                "     \"numberOfFailedWithErrors\": 4" +
                "     }" +
                "}";
        JsonNode node = mapper.readTree(newReportForCounters);
        ObjectNode countersJsonObject = gazelleValidation10ReportMapper.getCountersJsonObject(node, "counters");
        Assert.assertNotNull(countersJsonObject);
        Assert.assertEquals(1, countersJsonObject.get("numberOfConstraints").asInt());
        Assert.assertEquals(2, countersJsonObject.get("failedWithInfoNumber").asInt());
        Assert.assertEquals(3, countersJsonObject.get("numberOfWarnings").asInt());
        Assert.assertEquals(4, countersJsonObject.get("numberOfErrors").asInt());

        ValidationCountersDTO validationCountersDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReportForCounters).getCounters();
        assertEquals((Integer) 1, validationCountersDTO.getNumberOfConstraints());
        assertEquals((Integer) 2, validationCountersDTO.getFailedWithInfoNumber());
        assertEquals((Integer) 3, validationCountersDTO.getNumberOfWarnings());
        assertEquals((Integer) 4, validationCountersDTO.getNumberOfErrors());

    }

    @Test
    public void testGetAdditionalMetadataJsonObject() throws IOException {
        String newReportForAdditionalMetadata = "{\n" +
                "  \"overallResult\": \"PASSED\",\n" +
                "  \"validationMethod\": {\n" +
                "    \"validationServiceName\": \"ValidationService\",\n" +
                "    \"validationServiceVersion\": \"1.0\",\n" +
                "    \"validationProfileID\": \"ValidationProfile\",\n" +
                "    \"validationProfileVersion\": \"1.0\"\n" +
                "  },\n" +
                "  \n" +
                "  \"additionalMetadata\": [\n" +
                "    {\n" +
                "      \"name\": \"metadataId\",\n" +
                "      \"value\": \"metadataValue\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JsonNode node = mapper.readTree(newReportForAdditionalMetadata);
        ArrayNode additionalMetadataJsonObject = gazelleValidation10ReportMapper.getAdditionalMetadataJsonObject(node);
        Assert.assertNotNull(additionalMetadataJsonObject);
        Assert.assertEquals("metadataId", additionalMetadataJsonObject.get(0).get("name").asText());
        Assert.assertEquals("metadataValue", additionalMetadataJsonObject.get(0).get("value").asText());

        List<MetadataDTO> validationAdditionalMetadataDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReportForAdditionalMetadata)
                .getValidationOverview().getAdditionalMetadata();
        assertEquals(1, validationAdditionalMetadataDTO.size());
        assertEquals("metadataId", validationAdditionalMetadataDTO.get(0).getName());
        assertEquals("metadataValue", validationAdditionalMetadataDTO.get(0).getValue());
    }

    @Test
    public void testGetAdditionalMetadataJsonObjectWithNullAValidationMethod() throws IOException {
        String newReportForAdditionalMetadata = "{\n" +
                "  \"overallResult\": \"PASSED\",\n" +
                "  \n" +
                "  \"additionalMetadata\": [\n" +
                "    {\n" +
                "      \"name\": \"metadataId\",\n" +
                "      \"value\": \"metadataValue\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        JsonNode node = mapper.readTree(newReportForAdditionalMetadata);
        ArrayNode additionalMetadataJsonObject = gazelleValidation10ReportMapper.getAdditionalMetadataJsonObject(node);
        Assert.assertNotNull(additionalMetadataJsonObject);
        Assert.assertEquals("metadataId", additionalMetadataJsonObject.get(0).get("name").asText());
        Assert.assertEquals("metadataValue", additionalMetadataJsonObject.get(0).get("value").asText());
        assertNull(gazelleValidation10ReportMapper.convertToRetroReport(newReportForAdditionalMetadata)
                .getValidationOverview().getAdditionalMetadata());
    }

    @Test
    public void testGetSubReportsJsonObject() throws IOException {
        String newReportForSubReports = loadReport("new-subreport.json");
        JsonNode node = mapper.readTree(newReportForSubReports);
        ArrayNode subReportsJsonObject = gazelleValidation10ReportMapper.getSubReportsJsonObject(node,"reports");
        Assert.assertNotNull(subReportsJsonObject);
        assertEquals(1, subReportsJsonObject.size());
        Assert.assertEquals("validationSubReport3", subReportsJsonObject.get(0).get("name").asText());
        assertNotNull(subReportsJsonObject.get(0).get("standards"));
        assertEquals("standard3",subReportsJsonObject.get(0).get("standards").get(0).asText());
        assertEquals("standard4",subReportsJsonObject.get(0).get("standards").get(1).asText());
        assertEquals("FAILED",subReportsJsonObject.get(0).get("subReportResult").asText());
        assertEquals(1,subReportsJsonObject.get(0).get("subReports").size());
        assertEquals("validationSubReport4",subReportsJsonObject.get(0).get("subReports").get(0).get("name").asText());
        assertEquals("standard5",subReportsJsonObject.get(0).get("subReports").get(0).get("standards").get(0).asText());
        assertEquals("standard6",subReportsJsonObject.get(0).get("subReports").get(0).get("standards").get(1).asText());
        assertEquals("FAILED",subReportsJsonObject.get(0).get("subReports").get(0).get("subReportResult").asText());
        assertEquals(0,subReportsJsonObject.get(0).get("subReports").get(0).get("subReports").size());
        assertEquals(1,subReportsJsonObject.get(0).get("subReports").get(0).get("constraints").size());

        List<ValidationSubReportDTO> validationSubReportsDTO = gazelleValidation10ReportMapper.convertToRetroReport(newReportForSubReports)
                .getSubReports();
        assertEquals(1, validationSubReportsDTO.size());
        assertEquals("validationSubReport3", validationSubReportsDTO.get(0).getName());
        assertEquals(2, validationSubReportsDTO.get(0).getStandards().size());
        assertEquals("standard3", validationSubReportsDTO.get(0).getStandards().get(0));
        assertEquals("standard4", validationSubReportsDTO.get(0).getStandards().get(1));
        assertEquals(ValidationTestResult.FAILED, validationSubReportsDTO.get(0).getSubReportResult());
        assertEquals(1, validationSubReportsDTO.get(0).getSubReports().size());
        assertEquals("validationSubReport4", validationSubReportsDTO.get(0).getSubReports().get(0).getName());
        assertEquals(2, validationSubReportsDTO.get(0).getSubReports().get(0).getStandards().size());
        assertEquals("standard5", validationSubReportsDTO.get(0).getSubReports().get(0).getStandards().get(0));
        assertEquals("standard6", validationSubReportsDTO.get(0).getSubReports().get(0).getStandards().get(1));
        assertEquals(ValidationTestResult.FAILED, validationSubReportsDTO.get(0).getSubReports().get(0).getSubReportResult());
        assertEquals(0, validationSubReportsDTO.get(0).getSubReports().get(0).getSubReports().size());
        assertEquals(1, validationSubReportsDTO.get(0).getSubReports().get(0).getConstraints().size());

    }

    @Test
    public void testGetConstraintsJsonObject() throws IOException {
        String newReportForAssertions = loadReport("new-assertions.json");
        JsonNode node = mapper.readTree(newReportForAssertions);
        ArrayNode constraintsJsonObject = gazelleValidation10ReportMapper.getConstraintsJsonObject(node);
        Assert.assertNotNull(constraintsJsonObject);
        assertEquals(1, constraintsJsonObject.size());
        Assert.assertEquals("assertionID", constraintsJsonObject.get(0).get("constraintID").asText());
        Assert.assertEquals("assertionType", constraintsJsonObject.get(0).get("constraintType").asText());
        Assert.assertEquals("description", constraintsJsonObject.get(0).get("constraintDescription").asText());
        Assert.assertEquals("FAILED", constraintsJsonObject.get(0).get("testResult").asText());
        Assert.assertEquals("line 1, column 1", constraintsJsonObject.get(0).get("locationInValidatedObject").asText());
        Assert.assertEquals("subjectValue", constraintsJsonObject.get(0).get("valueInValidatedObject").asText());
        Assert.assertEquals(2, constraintsJsonObject.get(0).get("assertionID").size());
        Assert.assertEquals("requirementID1", constraintsJsonObject.get(0).get("assertionID").get(0).asText());
        Assert.assertEquals("requirementID2", constraintsJsonObject.get(0).get("assertionID").get(1).asText());
        Assert.assertEquals("ERROR", constraintsJsonObject.get(0).get("severity").asText());
        Assert.assertEquals("PERMITTED", constraintsJsonObject.get(0).get("priority").asText());
        Assert.assertEquals(1, constraintsJsonObject.get(0).get("unexpectedErrors").size());

        List<ConstraintValidationDTO> validationConstraintsDTO = gazelleValidation10ReportMapper.convertToRetroReport(loadReport("new-subreport.json"))
                .getSubReports().get(0).getConstraints();
        assertEquals(1, validationConstraintsDTO.size());
        assertEquals("assertionID", validationConstraintsDTO.get(0).getConstraintID());
        assertEquals("assertionType", validationConstraintsDTO.get(0).getConstraintType());
        assertEquals("description", validationConstraintsDTO.get(0).getConstraintDescription());
        assertEquals(ValidationTestResult.FAILED, validationConstraintsDTO.get(0).getTestResult());
        assertEquals("line 1, column 1", validationConstraintsDTO.get(0).getLocationInValidatedObject());
        assertEquals("subjectValue", validationConstraintsDTO.get(0).getValueInValidatedObject());
        assertEquals(2, validationConstraintsDTO.get(0).getAssertionIDs().size());
        assertEquals("requirementID1", validationConstraintsDTO.get(0).getAssertionIDs().get(0));
        assertEquals("requirementID2", validationConstraintsDTO.get(0).getAssertionIDs().get(1));
        assertEquals(SeverityLevel.ERROR, validationConstraintsDTO.get(0).getSeverity());
        assertEquals(ConstraintPriority.PERMITTED, validationConstraintsDTO.get(0).getPriority());
        assertEquals(1, validationConstraintsDTO.get(0).getUnexpectedErrors().size());

    }

    @Test
    public void testGetUnexpectedErrorsJsonObject() throws IOException {
        String newReportForAssertions = "{\"unexpectedErrors\": [\n" +
                "        {\n" +
                "          \"name\": \"name\",\n" +
                "          \"message\": \"message\",\n" +
                "          \"cause\": {\n" +
                "            \"name\": \"causeName\",\n" +
                "            \"message\": \"causeMessage\",\n" +
                "            \"cause\": null\n" +
                "          }\n" +
                "        }\n" +
                "      ]}";

        JsonNode node = mapper.readTree(newReportForAssertions);
        ArrayNode unexpectedErrorsJsonObject = gazelleValidation10ReportMapper.getUnexpectedErrorsJsonObject(node);
        Assert.assertNotNull(unexpectedErrorsJsonObject);
        assertEquals(1, unexpectedErrorsJsonObject.size());
        Assert.assertEquals("name", unexpectedErrorsJsonObject.get(0).get("name").asText());
        Assert.assertEquals("message", unexpectedErrorsJsonObject.get(0).get("message").asText());
        assertNotNull(unexpectedErrorsJsonObject.get(0).get("cause"));
        Assert.assertEquals("causeName", unexpectedErrorsJsonObject.get(0).get("cause").get("name").asText());
        Assert.assertEquals("causeMessage", unexpectedErrorsJsonObject.get(0).get("cause").get("message").asText());
        assertNull(unexpectedErrorsJsonObject.get(0).get("cause").get("cause"));


        List<UnexpectedErrorDTO> unexpectedErrorsDTO = gazelleValidation10ReportMapper.convertToRetroReport(loadReport("new-subreport.json"))
                .getSubReports().get(0).getConstraints().get(0).getUnexpectedErrors();
        assertEquals(1, unexpectedErrorsDTO.size());
        assertEquals("name", unexpectedErrorsDTO.get(0).getName());
        assertEquals("message", unexpectedErrorsDTO.get(0).getMessage());
        assertNotNull(unexpectedErrorsDTO.get(0).getCause());
        assertEquals("causeName", unexpectedErrorsDTO.get(0).getCause().getName());
        assertEquals("causeMessage", unexpectedErrorsDTO.get(0).getCause().getMessage());
        assertNull(unexpectedErrorsDTO.get(0).getCause().getCause());
    }


    private String loadReport(String filename) throws IOException {
        InputStream is = GazelleValidation10ReportMapperTest.class.getResourceAsStream("/validation-reports/" + filename);
        return IOUtils.toString(is);
    }

    private ValidationReport loadValidRetroReport() throws IOException {
        String report = loadReport("retro-validation-report.json");
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(report, ValidationReportDTO.class).toDomain();
    }

    private void prettyPrint(ValidationReportDTO validationReport) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(validationReport);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}
