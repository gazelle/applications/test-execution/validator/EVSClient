package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintPriority;
import net.ihe.gazelle.evsclient.domain.validation.report.ConstraintValidation;
import net.ihe.gazelle.evsclient.domain.validation.report.Metadata;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationOverview;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationSubReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;

import javax.xml.bind.JAXBException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

class TestDataValidationReport {

   // STATIC METHODS

   static TestDataValidationReport getDefault() {
      return new TestDataValidationReport(
            "/validation-reports/validation-report-default.xml",
            instantiateDefault()
      );
   }


   static TestDataValidationReport getMinimal() {
      return new TestDataValidationReport(
            "/validation-reports/validation-report-minimal.xml",
            instantiateMinimal()
      );
   }

   static TestDataValidationReport getFull() {
      return new TestDataValidationReport(
            "/validation-reports/validation-report-full.xml",
            instantiateFull()
      );
   }

   private static ValidationReport instantiateDefault() {
      ValidationReport report = new ValidationReport("0123456789abcdef",
            new ValidationOverview(
                  "Disclaimer",
                  asDate("2021-11-22T10:20:02.206+01:00"),
                  "Service name",
                  "1.0",
                  "Validator name"));

      ValidationSubReport subReport1 = new ValidationSubReport("Sub report 1", new ArrayList<String>());
      subReport1.addConstraintValidation(new ConstraintValidation("Constraint 1 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.PASSED));
      subReport1.addConstraintValidation(new ConstraintValidation("Constraint 2 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.PASSED));

      ValidationSubReport subReport2 = new ValidationSubReport("Sub report 2", new ArrayList<String>());
      subReport2.addConstraintValidation(new ConstraintValidation("Constraint 3 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.FAILED));
      subReport2.addConstraintValidation(new ConstraintValidation("Constraint 4 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.PASSED));

      report.addSubReport(subReport1);
      report.addSubReport(subReport2);
      return report;
   }

   private static ValidationReport instantiateMinimal() {
      return new ValidationReport("0123456789abcdef",
            new ValidationOverview(
                  "Disclaimer",
                  asDate("2021-11-22T10:20:02.206+01:00"),
                  "Service name",
                  "1.0",
                  "Validator name"));
   }

   private static ValidationReport instantiateFull() {
      ValidationReport report = new ValidationReport("0123456789abcdef",
            new ValidationOverview(
                  "Disclaimer",
                  asDate("2021-11-22T10:20:02.206+01:00"),
                  "Service name",
                  "1.0",
                  "Validator name",
                  "Validator 2021-11",
                  Arrays.asList(
                        new Metadata("Profile", "TEST_U"),
                        new Metadata("Template", "1.2.3.4"))));

      ValidationSubReport subReport0 = new ValidationSubReport("Sub report empty", new ArrayList<String>());
      subReport0.addUnexpectedError(new IllegalArgumentException("exception example"));

      ValidationSubReport subReport1 = new ValidationSubReport("Sub report 1", Arrays.asList(
            "XML", "OASIS WS-TRUST"));
      ConstraintValidation constraintWithException = new ConstraintValidation("Constraint with exception",
              ConstraintPriority.MANDATORY, ValidationTestResult.UNDEFINED);
      constraintWithException.addUnexpectedError(new JAXBException("exception example", new NullPointerException("Cause " +
              "example")));
      subReport1.addConstraintValidation(constraintWithException);

      subReport1.addConstraintValidation(new ConstraintValidation("Constraint 1 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.PASSED));
      subReport1.addConstraintValidation(new ConstraintValidation(
            "ws-trust-2",
            "data-type",
            "Constraint description...",
            "data instanceof typeA",
            "line 20, column 47",
            "label",
            Arrays.asList("WS-23","WS-24"),
            null,
            ConstraintPriority.MANDATORY,
            ValidationTestResult.PASSED));


      ValidationSubReport subReport2 = new ValidationSubReport("Sub report 2", new ArrayList<String>());
      subReport2.addConstraintValidation(new ConstraintValidation("Constraint 4 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.FAILED));

      ValidationSubReport subSubReport1 = new ValidationSubReport("Sub sub report 1 :)", new ArrayList<String>());
      subSubReport1.addConstraintValidation(new ConstraintValidation("Constraint 5 description",
            ConstraintPriority.MANDATORY, ValidationTestResult.PASSED));

      subReport2.addSubReport(subSubReport1);

      report.addSubReport(subReport0);
      report.addSubReport(subReport1);
      report.addSubReport(subReport2);
      return report;
   }

   private static Date asDate(String date) {
      try {
         return new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX").parse(date);
      } catch (ParseException e) {
         throw new RuntimeException(e);
      }
   }

   // INSTANCE ATTRIBUTES AND METHOD

   private String validationReportResource;
   private ValidationReport validationReport;

   // Private constructor for singleton
   private TestDataValidationReport(String validationReportResource,
                                    ValidationReport validationReport) {
      this.validationReportResource = validationReportResource;
      this.validationReport = validationReport;
   }

   public String getValidationReportResource() {
      return validationReportResource;
   }

   public ValidationReport getValidationReport() {
      return validationReport;
   }
}
