INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'DICOM_WEB_file_types', 'txt, url', 'java.lang.String','Files extensions that can be used for PDF Validation');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'dicom_web_repository', '/opt/evs/validatedObjects/DICOM_WEB', 'java.lang.String', 'Where to store DICOM_WEB files');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'TLS_file_types', 'pem, txt, PEM, TXT, CRT, crt', 'java.lang.String', 'Files extensions that can be used for X509 certificate Validation');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'default_dsig_service_wsdl', 'http://localhost/xml-signature-validator-jar/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl', 'java.lang.String','XMLSignature validation service wsdl to be used for validating embedded signatures');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'show_styled_report', 'true', 'java.lang.Boolean', 'Show styled validation report.');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'show_xml_report', 'true', 'java.lang.Boolean', 'Show validation report XML source.');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'show_uploaded_message_analyzer', 'true', 'java.lang.Boolean', 'Show uploaded message analyzer.');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'analyze_uploaded_message', 'false', 'java.lang.Boolean', 'auto execute analyzer on uploaded message.');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'show_message_analyzer', 'false', 'java.lang.Boolean', 'Show message analyzer in file content section.');
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description) VALUES (nextval('cmn_application_preference_id_seq'), 'has_filter_mode', 'true', 'java.lang.Boolean', 'Show xpath node filter in XML tree.');

INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_host', 'localhost', 'java.lang.String', 'SMTP Host for sending emails')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_password', 'password', 'java.lang.String', 'SMTP Password for sending emails')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_port', '25', 'java.lang.Integer', 'SMTP Port for sending emails')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'smtp_username', 'username', 'java.lang.String', 'SMTP Username for sending emails')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'use_specific_smtp', 'false', 'java.lang.Boolean', 'If true will use information from smtp_host, smtp_usernamen and smtp_password. Otherwize will use localhost on port 25  ')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'add_on_x_validation_enabled', 'false', 'java.lang.Boolean', 'Indicates if the X validation module is accessible in add-ons menu')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'document_bin_view_mode_columns', '60', 'java.lang.Integer', 'Number of chars by lines in binary file pretty view.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'document_pretty_view_mode', 'true', 'java.lang.Boolean', 'Default view mode (when pretty mode exists).')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'mca_force_show_namespaces_status', 'false', 'java.lang.Boolean', 'Show tip when no namespaces where added to an XML part.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'mca_part_edit_mode_enabled', 'false', 'java.lang.Boolean', 'Enable edit mode on message content analysis part.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'show_uploaded_message', 'false', 'java.lang.Boolean', 'Show uploaded message.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'ws_validation_report_severity_threshold', 'INFO', 'java.lang.String', 'Default severity level to filter validation reports notifications in web-service calls (value can be INFO, WARNING and ERROR). This value can also be modified by web-service clients with the query parameter ''severityThreshold''.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'ip_loc_provider', 'net.ihe.gazelle.evsclient.interlay.adapters.statistics.IpGeolocation', 'java.lang.String', 'Qualified name of IpLocProvider implementation class (expert mode only). If empty, ''net.ihe.gazelle.evsclient.interlay.adapters.statistics.IpGeolocation'' will be used.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'ip_loc_service_configuration', 'https://api.ipgeolocation.io/ipgeo?apiKey=7854d7fe1d7b460980d57fdec5261062&ip={0}', 'java.lang.String', 'Configuration for IP Location Service. On IpGeolocation service, it''s the api url *including* *apikey* (Default is ''https://api.ipgeolocation.io/ipgeo?apiKey=7854d7fe1d7b460980d57fdec5261062&ip={0}'')')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'ip_loc_service_local_country', 'France', 'java.lang.String', 'Default country, in case of local IP or unavailable IP location service. If empty, "France" will be used.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'ip_loc_validity_period', '7', 'java.lang.String', 'Validity period, in days, of IP Location ; when expired, location validity will be checked again on API call. If empty or less than 1 location will be checked at every EVS API call. Default value is empty.')
    ON CONFLICT (preference_name) DO NOTHING;
INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
    VALUES (nextval('cmn_application_preference_id_seq'), 'xval_repository', '/opt/evs/validatedObjects/XVAL', 'java.lang.String', 'Where to store X validation files')
    ON CONFLICT (preference_name) DO NOTHING;



-- Update cda_scorecard_xsl_path to scorecard_xsl_path
UPDATE cmn_application_preference SET preference_name = 'scorecard_xsl_path' WHERE preference_name = 'cda_scorecard_xsl_path';

-- Create indexes
CREATE INDEX IF NOT EXISTS evsc_processing_oid_index ON evsc_processing(oid);
CREATE INDEX IF NOT EXISTS evsc_processing_date_index ON evsc_processing(validation_date);

CREATE INDEX IF NOT EXISTS evsc_validation_validation_service_index ON evsc_validation(validation_service);
CREATE INDEX IF NOT EXISTS evsc_validation_validator_keyword_index ON evsc_validation(validator_keyword);
CREATE INDEX IF NOT EXISTS evsc_validation_status_index ON evsc_validation(status);

CREATE INDEX IF NOT EXISTS evsc_owner_metadata_username_index ON evsc_owner_metadata(username);
CREATE INDEX IF NOT EXISTS evsc_owner_metadata_organization_index ON evsc_owner_metadata(organization);

CREATE INDEX IF NOT EXISTS evsc_caller_metadata_entrypoint_index ON evsc_caller_metadata(entrypoint);

CREATE INDEX IF NOT EXISTS cmn_application_preference_preference_name_index ON cmn_application_preference(preference_name);
CREATE INDEX IF NOT EXISTS cmn_application_preference_preference_value_index ON cmn_application_preference(preference_value);
CREATE INDEX IF NOT EXISTS cmn_application_preference_class_name_index ON cmn_application_preference(class_name);

CREATE INDEX IF NOT EXISTS evsc_validation_service_conf_display_name_index ON evsc_validation_service_conf(display_name);
CREATE INDEX IF NOT EXISTS evsc_validation_service_conf_validation_type_index ON evsc_validation_service_conf(validation_type);

CREATE INDEX IF NOT EXISTS evsc_referenced_standard_display_name_index ON evsc_referenced_standard(display_name);
CREATE INDEX IF NOT EXISTS evsc_referenced_standard_validation_type_index ON evsc_referenced_standard(validation_type);

-- Update schema

-- Create evsc_validation_service_conf table
CREATE TABLE public.evsc_extension_service_conf (
        id integer NOT NULL,
        description text,
        display_name character varying(255),
        validation_type character varying(255) NOT NULL,
        factory_canonical_classname character varying(255),
        available boolean
);

ALTER TABLE public.evsc_extension_service_conf OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_extension_service_conf
    ADD CONSTRAINT evsc_extension_service_conf_pkey PRIMARY KEY (id);

-- Create evsc_web_extension_service_conf table
CREATE TABLE public.evsc_web_extension_service_conf (
        url character varying(255),
        zip_transport boolean,
        id integer NOT NULL
);

ALTER TABLE public.evsc_web_extension_service_conf OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_web_extension_service_conf
    ADD CONSTRAINT evsc_web_extension_service_conf_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_web_extension_service_conf
    ADD CONSTRAINT fk_e4flec41r2kxu1p1w0p3qbd2n FOREIGN KEY (id) REFERENCES public.evsc_extension_service_conf(id);

-- Create evsc_scorecard_service_conf table

CREATE TABLE public.evsc_scorecard_service_conf (
    id integer NOT NULL
);

ALTER TABLE public.evsc_scorecard_service_conf OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_scorecard_service_conf
    ADD CONSTRAINT evsc_scorecard_service_conf_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_scorecard_service_conf
    ADD CONSTRAINT fk_ii13oh1ck5lvwqt07g755b4o5 FOREIGN KEY (id) REFERENCES public.evsc_web_extension_service_conf(id);


-- Create evsc_digital_signature_service_conf table

CREATE TABLE public.evsc_digital_signature_service_conf (
    id integer NOT NULL
);

ALTER TABLE public.evsc_digital_signature_service_conf OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_digital_signature_service_conf
    ADD CONSTRAINT evsc_digital_signature_service_conf_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_digital_signature_service_conf
    ADD CONSTRAINT fk_t0f0erds6a1q7j55dyh0td70i FOREIGN KEY (id) REFERENCES public.evsc_web_extension_service_conf(id);


-- evsc_validation_extension

CREATE TABLE public.evsc_validation_extension (
      status character varying(255),
      validation_oid character varying(255),
      id integer NOT NULL,
      extension_report_id integer
);

ALTER TABLE public.evsc_validation_extension OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT evsc_validation_extension_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT fk_qdfwx53ur9vdac4vqr4kmo4y8 FOREIGN KEY (extension_report_id) REFERENCES public.evsc_report(id);

ALTER TABLE ONLY public.evsc_validation_extension
    ADD CONSTRAINT fk_s9jty93uscbkopxei2idfdmvu FOREIGN KEY (id) REFERENCES public.evsc_processing(id);


-- evsc_validation_digital_signature

CREATE TABLE public.evsc_validation_digital_signature (
    id integer NOT NULL
);

ALTER TABLE public.evsc_validation_digital_signature OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_validation_digital_signature
    ADD CONSTRAINT evsc_validation_digital_signature_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_validation_digital_signature
    ADD CONSTRAINT fk_pgntid6j1vvh51aq2n0ir6sy7 FOREIGN KEY (id) REFERENCES public.evsc_validation_extension(id);

-- evsc_validation_scorecard

CREATE TABLE public.evsc_validation_scorecard (
    id integer NOT NULL
);

ALTER TABLE public.evsc_validation_scorecard OWNER TO gazelle;

ALTER TABLE ONLY public.evsc_validation_scorecard
    ADD CONSTRAINT evsc_validation_scorecard_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.evsc_validation_scorecard
    ADD CONSTRAINT fk_p8uvuhy8ne9rxtmfw7kf494s9 FOREIGN KEY (id) REFERENCES public.evsc_validation_extension(id);

