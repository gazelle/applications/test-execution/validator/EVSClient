Pre-requisites:
- jdk 1.8
- maven 3.x
- installed evsclient (version>=5.13.4-SNAPSHOT)

To run integration tests (set `it.evs-ws-host system variable to the expected host) :
> mvn verify -Dit.evs-ws-host=https://qualification.ihe-europe.net

To skip Integration Tests : 

```
mvn clean install -DskipITs
