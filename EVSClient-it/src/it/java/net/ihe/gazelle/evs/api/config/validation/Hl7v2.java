package net.ihe.gazelle.evs.api.config.validation;

public class Hl7v2 {

    public ValidationRequest adt_a31_adt_a05() {
        return new ValidationRequest("/it/evs/ws/services/hl7v2/adt-a31-adt_a05/service",
                "/it/evs/ws/services/hl7v2/adt-a31-adt_a05/validator",
                "/it/evs/ws/services/hl7v2/adt-a31-adt_a05/filename");
    }
    public ValidationRequest adt_a54() {
        return new ValidationRequest("/it/evs/ws/services/hl7v2/adt_a54/service",
        "/it/evs/ws/services/hl7v2/adt_a54/validator",
        "/it/evs/ws/services/hl7v2/adt_a54/filename");
    }
    public ValidationRequest adt_a28_adt_a05() {
        return new ValidationRequest("/it/evs/ws/services/hl7v2/adt-a28-adt_a05/service",
                "/it/evs/ws/services/hl7v2/adt-a28-adt_a05/validator",
                "/it/evs/ws/services/hl7v2/adt-a28-adt_a05/filename");
    }
    public ValidationRequest passed() {
        return new ValidationRequest("/it/evs/ws/services/hl7v2/passed/service",
                "/it/evs/ws/services/hl7v2/passed/validator",
                "/it/evs/ws/services/hl7v2/passed/filename");
    }
}
