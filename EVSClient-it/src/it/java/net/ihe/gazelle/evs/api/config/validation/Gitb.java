package net.ihe.gazelle.evs.api.config.validation;

public class Gitb {

    public ValidationRequest oots() {
        return new ValidationRequest("/it/evs/ws/services/default/gitb/service",
                "/it/evs/ws/services/default/gitb/validator",
                "/it/evs/ws/services/default/gitb/filename");
    }
}
