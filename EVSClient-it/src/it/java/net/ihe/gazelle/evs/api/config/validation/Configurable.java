package net.ihe.gazelle.evs.api.config.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.ihe.gazelle.evs.api.IT;

import java.io.IOException;

public class Configurable {

    protected JsonNode applicationConfig;

    public Configurable() {
        initApplicationConfig();
    }

    public JsonNode getApplicationConfig() {
        return applicationConfig;
    }

    public void setApplicationConfig(JsonNode applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    public String getApiKey() {
        return applicationConfig.at("/it/evs/ws/api-key/ok").asText();
    }

    public String getExpiredApiKey() {
        return applicationConfig.at("/it/evs/ws/api-key/expired").asText();
    }

    public String getWrongApiKey() {
        return applicationConfig.at("/it/evs/ws/api-key/ko").asText();
    }
    public String getWrongOrganizationApiKey() {
        return applicationConfig.at("/it/evs/ws/api-key/wrong-organization").asText();
    }

    private void initApplicationConfig() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            try {
                applicationConfig = mapper.readTree(IT.class.getResourceAsStream("config/"+System.getProperty("validations-it.configuration","dev/"+System.getProperty("user.name")+"/validations-it.yml")));
            } catch (IOException e) {
                applicationConfig = mapper.readTree(IT.class.getResourceAsStream("config/validations-it.yml"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
