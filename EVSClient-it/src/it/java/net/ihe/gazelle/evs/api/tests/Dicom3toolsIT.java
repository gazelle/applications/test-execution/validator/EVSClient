package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class Dicom3toolsIT extends DefaultAdapterIT
{
    @Override
    @SuppressWarnings("java:S2699") // assertion is in called methods
    protected ValidationRequest getRequest() {
        return given().systemBased().dicom3tools();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationFailed() {
        whenPublicValidation(getRequest().fileName("XA-MONO2-8-12x-catheter.dcm"))
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationFailed();
    }

}
