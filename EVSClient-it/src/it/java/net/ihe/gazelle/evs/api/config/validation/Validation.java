package net.ihe.gazelle.evs.api.config.validation;

import io.restassured.response.Response;

public abstract class Validation {

    protected ValidationResponse validationResponse;
    protected Response response;

    public Validation(ValidationResponse validationResponse, Response response) {
        this.validationResponse = validationResponse;
        this.response = response;
    }

    public ValidationResponse getValidationResponse() {
        return validationResponse;
    }

    protected void setValidationResponse(ValidationResponse validationResponse) {
        this.validationResponse = validationResponse;
    }

    public Response getResponse() {
        return response;
    }

    protected void setResponse(Response response) {
        this.response = response;
    }
}
