package net.ihe.gazelle.evs.api.config.gherkin;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.RestApiConnector;
import net.ihe.gazelle.evs.api.config.validation.ValidationReportResult;
import net.ihe.gazelle.evs.api.config.validation.ValidationResponse;
import net.ihe.gazelle.evs.api.config.validation.ValidationResult;

import java.util.HashMap;
import java.util.Map;

public class WhenAccessValidation {

    private ValidationResponse validationResponse;

    private RestApiConnector restApiConnector;

    public WhenAccessValidation(ValidationResponse validationResponse) {
        this.validationResponse = validationResponse;
        restApiConnector = RestApiConnector.getInstance();
    }

    public ValidationResult accessValidation() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"));
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationResult accessPrivateValidation() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), validationResponse.getApiKey(), true, null);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationResult accessPrivateValidationWithExpiredApiKey() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), validationResponse.getExpiredApiKey(), true, null);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationResult accessPrivateValidationWithWrongApiKey() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), validationResponse.getWrongApiKey(), true, null);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationResult accessValidationWithWrongAcceptedContentType() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), ContentType.TEXT);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationResult accessValidationWithNoAcceptedContentType() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), null);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }
    public ValidationResult accessValidationWithWrongOrganizationApiKey() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("Location"), validationResponse.getWrongOrganizationApiKey(), true, null);
        ValidationResult validationResult = new ValidationResult(validationResponse, response);
        return validationResult;
    }

    public ValidationReportResult accessValidationReport() {
        return accessValidationReport(null) ;
    }
    public ValidationReportResult accessValidationReport(String severityThreshold) {
        Map<String,String> params = new HashMap<>();
        params.put("severityThreshold",severityThreshold);
        Response response = restApiConnector.getParameterizedRequest(
                validationResponse.getResponse()
                        .getHeader("X-Validation-Report-Redirect"),
                ContentType.XML,
                null, true, null, params
        );
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

    public ValidationReportResult accessPrivateValidationReport() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), validationResponse.getApiKey(), true, null);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

    public ValidationReportResult accessPrivateValidationReportWithExpiredApiKey() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), validationResponse.getExpiredApiKey(), true, null);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

    public ValidationReportResult accessPrivateValidationReportWithWrongApiKey() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), validationResponse.getWrongApiKey(), true, null);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }
    public ValidationReportResult accessPrivateValidationReportWithEmptyAuthorizationHeader() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), "", true, null);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

    public ValidationReportResult accessValidationReportWithWrongAcceptedContentType() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), ContentType.TEXT);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

    public ValidationReportResult accessValidationReportWithNoAcceptedContentType() {
        Response response = restApiConnector.getRequest(validationResponse.getResponse().getHeader("X-Validation-Report-Redirect"), null);
        ValidationReportResult validationReportResult = new ValidationReportResult(validationResponse, response);
        return validationReportResult;
    }

}
