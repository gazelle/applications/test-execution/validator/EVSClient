package net.ihe.gazelle.evs.api.config.validation;

public class SystemBased {

    public ValidationRequest dicom3tools() {
        return new ValidationRequest("/it/evs/ws/services/system-based/dicom3tools/service",
                "/it/evs/ws/services/system-based/dicom3tools/validator",
                "/it/evs/ws/services/system-based/dicom3tools/filename");
    }
    public ValidationRequest dcmcheck() {
        return new ValidationRequest("/it/evs/ws/services/system-based/dcmcheck/service",
                "/it/evs/ws/services/system-based/dcmcheck/validator",
                "/it/evs/ws/services/system-based/dcmcheck/filename");
    }

}
