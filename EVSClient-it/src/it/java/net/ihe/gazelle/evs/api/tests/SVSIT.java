package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;

public class SVSIT extends DefaultAdapterIT{

    @Override
    protected ValidationRequest getRequest() {
        return given().modelbased().saml();
    }
}
