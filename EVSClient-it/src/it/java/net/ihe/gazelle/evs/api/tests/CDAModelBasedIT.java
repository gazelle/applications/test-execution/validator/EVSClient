package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.gherkin.WhenAccessValidation;
import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

public class CDAModelBasedIT extends DefaultAdapterIT {

    @Override
    protected ValidationRequest getRequest() {
        return given().modelbased().cda();
    }


    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongEncodingFile() {
        validationWrongContent(
                getRequest(),
                "CR-BIO-wrong-encoding.base64",
                false
        );
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongBomFile() {
        validationUnparseableFile(
                getRequest(),
                "wrong-bom-CDA2.xml"
        );
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongSyntaxFile() {
        validationUnparseableFile(
                getRequest(),
                "wrong-syntax-CDA2.xml"
        );
    }


    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationUndefined() {
        validationUndefined(getRequest()
                .fileName("undefined.base64",false));
    }

    /*
    @Test
    public void validationReport() {
        publicValidationReport(
                getRequest());
    }
    protected void publicValidationReport(ValidationRequest request) {
        when().accessValidationReport("1.2.3.7")
                .then()
                .success();
    }
    */

    @Test // check EVSCLT-949 fix
    public void validationReportCounters() {
        publicValidationReportCounters(
                getRequest());
    }

    protected void publicValidationReportCounters(ValidationRequest request) {
        WhenAccessValidation when = request
                .when()
                .publicValidation()
                .then()
                .created()
                .when();
        when.accessValidationReport()
                .getResponse()
                .then()
                .body("validationReport.counters.@numberOfConstraints",equalTo("4"));
        // still same number of constraints on a second check
        when.accessValidationReport()
                .getResponse()
                .then()
                .body("validationReport.counters.@numberOfConstraints",equalTo("4"));
    }

    @Test
    public void publicValidationPassedPartialReport() {
        WhenAccessValidation when = getRequest()
                .when()
                .publicValidation()
                .then()
                .created()
                .when();
        when.accessValidationReport("WARNING")
                .getResponse()
                .then()
                .body("validationReport.counters.@numberOfConstraints",equalTo("4"))
                .body("validationReport.subReport[0]", Matchers.not(Matchers.hasProperty("constraint")));
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationPassedPartialReportWrongThreshold() {
        getRequest()
                .when()
                .publicValidation()
                .then()
                .created()
                .when()
                .accessValidationReport("WRONG")
                .then()
                .badRequest();
    }
}
