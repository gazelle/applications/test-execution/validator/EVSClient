package net.ihe.gazelle.evs.api.config.validation;

import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.gherkin.ThenAccessValidationReportResult;

public class ValidationReportResult extends Validation {

    public ValidationReportResult(ValidationResponse validationResponse, Response response) {
        super(validationResponse, response);
    }

    public ThenAccessValidationReportResult then(){
        return new ThenAccessValidationReportResult(this);
    }

}
