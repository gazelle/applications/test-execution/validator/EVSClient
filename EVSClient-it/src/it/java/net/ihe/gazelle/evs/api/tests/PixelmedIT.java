package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class PixelmedIT extends DefaultAdapterIT
{
    @Override
    @SuppressWarnings("java:S2699") // assertion is in called methods
    protected ValidationRequest getRequest() {
        return given().embedded().pixelmed();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationUndefined() {
        whenPublicValidation(getRequest().fileName("XA-MONO2-8-12x-catheter.dcm"))
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationUndefined();
    }

}
