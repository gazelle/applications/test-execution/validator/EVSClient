package net.ihe.gazelle.evs.api.config.validation;

public class TLS {

    public ValidationRequest pem() {
        return new ValidationRequest("/it/evs/ws/services/tls/pem/service",
                "/it/evs/ws/services/tls/pem/validator",
                "/it/evs/ws/services/tls/pem/filename");
    }

}
