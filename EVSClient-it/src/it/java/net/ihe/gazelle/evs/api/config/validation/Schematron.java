package net.ihe.gazelle.evs.api.config.validation;

public class Schematron {

    public ValidationRequest cda() {
        return new ValidationRequest("/it/evs/ws/services/schematron/cda/service",
        "/it/evs/ws/services/schematron/cda/validator",
        "/it/evs/ws/services/schematron/cda/filename");
    }

    public ValidationRequest hprim() {
        return new ValidationRequest("/it/evs/ws/services/schematron/hprim/service",
                "/it/evs/ws/services/schematron/hprim/validator",
                "/it/evs/ws/services/schematron/hprim/filename");
    }
}
