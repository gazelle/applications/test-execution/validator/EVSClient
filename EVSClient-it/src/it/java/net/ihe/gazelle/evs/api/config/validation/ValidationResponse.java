package net.ihe.gazelle.evs.api.config.validation;

import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.gherkin.Then;

public class ValidationResponse extends Configurable {

    protected ValidationRequest validationRequest;

    protected Response response;
    
    protected String validationStatus;

    public ValidationResponse(ValidationRequest validationRequest, Response response) {
        this.validationRequest = validationRequest;
        this.response = response;
        this.applicationConfig = validationRequest.applicationConfig;
    }

    protected ValidationRequest getValidationRequestDSL() {
        return validationRequest;
    }

    protected void setValidationRequestDSL(ValidationRequest validationRequest) {
        this.validationRequest = validationRequest;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    protected int getCreationStatusCode() {
        return response.getStatusCode();
    }

    protected String getOid() {
        return response.getHeader("Location").replaceAll(".*./validations/(.*?)", "$1");
    }

    protected String getValidationStatus() {
        return validationStatus;
    }

    protected void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public Then then() {
        return new Then(this);
    }
}
