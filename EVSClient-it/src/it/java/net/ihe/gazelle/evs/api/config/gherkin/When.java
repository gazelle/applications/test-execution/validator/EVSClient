package net.ihe.gazelle.evs.api.config.gherkin;

import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.RestApiConnector;
import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import net.ihe.gazelle.evs.api.config.validation.ValidationResponse;

public class When {

    private ValidationRequest validationRequest;

    private RestApiConnector restApiConnector;

    public When(ValidationRequest validationRequest) {
        this.validationRequest = validationRequest;
        restApiConnector = RestApiConnector.getInstance();
    }

    public ValidationResponse publicValidation() {
        RestApiConnector restApiConnector = RestApiConnector.getInstance();
        Response response = restApiConnector.publicValidation(validationRequest.getBody());
        ValidationResponse validationResponse = new ValidationResponse(validationRequest, response);
        return validationResponse;
    }

    private ValidationResponse privateValidation(String apiKey, boolean wellFormed) {
        RestApiConnector restApiConnector = RestApiConnector.getInstance();
        Response response = restApiConnector.privateValidation(validationRequest.getBody(), apiKey, wellFormed);
        ValidationResponse validationResponse = new ValidationResponse(validationRequest, response);
        return validationResponse;
    }

    public ValidationResponse privateValidation() {
        return privateValidation(validationRequest.getApiKey(), true);
    }

    public ValidationResponse privateValidationWithExpiredKey() {
        return privateValidation(validationRequest.getExpiredApiKey(), true);
    }

    public ValidationResponse privateValidationWithWrongKey() {
        return privateValidation(validationRequest.getWrongApiKey(), true);
    }

    public ValidationResponse privateValidationWithBadAuthorizationHeader() {
        return privateValidation(validationRequest.getApiKey(), false);
    }
    public ValidationResponse privateValidationWithEmptyAuthorizationHeader() {
        return privateValidation("", false);
    }

}
