package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;

public class DsubIT extends DefaultAdapterIT{

    @Override
    protected ValidationRequest getRequest() {
        return given().modelbased().dsub();
    }
}
