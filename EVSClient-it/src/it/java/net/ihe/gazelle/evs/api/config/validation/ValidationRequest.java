package net.ihe.gazelle.evs.api.config.validation;

import com.fasterxml.jackson.databind.JsonNode;
import net.ihe.gazelle.evs.api.config.MustacheTemplate;
import net.ihe.gazelle.evs.api.config.gherkin.When;

import java.util.ArrayList;
import java.util.List;

public class ValidationRequest extends Configurable {

    // request
    protected String body;


    // application parameters;
    protected String service;
    protected String validator;

    protected List<HandledObject> objects;

    public ValidationRequest(String service, String validator, String objects, boolean encode) {

        super();

        computeServiceFromConfig(service);
        computeValidatorFromConfig(validator);
        computeObjectsFromConfig(objects,encode);

    }
    public ValidationRequest(String service, String validator, String fileName) {
        this(service,validator,fileName,true);
    }

    protected String getService() {
        return service;
    }

    protected void computeServiceFromConfig(String service) {
        this.service = applicationConfig.at(service).asText();
    }

    protected String getValidator() {
        return validator;
    }

    protected void computeValidatorFromConfig(String validator) {
        this.validator = applicationConfig.at(validator).asText();
    }

    protected void computeObjectsFromConfig(String objects,boolean encode) {
        this.objects = new ArrayList<>();
        JsonNode node = applicationConfig.at(objects);
        if (node.isArray()) {
            for (final JsonNode obj : node) {
                addHandleObject(obj,encode);
            }
        } else {
            addHandleObject(node, encode);
        }
    }

    private void addHandleObject(JsonNode node, boolean encode) {
        HandledObject object = new HandledObject();
        if (node.isTextual()) {
            object.setFileName(node.asText(), encode);
        } else {
            object.setRole(node.get("role").asText());
            object.setFileName(node.get("filename").asText(), encode);
        }
        this.objects.add(object);
    }


    public String getBody() {
        return body;
    }

    protected void setBody(String body) {
        this.body = body;
    }

    public ValidationRequest wrongService() {
        computeServiceFromConfig("/it/evs/ws/services/wrong/service");
        return this;
    }

    public ValidationRequest wrongValidator() {
        computeValidatorFromConfig("/it/evs/ws/services/wrong/validator");
        return this;
    }

    public ValidationRequest wrongFilename() {
        computeObjectsFromConfig("/it/evs/ws/services/wrong/filename",true);
        return this;
    }


    public ValidationRequest validator(String validator) {
        this.validator = validator;
        return this;
    }


    public When when() {
        setBody(new MustacheTemplate("validation-for-creation")
                .execute(new Object() {
                    String service = getService();
                    String validator = getValidator();
                    List<HandledObject> objects = getObjects();
                }));
        return new When(this);
    }

    public List<HandledObject> getObjects() {
        return objects;
    }

    @Deprecated
    public ValidationRequest modifyContent(String pattern, String replacement) {
        if (getObjects()==null||getObjects().isEmpty()||getObjects().size()>1) {
            throw new UnsupportedOperationException();
        }
        getObjects()
                .iterator()
                .next()
                .modifyContent(pattern,replacement);
        return this;
    }
    @Deprecated
    public ValidationRequest fileName(String filename) {
        return fileName(filename,true);
    }
    @Deprecated
    public ValidationRequest fileName(String filename, boolean encode) {
        if (getObjects()==null||getObjects().isEmpty()||getObjects().size()>1) {
            throw new UnsupportedOperationException();
        }
        getObjects()
                .iterator()
                .next()
                .setFileName(filename,encode);
        return this;
    }}
