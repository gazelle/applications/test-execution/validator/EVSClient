<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
                omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Aug 31, 2010
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Ga�lle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:variable name="limitDisplayTo">50</xsl:variable>
        <div>
            <xsl:if
                    test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">

                <xsl:choose>
                    <xsl:when test="count(SummaryResults/ValidationResults/ResultXML/Error) &gt;= $limitDisplayTo">
                        <p id="errors">
                            <b>
                                <u>Errors</u>
                                <span class="FAILED">(only first <xsl:value-of select="$limitDisplayTo"/>th displayed)
                                </span>
                            </b>
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p id="errors">
                            <b>
                                <u>Errors</u>
                            </b>
                        </p>
                    </xsl:otherwise>
                </xsl:choose>


                <xsl:for-each
                        select="SummaryResults/ValidationResults/ResultXML/Error[position() &lt;= $limitDisplayTo]">
                    <table width="100%" class="Error">
                        <xsl:if test="count(Test) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Test</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Test"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">

                <xsl:choose>
                    <xsl:when test="count(SummaryResults/ValidationResults/ResultXML/Warning) &gt;= $limitDisplayTo">
                        <p id="warnings">
                            <b>
                                <u>Warnings</u>
                                <span class="FAILED">(only first <xsl:value-of select="$limitDisplayTo"/>th displayed)
                                </span>
                            </b>
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p id="warnings">
                            <b>
                                <u>Warnings</u>
                            </b>
                        </p>
                    </xsl:otherwise>
                </xsl:choose>


                <xsl:for-each
                        select="SummaryResults/ValidationResults/ResultXML/Warning[position() &lt;= $limitDisplayTo]">
                    <table width="100%" class="Warning">
                        <xsl:if test="count(Test) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Test</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Test"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
                <p id="notes">
                    <b>
                        <u>Conditions</u>
                    </b>
                </p>
                <xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Condition">
                    <table width="100%" class="Note">
                        <xsl:if test="count(Test) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Test</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Test"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
                <p id="unknows">
                    <b>
                        <u>Unknown Exceptions</u>
                    </b>
                </p>
                <xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Unknown">
                    <table width="100%" class="Unknown">
                        <xsl:if test="count(Test) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Test</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Test"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="count(Location) = 1">
                            <tr>
                                <td valign="top" width="10%">
                                    <b>Location</b>
                                </td>
                                <td align="left">
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <tr>
                            <td valign="top" width="10%">
                                <b>Description</b>
                            </td>
                            <td align="left">
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>