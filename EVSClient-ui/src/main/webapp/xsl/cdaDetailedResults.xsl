<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html"
                media-type="text/html"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                June 30, 2011
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div class="styleResultBackground" id="schematron">
            <table>
                <tr>
                    <td>
                        <b>Result</b>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when
                                    test="contains(detailedResult/SchematronValidation/Result, 'PASSED')">
                                <div class="PASSED">
                                    PASSED
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="FAILED">
                                    FAILED
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <b>Summary</b>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfChecks &gt; 0">
                                <xsl:value-of
                                        select="detailedResult/SchematronValidation/ValidationCounters/NrOfChecks"/>
                                check(s) performed
                            </xsl:when>
                            <xsl:otherwise>
                                No check was performed
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                                <a href="#errors">
                                    <xsl:value-of
                                            select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors"/>
                                    error(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No error
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                                <a href="#warnings">
                                    <xsl:value-of
                                            select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings"/>
                                    warning(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No warning
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                                <a href="#notes">
                                    <xsl:value-of
                                            select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes"/>
                                    note(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No note
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports &gt; 0">
                                <a href="#reports">
                                    <xsl:value-of
                                            select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports"/>
                                    successful check(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No successful check
                            </xsl:otherwise>
                        </xsl:choose>
                        <br/>
                        <xsl:choose>
                            <xsl:when
                                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown &gt; 0">
                                <a href="#unknown">
                                    <xsl:value-of
                                            select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown"/>
                                    unknown exception(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                No unknown exception
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
            </table>
            <xsl:if
                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                <p id="Errors">
                    <b>Errors</b>
                </p>
                <xsl:for-each select="detailedResult/SchematronValidation/Error">
                    <table class="error">
                        <tr>
                            <td valign="top">
                                <b>Test</b>
                            </td>
                            <td>
                                <xsl:value-of select="Test"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="Location"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                <p id="Warnings">
                    <b>Warnings</b>
                </p>
                <xsl:for-each select="detailedResult/SchematronValidation/Warning">
                    <table class="warning">
                        <tr>
                            <td valign="top">
                                <b>Test</b>
                            </td>
                            <td>
                                <xsl:value-of select="Test"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="Location"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                <p id="Notes">
                    <b>Notes</b>
                </p>
                <xsl:for-each select="detailedResult/SchematronValidation/Note">
                    <table class="note">
                        <tr>
                            <td valign="top">
                                <b>Test</b>
                            </td>
                            <td>
                                <xsl:value-of select="Test"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="Location"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown &gt; 0">
                <p id="Unknown">
                    <b>Unknown exceptions</b>
                </p>
                <xsl:for-each select="detailedResult/SchematronValidation/Unknown">
                    <table class="unknown">
                        <tr>
                            <td valign="top">
                                <b>Test</b>
                            </td>
                            <td>
                                <xsl:value-of select="Test"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="Location"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if
                    test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports &gt; 0">
                <p id="Reports">
                    <b>Reports</b>
                </p>
                <xsl:for-each select="detailedResult/SchematronValidation/Report">
                    <table class="report">
                        <tr>
                            <td valign="top">
                                <b>Test</b>
                            </td>
                            <td>
                                <xsl:value-of select="Test"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Location</b>
                            </td>
                            <td>
                                <xsl:value-of select="Location"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Description</b>
                            </td>
                            <td>
                                <xsl:value-of select="Description"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </xsl:for-each>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>