/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

Richfaces.ComboBoxList.addMethods({

    createDefaultList: function () {
        var items = [];
        for (var i = 0; i < this.itemsText.length; i++) {
            // if itemsValue[] is defined, send also the custom value to this.createItem()
            if (this.itemsValue)
                items.push(this.createItem(this.itemsText[i], this.classes.item.normal, this.itemsValue[i]));
            else
                items.push(this.createItem(this.itemsText[i], this.classes.item.normal));
        }
        this.createNewList(items);
    },

    doSelectItem: function (item) {
        this.selectedItem = item;
        // Save selected item's value locally, which should be stored in the 'id' attribute of the 'span' tag
        if (item !== null && item.id !== null)
            this.selectedItemValue = item.id;
    },

    getFilteredItems: function (text) {
        var items = [];
        for (var i = 0; i < this.itemsText.length; i++) {
            var itText = this.itemsText[i];
            if (itText.toLowerCase().indexOf(text.toLowerCase()) !== -1) {
                items.push(this.createItem(itText, this.classes.item.normal));
            }
        }
        return items;
    },

    findItemBySubstr: function (substr) {
        var items = this.getItems();
        for (var i = 0; i < items.length; i++) {
            var item = items[i]
            var itText = item.innerHTML.unescapeHTML();
            if (itText.toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                return item;
            }
        }
    },

    createItem: function (text, className, value) {
        // receive the custom value
        var escapedText = text.escapeHTML();
        // Save custom value as 'id' in 'span', so it can be recovered later
        if (value !== null)
            return "<span id=\"" + value + "\" class=\"" + className + "\">" + escapedText + "</span>";
        else
            return "<span class=\"" + className + "\">" + escapedText + "</span>";
    }

});
