/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var gbl_isxpath ;



// The observer is used to detect when the validation panel is updated (switch between detailedResult & standard report)
// should be removed when the detailed result is deprecated
const observer = new MutationObserver(function(mutationsList, observer) {
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            gbl_isxpath = !isThere0IndexPath();
        }
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const validationPanel = document.querySelector("[target-name='result-panel']").children[0];
    let config = { childList: true};
    observer.observe(validationPanel, config);
    //init the value
    gbl_isxpath = !isThere0IndexPath();
});


// Goto next node
function gotoNext(id){
    gotoNode(getNext(id),gbl_isxpath);
}

// Goto previous node
function gotoPrevious(id){
    gotoNode(getPrevious(id),gbl_isxpath);
}


// if isXpath is true, then the path is a valid Xpath, otherwize this is an Xpath but with
// incorrect numbering of the nodes. In otherwords when false the node numbering starts with
// 1 and not with zero as it should
function gotoNode(myNode, isxpath) {
    if(isxpath !== undefined){
        gbl_isxpath = isxpath;
    }

    //init document viewer scroll
    document.getElementsByClassName("document-viewer")[0].scrollTop = 0

    // gbl_isxpath=isxpath;
    var path = myNode.parentNode.innerHTML.split('<')[0];
    path = path.trim();
    path = cleanPath(path);
    //  jq162('[data-toggle="popover"]').popover('hide');

    unselectNode();
    markNodeAsSelected(myNode);


    // Get all the all the span elements in the document.
    var allSpanElements = document.getElementsByTagName("span");
    var goodSpanElements = [];
    var ell;
    var j = 0;


    // loop through all the span elements (allSpanElements) and identify the ones that are from the XML document.
    for (var i = 0; i < allSpanElements.length; i++) {
        // Take care of the eventual last message. Clean
        if (allSpanElements[i].className.substring(0,12) === "xml_tag_name"){
            goodSpanElements[j] = allSpanElements[i];
            goodSpanElements[j].className = "xml_tag_name";
            // Here we remove the text of the report that was added by the method addReportInformation
            if (goodSpanElements[j].parentNode.getElementsByTagName("div").length > 0){
                var elementToRemove = goodSpanElements[j].parentNode.getElementsByTagName("div")[0];
                goodSpanElements[j].parentNode.removeChild(elementToRemove);
            }
            // Here we need to take care of the link that was added
            if (goodSpanElements[j].getElementsByTagName("a").length > 0) {
                jq162(goodSpanElements[j].getElementsByTagName("a")[0]).contents().unwrap();
            }
            j++;
        }
        else if (allSpanElements[i].className === "xml_tag_name" ) {
            goodSpanElements[j] = allSpanElements[i];
            // make sure we are cleaning the background while looping through the spans
            goodSpanElements[j].style.backgroundColor = goodSpanElements[j].parentNode.style.backgroundColor;
            j++;
        }
    }

    // If there is an element with named with the path we are looking for !
    // This means we already have search for this in the passed.
    // Then we search for the element in yellow and we append the popup.
    if (document.getElementsByName(path).length > 0) {
        document.getElementsByName(path)[0].className = "issue-list";
        addReportInformation(document.getElementsByName(path)[0].parentNode,  myNode);
    }

    // if this is not the case, we need to find the path.
    else {
        var wellsplits = [];
        var list = path.split('/');
        var w = 0;
        for (var element = 0; element < list.length; element++) {
            if (list[element] !== '') {
                var ll = list[element].replace('[', '/').replace(']', '/').split('/');
                var xxpath;
                if (ll.length > 1) {
                    xxpath = new XPathElement();
                    if (gbl_isxpath) {
                        xxpath.elementIndex = parseInt(ll[1]) - 1;
                    }
                    else {
                        xxpath.elementIndex = parseInt(ll[1]);
                    }
                    xxpath.elementName = ll[0];
                    if (xxpath.elementName.indexOf(':') !== -1) {
                        xxpath.elementName = xxpath.elementName.split(':')[1];
                    }
                    wellsplits[w] = xxpath;
                    w++;
                }
                else {
                    xxpath = new XPathElement();
                    xxpath.elementIndex = 0;
                    xxpath.elementName = ll[0];
                    if (xxpath.elementName.indexOf(':') !== -1) {
                        xxpath.elementName = xxpath.elementName.split(':')[1];
                    }
                    wellsplits[w] = xxpath;
                    w++;
                }
            }
        }

        var root = new xmlNode();
        root = extractXmlStructure(root, wellsplits[0].elementName);
        ell = extractElementByXpath(root, wellsplits);
        addReportInformation(ell, myNode);

        ell.innerHTML = ell.innerHTML.anchor(path);
        ell.className = "xml_tag_name source-line-code-issue source-line-code-secondary-issue "+ getAlert(myNode);
    }
    var targetOffset = $(ell).offset().top - 200;

    //scroll to the element
    var targetOffsetDocumentViewer = targetOffset - $(".document-viewer").offset().top;
    $("html,body").animate({scrollTop: targetOffset}, 30);
    if(targetOffsetDocumentViewer > 0){
        $(".document-viewer").animate({scrollTop: targetOffsetDocumentViewer}, 10);
    }

}



function cleanPath(input) {
    const regex = /\[namespace-uri\(\)='[^']+'\]\[([0-9]+)\]/g;

    const replaceFn = (match, index) => {
        // Replace with [index]
        return `[${index}]`;
    };

    return input.replace(regex, replaceFn);
}

function isThere0IndexPath(){
    let locations = document.getElementsByClassName("constraint-location")
    for(let loc of locations){
        if(loc.innerText.includes("[0]")){
            return true;
        }
    }
    return false;
}

function addReportInformation(element, currentNode) {
    var issuelist = document.createElement("div");
    var issue = document.createElement("div");
    var issueinner = document.createElement("div");
    var issuetable = document.createElement("table");
    var issuetbody = document.createElement("tbody");
    var issuetr = document.createElement("tr");
    var issuetdNav = document.createElement("td");
    var issuetd = document.createElement("td");
    var issuetdIcon = document.createElement("td");
    var issuemessage = document.createElement("dl");
    var issueIcon = document.createElement("div");
    var issueNav = document.createElement("div");

    issue.setAttribute("class", "issue selected "+ getAlert(currentNode) +" row col-md-16")
    issuelist.setAttribute("class", "issue-list container");
    issuetdNav.setAttribute("class", "col-md-1");
    issueinner.setAttribute("class", "issue-inner");
    issuetable.setAttribute("class", "issue-table");
    issuemessage.setAttribute("class", "issue-message dl-horizontal");
    issuelist.appendChild(issue);
    issue.appendChild(issueinner);
    issueinner.appendChild(issuetable);
    issuetable.appendChild(issuetbody);
    issuetbody.appendChild(issuetr);
    issuetr.appendChild(issuetdNav);
    issuetr.appendChild(issuetd);
    issuetr.appendChild(issuetdIcon);
    issuetdIcon.appendChild(issueIcon);
    issuetd.appendChild(issuemessage);
    issuetdNav.appendChild(issueNav);
    var dtSize = currentNode.parentNode.parentNode.getElementsByTagName("dt").length;
    for (var i = 0; i< dtSize ; i++){
        var dt = document.createElement("dt");
        var dd = document.createElement("dd");
        dt.innerHTML = currentNode.parentNode.parentNode.getElementsByTagName("dt")[i].innerHTML;
        dd.innerHTML = currentNode.parentNode.parentNode.getElementsByTagName("dd")[i].innerHTML;
        // remove the "eye" link icon
        if (i===1 && dd.getElementsByTagName("a").length) {
            dd.removeChild(dd.getElementsByTagName("a")[0]);
        }
        issuemessage.appendChild(dt);
        issuemessage.appendChild(dd);
    }
    issueNav.innerHTML = "";
    var tmpContent = "";
    var current = getPositionOfNode(currentNode)  ;
    var n = getNumberOfNodes(currentNode)  ;

    if (current > 1 ) {
        tmpContent += "<a href='#' onclick=gotoPrevious('" + currentNode.id + "')><span class='fa fa-arrow-up'/></a>";
    }

    // Here we are displaying the numbers report (x/n)
    tmpContent += "(<a href=#"+getShortAlert(currentNode)+ current + " >";
    tmpContent += current  ;
    tmpContent += "</a>/<a href=#"+getShortAlert(currentNode)+ n + " >" ;
    tmpContent +=  n ;
    tmpContent +=  "</a>)";


    if (current < n){
        tmpContent += "<a href='#'  onclick=gotoNext('" + currentNode.id + "')><span class='fa fa-arrow-down'/></a>";
    }

    issueNav.innerHTML = tmpContent;
    element.parentNode.appendChild(issuelist);
}

function jump(h, alert) {
    var targetId = '#' + alert + h;
    jq162(targetId).trigger("scrollIntoView");
}


function unselectNode() {
    var allDlElements = document.getElementsByTagName("dl");

    for (var i = 0; i < allDlElements.length; i++) {
        // Take care of the eventual last message. Clean
        if (allDlElements[i].className.indexOf("gzl-notification-selected")>0) {
            allDlElements[i].className = allDlElements[i].className.replace("gzl-notification-selected", "");
        }
    }
}

function markNodeAsSelected(myNode) {
    var className = myNode.parentNode.parentNode.className;
    if ( className.indexOf("gzl-notification-selected") < 0 ) {
        myNode.parentNode.parentNode.className = className + " gzl-notification-selected"
    }

}

// This methods reads the content of the dd element (it includes the description of the error and the link if there is one in the description.
function getAlertContent(currentNode) {

    return currentNode.parentNode.parentNode.getElementsByTagName("dd")[2].innerHTML;
}

function getNumberOfNodes(currentNode){
    // We need to substract one as there is a "p" as the first element.
    return jq162(currentNode).parent().parent().parent().children().length - 1;
}

function getPositionOfNode(currentNode){
    return jq162(currentNode).parent().parent().parent().children().index(jq162(currentNode).parent().parent());
}


function    getNext(id){
    if (id ===""){
        alert("calling getNext with an id that is empty");
    }
    var node = document.getElementById(id);
    var n = getNumberOfNodes(node);
    var p = getPositionOfNode(node);
    if (p < n) {
        return jq162(node).parent().parent().parent()
            .children()[p+1].getElementsByTagName("dd")[1].getElementsByTagName("a")[0];
    }
}

function getPrevious(id){
    var node = document.getElementById(id);
    var p = getPositionOfNode(node);
    if (p  > 0  ) {
        return jq162(node).parent().parent().parent().children()[p-1].getElementsByTagName("dd")[1].getElementsByTagName("a")[0];
    }
}

// this one returns pass, failed, info, report
function getAlert(myNode) {
    var returnValue ;
    switch (myNode.parentNode.parentNode.classList[2]) {
        case "gzl-notification-orange":
            returnValue = "EVS_ABORTED";
            break;
        case "gzl-notification-red":
            returnValue = "EVS_FAILED";
            break;
        case "gzl-notification-green":
            returnValue = "EVS_PASSED";
            break;
        case "gzl-notification-blue":
            returnValue = "EVS_INFO";
            break;
        default:
            returnValue = "EVS_UNKNOWN";
    }
    return returnValue;
}

function getShortAlert(myNode) {
    var returnValue ;
    switch (myNode.parentNode.parentNode.classList[2]) {
        case "gzl-notification-orange":
            returnValue = "w";
            break;
        case "gzl-notification-red":
            returnValue = "e";
            break;
        case "gzl-notification-green":
            returnValue = "r";
            break;
        case "gzl-notification-blue":
            returnValue = "i";
            break;
        default:
            returnValue = "u";
    }
    return returnValue;
}

function extractElementByXpath(rootEl, xxp) {
    var ell = null;
    var i;
    var j;
    var tocompare;
    if (xxp.length === 1) {
        j = 0;
        for ( i = 0; i < rootEl.children.length; i++) {
            tocompare = extractText(rootEl.children[i].element);
            if (tocompare === xxp[0].elementName) {
                if (j === xxp[0].elementIndex) {
                    ell = rootEl.children[i].element;
                    return ell;
                }
                else {
                    j++;
                }
            }
        }
    }
    else {
        j = 0;
        for ( i = 0; i < rootEl.children.length; i++) {
            tocompare = extractText(rootEl.children[i].element);
            if (tocompare === xxp[0].elementName) {
                if (j === xxp[0].elementIndex) {
                    var ell1 = rootEl.children[i];
                    ell = extractElementByXpath(ell1, copieXPathWithDeleteOfFirstElemnt(xxp));
                    return ell;
                }
                else {
                    j++;
                }
            }
        }
    }
    return ell;
}

function copieXPathWithDeleteOfFirstElemnt(tab) {
    var res = [];
    for (var i = 1; i < tab.length; i++) {
        res.push(tab[i]);
    }
    return res;
}

function extractXmlStructure(myXmlNode, requestedRoot="") {
    var allSpanElements = document.getElementsByTagName("span");
    var goodSpanElements = [];
    var j = 0;
    for (var i = 0; i < allSpanElements.length; i++) {
        if ((allSpanElements[i].className === "xml_tag_name")) {
            goodSpanElements[j] = allSpanElements[i];
            goodSpanElements[j].style.backgroundColor = goodSpanElements[j].parentNode.style.backgroundColor;
            if (goodSpanElements[j].getElementsByTagName("a").length > 0) {
                goodSpanElements[j].getElementsByTagName("a")[0].style.backgroundColor = goodSpanElements[j].parentNode.style.backgroundColor;
            }
            j++;
        }
        if ((allSpanElements[i].className === "xml_tag_symbols") && (allSpanElements[i].innerHTML !== '=')) {
            if ((allSpanElements[i].innerHTML === '&gt;&lt;') ||
                (allSpanElements[i].innerHTML === '&gt;&lt;/') ||
                (allSpanElements[i].innerHTML === '/&gt;&lt;') ||
                (allSpanElements[i].innerHTML === '/&gt;&lt;/')) {
                goodSpanElements[j] = allSpanElements[i];
                j++;
                goodSpanElements[j] = allSpanElements[i];
                j++;
            }
            else {
                goodSpanElements[j] = allSpanElements[i];
                j++;
            }
        }
    }
    var ind = 0;
    populateChildrenXmlNode(myXmlNode, ind, goodSpanElements);
    if (requestedRoot !== "") {
        var res = new xmlNode();
        res.children = [getXmlNode(myXmlNode, requestedRoot)];
        return res;
    }
    return myXmlNode;
}

function getXmlNode(myXmlNode, requestedRoot) {
    if (myXmlNode.element !== null && extractText(myXmlNode.element) === requestedRoot) {
        return myXmlNode;
    }
    else {
        for (var i = 0; i < myXmlNode.children.length; i++) {
            var res = getXmlNode(myXmlNode.children[i], requestedRoot);
            if (res !== null && res !== undefined) {
                return res;
            }
        }
    }
    return res;
}

function populateChildrenXmlNode(myXmlNode, ind, goodSpanElements) {
    var end = false;
    while ((!end) && (ind < goodSpanElements.length)) {
        if ((goodSpanElements[ind].innerHTML === '&lt;') ||
            (goodSpanElements[ind].innerHTML === '&gt;&lt;') ||
            (goodSpanElements[ind].innerHTML === '/&gt;&lt;')  ) { // < || >< || /><
            var child = new xmlNode();
            child.element = goodSpanElements[ind + 1];
            myXmlNode.children.push(child);
            if ((goodSpanElements[ind + 2].innerHTML === '/&gt;') ||
                (goodSpanElements[ind + 2].innerHTML === '/&gt;&lt;') ||
                (goodSpanElements[ind + 2].innerHTML === '/&gt;&lt;/')  ) { // /> || />< || /></
                ind = ind + 3;
            }
            else if ((goodSpanElements[ind + 2].innerHTML === '&gt;') ||
                (goodSpanElements[ind + 2].innerHTML === '&gt;&lt;') ||
                (goodSpanElements[ind + 2].innerHTML === '&gt;&lt;/')  ) { // > || >< || ></
                ind = ind + 3;
                ind = populateChildrenXmlNode(child, ind, goodSpanElements);
            }
        }
        else if ((goodSpanElements[ind].innerHTML === '&lt;/') ||
            (goodSpanElements[ind].innerHTML === '&gt;&lt;/') ||
            (goodSpanElements[ind].innerHTML === '/&gt;&lt;/')  ) { // </ || ></ || /></
            end = true;
            ind = ind + 3;
        }
        else {
            ind = ind + 3;
        }
    }
    return ind;
}

function extractText(myNode) {
    var tocompare = myNode.innerHTML;
    if (myNode.getElementsByTagName('a').length > 0) {
        tocompare = myNode.getElementsByTagName('a')[0].innerHTML;
    }

    if (tocompare.indexOf(':') !== -1) {
        tocompare = tocompare.split(':')[1];
    }
    return tocompare;
}

function xmlNode() {
    this.element = null;
    this.children = [];

    xmlNode.prototype.display = function (childNumb) {
        var res = '';
        var i ;
        if (this.element !== null) {
            res = res + '\n';
            for ( i = 0; i < (childNumb + 1); i++) {
                res = res + '-';
            }
            res = res + this.element.innerHTML;
        }
        if (this.children.length > 0) {
            for ( i = 0; i < this.children.length; i++) {
                res = res + this.children[i].display(childNumb + 1);
            }
        }
        return res;
    }
}

function XPathElement() {
    this.elementName = null;
    this.elementIndex = 0;
}

