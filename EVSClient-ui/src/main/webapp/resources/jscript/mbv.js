/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

function hideOrUnhideMB(elem){
    var elemToHide = document.getElementById(elem.name + '_p');
    hideOrUnhide(elemToHide, elem.checked);
}

function hideOrUnhideSch(elem){
    var elemToHide = document.getElementById(elem.name + '_sch');
    hideOrUnhide(elemToHide, elem.checked);
}

function hideOrUnhide(elemToHide, hide){
    if (elemToHide != null){
        if (hide){
            elemToHide.style.display = 'none';
        }
        else{
            elemToHide.style.display = 'block';
        }
    }
}

function updateColorationOfLines(lineNumber) {
    var style='xml-bad';
    switch (severity){
        case "error":
            style = 'xml-bad';
            break;
        case "warning":
            style = 'xml-warning';
            break;
        default :
            style = 'xml-bad';
            break;

    }
    elementToHighlight = document.getElementsByName('line_' + lineNumber)[0].parentNode.parentNode.getElementsByTagName('TD')[1];
    elementToHighlight.innerHTML = "<span class='"+style+"'>" + elementToHighlight.innerHTML + "</span>";
}