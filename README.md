# EVSClient

<!-- TOC -->
* [EVSClient](#evsclient)
* [Pre-requisites:](#pre-requisites)
* [Application build](#application-build)
  * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [plain build with maven](#plain-build-with-maven)
  * [maven profiles](#maven-profiles)
    * [`it` maven profile](#it-maven-profile)
    * [`createDB` maven profile](#createdb-maven-profile)
    * [`createInitDB` maven profile](#createinitdb-maven-profile)
    * [`dev` maven profile](#dev-maven-profile)
    * [Other data generation maven profile](#other-data-generation-maven-profile)
<!-- TOC -->

# Pre-requisites:

- jdk 1.8
- maven 3.x

# Application build

## Configuration for sso-client-v7

As EVSClient depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## plain build with maven

To build EVSClient without integration tests activated (no ITs launched) :

```bash
EVSClient$ mvn clean install
```



## maven profiles

### `it` maven profile

Integration Tests in EVSClient are organized in two different parts :

- JUnit java tests
- data initialisation

JUnit java integration-tests are concealed in a specific maven module `evsclient-it`
under `src/it/java/`, they are classes suffixed with `IT`. Those tests are automatically ran by __
maven-failsafe-plugin__ if `it` maven profile is activated.

Data generation is made during `evsclient-ejb` build process while `it` maven profile is activated.

Defaults data generation source files are stored in `EVSClient-ejb/src/it/resources/sql` directory.
IT customizations are files that overrides defaults files stored in `EVSClient-ejb/src/it/resources/sql` directory. 
IT customization files must be in `EVSClient-ejb/src/it/resources/sql/it` directory. 
All files in `EVSClient-ejb/src/it/resources/sql/it` supplants default files.

To build EVSClient with IT on an already installed database :

```bash
EVSClient$ mvn clean install -Pit
```

IT data are produced in `EVSClient/EVSClient-ejb/target/classes/import.sql` file.
`ìmport.sql` file is packaged in `evsclient-ejb.jar`, itself packaged in `evsclient.ear`. By
convention, __hibernate__ will run this script at startup to initialize data if database is
empty.

`it` maven profile can be actived on `it.active` file existence in `EVSClient` project root
directory. So, IT can be always active, just do following command :

```bash
EVSClient$ touch it.active
```

Integration tests can be run without generating data this way:

```bash
EVSClient$ cd EVSClient-it/
EVSCLient/EVSClient-it$ mvn verify -Pit
```

### `createDB` maven profile

`createDB` maven profile builds an EVSClient ear that re-create database at startup from JPA entities.

### `createInitDB` maven profile

`createInitDB` maven profile builds an EVSClient ear that re-create database at startup from JPA entities AND runs an
import.sql with initial data from mustache templates.

```bash
EVSClient$ mvn clean install -PcreateInitDB
```

`createInitDB` profile can be combined with `it` profile to produce an EVSClient ear that re-create database
and initialize it with data required for integrations tests at startup :

```bash
EVSClient$ mvn clean install -PcreateInitDB -Pit
```

### `dev` maven profile

`dev` maven profile produces data initialization contextualized for the local platform (local
database, local jboss, etc.)

```bash
EVSClient$ mvn clean install -Pdev -Pit
```

`dev` profile can be combined with `it` and `init` profiles to produce an EVSClient ear that
re-create database and initialize it with data required for integrations tests at startup with local
customization :

```bash
EVSClient$ mvn clean install -PcreateInitDB -Pit -Pdev
```

Local customizations are files that overrides defaults files stored
in `EVSClient-ejb/src/it/resources/sql` directory. Local customization files must be
in `EVSClient-ejb/src/it/resources/sql/dev/${env.USER}` directory, where `${env.USER}` is the current
system connected user name as known under the same named variable in maven. Main local customization
file is `default-init-data.yml` which contains mains parameters for data generation, but all files
in `EVSClient-ejb/src/it/resources/sql` can be duplicated in `dev/${env.USER}` to supplant default files
and IT customization files.


### Other data generation maven profile

Custom maven profiles produces data initialization contextualized for a specific platform (qa, pprod, prod, etc.)

Custom profile can be combined with `createInitDB` profile to produce an EVSClient ear that
re-create database and initialize data at startup for custom platform :

Custom profile is exclusive from `dev` and `it` profiles and should not be activated simultaneously.

Customizations are files that overrides defaults files stored
in `EVSClient-ejb/src/it/resources/sql` directory. 
Customization files must be in `EVSClient-ejb/src/it/resources/sql/${custom-data}` directory, 
where `${custom-data}` is the named variable in maven. 
All files in `EVSClient-ejb/src/it/resources/sql` can be duplicated in `EVSClient-ejb/src/it/resources/sql/${custom-data}` 
to supplant default files.

Customization files should be stored in `EVSClient-ejb/src/it/resources/sql/custom/<myplatform>` directory
in order to not be shared in Git public repository ; so the `${custom-data}` variable should start by `custom/`

